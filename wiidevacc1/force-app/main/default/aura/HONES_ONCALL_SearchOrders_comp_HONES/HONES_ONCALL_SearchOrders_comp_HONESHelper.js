({
    searchHelper: function (component, event, getInputkeyWord) {
        //call the apex class method 
        var wpVaraibles = component.get("v.initVariables");
        var recordId = component.get("v.recordId");
        var action = component.get("c.fetchLookUpValuesContact");
        
        var initList = component.get("v.listOfSearchRecordsInit");
        // set param to method      
            action.setParams({
                'recordId' : recordId,
                'searchKeyWord': getInputkeyWord,
                'stringWpVariables': JSON.stringify(wpVaraibles),
                'ObjectName': component.get("v.objectAPIName")
            });
            // set a callBack         
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var storeResponse = response.getReturnValue();
                    for(var i = 0; i < storeResponse.length; i++){
                        storeResponse[i].quantity = "";
                        storeResponse[i].selectedValue = storeResponse[i].lista && storeResponse[i].lista[0] ? storeResponse[i].lista[0] : '';
                    }
                    component.set("v.listOfSearchRecordsInit", storeResponse);
                    component.set("v.listOfSearchRecords", storeResponse);
                    console.log('storeResponseSearchHelper', storeResponse);
                    component.set("v.isLoading", false);
                }
                
            });
            // enqueue the Action  
            $A.enqueueAction(action);     
    },
    
    searchLocalHelper: function (component, event, getInputkeyWord) {
        // call the apex class method 
        var listInit = component.get("v.listOfSearchRecordsInit");
        var expresion = new RegExp(getInputkeyWord, 'gi');
        var listFilter = [];
        listFilter = listInit.filter(function filterByCodeAndMat(prod) {
            if ('productCode' in prod && 'materialProduct' in prod && (prod.productCode.match(expresion) || prod.materialProduct.match(expresion) ) ) {
                return true;
            } else {
                return false;
            }
        });
        component.set("v.listOfSearchRecords", listFilter);              
    },
    
    getInitVariablesForSearch: function(component,recordId2) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.initFetchLookUp");
        // set param to method  
        action.setParams({
            'recordId' : recordId
        });
        // set a callBack    
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                component.set("v.initVariables", storeResponse);
                this.searchHelper(component, event, '');
            }
            
        });
        // enqueue the Action  
        $A.enqueueAction(action);
    },

    loadOptions: function (component, event, helper) {
        var options = [{
            label: '        ',
            value: '        '
        },
        {
            label: 'obsequio',
            value: 'obsequio'
        },
        {
            label: 'siembra',
            value: 'siembra'
        },
        
        
        ];
        component.set("v.statusOptions", options);
        },
        // fetch picklist values dynamic from apex controller 
        fetchPickListVal: function (component, fieldName, picklistOptsAttributeName) {
        var action = component.get("c.getselectOptions");
        action.setParams({
        "objObject": component.get("v.objInfoForPicklistValues"),
        "fld": fieldName
        });
        var opts = [];
        action.setCallback(this, function (response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.set("v." + picklistOptsAttributeName, opts);
            }
        });
        $A.enqueueAction(action);
    },
    
    gettheID : function(cmp,recordId) {
        var action = cmp.get("c.getId");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.Idcuenta", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    gettheDate : function(cmp,recordId) {
        var action = cmp.get("c.getHour");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.date", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getDataAccountDis : function(cmp,recordId2) {
        var action = cmp.get("c.disponible");
        action.setParams({ recordId2 : recordId2});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.creditLimit", conts);
                cmp.set("v.creditLimitInit", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getResponse : function(component,recordId, event) {
        var action = component.get("c.genOrder");
        var oncall = component.get("v.Idcuenta");
        var product = component.get("v.lookName");
        var promos = component.get("v.promosEnviar2");
        var dateValue = component.get("v.date");
        var creCash = component.get("v.creditCashValues");
        var initVariablesAccount = component.get("v.initVariablesAccount");

        console.log('product: ', product);

        component.set("v.showtabOI",false);
        component.set("v.isLoading",true);
        
        action.setParams({
            product : JSON.stringify(product),
            call:oncall,
            recordId:recordId,
            promos:JSON.stringify(promos),
            dateValue:dateValue,
            creCash:creCash,
            account:JSON.stringify(initVariablesAccount)
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var x = response.getReturnValue();
                console.log('x: ', x);
                component.set("v.response", x);
                var appEventSearch = $A.get("e.c:eventReloadComponents");
                appEventSearch.setParams({"reloadView": true});
                setTimeout($A.getCallback(function() {
                 
                    appEventSearch.fire();
                    //Refresh variable
                    component.set("v.lookName", []);
                    component.set("v.promosEnviar2", []);
                    component.set("v.promosEnviar", []);
                    component.set("v.promosView", []);
                    component.set("v.tablePromotion",false);
                    
                    $A.get('e.force:refreshView').fire();
                    
                }), 3000);                
                
            }
            else if (state === "INCOMPLETE") {
                // do something
                component.set("v.isLoading",false);
            }
                else if (state === "ERROR") {
                    component.set("v.isLoading",false);
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
        
    },

    getResponseupdate : function(component,recordId, event) {
        var action = component.get("c.genOrder");
        var oncall = component.get("v.Idcuenta");
        var product = component.get("v.lookName");
        var promos = component.get("v.promosEnviar2");
        var creCash = component.get("v.creditCashValues");
        
        action.setParams({
            product : JSON.stringify(product),
            call:oncall,
            recordId:recordId,
            promos:JSON.stringify(promos),
            creCash:creCash,
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                var x = response.getReturnValue(); 
                component.set("v.response", x);
                window.setTimeout(
                    $A.getCallback(function() {
                        alert("Orden generada correctamente");				
                    }), 600
                    
                ); 	
                
                var eUrl= $A.get("e.force:navigateToURL");
                eUrl.setParams({
                    "url": 'https://abinbevcopec--wiidevacc1.lightning.force.com/lightning/o/ONCALL__Call__c/list?filterName=Recent' 
                });
                eUrl.fire();            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },
    
    getprice : function(component, recordId) {
        var action = component.get("c.callourPrice");
        var product = component.get("v.lookName");
        console.log(product);
        var promos = component.get("v.promosEnviar");        
        var cash = component.get("v.cash");
        var cuenta = component.get("v.Idcuenta");
        var promosV = [];
        var promosSend = [];
        var payConditionAcc = cuenta && cuenta.length > 0 && cuenta[0].ONCALL__POC__c ? cuenta[0].ONCALL__POC__r.ONTAP__Credit_Condition__c : '';
        var cuenta = component.get('v.recordId');
        var initAccount = component.get('v.initVariablesAccount[0]');
        var atr4 = component.get("v.initVariablesAccount");
        var atr4Value = atr4[0].ONCALL__KATR4__c;

        action.setParams({
            recordId:recordId,
            product : JSON.stringify(product),
            promos:JSON.stringify(promos)
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {
                var productsResponse = response.getReturnValue();
                if(productsResponse == null){
                    alert($A.get("$Label.c.ISSM_Price_not_found"));
                } else{
                        component.set("v.orderTotal", productsResponse[0].orderTotal);
                        var recoleccion = $A.get("$Label.c.ORDER_EMPTIES_PICKING");
                        var vect = false;
                        //var arco = $A.get("$Label.c.ORDER_EMPTIES_PICKING"); 
                        for(var a= 0; a < productsResponse.length; a++){
                            if(productsResponse[a].productType != recoleccion){
                                vect = true;
                                break;
                            }
                        }
                        if(productsResponse[0].orderTotal < 0 && vect == true){
                            alert($A.get("$Label.c.You_cannot_send_this_order"));
                        }else{
                            console.log('Aqui entro buajajajaja');
                            component.set("v.boton", true);
                        }    

                        component.set("v.pesos", true);
                    console.log('>>> 1.0 productsResponse: ', productsResponse);
                    //Comparación para porder obtener el numero tota de envace ERG
                    var total = 0;
                    var PDF = $A.get("$Label.c.ORDER_ADD_EMPTIES");
                    var PFN = $A.get("$Label.c.OUT_OF_REGULATION_PRODUCT");
                    var PSG = $A.get("$Label.c.GOOD_SHAPE_PRODUCT");
                    var recoleccion = $A.get("$Label.c.ORDER_EMPTIES_PICKING");
                    console.log('productsResponse.Keyadd' + productsResponse.Keyadd);
                    //if(productsResponse.Keyadd != undefined){
                        for(var a= 0; a < productsResponse.length; a++){
                            if(productsResponse[a].productType == recoleccion || productsResponse[a].productType == PSG || productsResponse[a].productType == PFN || productsResponse[a].productType == PDF){
                                if(productsResponse[a].productType == PFN){
                                    var res = productsResponse[a].Keyadd;
                                    var key  = res.split("-");
                                    if(key[2] != 'Botella'){
                                        total += productsResponse[a].quantity;  
                                    }    
                                }else{
                                    total += productsResponse[a].quantity;
                                }
                            }
                        }
                    //}
                    component.set("v.totalenvaces", total);
        

                    component.set("v.lookName", productsResponse);
                    
                    var newArray=[];
                    var promos=productsResponse;
                    var arraypromo=[];
                    var arraypromotosend=[];
                        
                    promos.forEach(element=> {
                            // CAmbios Aqui ERG
                            var SendemptiesProduct = $A.get("$Label.c.ORDER_SEND_EMPTIES");
                            var recoleccion = $A.get("$Label.c.ORDER_EMPTIES_PICKING");
                            var goodProduct = $A.get("$Label.c.GOOD_SHAPE_PRODUCT");
                            var recPBE = $A.get("$Label.c.Empties_PBE");
                            
                            //var Empti = $A.get("$Label.c.ORDER_EMPTIES");
                            
                            var regulationproduct = $A.get("$Label.c.OUT_OF_REGULATION_PRODUCT");
                            var recPFNC = $A.get("$Label.c.Empties_PFN_C");
                            var recPFNB = $A.get("$Label.c.Empties_PFN_B");
                            
                            if(element.productType == recPBE || element.productType == recPFNC || element.productType == recPFNB || element.productType == recoleccion || element.productType == regulationproduct || element.productType == SendemptiesProduct || element.productType == goodProduct ){
                                console.log(element.productType);  
                                var prices = element.price * -1;  
                                console.log(prices);  
                                var totales = element.total * -1;  
                                console.log(prices);  
                                element.price = prices;
                                element.total = totales;
                            }
                        if(element.listadeals != null && element.listadeals != ""){  
                            //Se identifica deal con freegood
                            if(element.freeGood && element.listadeals && element.listadeals.length){                            
                                element.listadeals.forEach(elementdeal=> {
                                    elementdeal.freeGood = true;
                                    console.log("elementdeal " + elementdeal);
                                });
                            }                    
                            arraypromo.push(element.listadeals);
                        }
                        if(payConditionAcc == 'Credit'){
                            if(cash){
                                element.pay=$A.get("$Label.c.OnCall_Cash");
                            }else{
                                element.pay=$A.get("$Label.c.OnCall_Credit");   
                            }        
                        }else{
                            element.pay=$A.get("$Label.c.OnCall_Cash");
                        }
                    
                    });
                        if(arraypromo != null && arraypromo != "") {
                            arraypromo.forEach(element=> {
                                element.forEach(element2=> {
                                    newArray.push(element2);
                                    console.log('element2: '+JSON.stringify(element2));
                                });
                            });
                            arraypromotosend = [];
                               var objectPromod = {};
                            var objectPromod2 = {};
                            newArray.forEach(element2=> {
                                if(element2.pronr!= null){
                                    var prom={
                                        promoNamedescription:element2.pronr,
                                        promoDescTosend:element2.description,
                                        condcode:element2.condcode,
                                        amount:element2.amount,
                                        posex:element2.posex,
                                        freeGood : element2.freeGood
                                    };
                                    
                                    if(!objectPromod.hasOwnProperty(prom.promoNamedescription)){
                                        objectPromod[prom.promoNamedescription] = prom;
                                        promosV.push(prom);
                                    }
    
                                    if(!objectPromod2.hasOwnProperty(prom.promoNamedescription) && (prom.condcode || prom.freeGood)){
                                        objectPromod2[prom.promoNamedescription] = prom;
                                        promosSend.push(prom);
                                    }
    
                                    console.log("objectPromod", objectPromod2);
                                    arraypromotosend.push(prom);
                                    component.set("v.tablePromotion",true);
                                }
                            });                
                        component.set("v.promosEnviar", arraypromotosend);  
                        component.set("v.promosEnviar2", arraypromotosend); 
                    	console.log('promosV:::::', promosV);
                        component.set("v.promosView",promosV);  
                    
                    }
                
                    if(arraypromotosend.length == 0){
                        component.set("v.tablePromotion",false);
                        var emptyArray = [];
                        component.set("v.promosEnviar",emptyArray);
                        component.set("v.promosEnviar2",emptyArray);
                    }
                }
            }
            console.log('calcWeight');
            component.set("v.weight", 0);
            var products = component.get("v.lookName");
            var confer = ''; 
            for(var i = 0; i < products.length; i++ ){
                console.log('products[0].productCode' + products[i].productCode);
                if(i == products.length-1){
                    confer += products[i].productCode ; 
                }else{
                    confer =+ products[i].productCode + ','; 
                }   
            }
            console.log('confer' + confer);
            var actionmax = component.get("c.MaxWeight");
                actionmax.setParams({ confer : confer });
                actionmax.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var weightList = response.getReturnValue();
                        console.log(weightList);
                        var weight = 0;
                        for(var i = 0;i < products.length; i++ ){
                            console.log(products[i].quantity);
                            for(var a = 0; a< weightList.length; a++ ){
                                console.log(weightList[a].ISSM_Weight__c);
                                if(weightList[a].ONTAP__ProductCode__c == products[i].productCode ){
                                    if(weightList[a].ISSM_Weight__c != undefined){
                                        weight += weightList[a].ISSM_Weight__c * products[i].quantity;  
                                    }
                                }
                            }
                        }
                        console.log(weight)
                        if(weight > 8000){
                            alert($A.get("$Label.c.You_have_reached_weight_limit"));
                        }
                        component.set("v.weight", weight);
                    }
                })
            $A.enqueueAction(actionmax);
        });

        $A.enqueueAction(action);
        var recordId2 = component.get("v.recordId")
        var action3 = component.get("c.getMaxWeight");
        action3.setParams({
            recordId2 : recordId2
        });
        action3.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var weightList = response.getReturnValue();
                component.set("v.weightList", weightList);
            }
        })
        $A.enqueueAction(action3);
    },
    
    getpaymet : function(component,recordId2) {
        var action = component.get("c.paymentMethod");
        action.setParams({ recordId2 : recordId2});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                component.set("v.creditCashValues", conts);
            }
            else if (state === "INCOMPLETE") {
                // do something
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },

    getWeight : function(component,recordId2) {
    var action = component.get("c.getMaxWeight");
        action.setParams({
            recordId2 : recordId2
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var weightList = response.getReturnValue();
                console.log('getWeight::weightList');
                console.log(weightList);
                component.set("v.weightList", weightList);
            }
        })
        $A.enqueueAction(action);
    },

    getAccountData : function(component,recordId2) {
        var action = component.get("c.getAccountDataObject");
            action.setParams({
                recordId2 : recordId2
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var accountData = response.getReturnValue();
                    component.set("v.initVariablesAccount", accountData);
                    var initaccount = component.get("v.initVariablesAccount");
                }
            })
            $A.enqueueAction(action);
    },
        getCreditTolerance : function(component, sum, creditCash, lookName) {
            var action = component.get("c.getCreditTolerance");
            var actualClient = component.get("v.initVariablesAccount[0]");
            action.setParams({
                client : actualClient,
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS"){
                    var creditTolerance = response.getReturnValue();
                    var tolerance = creditTolerance['HONES_Porcentaje_Tolerancia__c'];
                    var accountGroup = creditTolerance['ISSM_AccountGroup__c'];
                    var creditlimit = actualClient['ISSM_CreditLimit__c'];
                    var appliedTolerance = creditlimit + (creditlimit * tolerance)/100;
                    component.set('v.isValidPurchaseOrder', true);
                    
                    console.log('>>>>>>> actualClient: ', actualClient);
                    console.log('>>>>>>> creditTolerance: ', creditTolerance);
                    console.log('>>>>>>> tolerance: ', tolerance);
                    console.log('>>>>>>> creditlimit: ', creditlimit);
                    console.log('>>>>>>> appliedTolerance: ', appliedTolerance);
                    console.log('>>>>>>> sum order total: ', sum);
                    
                    if((tolerance == null || tolerance == undefined) && accountGroup!= null && accountGroup != undefined){
                        console.log('>>>>>>> exclusion applied to account Group: ', accountGroup);
                        component.set("v.ExcessOfLimit", false);
                        component.set('v.isValidPurchaseOrder', true);
                    }else if(tolerance != null && tolerance != undefined){
                        if(sum > appliedTolerance && ((creditCash[1]).toLowerCase() == (lookName[0].pay).toLowerCase() || ($A.get("$Label.c.OnCall_Credit")).toLowerCase() == (lookName[0].pay).toLowerCase()) ){
                            console.log('>>>>>>> exceeded tolerance');
                            component.set("v.ExcessOfLimit", true);
                            component.set('v.isValidPurchaseOrder', false);
                        }
                    } else{
                        if(sum > creditlimit && ((creditCash[1]).toLowerCase() == (lookName[0].pay).toLowerCase() || ($A.get("$Label.c.OnCall_Credit")).toLowerCase() == (lookName[0].pay).toLowerCase()) ){
                            console.log('>>>>>>> without tolerance defined');
                            component.set("v.ExcessOfLimit", true);
                            component.set('v.isValidPurchaseOrder', false);
                        } else{
                            console.log('>>>>>>> inside limit');
                            component.set("v.ExcessOfLimit", false);
                            component.set('v.isValidPurchaseOrder', true);
                        }
                    }
                }else{
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            })
            $A.enqueueAction(action);
        },

})