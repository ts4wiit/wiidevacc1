({
    doInit: function (component, event, helper) {
        var Idcuenta2 = component.get("v.Idcuenta");
        var recordId = component.get("v.idcall");
        var creditCashArray = ['Cash', 'Credit'];
        component.set("v.isLoading", true);
        component.set("v.boton", false);
        component.set("v.creditCash", creditCashArray);
        helper.gettheID(component,recordId);
        helper.gettheDate(component,recordId);
        
        var recordId2 = component.get("v.recordId");
        helper.getDataAccountDis(component,recordId2);
        helper.getpaymet(component,recordId2);
        helper.getInitVariablesForSearch(component,recordId2);
        helper.getWeight(component,recordId2);
        helper.getAccountData(component,recordId2)        
    },
    
    notview: function(component, event, helper) {
        console.log('Aqui entro jejeje');
        component.set("v.boton", false);    
    },
    onChangeChildCheckbox : function(component, event, helper) {
        
        var check = event.getSource().get('v.value');
        var creditCash = component.get("v.creditCash");
        //Hide button Generate Order if change value;
        component.set("v.boton", false);
        if(check == true){
            var generalList=component.get("v.lookName");
            generalList.forEach(function(element) {
                element.pay=$A.get("$Label.c.OnCall_Cash");
                
            });
            component.set("v.lookName",generalList);
            var cash = component.get("v.cash");
            component.set("v.cash", true);
            component.set("v.creditCashValues", creditCash[0]);
        }
        else{
            var generalList2=component.get("v.lookName");
            generalList2.forEach(function(element) {
                element.pay=$A.get("$Label.c.OnCall_Credit");
                
            });
            component.set("v.cash", false);
            component.set("v.creditCashValues", creditCash[1]);
            component.set("v.lookName",generalList2);
        } 
        
    },
    
    onCheck: function (cmp, event, helper) {
        var checkCmp = cmp.find("checkbox");
    },
    
    handleContactSearchComplete : function(component, event, helper) {
        var opt = [];
        opt.push(event.getParam("pickval"));  
        component.set("v.selected", opt);
    }, 
    
    pickNameChanged : function(component, event, helper) {
        var newValue = event.getParam("value");
        var oldValue = event.getParam("oldValue");
        component.set("v.newvalue",newValue);
        alert("Expense name changed from '" + oldValue + "' to '" + newValue + "'");
    },
    
    focus: function (component, event, helper) {
        var forOpen = component.find("lookup");
        forOpen.focus();
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        // Get Default 10 Records order by createdDate DESC  
        var getInputkeyWord = '';
    },
    
    onfocus: function (component, event, helper) {
        var forOpen = component.find("searchRes");
        $A.util.addClass(forOpen, 'slds-is-open');
        $A.util.removeClass(forOpen, 'slds-is-close');
        // Get Default 10 Records order by createdDate DESC  
        var getInputkeyWord = '';
    },
    
    keyPressController: function (component, event, helper) {
        // get the search Input keyword   
        var typingTimer = component.get("v.typingTimer");    
        if(typingTimer){clearTimeout(typingTimer);}
        var getInputkeyWord = component.get("v.SearchKeyWord");
        // check if getInputKeyWord size id more then 0 then open the lookup result List and 
        // call the helper 
        // else close the lookup result List part.   
        if ((getInputkeyWord != null && getInputkeyWord.length >= 3)  || (getInputkeyWord != null && getInputkeyWord.length == 0)) {
            var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
  			typingTimer = setTimeout(
                $A.getCallback(function() {
                    helper.searchHelper(component, event, getInputkeyWord);
                }), 300);
            component.set("v.typingTimer", typingTimer);
            
            
        } else {
            component.set("v.listOfSearchRecords", null);
            var forclose = component.find("searchRes");
            $A.util.addClass(forclose, 'slds-is-close');
            $A.util.removeClass(forclose, 'slds-is-open');
        }
        
        
    },
    
    // function for clear the Record Selaction 
    clear: function (component, event, helper) {
        
        component.set("v.SearchKeyWord", null);
        component.set("v.listOfSearchRecords", null);
        component.set("v.selectedRecord", {});
        component.set("v.search", "slds-input__icon slds-show");
    }, 
    
    toggleOptionalTab: function (cmp) {
        cmp.set('v.isPFNVisible', !cmp.get('v.isPFNVisible'));
    },
    
    toggleOptionalTab2: function (cmp) {

    },
    
    handlePromoEvent: function (component, event, helper) {
        var selectedPromo = event.getParam("promoName");
        component.set('v.selectedPromo', selectedPromo);        
    },
        
    // This function call when the end User Select any record from the result list.  
    //RJP 
    handleComponentEvent: function (component, event, helper) {
        // get the selected Account record from the COMPONENT event
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");        
        var element = '';
        var validation = true;
        var newElem = true;
        var listSelectedItems  = component.get("v.lookName");
        var emptiesPicking = $A.get("$Label.c.ORDER_EMPTIES_PICKING");
        var listSelectedItemsList  = [];
        var selectRecord = selectedAccountGetFromEvent.relatedProducts; 
        var unidades = selectedAccountGetFromEvent.measureCode;
        var cantidad = selectedAccountGetFromEvent.quantity;
        var unidadesdm = '';
        var PDF = $A.get("$Label.c.ORDER_FINISHED_PRODUCT");
        var ADE = $A.get("$Label.c.ORDER_ADD_EMPTIES");
        var PFN = $A.get("$Label.c.OUT_OF_REGULATION_PRODUCT");
        var PSG = $A.get("$Label.c.GOOD_SHAPE_PRODUCT");
        console.log('Eduardo Reyna selectedAccountGetFromEvent:::::::1', selectedAccountGetFromEvent);
        if(selectedAccountGetFromEvent.productType == PFN){
            console.log('selectedAccountGetFromEvent:::::::1', selectedAccountGetFromEvent);
            if(unidades == 'Botella' || unidades == 'Bottle'){
                unidadesdm = 'BOTELLA';
                selectedAccountGetFromEvent.Keyadd = selectedAccountGetFromEvent.Keyadd + '-Botella';
                //selectedAccountGetFromEvent.Keyadd = selectedAccountGetFromEvent. + '-Botella';                 
            } else {
                unidadesdm = 'CAJA';
                selectedAccountGetFromEvent.Keyadd = selectedAccountGetFromEvent.Keyadd + '-Caja';
                //selectedAccountGetFromEvent.Keyadd = selectedAccountGetFromEvent.Keyadd + '-Caja';
            }    
            for(var i =  0; i < selectRecord.length; i++ ){
                var recordrelated = selectRecord[i].materialProduct;
                var barsten = recordrelated. split(" ");
                console.log(barsten[0])
                if(barsten[0] == unidadesdm){
                    selectedAccountGetFromEvent.relatedProducts = [];
                    if(unidades == 'Botella' || unidades == 'Bottle'){
                        selectRecord[i].productType = selectRecord[i].productType + ' B';
                    } else {
                        selectRecord[i].productType = selectRecord[i].productType + ' C';
                    }   
                    selectedAccountGetFromEvent.KeyDltINC = selectRecord[i].KeyDltINC;
                    selectRecord[i].quantity = cantidad;
                    selectedAccountGetFromEvent.relatedProducts.push(selectRecord[i]);
                    break;
                }
            }
        }else if(selectedAccountGetFromEvent.productType == PSG){
            if(selectedAccountGetFromEvent.relatedProducts.length >0){
                selectRecord[0].quantity = cantidad;
            }
        }else{
            console.log('::::::  Aqui devemos armar las sigueientes llaves ::::::')
            console.log(selectedAccountGetFromEvent)
            if(selectedAccountGetFromEvent.productType == PDF || selectedAccountGetFromEvent.productType == ADE){
                selectedAccountGetFromEvent.Keyadd = selectedAccountGetFromEvent.Keyadd + PDF;
            }else{
                selectedAccountGetFromEvent.Keyadd = selectedAccountGetFromEvent.Keyadd + selectedAccountGetFromEvent.productType;
            }
            if(selectedAccountGetFromEvent.productType == PDF && selectedAccountGetFromEvent.relatedProducts.length> 0 ){
                selectedAccountGetFromEvent.isReturnable = true;
            }
            if(selectedAccountGetFromEvent.productType == PDF){
                component.set("v.Addempties", true);
            }
        }
        
    
        //Eduardo Reyna Gonzalez
        listSelectedItemsList.push(selectedAccountGetFromEvent);

        console.log('selectedAccountGetFromEvent: Consert ', selectedAccountGetFromEvent);

        if(listSelectedItems.length>0){
            var element = component.get("v.lookName");
            console.log('element: ', element);

            for(var keySelected = 0; keySelected < listSelectedItemsList.length; keySelected++){

                for(var i= 0; i< element.length; i++){
                    console.log(selectedAccountGetFromEvent.productType)
                    console.log(emptiesPicking)
                    console.log(element[i].productCode)
                    console.log(selectedAccountGetFromEvent.productCode)
                    console.log(selectedAccountGetFromEvent.productMetadataType)
                    console.log(selectedAccountGetFromEvent.pfntag)
                    console.log(selectedAccountGetFromEvent.pbetag)

                    var PDF = $A.get("$Label.c.ORDER_FINISHED_PRODUCT");
                    var ADE = $A.get("$Label.c.ORDER_ADD_EMPTIES");
                    var PFN = $A.get("$Label.c.OUT_OF_REGULATION_PRODUCT");
                    var PSG = $A.get("$Label.c.GOOD_SHAPE_PRODUCT");

                    if(listSelectedItemsList[keySelected].productType == PDF){
                        component.set("v.Addempties", true);
                    }
                    /*if(selectedAccountGetFromEvent.productType = element[i].productCode == selectedAccountGetFromEvent.productCode && selectedAccountGetFromEvent.productMetadataType == null && selectedAccountGetFromEvent.pfntag == null && selectedAccountGetFromEvent.pbetag == null){
                        if(selectedAccountGetFromEvent.productType =
                    }*/
                    console.log('selectedAccountGetFromEvent' + selectedAccountGetFromEvent.Keyadd); 
                    if(element[i].Keyadd ==selectedAccountGetFromEvent.Keyadd){
                        console.log('Entro Aqui jajaja'); 
                        console.log('selectedAccountGetFromEvent' + selectedAccountGetFromEvent.Keyadd); 
                        console.log('Entro Aqui jajaja'); 
                        newElem=false;                   
                        alert($A.get("$Label.c.You_have_already_added_this_product"));
                    }

                    console.log('IF');
                }
            }
            //);  
        }
        else{
            //_____FANNY
            console.log('ELSE');
            if(selectedAccountGetFromEvent.productType == emptiesPicking){
                /*var relatedEmptiesPicking = selectedAccountGetFromEvent.relatedProducts;
                console.log('relatedEmptiesPicking' + relatedEmptiesPicking);
                for(var key = 0; key < relatedEmptiesPicking.length; key++){
                    relatedEmptiesPicking[key].productType = emptiesPicking;
                    relatedEmptiesPicking[key].isRelated = false;
                    listSelectedItems.push(relatedEmptiesPicking[key]);
                }*/
                listSelectedItems.push(selectedAccountGetFromEvent);
            } else{
                listSelectedItems.push(selectedAccountGetFromEvent);
            }
            //_____FANNY  
            console.log('listSelectedItems' + listSelectedItems);   
            component.set("v.lookName",listSelectedItems );  
            component.set("v.showtabOI",true);
            newElem = false;
            component.set("v.showtabOI",true);    
            
        }
        if(newElem== true  && listSelectedItems.length>0){

            //FANNY_____
            
            if(selectedAccountGetFromEvent.productType == emptiesPicking){
                /*var relatedEmptiesPicking = selectedAccountGetFromEvent.relatedProducts;
                for(var key = 0; key < relatedEmptiesPicking.length; key++){
                    relatedEmptiesPicking[key].productType = emptiesPicking;
                    relatedEmptiesPicking[key].isRelated = false;
                    listSelectedItems.push(relatedEmptiesPicking[key]);
                }*/
                listSelectedItems.push(selectedAccountGetFromEvent);
            } else{
                listSelectedItems.push(selectedAccountGetFromEvent);
            }

            //FANNY_____
            component.set("v.lookName",listSelectedItems );  	
            component.set("v.showtabOI",true);            
        }

        component.set("v.weightOperation", true);        
        var action = component.get('c.calcWeight');
        $A.enqueueAction(action);
    },
         
    selectRecord: function (component, event, helper) {
        // get the selected record from list  -- selected added item
        var getSelectRecord = component.get("v.oRecord");
        // call the event   
        var compEvent = component.getEvent("oSelectedRecordEvent");
        // set the Selected sObject Record to the event attribute.  
        compEvent.setParams({
            "recordByEvent": getSelectRecord
        });
        // fire the event  
        compEvent.fire();
    },
        
    inlineEditName: function (component, event, helper) {
        // show the name edit field popup 
        component.set("v.nameEditMode", true);
        // after the 100 millisecond set focus to input field   
        setTimeout(function () {
            component.find("inputId");
        }, 100);                
    },
    
    inlineEditRating: function (component, event, helper) {
        // show the rating edit field popup 
        component.set("v.ratingEditMode", true);
        // after set ratingEditMode true, set picklist options to picklist field 
        component.find("accRating").set("v.options", component.get("v.ratingPicklistOpts"));
        // after the 100 millisecond set focus to input field   
        setTimeout(function () {
            component.find("accRating");
        }, 100);
    },
    
    onNameChange: function (component, event, helper) {
        // if edit field value changed and field not equal to blank,
        // then show save and cancel button by set attribute to true
        if (String(event.getSource().get("v.value")).trim() != '') {
            component.set("v.showSaveCancelBtn", true);
        }
    },
    
    onRatingChange: function (component, event, helper) {
        // if picklist value change,
        // then show save and cancel button by set attribute to true
        component.set("v.showSaveCancelBtn", true);
    },
    
    closeNameBox: function (component, event, helper) {
        // on focus out, close the input section by setting the 'nameEditMode' att. as false   
        component.set("v.nameEditMode", false);
        // check if change/update Name field is blank, then add error class to column -
        // by setting the 'showErrorClass' att. as True , else remove error class by setting it False   
        if (String(event.getSource().get("v.value")).trim() == '') {
            component.set("v.showErrorClass", true);
        } else {
            component.set("v.showErrorClass", false);
        }
    },
    
    closeRatingBox: function (component, event, helper) {
        // on focus out, close the input section by setting the 'ratingEditMode' att. as false
        component.set("v.ratingEditMode", false);
    },
    
    openModel: function(component, event, helper) {
        var initAccount = component.get("v.initVariablesAccount[0].ONCALL__KATR4__c");
        var atr4 = initAccount.slice(0, 2);

        if(atr4 == 'YA'){
            component.set("v.showDirections", true)
        }
        // for Display Model, set the "Opisen" attribute to "true"
        var disponible = component.get("v.creditLimitInit");
        var lookName = component.get("v.lookName");
        var call = component.get("v.Idcuenta");
        var sum = 0;
        var creditCash = component.get("v.creditCash");

        component.set("v.isOpen", true);
        
        lookName.forEach(function(element) {
            sum = sum + element.total;
        });
        
        if((creditCash[1]).toLowerCase() == (lookName[0].pay).toLowerCase() || ($A.get("$Label.c.OnCall_Credit")).toLowerCase() == (lookName[0].pay).toLowerCase()){
            var totalCredito = lookName[0].orderTotal ? disponible - lookName[0].orderTotal : disponible;
            //console.log("totalCredito", totalCredito);
            component.set("v.creditLimit", totalCredito);            
        }else{
            component.set("v.creditLimit", disponible);
        }
        
        helper.getCreditTolerance(component, sum, creditCash, lookName);
        
        /** The method is remplaced to block by total credit limit, not by warning on credit available excedeed, 'sum' is the total amount of the order
        if(sum > disponible && ((creditCash[1]).toLowerCase() == (lookName[0].pay).toLowerCase() || ($A.get("$Label.c.OnCall_Credit")).toLowerCase() == (lookName[0].pay).toLowerCase()) ){
                component.set("v.ExcessOfLimit", true);
            }
            else{
                component.set("v.ExcessOfLimit", false);
            }**/
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
    },
    
    likenClose: function(component, event, helper) {
        // Display alert message on the click on the "Like and Close" button from Model Footer 
        // and set set the "isOpen" attribute to "False for close the model Box.
        alert('Se ha generado la orden:)');
        component.set("v.isOpen", false);
    },
        
    deleteProd: function (component, event, helper) {
        component.set("v.Addempties", false);
        var foralln = component.get("v.lookName");     
        var i = 0;            
        var index = event.target.dataset.index;
        component.set("v.boton", false);
        var Finishprod = $A.get("$Label.c.ORDER_FINISHED_PRODUCT");
        var PFN = $A.get("$Label.c.OUT_OF_REGULATION_PRODUCT");
        var PBE = $A.get("$Label.c.GOOD_SHAPE_PRODUCT");
        var buscar = foralln[index].KeyDltINC;
        var tregistro = foralln[index].productType;
        if(tregistro == Finishprod || tregistro == PFN ||  tregistro == PBE){
            if(foralln[index].relatedProducts.length >0){
                foralln[index].relatedProducts= [];
                foralln.splice( index, 1 );
            }
            else{
                console.log(foralln);
                foralln.splice( index, 1 );      
                var relacionado = foralln;
                console.log('Entro relacionado', relacionado);
                console.log('Buscar KeyDltINC:: ' + buscar);
                for(var a = 0; a<relacionado.length; a++){
                    if(buscar == relacionado[a].KeyDltINC){ 
                        if(buscar != undefined && buscar != '' ){
                            foralln.splice( a, 1 );      
                        }
                        else{
                            relacionado.splice( index, 1 );
                        }
                    }
                } 
            } 
        }
        else{
            foralln.splice( index, 1 );
        }     
        if( foralln.length==0){
            component.set("v.showtabOI", false);   
            component.set("v.Addempties", false);         
        }
        var total = 0;
        var PDF = $A.get("$Label.c.ORDER_FINISHED_PRODUCT");
        var PAE = $A.get("$Label.c.ORDER_ADD_EMPTIES");
        
        var PFN = $A.get("$Label.c.OUT_OF_REGULATION_PRODUCT");
        var PSG = $A.get("$Label.c.GOOD_SHAPE_PRODUCT");        
        var PRE = $A.get("$Label.c.ORDER_EMPTIES_PICKING");        
        var newTotalOrder = 0;
        var newTotalWeight = 0;
        for(i = 0; i < foralln.length; i++){
            newTotalOrder += foralln[i].total;
            newTotalWeight += foralln[i].weight * foralln[i].quantity;
            if(foralln[i].productType == PRE || foralln[i].productType == PSG || foralln[i].productType == PFN || foralln[i].productType == PAE){
                if(foralln[i].productType == PFN){
                    var res = foralln[i].Keyadd;
                    var key  = res.split("-");
                    if(key[2] != 'Botella'){
                        total += foralln[i].quantity;  
                    }
                }else{
                    total += foralln[i].quantity;
                }
            }
            if(foralln[i].productType == PDF){
                component.set("v.Addempties", true);
            }
        }
        component.set("v.totalenvaces", total);


        component.set("v.orderTotal", newTotalOrder.toFixed(3));
        component.set("v.weight", newTotalWeight);
        component.set("v.lookName",foralln);
        component.set("v.weightOperation", false);        
        var action = component.get('c.calcWeight');
        $A.enqueueAction(action);
    },

    calcWeight: function(component, event, helper) {
        var products = component.get("v.lookName");
        var weightOperation = component.get("v.weightOperation");
        var weight = component.get("v.weight");
        if(products.length == 0 || weight == undefined || (products.length == 1 && products[0].productType == $A.get("$Label.c.ORDER_EMPTIES_PICKING")) || (products.length == 1 && products[0].productType == $A.get("$Label.c.OUT_OF_REGULATION_PRODUCT")) || (products.length == 1 && products[0].productType == $A.get("$Label.c.GOOD_SHAPE_PRODUCT"))){
            component.set("v.weight", 0);
            weight = 0;
        }

        for(var i = products.length - 1;i < products.length; i++ ){
            if(weightOperation == true && products[i].productType != $A.get("$Label.c.ORDER_EMPTIES_PICKING") && products[i].productType != $A.get("$Label.c.OUT_OF_REGULATION_PRODUCT") && products[i].productType != $A.get("$Label.c.GOOD_SHAPE_PRODUCT")){
                weight += products[i].weight * products[i].quantity;  
            }
        }

        var weightData = component.get("v.weightList");
        if(weight >= weightData[0].Max_Weight__c){
            alert($A.get("$Label.c.You_have_reached_weight_limit"));
        }
        component.set("v.weight", weight);
        //$A.get('e.force:refreshView').fire();
    },
    
    deleteProm: function (component, event, helper) {
        var foralln = component.get("v.promosView");            
        var i = 0;            
        var index = event.target.dataset.index;  
        console.log(index);
        foralln.splice(index, 1);      
        component.set("v.promosEnviar2",foralln);
        component.set("v.promosView",foralln);

        if(foralln.length ==0){
            component.set("v.tablePromotion", false);
        }
/*        for (i = 0; i < foralln.length; i++) {
            if(i!=index) {
                tempList.push(foralln[i]);  
                component.set("v.tablePromotion", false);
            }
        }	
        if( tempList.length==0){
            component.set("v.tablePromotion", false);
        }*/
    },
    
    openModel2: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpen", true);
    },
    
    closeModel2: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen2", false);
    },
    
    likenClose2: function(component, event, helper) {
        // and set set the "isOpen" attribute to "False for close the model Box.
        alert('Se ha generado la orden:)');
        component.set("v.isOpen2", false);
    },   
    
    closeModel11: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpenModel1", false);
        component.set("v.boton", false);
    },
    closeModel22: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpenModel2", false);
        component.set("v.boton", false);
    },
    
    openModel1: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpenModel1", true);
        component.set("v.isOpen", false);
    },
    
    openModel2: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isOpenModel2", true);
        component.set("v.isOpen", false);
    },
        
    sendOrder: function(component, event, helper) {
        var initAccount = component.get("v.initVariablesAccount[0].ONCALL__KATR4__c");
        var atr4 = initAccount.slice(0, 2);
        var validData = true;

        if(atr4 == 'YA'){
            var validDataLegalName = component.find('legalName');
            var validDataName = component.find('name');
            var validDataStreet = component.find('street');
            var validDataPostalCode = component.find('postalCode');
            var validDataCity = component.find('city');
            var validDataDistrict = component.find('district');
            var validDataRegion = component.find('region');
            var validDataPhone = component.find('phone');

            if( validDataLegalName.get('v.validity').valid == false || 
                validDataName.get('v.validity').valid == false || 
                validDataStreet.get('v.validity').valid == false || 
                validDataPostalCode.get('v.validity').valid == false || 
                validDataCity.get('v.validity').valid == false || 
                validDataDistrict.get('v.validity').valid == false || 
                validDataRegion.get('v.validity').valid == false || 
                validDataPhone.get('v.validity').valid == false)
            {   
                validData = false;

                validDataLegalName.showHelpMessageIfInvalid();
                validDataName.showHelpMessageIfInvalid();
                validDataStreet.showHelpMessageIfInvalid();
                validDataPostalCode.showHelpMessageIfInvalid();
                validDataCity.showHelpMessageIfInvalid();
                validDataDistrict.showHelpMessageIfInvalid();
                validDataRegion.showHelpMessageIfInvalid();
                validDataPhone.showHelpMessageIfInvalid();
                
            } else {
                validData = true;
            }

        } else
        {   
            var validData = true;
        };

        if (validData) {
            component.set("v.isOpen", false);
            component.set("v.boton", false);
            component.set("v.weight", 0);

            var action = component.get("c.sendAccountData");
            $A.enqueueAction(action);
        } else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "message": "Please correct form values before saving.",
                "type" : "error"
            });
            toastEvent.fire();
        }
    },
    
    sendOrderandUpdate: function(component, event, helper) {
        component.set("v.isOpen", false);
        var recordId = component.get("v.recordId");
        helper.getResponseupdate(component,recordId,event);                
    },
    
    changeButton: function(component, event, helper,document) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.hiddenButtonCalculate", false);
        component.set("v.hiddenButtonGenerateOrder", true);

        var recordId = component.get("v.recordId");
        helper.getprice(component,recordId);
        
        var promos = component.get("v.promosEnviar2");
        var lookName = component.get("v.lookName");
 
    },


    handleSearchEvent: function(component, event, helper) {
        var value = event.getParam("cleanAndFocus");
        component.set("v.boton", false);
        if(value == true){
            var searchId = component.find("lookup").focus();
            component.set("v.SearchKeyWord", "");  
            var getInputkeyWord = '';
            helper.searchHelper(component, event, getInputkeyWord);
        }        
    },
    
    handleClickButtonCloseModal: function(component, event, helper) {
        component.set("v.ExcessOfLimit", false); 
    
    },
    disableGenerateButton: function(component, event, helper) {
        component.set("v.boton",false);
    },

    //FANNY cloning _________________
    cloneComponentRender : function(component, event, helper){
        var recordFromCurrentCall = component.get('v.recordId');
        var currentCall = component.get('v.Idcuenta[0].Id');
        var currentCallName = component.get('v.Idcuenta[0].Name');

        component.set('v.dataValues.accountId', recordFromCurrentCall);
        component.set('v.dataValues.callId', currentCall);
        component.set('v.dataValues.callName', currentCallName);
        component.set('v.cloneComponent', true);
    },

    getCallsJS : function(component, event, helper) {
        var action = component.get('c.getCalls');
        var accountValue = component.get('v.dataValues.accountId');
        component.set('v.showTableOfCalls', true);

        if(accountValue != '' && accountValue != null){
            action.setParams({
                "accountValue": accountValue
            });
            
            action.setCallback(this, function(response){
                if(response.getState() === "SUCCESS"){
                    var result = response.getReturnValue();

                    for(var i = 0; i < result.length ; i++){
                        result[i].checkboxCalls = false;
                        var fecha_new ='';
                        var fecha_before = '' + result[i].ISSM_Date__c;
                        if(fecha_before != "undefined"){
                            console.log(fecha_before);
                            var fecha = fecha_before.split('-');
                            console.log(fecha);
                            fecha_new = fecha[2] + '/' + fecha[1] + '/' + fecha[0];
                            console.log(fecha_new);
                        }else{
                            fecha_new = '';
                        }
                        result[i].checkboxCalls = false;
                        result[i].fechacreada = fecha_new;    

                    };

                    if(result){
                        component.set('v.tableCallsToClone', result);
                        component.set('v.showTableOfCalls', true);
                    }
                    
                } else if (action.getState() === "ERROR") {
                    var errors = response.getError();
                } else {
                    toastError.setParams({"message": "Ocurrió un error inesperado al recibir el callback."}).fire();
                }
            });
            $A.enqueueAction(action);
        } else {
            //console.log('Debes ingresar una cuenta valida');
        }
    },

    getCallsJSCheckbox : function(component, event, helper) {
        var table = component.get('v.tableCallsToClone');
        var arrayTableCall = [];
        var arrayTableCallName = [];

        for(var i = 0; i < table.length; i++){
            if(table[i].checkboxCalls == true){
                arrayTableCall.push(table[i].Id);
                arrayTableCallName.push(table[i].Name);
            }
        }

        var callListString = arrayTableCall.toString()
        var callListStringName = arrayTableCallName.toString()
        component.set('v.dataValues.callId', callListString);
        component.set('v.dataValues.callName', callListStringName);
    },

    getOrdersJSCheckbox : function(component, event, helper) {
        var table = component.get('v.tableOrdersToClone');
        var arrayTableOrder = [];
        var arrayTableOrderName = [];

        for(var i = 0; i < table.length; i++){
            if(table[i].checkboxOrders == true){
                arrayTableOrder.push(table[i].Id);
                arrayTableOrderName.push(table[i].Name);
            }
        }

        var orderListString = arrayTableOrder.toString()
        var orderListStringName = arrayTableOrderName.toString()
        component.set('v.dataValues.orderIdList', orderListString);
        component.set('v.dataValues.orderName', orderListStringName);
    },

    getOrdersJS : function(component, event, helper) {
        var orderList = [];
        var callsCheckbox = component.get("v.dataCalls.checkbox");
        var action = component.get('c.getOrders');
        var callValue = component.get('v.dataValues.callId');
        
        component.set('v.showTableOfOrders', true);

        if(callValue != '' && callValue != null){
            action.setParams({
                "callValue": callValue
            });
            
            action.setCallback(this, function(response){
                if(response.getState() === "SUCCESS"){
                    var result = response.getReturnValue();
                    if(result){
                        for(var i = 0; i < result.length; i++){
                            if(typeof(result[i].ONCALL__Call__r) == "undefined" && result[i].ONTAP__Event_Id__c != null){
                                result[i].ONCALL__Call__r = {Name :'ONTAP'}
                            }
                        }
                        component.set('v.tableOrdersToClone', result);
                        component.set('v.showTableOfOrders', true);

                        for(var i = 0; i < result.length; i++){
                            var orderValueId = result[i].Id;
                        }

                        var orderListString = orderList.toString()
                        component.set('v.dataValues.orderIdList', orderListString);
                    }
                    
                } else if (action.getState() === "ERROR") {
                    var errors = response.getError();
                } else {
                    toastError.setParams({"message": "Ocurrió un error inesperado al recibir el callback."}).fire();
                }
            });
            $A.enqueueAction(action);
        } else {
            //console.log('Debes ingresar una cuenta valida');
        }
    },

    getAllOrdersJS : function(component, event, helper) {
        var accountId = component.get("v.recordId");
        var action = component.get('c.getAllOrders');

        action.setParams({
            "accountId": accountId
        });

        action.setCallback(this, function(response){
            if(response.getState() === "SUCCESS"){
                var result = response.getReturnValue();

                if(result){
                    for(var i = 0; i < result.length; i++){
                        if(typeof(result[i].ONCALL__Call__r) == "undefined" && result[i].ONTAP__Event_Id__c != null){
                            result[i].ONCALL__Call__r = {Name :'ONTAP'}
                        }
                    }

                    component.set('v.tableOrdersToClone', result);
                    component.set('v.showTableOfOrders', true);
                }
            } else if (action.getState() === "ERROR") {
                var errors = response.getError();
            } else {
                toastError.setParams({"message": "Ocurrió un error inesperado al recibir el callback."}).fire();
            }
        });
        $A.enqueueAction(action);
    },

    getItemsJSList : function(component, event, helper) {
        var action = component.get('c.getItemsList');
        var orderValueList = component.get('v.dataValues.orderIdList');
        var recordId = component.get("v.recordId");

        if(orderValueList != '' && orderValueList != null){
            action.setParams({
                "orderValueList": orderValueList,
                "recordId" : recordId
            });
            
            action.setCallback(this, function(response){
                if(response.getState() === "SUCCESS"){
                    var result = response.getReturnValue();
                    console.log('Aqui va mas ', result);
                    for(var i = 0; i<result.length; i++){
                        var ADD_EMPTIES = $A.get("$Label.c.ORDER_ADD_EMPTIES");
                        if(result[i].productType == ADD_EMPTIES){
                            result[i].relatedProducts = [];
                        }
                        var OFP = $A.get("$Label.c.ORDER_FINISHED_PRODUCT");
                        if(result[i].productType == OFP){
                            component.set("v.Addempties", true);
                        }
                    }

                    if(result){
                        component.set('v.renglonesToClone', result);
                        component.set('v.tableItemsToClone', result);
                        component.set('v.showTableOfItems', true);
                    }
                } else if (action.getState() === "ERROR") {
                    var errors = response.getError();
                } else {
                    toastError.setParams({"message": "Ocurrió un error inesperado al recibir el callback."}).fire();
                }
            });
            $A.enqueueAction(action);    
        } else {
            //console.log('Debes ingresar una cuenta valida');
        }
    },

    handleComponentEventClone: function (component, event, helper) {
        var renglonesToClone = component.get('v.renglonesToClone');
        var arrayToClone = component.get('v.tableItemsToClone');
        var listSelectedItems = component.get("v.lookName");
        var allData = component.get('v.dataItems');

        for(var i = 0 ; i < arrayToClone.length ; i++){
            listSelectedItems.push(arrayToClone[i]);
        }
        console.log('>>> 6 listSelectedItems: ', listSelectedItems);
        component.set("v.lookName",listSelectedItems);
        component.set("v.showtabOI",true);
        component.set("v.cloneComponent",false);
    },

    sendAccountData : function (component, event, helper) {
        var recordId = component.get("v.recordId");
        var initVariablesAccount = component.get("v.initVariablesAccount");
        var action = component.get("c.sendAccountDataObject");
            action.setParams({
                initVariablesAccount : initVariablesAccount
            });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var accountData = response.getReturnValue();
                    helper.getResponse(component,recordId,event);
                }
            })
            $A.enqueueAction(action);
    },

    quantityUpdate : function (component, event, helper) {
        var basket = component.get("v.lookName");
        var liquidQuantity = 0;
        var relatedLiquidQuantity = 0;
        var indexvar = event.getSource().get("v.title");
        console.log("indexvar:::" + indexvar);
        component.set("v.boton", false);

        var Finishprod = $A.get("$Label.c.ORDER_FINISHED_PRODUCT");
        var PFN = $A.get("$Label.c.OUT_OF_REGULATION_PRODUCT");
        var PBE = $A.get("$Label.c.GOOD_SHAPE_PRODUCT");
        var cantidad = basket[indexvar].quantity;
        var buscar = basket[indexvar].KeyDltINC;
        console.log('cantidad', cantidad);
        var tregistro = basket[indexvar].productType;
        console.log('tr' + tregistro)
        console.log('pfn' + PFN)
        if(tregistro == Finishprod || tregistro == PFN ||  tregistro == PBE){
            var conter = basket[indexvar].relatedProducts
            console.log('conter' + conter);
            if(conter.length >0){
                basket[indexvar].relatedProducts[0].quantity = cantidad;             
            }else{
                var relacionado = basket;
                console.log('relacionado', relacionado);
                for(var a = 0; a<relacionado.length; a++){                
                    console.log('buscar' + buscar)
                    console.log('relacionado' + relacionado)
                     
                    if(buscar == relacionado[a].KeyDltINC){
                        
                        if(buscar != ''){
                            basket[a].quantity = cantidad;
                        }
                    }        
                }    
            }
        }
        // Eduardo Reyna Gonzalez   
        component.set("v.lookName", basket);
        component.set("v.boton", false);
    },


    AddAll : function (component, event, helper) {
        console.log('Enterado');
        component.set("v.Addempties", false);
        var basket = component.get("v.lookName");
        component.set("v.boton", false);
        var ADD_EMPTIES = $A.get("$Label.c.ORDER_ADD_EMPTIES");
        var Finishprod = $A.get("$Label.c.ORDER_FINISHED_PRODUCT");
        for(var a = 0; a<basket.length; a++){  
            if(basket[a].productType == Finishprod) { 
                if(basket[a].relatedProducts.length>0){
                    basket[a].productType = ADD_EMPTIES;
                    basket[a].relatedProducts=[];
                    basket[a].isReturnable=false;
                } else{
                    if(basket[a].isReturnable == true){
                        basket[a].productType = ADD_EMPTIES;
                        basket[a].addempties = 'X';
                        basket[a].isReturnable=false;   
                    }
                }     
            }
        }
        for(var a = 0; a<basket.length; a++){  
            if(basket[a].isRelated == true){
                console.log('Entro', basket[a].isRelated)
                basket.splice(a, 1);
                a =0;
            }
        }
        var total = 0;
        var PAE = $A.get("$Label.c.ORDER_ADD_EMPTIES");
        var PFN = $A.get("$Label.c.OUT_OF_REGULATION_PRODUCT");
        var PSG = $A.get("$Label.c.GOOD_SHAPE_PRODUCT");    
        var PRE = $A.get("$Label.c.ORDER_EMPTIES_PICKING");      
        var newTotalOrder = 0;
        var newTotalWeight = 0;
        var orderTotal = 0;
        for(i = 0; i < basket.length; i++){
            orderTotal += basket[i].total;
            newTotalOrder += basket[i].total;
            newTotalWeight += basket[i].weight * basket[i].quantity;
            if(basket[i].productType == PRE || basket[i].productType == PSG || basket[i].productType == PFN || basket[i].productType == PAE){
                var res = basket[i].Keyadd;
                console.log(res);
                if(basket[i].productType == PFN){
                    var key  = res.split("-");
                    if(key[2] != 'Botella'){
                        total += basket[i].quantity;  
                    }
                }else{
                    total += basket[i].quantity; 
                }
            }
        }
        component.set("v.orderTotal", orderTotal);
        component.set("v.totalenvaces", total);
        component.set("v.lookName", basket);
    },

    ChangetypeUpdate : function (component, event, helper) {
        component.set("v.Addempties", false);
        var basket = component.get("v.lookName");
        var indexvar = event.getSource().get("v.title");
        console.log("indexvar:::" + indexvar);
        component.set("v.boton", false);
        var Finishprod = $A.get("$Label.c.ORDER_FINISHED_PRODUCT");
        var PFN = $A.get("$Label.c.OUT_OF_REGULATION_PRODUCT");
        var PBE = $A.get("$Label.c.GOOD_SHAPE_PRODUCT");
        var ADD_EMPTIES = $A.get("$Label.c.ORDER_ADD_EMPTIES");
                               
        var cantidad = basket[indexvar].quantity;
        var buscar = basket[indexvar].KeyDltINC;
        if(basket[indexvar].relatedProducts.length>0){
            var ADD_EMPTIES = $A.get("$Label.c.ORDER_ADD_EMPTIES");
            basket[indexvar].productType = ADD_EMPTIES;
            basket[indexvar].relatedProducts=[];
            basket[indexvar].isReturnable=false;
        }else{
            var tregistro = basket[indexvar].productType;
                var relacionado = basket;
                console.log('relacionado', relacionado);
                var orderTotal = 0;
			basket[indexvar].productType = ADD_EMPTIES;
            basket[indexvar].addempties = 'X';
            basket[indexvar].isReturnable=false;
            for(var a = 0; a<relacionado.length; a++){ 
                        if(buscar == relacionado[a].KeyDltINC){
                            console.log ('indexvar', indexvar);
                            if(a != indexvar){
                                basket.splice( a, 1 );
                            }
                        }        
                    } 
                console.log('basket', basket);   
        }
        var PDF = $A.get("$Label.c.ORDER_FINISHED_PRODUCT");
        for(var e = 0; e<basket.length; e++){
            if(basket[e].productType == PDF){
                component.set("v.Addempties", true);
            }
        }

        var total = 0;
        var PAE = $A.get("$Label.c.ORDER_ADD_EMPTIES");
        var PFN = $A.get("$Label.c.OUT_OF_REGULATION_PRODUCT");
        var PSG = $A.get("$Label.c.GOOD_SHAPE_PRODUCT");  
        var PRE = $A.get("$Label.c.ORDER_EMPTIES_PICKING");          
        var newTotalOrder = 0;
        var newTotalWeight = 0;
        var orderTotal = 0;
        for(i = 0; i < basket.length; i++){
            orderTotal += basket[i].total;
            newTotalOrder += basket[i].total;
            newTotalWeight += basket[i].weight * basket[i].quantity;
            if(basket[i].productType == PRE || basket[i].productType == PSG || basket[i].productType == PFN || basket[i].productType == PAE){
                var res = basket[i].Keyadd;
                console.log(res);
                if(basket[i].productType == PFN){
                    var key  = res.split("-");
                    if(key[2] != 'Botella'){
                        total += basket[i].quantity;  
                    }
                }else{
                    total += basket[i].quantity; 
                }
            }
        }
        component.set("v.orderTotal", orderTotal);
        component.set("v.totalenvaces", total);
        component.set("v.lookName", basket);
    }

})