({
    getQuestionsAnswersFunc : function(component, event)
    {
        //Get Map with list of questions and answers
        var getQuestionsAnswersAction = component.get("c.getQuestionsAnswers");
        getQuestionsAnswersAction.setParams({"IdCase": component.get("v.recordId")});
        
        getQuestionsAnswersAction.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                var result = response.getReturnValue();            
                component.set("v.answers", result.listAnswers);                
                component.set("v.questions", result.listQuestions);
                component.set("v.caseElement", result.listCase);
                this.createForm(component);
                if(result.listQuestions && result.listQuestions.length > 1)
                {
                    component.set("v.formVisible", true);
                }
            }
            else
            {
                console.log("Failed with state: " + state);
            } 
        });
        
        $A.enqueueAction(getQuestionsAnswersAction);  
    },
    
	createForm : function(component) 
    {
        //Create a dynamic form
        var listQuestion = component.get("v.questions");
        var listAnswer = component.get("v.answers");
        var listCases = component.get("v.caseElement");
        var caseEle = listCases[0];
        
        var tipoSelect = "select";
        var tipoInput = "input";
        var arregloInicial = [];         
        var listComponent = [];
        
        for(var i=0; i<listQuestion.length; i++)
        {
            var arregloSecundario = []; 
            if(listQuestion[i].CS_Question_Type__c == "Unique Selection" || listQuestion[i].CS_Question_Type__c == "Selección Unica")
            {                
                //Add options
                var optionArray = listQuestion[i].CS_Options__c ? (listQuestion[i].CS_Options__c).split("|") : [];
                //Initial option
                arregloInicial.push(["option", {"label": "Seleccione una opción", "value": "", "style": "color: rgb(22, 50, 92)" }]);
                listComponent.push({"typehtml":"option","dependency": i + optionArray.length});
                
                //Add option list
                for(var j in optionArray)
                {
                    let options = [];
                    options.push("option");
                    options.push({"label": optionArray[j], "value": optionArray[j], "style": "color: rgb(22, 50, 92)"});
                	let position = i + optionArray.length;
                    listComponent.push({"typehtml":"option","dependency":position});
                    arregloInicial.push(options);
                }
                
                arregloSecundario.push("lightning:"+tipoSelect);
                arregloSecundario.push({"aura:id": component.getReference("v.questions["+i+"].Id"),"label": listQuestion[i].CS_Question__c,"value": component.getReference("v.answers["+i+"].Answer__c"),
                                                                          "style": "color: rgb(22, 50, 92);", "disabled" : caseEle && caseEle.Status == "Closed" ? true : false });
                listComponent.push({"typehtml":"picklist","dependency":""});
                arregloInicial.push(arregloSecundario);
            }
            
            if(listQuestion[i].CS_Question_Type__c == "Text" || listQuestion[i].CS_Question_Type__c == "Time" || listQuestion[i].CS_Question_Type__c == "Tiempo" || listQuestion[i].CS_Question_Type__c == "Texto")
            {  
                arregloSecundario.push("lightning:"+ tipoInput);
                var element = {};
                element.Id = listQuestion[i].Id;
                element.label = listQuestion[i].CS_Question__c;
                element.value = component.getReference("v.answers[" + i + "].Answer__c");
                element.style = "color: rgb(22, 50, 92)";
                element.maxlength = "255";
                element.disabled = caseEle && caseEle.Status == "Closed" ? true : false;
                
                if(listQuestion[i].CS_Question_Type__c == "Time")
                {
                    element.type = "time";
                    element.min = "09:00:00.000Z";
                    element.max = "21:00:00.000Z";
                    element.formatter = "hh:mm a";
                }
                
                arregloSecundario.push(element);
                listComponent.push({"typehtml":"text","dependency":""});
                arregloInicial.push(arregloSecundario);
            }
            
			if(listQuestion[i].CS_Question_Type__c == "Numeric" || listQuestion[i].CS_Question_Type__c == "Numérico")
            {  
                arregloSecundario.push("ui:inputNumber");
                var element = {};
                element.Id = listQuestion[i].Id; 
                element.label = listQuestion[i].CS_Question__c;
                element.value = component.getReference("v.answers["+ i +"].Answer__c");
                element.style = "color: rgb(22, 50, 92)";
                element.maxlength = "255";
                element.format = "#,###";
                element.step = "1";
                element.disabled = caseEle && caseEle.Status == "Closed" ? true : false;                
                arregloSecundario.push(element);
                listComponent.push({"typehtml":"text","dependency":""});
                arregloInicial.push(arregloSecundario);
                
            }
            
            if(listQuestion[i].CS_Question_Type__c == "Date" || listQuestion[i].CS_Question_Type__c == "Fecha")
            {
                arregloSecundario.push("ui:inputDate");
                var element = {};
                element.Id = listQuestion[i].Id;
                element.label = listQuestion[i].CS_Question__c;
                element.displayDatePicker = true;
                element.format = "dd/MM/yyyy";
                element.class = "slds-input";
                element.value = component.getReference("v.answers["+i+"].Answer__c");
                element.style = "color: rgb(22, 50, 92); width: 98%;";
                element.disabled = caseEle && caseEle.Status == "Closed" ? true : false;
                arregloSecundario.push(element);
                listComponent.push({"typehtml":"text","dependency":""});
                arregloInicial.push(arregloSecundario);
            }
            
            if(listQuestion[i].CS_Question_Type__c == "Telephone" || listQuestion[i].CS_Question_Type__c == "Teléfono")
            {                                  
                arregloSecundario.push("lightning:"+ tipoInput);
                var element = {};
                element.Id = listQuestion[i].Id; 
                element.label = listQuestion[i].CS_Question__c;
                element.value = component.getReference("v.answers["+ i +"].Answer__c");
                element.style = "color: rgb(22, 50, 92)";
                element.maxlength = "11";
                element.type = "tel";
                element.onblur = component.getReference("c.functionPhone");
                element.onfocus = component.getReference("c.functionNumber");
                element.onchange = component.getReference("c.functionNumber");
                //element.format = "#,###";
                element.disabled = caseEle && caseEle.Status == "Closed" ? true : false;                
                arregloSecundario.push(element);
                listComponent.push({"typehtml":"text","dependency":""});
                arregloInicial.push(arregloSecundario);
                
            }
        }
        
		//Create components                
		$A.createComponents
        (
            arregloInicial,
            function(components, status, errorMessage)
            {
                if (status === "SUCCESS") 
                {                    
                    var body = component.get("v.body");                     
                    var arrayOptions = [];
                    
                    for(var i = 0; i<components.length; i++)
                    {                        
                        if(listComponent[i].typehtml == "option")
                        {                            
                            arrayOptions.push(components[i]);
                        }
                        else
                        {
                            if(listComponent[i].typehtml == "picklist")
                            {
                            	components[i].set("v.body", arrayOptions);
                                arrayOptions = [];
                            }
                            
                            body.push(components[i]);
                        }                      
                    } 
                    
					component.set("v.body", body);                                       
                }
                else if (status === "INCOMPLETE") 
                {
                    console.log("No response from server or client is offline.")
                }
                else if (status === "ERROR") 
                {
                    console.log("Error: " , errorMessage);
                }
            }
        );
	},    

    formatPhone : function(component, event)
    {
        var elementPhone = event.getSource().get("v.value");
        var temp = elementPhone;
        if(elementPhone && elementPhone.length < 10)
        {
            temp = elementPhone.replace(/(\d{4})(\d{4})/, '$1-$2'); 
        }
        else if(elementPhone && elementPhone.length > 10)
        {
            var lada = elementPhone.substring(0,3);
            var phone = elementPhone.substring(3, elementPhone.length);            
            temp = "(+" + lada + ")" + phone.replace(/(\d{4})(\d{4})/, '$1-$2');            	
        }
        else if(elementPhone && elementPhone.length == 10)
        {
            var lada = elementPhone.substring(0,2);
            var phone = elementPhone.substring(2, elementPhone.length);            
            temp = "(+" + lada + ")" + phone.replace(/(\d{4})(\d{4})/, '$1-$2');            	
        }
        
        event.getSource().set("v.value", temp);        
	},
    
    formatNumber : function(component, event){
        var elementPhone = event.getSource().get("v.value");
        var temp = elementPhone ? elementPhone.replace(/\D+/g, '') : '';        
        event.getSource().set("v.value", temp);
    },
    
    saveFunc : function(component, event)
    {
        // Save list of answers
        var listAnswers = component.get("v.answers");
        console.log("listAnswers" , listAnswers);
        
        var saveAnswerAction = component.get("c.saveAnswers");
        saveAnswerAction.setParams({"listAnswers":  JSON.stringify(listAnswers)});
        
        saveAnswerAction.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                var reultList = response.getReturnValue()
                component.set("v.answers", reultList.listAnswer);                
                this.showSuccessToast($A.get("$Label.c.CS_Pregunta_Exitosa"), $A.get("CS_Questions_Guide_saved"));
                component.set("v.saveVisible", true);
                window.setTimeout($A.getCallback(function() { component.set("v.saveVisible", false); }), 1000);
            }
            else 
            {
                console.log("Failed with state: " , state);
                console.log(response.getReturnValue());
            } 
        });
        
        $A.enqueueAction(saveAnswerAction);
    },
    
    collapseFunc : function(component,event,secId) 
    {
	    var acc = component.find(secId);
        for(var cmp in acc) 
        {
            $A.util.toggleClass(acc[cmp], 'slds-show');  
        	$A.util.toggleClass(acc[cmp], 'slds-hide');  
        }
    },
    
    showSuccessToast : function(titleToast, messageToast) 
    {
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            console.log("toastEvent", toastEvent);
            toastEvent.setParams({
                title : titleToast,
                message: messageToast,
                messageTemplate: 'Record {0} created! See it {1}!',
                duration:' 3000',
                key: 'info_alt',
                type: 'success',
                mode: 'pester'
            });
            toastEvent.fire();
        }        
    }
})