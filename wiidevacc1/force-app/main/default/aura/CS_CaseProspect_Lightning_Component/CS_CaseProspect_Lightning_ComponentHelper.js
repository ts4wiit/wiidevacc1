({
	sendCaseMuleSoft : function(cmp, idCase) 
    {
		var action = cmp.get('c.SendCaseProspect');
        action.setParams({IdCase:idCase});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                var estatusProceso = response.getReturnValue();
                cmp.set("v.estatusProceso", estatusProceso);
                
                if(estatusProceso)
                {
                    cmp.set("v.mensajeEnvio", $A.get("$Label.c.CS_Mensaje_Exito_Envio"));
                    cmp.set("v.buttonDisabled", true);
                }
                else
                {
                    cmp.set("v.mensajeEnvio", $A.get("$Label.c.CS_Mensaje_Error_Envio"));
                    cmp.set("v.buttonDisabled", false);
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
            cmp.set("v.showSpinner", false);
        });
        $A.enqueueAction(action);
	},
    
    getFlagMuleSoft : function(cmp)
    {
        var action = cmp.get("c.SendProspectMdWeb");
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                cmp.set("v.sendMuleSoft", response.getReturnValue());
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})