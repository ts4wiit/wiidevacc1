({
	doInit : function(cmp, event, helper) 
    {
	  var mensajeMDWeb = cmp.get('v.mensajeMDWeb');
      if(mensajeMDWeb == null || mensajeMDWeb == '')
          cmp.set("v.mensajeMDWeb", $A.get("$Label.c.CS_Mensaje_Error_Prospecto"));
      helper.getFlagMuleSoft(cmp);
	},
    
    clickButtonSend : function(cmp, event, helper)
    {
        cmp.set("v.showSpinner", true);
        var IdCase = cmp.get("v.RecordId");
        helper.sendCaseMuleSoft(cmp, IdCase);
    }
})