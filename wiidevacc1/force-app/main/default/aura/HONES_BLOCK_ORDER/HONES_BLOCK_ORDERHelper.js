({
    serchRelatedOrders : function(cmp,recordId,event) {
        var action = cmp.get("c.hasRelatedOrders");
        console.log('serchRelatedOrders::recordId::' + recordId);
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                console.log("serchRelatedOrders:conts: " + conts);
                
                if(conts){
                    console.log("serchRelatedOrders:message");
                    this.setMessagge(cmp, event, "CALL HAS MORE RELATED ORDERS", "warning");
                }
            }
            else if (state === "ERROR") { 
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("serchRelatedOrders:Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("serchRelatedOrders:Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },

    sendOrderToBlock: function(cmp,recordId,event){
        var action = cmp.get("c.blockOrder");
        action.setParams({ recordId : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                console.log("sendOrderToBlock:conts: " + conts);
                console.log(conts);
                console.log("sendOrderToBlock:status: " + conts.status);
                console.log("sendOrderToBlock:success: " + conts.success);
                if(conts.status == 'OK' && conts.success){
                    cmp.set('v.sendSAPSucces', true);
                    var fields = event.getParam('fields');
                    cmp.find('blockEditForm').submit(fields);
                    if(conts.status == 'OK'){
                        this.setMessagge(cmp, event, "SEND OK TO SAP " + conts.message, "confirm");
                    }else{
                        this.setMessagge(cmp, event, "SEND OK TO SAP " + conts.message, "warning");
                    }

                }else if(conts.status == 'OK' && !conts.success){
                    this.setMessagge(cmp, event, "SEND OK TO SAP BUT " + conts.message, "warning");
                }else{
                    this.setMessagge(cmp, event, "SEND ERROR TO SAP " + conts.message, "error");
                }
            }
            else if (state === "ERROR") { 
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("sendOrderToBlock:Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("sendOrderToBlock:Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    },

    setMessagge : function(cmp,event,message, severity){
        cmp.set("v.message", message);
        cmp.set("v.severity", severity);
        $A.get('e.force:refreshView').fire();
    }

})