({
	doInit : function(component, event, helper) {
        var idCase = component.get("v.recordId");
        helper.GetTypificationCase(component, idCase);
        helper.getTypificationN1(component);
	},
    handleChangeTypificationN1: function(component, event, helper)
    {
        var TypificationN1 = component.find("SelectTypificationN1").get("v.value");
        component.set("v.TypificationN1", TypificationN1)
        
        if(typeof TypificationN1 != undefined && TypificationN1)
        {
            helper.getTypificationN2(component, TypificationN1)
        }
        
        component.set("v.ShowlistN2", 'False');
        component.set("v.ShowlistN3", 'False');
        component.set("v.ShowlistN4", 'False'); 
        
        component.set("v.TypificationN2","");
        component.set("v.TypificationN3","");
        component.set("v.TypificationN4","");
    },
    
    handleChangeTypificationN2: function(component, event, helper)
    {
        var TypificationN2 = component.find("SelectTypificationN2").get("v.value");
        component.set("v.TypificationN2", TypificationN2);
        
        var TypificationN1 = component.get("v.TypificationN1");
        if(typeof TypificationN2 != undefined && TypificationN2)
        {
            helper.getTypificationN3(component, TypificationN1, TypificationN2);
        }
        component.set("v.ShowlistN3", 'False');
        component.set("v.ShowlistN4", 'False');
        
        component.set("v.TypificationN3","");
        component.set("v.TypificationN4","");
    },
    
    handleChangeTypificationN3: function(component, event, helper)
    {
        var TypificationN3 = component.find("SelectTypificationN3").get("v.value");
        component.set("v.TypificationN3", TypificationN3);
        
        var TypificationN1 = component.get("v.TypificationN1");
        var TypificationN2 = component.get("v.TypificationN2");
        
        if(typeof TypificationN3 != undefined && TypificationN3)
        {
            helper.getTypificationN4(component, TypificationN1, TypificationN2, TypificationN3);
        }
        component.set("v.ShowlistN4", 'False');
        component.set("v.TypificationN4","");
    },
    
    handleChangeTypificationN4: function(component, event, helper)
    {
        var TypificationN3 = component.find("SelectTypificationN4").get("v.value");
        component.set("v.TypificationN4", TypificationN3);
    },
    
    handleClickButtonAceptar: function(component, event, helper)
    {
        component.set("v.showSpinner", true);       
        var inputValid = true;
        var TypificacionN1; 
        var TypificacionN2;
        var TypificacionN3;
        var TypificacionN4;
        var DescripcionCaso;
        var tip1Current = component.get("v.DetailCase.ISSM_TypificationLevel1__c");
        var tip2Current = component.get("v.DetailCase.ISSM_TypificationLevel2__c");
        var tip3Current = component.get("v.DetailCase.ISSM_TypificationLevel3__c");
        var tip4Current = component.get("v.DetailCase.ISSM_TypificationLevel4__c");
        var mensajesError = [];
    
        /*Validando que se haya seleccionado un valor en el primer nivel de tipificacion*/
        TypificacionN1 = component.get("v.TypificationN1");
        if(typeof TypificacionN1 === undefined || TypificacionN1 === '')
        {
            inputValid = false;
            mensajesError.push($A.get("$Label.c.CS_Mensaje_T1"));
        }
        
        /* Validando que se haya seleccionado un valor en el segundo nivel de tipificacion */
        if(component.get("v.ShowlistN2") === 'True')
        {
           TypificacionN2  = component.find("SelectTypificationN2").get("v.value");
           if(typeof TypificacionN2 === undefined || TypificacionN2 === '')
           {
              inputValid = false;
              mensajesError.push($A.get("$Label.c.CS_Mensaje_T2"));
           }
        }
       
        /* Validando que se haya seleccionado un valor en el tercer nivel de tipificacion */
        if(component.get("v.ShowlistN3") === 'True')
        {
           TypificacionN3  = component.find("SelectTypificationN3").get("v.value");
           if(typeof TypificacionN3 === undefined || TypificacionN3 === '')
           {
              inputValid = false;
              mensajesError.push($A.get("$Label.c.CS_Mensaje_T3"));
           }
        }
        
        /* Validando que se haya seleccionado el cuarto nivel de tipificacion */
        if(component.get("v.ShowlistN4") === 'True')
        {
           TypificacionN4  = component.find("SelectTypificationN4").get("v.value");
           if(typeof TypificacionN4 === undefined || TypificacionN4 === '')
           {
              inputValid = false;
              mensajesError.push($A.get("$Label.c.CS_Mensaje_T4"));
           }
        }
        
        var currentTyp = tip1Current.concat("|",tip2Current,"|",tip3Current,"|",tip4Current);             
        var NewTyp = TypificacionN1.concat("|",TypificacionN2,"|",TypificacionN3,"|",TypificacionN4);
        console.log(currentTyp);
        console.log(NewTyp);
        
        if(currentTyp === NewTyp){
             inputValid = false;
             mensajesError.push($A.get("$Label.c.CS_Same_Typification"));
        }        
        
        if(inputValid)
        {            
           helper.UpdateTypification(component);
        }
        else
        {
            component.set("v.tituloModal", $A.get("$Label.c.CS_Titulo_Campos_Obligatorios"));
            component.set("v.mensajesValidacion", mensajesError);
            component.set("v.showModal", !inputValid); 
            component.set("v.showSpinner", false);
        }
    },    
    
    handleClickButtonCloseModal: function(component, event, helper)
    {
        
        component.set("v.showModal", false);
    }
    
})