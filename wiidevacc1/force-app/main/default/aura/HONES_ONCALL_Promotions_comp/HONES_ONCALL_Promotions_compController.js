({
    doInit : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        helper.getDataMap2(component,recordId);
        helper.getResponseDeals(component,recordId);
        helper.getAccountData(component,recordId);
    },
    
    onCheck: function(component, event, helper) {
        
        var booltosend =  component.get("v.booltosend");
        var capturedCheckboxName = event.getSource().get("v.name");
        var capturedCheckboxdesc = event.getSource().get("v.text");  
        var promotosend = component.get("v.promotosend"); 
        var cmpSearchOrders = component.find("cmpHONESONCALLSearchOrderscomp");
        cmpSearchOrders.set("v.boton",false);
        if(event.getSource().get("v.value") == true){
        	var promObject = {
            	promoNamedescription:capturedCheckboxName,
            	promoDescTosend:capturedCheckboxdesc
        	};
        
        	promotosend.push(promObject); 
        }else{
            promotosend.forEach(function(element) {
                if(element.promoNamedescription == event.getSource().get("v.name")){
                    var index =  promotosend.indexOf(element);
                    
                    promotosend = promotosend.filter(function(item){
                        return item !== element;
                    });
                }
               
            });
        }
        
        if(promotosend.length == 0){
            component.set("v.tablePromotion", false);
        }
        component.set("v.promotosend",promotosend);
        component.set("v.changebox",false);
        if(promotosend.length > 0){
            component.set("v.tablePromotion", true);
            var recordId = component.get("v.recordId");
        }
    }
    
    
})