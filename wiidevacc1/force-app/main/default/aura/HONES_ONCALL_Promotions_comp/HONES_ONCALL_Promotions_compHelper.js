({
    getDataMap2: function(cmp, recordId) {
        var action = cmp.get("c.getpromos");
        action.setParams({ recordId: recordId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.promo", conts);
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    getData: function(cmp, recordId) {
        var action = cmp.get("c.getpromos");
        action.setParams({ recordId: recordId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                var conts = response.getReturnValue();
                cmp.set("v.promotosend", conts);
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getResponse: function(component) {
        var action = component.get("c.getCalloutResponseContents");
        action.setParams({
            url: "https://abidev2.herokuapp.com/api/getResponseDeals"
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            alert(state);
            if (component.isValid() && state === "SUCCESS") {
                //console.log('res---->' + JSON.stringify(response.getReturnValue()));
                var x = response.getReturnValue();
                component.set("v.response", x);
            }
        });
        $A.enqueueAction(action);
    },
    
    getResponseDeals: function(component, recordId) {
        var action = component.get("c.genOrder");
        action.setParams({ recordId: recordId });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                console.log("responseDeals---->" + JSON.stringify(response.getReturnValue()));
                var x = response.getReturnValue();
                component.set("v.responseDeals", x);
                //component.set("v.soRespDeals", x);
            }
        });
        $A.enqueueAction(action);
    }
});