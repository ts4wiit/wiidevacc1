({
    IsOpenBasicSection : function(cmp, event, helper) {
        
        var sectionOneIsClosed = cmp.get('v.sectionOneIsClosed');
		var sectionOne = cmp.find('sectionOne');
		var sectionOneS1 = cmp.find('sectionOne-switch1');
        var sectionOneS2 = cmp.find('sectionOne-switch2');
        
        if(!sectionOneIsClosed){
            cmp.set('v.sectionOneIsClosed',true);
            $A.util.removeClass(sectionOne, 'slds-is-open');
            $A.util.addClass(sectionOneS1, 'toggle');
            $A.util.removeClass(sectionOneS2, 'toggle');

        }else{  
            cmp.set('v.sectionOneIsClosed',false);
            $A.util.addClass(sectionOne, 'slds-is-open');
            $A.util.removeClass(sectionOneS1, 'toggle');
            $A.util.addClass(sectionOneS2, 'toggle');
        }
    },
    
    IsOpenVisitSection : function(cmp, event, helper) {
        
        var sectionTwoIsClosed = cmp.get('v.sectionTwoIsClosed');
		var sectionTwo = cmp.find('sectionTwo');
		var sectionTwoS1 = cmp.find('sectionTwo-switch1');
        var sectionTwoS2 = cmp.find('sectionTwo-switch2');
        
        if(!sectionTwoIsClosed){
            cmp.set('v.sectionTwoIsClosed',true);
            $A.util.removeClass(sectionTwo, 'slds-is-open');
            $A.util.addClass(sectionTwoS1, 'toggle');
            $A.util.removeClass(sectionTwoS2, 'toggle');

        }else{  
            cmp.set('v.sectionTwoIsClosed',false);
            $A.util.addClass(sectionTwo, 'slds-is-open');
            $A.util.removeClass(sectionTwoS1, 'toggle');
            $A.util.addClass(sectionTwoS2, 'toggle');
        }
	},
    
    IsOpenClasifSection : function(cmp, event, helper) {
        
        var sectionThreeIsClosed = cmp.get('v.sectionThreeIsClosed');
		var sectionThree = cmp.find('sectionThree');
		var sectionThreeS1 = cmp.find('sectionThree-switch1');
        var sectionThreeS2 = cmp.find('sectionThree-switch2');
        
        if(!sectionThreeIsClosed){
            cmp.set('v.sectionThreeIsClosed',true);
            $A.util.removeClass(sectionThree, 'slds-is-open');
            $A.util.addClass(sectionThreeS1, 'toggle');
            $A.util.removeClass(sectionThreeS2, 'toggle');

        }else{  
            cmp.set('v.sectionThreeIsClosed',false);
            $A.util.addClass(sectionThree, 'slds-is-open');
            $A.util.removeClass(sectionThreeS1, 'toggle');
            $A.util.addClass(sectionThreeS2, 'toggle');
        }
	},
    
    IsOpenStructureSection : function(cmp, event, helper) {
        
        var sectionFourIsClosed = cmp.get('v.sectionFourIsClosed');
		var sectionFour = cmp.find('sectionFour');
		var sectionFourS1 = cmp.find('sectionFour-switch1');
        var sectionFourS2 = cmp.find('sectionFour-switch2');
        
        if(!sectionFourIsClosed){
            cmp.set('v.sectionFourIsClosed',true);
            $A.util.removeClass(sectionFour, 'slds-is-open');
            $A.util.addClass(sectionFourS1, 'toggle');
            $A.util.removeClass(sectionFourS2, 'toggle');

        }else{  
            cmp.set('v.sectionFourIsClosed',false);
            $A.util.addClass(sectionFour, 'slds-is-open');
            $A.util.removeClass(sectionFourS1, 'toggle');
            $A.util.addClass(sectionFourS2, 'toggle');
        }
    },
    
    IsOpenCreditSection : function(cmp, event, helper) {
        
        var sectionFiveIsClosed = cmp.get('v.sectionFiveIsClosed');
		var sectionFive = cmp.find('sectionFive');
		var sectionFiveS1 = cmp.find('sectionFive-switch1');
        var sectionFiveS2 = cmp.find('sectionFive-switch2');
        
        if(!sectionFiveIsClosed){
            cmp.set('v.sectionFiveIsClosed',true);
            $A.util.removeClass(sectionFive, 'slds-is-open');
            $A.util.addClass(sectionFiveS1, 'toggle');
            $A.util.removeClass(sectionFiveS2, 'toggle');

        }else{  
            cmp.set('v.sectionFiveIsClosed',false);
            $A.util.addClass(sectionFive, 'slds-is-open');
            $A.util.removeClass(sectionFiveS1, 'toggle');
            $A.util.addClass(sectionFiveS2, 'toggle');
        }
	},
    
    IsOpenKPIifSection : function(cmp, event, helper) {
        
        var sectionSixIsClosed = cmp.get('v.sectionSixIsClosed');
		var sectionSix = cmp.find('sectionSix');
		var sectionSixS1 = cmp.find('sectionSix-switch1');
        var sectionSixS2 = cmp.find('sectionSix-switch2');
        
        if(!sectionSixIsClosed){
            cmp.set('v.sectionSixIsClosed',true);
            $A.util.removeClass(sectionSix, 'slds-is-open');
            $A.util.addClass(sectionSixS1, 'toggle');
            $A.util.removeClass(sectionSixS2, 'toggle');

        }else{  
            cmp.set('v.sectionSixIsClosed',false);
            $A.util.addClass(sectionSix, 'slds-is-open');
            $A.util.removeClass(sectionSixS1, 'toggle');
            $A.util.addClass(sectionSixS2, 'toggle');
        }
	}
})