({
	GetDataCase: function(cmp, pIdCase)
    {
        var oDetailcase;
        var action = cmp.get("c.GetCaseInfo");
        action.setParams({idCase: pIdCase});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                oDetailcase = response.getReturnValue(); 
                if(oDetailcase.ISSM_TypificationLevel4__c == null || oDetailcase.ISSM_TypificationLevel4__c == '')
                {
                    if(oDetailcase.ISSM_TypificationLevel3__c != null &&  oDetailcase.ISSM_TypificationLevel3__c != '')
                        oDetailcase.ISSM_TypificationLevel4__c = oDetailcase.ISSM_TypificationLevel3__c;
                    else if (oDetailcase.ISSM_TypificationLevel2__c != null &&  oDetailcase.ISSM_TypificationLevel2__c != '')
                        oDetailcase.ISSM_TypificationLevel4__c = oDetailcase.ISSM_TypificationLevel2__c;
                    else if (oDetailcase.ISSM_TypificationLevel1__c != null &&  oDetailcase.ISSM_TypificationLevel1__c != '')
                        oDetailcase.ISSM_TypificationLevel4__c = oDetailcase.ISSM_TypificationLevel1__c;
                }
                cmp.set("v.DetailCase", oDetailcase);
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    GetAccountName: function(cmp, pIdCase)
    {
        var action = cmp.get("c.GetDetailAccount");
        action.setParams({idCase: pIdCase});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                cmp.set("v.AccountName", response.getReturnValue());
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    GetAssignedIntelocutorOption: function(cmp)
    {
        var action = cmp.get("c.GetCaseAssigned");
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                cmp.set("v.lstAssignedIntelocutor", response.getReturnValue());
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    GetInterlocutorDetail: function(cmp, pAssignedTo, pIdCase)
    {
        var action = cmp.get("c.GetInterlocutorName");
        action.setParams({assignedTo: pAssignedTo, idCase: pIdCase });
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                var oUser = response.getReturnValue();
                if(typeof oUser.Id === undefined || oUser.Id === '' || oUser.Id === undefined)
                {
                    cmp.set("v.tituloModal", $A.get("$Label.c.CS_Titulo_Error"));
                    cmp.set("v.mensajesValidacion", $A.get("$Label.c.CS_Error_Interlocutor") + " " + pAssignedTo);
                    cmp.set("v.showModal", true);  
                }
                cmp.set("v.DetailIntelocutor", oUser);
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    UpdateCaseOwner: function(cmp, pIdCase, pIdCaseForce, pIdOwner, pAssignedType)
    {
        var action = cmp.get("c.UpdateOwnerCase");
        action.setParams({ idCase : pIdCase, idCaseForce : pIdCaseForce, idOwner : pIdOwner, assignedTo : pAssignedType });
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                var statusProcess = response.getReturnValue();
                if(statusProcess)
                {
                    cmp.set("v.showModal", false);
                    cmp.set("v.showSpinner", false);
                    sforce.one.navigateToURL('/' + pIdCase, true);
                }
                else
                {
                    cmp.set("v.tituloModal", $A.get("$Label.c.CS_Titulo_Error"));
                    cmp.set("v.mensajesValidacion", $A.get("$Label.c.CS_Error_General_Asignacion"));
                    cmp.set("v.showModal", true);
                    cmp.set("v.showSpinner", false);
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    }
})