({
    getTypificationN1: function(cmp)
    {
        var action = cmp.get("c.GetTypificationN1");
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                cmp.set("v.listN1", response.getReturnValue());
                cmp.set("v.ShowlistN1", 'True');
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action);
    },
    
    getTypificationN2: function(cmp, typificationN1)
    {
        var action = cmp.get("c.GetTypificationN2");
        action.setParams({typificationNivel1: typificationN1});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                cmp.set("v.listN2", response.getReturnValue());
                if(response.getReturnValue().length > 1)
                {
                    cmp.set("v.ShowlistN2", 'True');
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getTypificationN3: function(cmp, typificationN1, typificationN2)
    {
        var action = cmp.get("c.GetTypificationN3");
        action.setParams({typificationNivel1: typificationN1, typificationNivel2: typificationN2});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                cmp.set("v.listN3", response.getReturnValue());
                if(response.getReturnValue().length > 1)
                {
                    cmp.set("v.ShowlistN3", 'True');
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getTypificationN4: function(cmp, typificationN1, typificationN2, typificationN3)
    {
        var action = cmp.get("c.GetTypificationN4");
        action.setParams({typificationNivel1:typificationN1, typificationNivel2:typificationN2, typificationNivel3:typificationN3});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                cmp.set("v.listN4", response.getReturnValue());
                if(response.getReturnValue().length > 1)
                {
                    cmp.set("v.ShowlistN4", 'True');   
                }
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    getPickListValues : function(cmp) 
    {
        var action = cmp.get("c.getPickListValuesIntoList");        
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                var conts = response.getReturnValue();
                cmp.set("v.questionTypes", conts);              
            }
            else if (state === "ERROR") 
            {
                var errors = response.getError();
                if (errors) 
                {
                    if (errors[0] && errors[0].message) 
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else 
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);		
	},
    
    createQuestion: function(cmp)
    {
        var typificationNivel1 = cmp.get("v.TypificationN1"); 
        var typificationNivel2 = cmp.get("v.TypificationN2");
        var typificationNivel3 = cmp.get("v.TypificationN3");
        var typificationNivel4 = cmp.get("v.TypificationN4");
        var question = cmp.find("textQuestion").get("v.value");
        var qType = cmp.get("v.selectedValue");
        
        var options = cmp.find("textAreaOptions");
        if(typeof options != undefined && options != null)
            var qOptions = options.get("v.value");
        
        var action = cmp.get("c.createNewQuestion");
        action.setParams({typificationN1:typificationNivel1, typificationN2:typificationNivel2, typificationN3:typificationNivel3, typificationN4:typificationNivel4, 
                          question:question, qType:qType, qOptions:qOptions});
        action.setCallback(this, function(response)
        {
            var state = response.getState();
            if(state === "SUCCESS")
            {
                var status = response.getReturnValue();
                if(status)
                {
                    options = cmp.find("textAreaOptions")
                    var question = cmp.find("textQuestion");
                    var questionType = cmp.find("selectQuestionType");
                    
                    if(typeof options != undefined && options != null)
                        options.set("v.value", "");
                    
                    question.set("v.value", "");
                    questionType.set("v.value", "");
                    
                    cmp.set("v.selectedValue", "");
                    cmp.set("v.showOptions", false);
                    cmp.set("v.tituloModal", $A.get("$Label.c.CS_Pregunta_Exitosa"));
                    cmp.set("v.mensajesValidacion", $A.get("$Label.c.CS_Mensaje_Exito"));
                }
                else
                {
                     cmp.set("v.tituloModal", $A.get("$Label.c.CS_Titulo_Error"));
                     cmp.set("v.mensajesValidacion", $A.get("$Label.c.CS_Error_Alta_Pregunta"));
                }
                cmp.set("v.showModal", true); 
                cmp.set("v.showSpinner", false); 
            }
            else if(state === "ERROR")
            {
                var errors = response.getError();
                if (errors)
                {
                    if (errors[0] && errors[0].message)
                    {
                        console.log("Error message: " + errors[0].message);
                    }
                } 
                else
                {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);   
    }
})