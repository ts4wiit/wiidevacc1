global class IREP_SharingRecordsWithCarrier_bch implements Database.Batchable<sObject> {
    
    String strQuery;
    
    global IREP_SharingRecordsWithCarrier_bch() {
        strQuery =  'Select Id, IREP_Carrier__c ';
        strQuery += 'From ONTAP__Tour__c ';
        //strQuery += 'Where ONTAP__TourDate__c = TODAY ';
        //strQuery += 'And IREP_IsSharing__c = false';
        strQuery += 'WHERE IREP_IsSharing__c = false';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        set<Id> setTourId = new set<Id>();
        set<Id> setUserId = new set<Id>();
        map<Id, ONTAP__Tour__c> mapTour = new map<Id, ONTAP__Tour__c>();
        list<ONTAP__Tour__Share> lstShareTours = new list<ONTAP__Tour__Share>();

        for(ONTAP__Tour__c objTour : (list<ONTAP__Tour__c>)scope){
            if(objTour.IREP_Carrier__c != null){
                setTourId.add(objTour.Id);
                setUserId.add(objTour.IREP_Carrier__c);
    
                objTour.IREP_IsSharing__c = true;
                mapTour.put(objTour.Id, objTour);
            }
        }

        map<Id, User> mapUser = new map<Id, User>([
            Select  Id, IsActive, UserRoleId
            From    User
            Where   Id = :setUserId
        ]);

        shareTourRecords(mapTour, mapUser);
        set<Id> setTourVisitIds = shareTourVisitRecords(mapTour.keySet(), mapUser);
        set<Id> setOrderIds = shareOrderRecords(setTourVisitIds, mapUser);
        shareOrderItemRecords(setOrderIds, mapUser);
        
        if(!mapTour.isEmpty())
            update mapTour.values();
    }
    

    global void finish(Database.BatchableContext BC) {
        programBatch();
    }

    private void shareTourRecords(map<Id, ONTAP__Tour__c> mapTours, map<Id, User> mapUsers){
        String strSharingReason = Schema.ONTAP__Tour__Share.rowCause.ByCarrier__c;
        list<ONTAP__Tour__Share> lstObjShare = new list<ONTAP__Tour__Share>();

        for(ONTAP__Tour__c objTour : mapTours.values()){
            User objTmpUser = mapUsers.get(objTour.IREP_Carrier__c);

            System.debug('\nobjTour.IREP_Carrier__c===>'+objTour.IREP_Carrier__c);
            System.debug('\nUsuario===>'+objTmpUser);

            if(objTour.IREP_Carrier__c != null && objTmpUser!= null && objTmpUser.IsActive && objTmpUser.UserRoleId != null)
                lstObjShare.add(createShareTour(objTmpUser.Id, objTour.Id, strSharingReason));
        }

        if(!lstObjShare.isEmpty()){
            insert lstObjShare;
        }
    }

    private ONTAP__Tour__Share createShareTour(Id userId, Id tourId, String strSharingReason){
        ONTAP__Tour__Share objShare = new ONTAP__Tour__Share();
        objShare.AccessLevel = 'Read';
        objShare.ParentId = tourId;
        objShare.RowCause = strSharingReason;
        objShare.UserOrGroupId = userId;
        return objShare;
    }

    private set<Id> shareTourVisitRecords(set<Id> setIdTours, map<Id, User> mapUsers){
        String strSharingReason = Schema.ONTAP__Tour_Visit__Share.rowCause.ByCarrier__c;
        set<Id> setTourVisitIds = new set<Id>();

        list<ONTAP__Tour_Visit__Share> lstObjShare = new list<ONTAP__Tour_Visit__Share>();

        for(ONTAP__Tour_Visit__c objTourVisit : [   Select Id, ONTAP__Tour__c, ONTAP__Tour__r.IREP_Carrier__c, IREP_IsSharing__c
                                                    From ONTAP__Tour_Visit__c
                                                    Where ONTAP__Tour__c = :setIdTours
                                                ]){
            User objTmpUser = mapUsers.get(objTourVisit.ONTAP__Tour__r.IREP_Carrier__c);

            if(objTourVisit.ONTAP__Tour__r.IREP_Carrier__c != null && objTmpUser!= null && objTmpUser.IsActive && objTmpUser.UserRoleId != null){
                lstObjShare.add(createShareTourVisit(objTmpUser.Id, objTourVisit.Id, strSharingReason));
                setTourVisitIds.add(objTourVisit.Id);
            }

        }

        if(!lstObjShare.isEmpty()){
            insert lstObjShare;
        }

        return setTourVisitIds;
    }

    private ONTAP__Tour_Visit__Share createShareTourVisit(Id userId, Id tourVisitId, String strSharingReason){
        ONTAP__Tour_Visit__Share objShare = new ONTAP__Tour_Visit__Share();
        objShare.AccessLevel = 'Read';
        objShare.ParentId = tourVisitId;
        objShare.RowCause = strSharingReason;
        objShare.UserOrGroupId = userId;
        return objShare;
    }

    private set<Id> shareOrderRecords(set<Id> setTourVisitIds, map<Id, User> mapUsers){
        String strSharingReason = Schema.ONTAP__Order__Share.rowCause.ByCarrier__c;
        list<ONTAP__Order__Share> lstObjShare = new list<ONTAP__Order__Share>();
        set<Id> setOrderIds = new set<Id>();

        for(ONTAP__Order__c objOrder : [    Select Id, ONTAP__TourVisit__c, ONTAP__TourVisit__r.ONTAP__Tour__c, ONTAP__TourVisit__r.ONTAP__Tour__r.IREP_Carrier__c, IREP_IsSharing__c
                                            From ONTAP__Order__c
                                            Where ONTAP__TourVisit__c = :setTourVisitIds
                                        ]){
            User objTmpUser = mapUsers.get(objOrder.ONTAP__TourVisit__r.ONTAP__Tour__r.IREP_Carrier__c);

            if(objOrder.ONTAP__TourVisit__r.ONTAP__Tour__r.IREP_Carrier__c != null && objTmpUser!= null && objTmpUser.IsActive && objTmpUser.UserRoleId != null){
                lstObjShare.add(createShareOrder(objTmpUser.Id, objOrder.Id, strSharingReason));
                setOrderIds.add(objOrder.Id);
            }

        }

        if(!lstObjShare.isEmpty()){
            insert lstObjShare;
        }

        return setOrderIds;
    }

    private ONTAP__Order__Share createShareOrder(Id userId, Id orderId, String strSharingReason){
        ONTAP__Order__Share objShare = new ONTAP__Order__Share();
        objShare.AccessLevel = 'Read';
        objShare.ParentId = orderId;
        objShare.RowCause = strSharingReason;
        objShare.UserOrGroupId = userId;
        return objShare;
    }

    private set<Id> shareOrderItemRecords(set<Id> setOrderIds, map<Id, User> mapUsers){
        String strSharingReason = Schema.ONTAP__Order_Item__Share.rowCause.ByCarrier__c;
        list<ONTAP__Order_Item__Share> lstObjShare = new list<ONTAP__Order_Item__Share>();

        for(ONTAP__Order_Item__c objOrderItem : [   Select Id, ONTAP__CustomerOrder__c, ONTAP__CustomerOrder__r.ONTAP__TourVisit__r.ONTAP__Tour__r.IREP_Carrier__c, IREP_IsSharing__c
                                            From ONTAP__Order_Item__c
                                            Where ONTAP__CustomerOrder__c = :setOrderIds
                                        ]){
            User objTmpUser = mapUsers.get(objOrderItem.ONTAP__CustomerOrder__r.ONTAP__TourVisit__r.ONTAP__Tour__r.IREP_Carrier__c);

            if(objOrderItem.ONTAP__CustomerOrder__r.ONTAP__TourVisit__r.ONTAP__Tour__r.IREP_Carrier__c != null && objTmpUser!= null && objTmpUser.IsActive && objTmpUser.UserRoleId != null){
                lstObjShare.add(createShareOrderItem(objTmpUser.Id, objOrderItem.Id, strSharingReason));
            }

        }

        if(!lstObjShare.isEmpty()){
            insert lstObjShare;
        }

        return setOrderIds;
    }

    private ONTAP__Order_Item__Share createShareOrderItem(Id userId, Id orderItemId, String strSharingReason){
        ONTAP__Order_Item__Share objShare = new ONTAP__Order_Item__Share();
        objShare.AccessLevel = 'Read';
        objShare.ParentId = orderItemId;
        objShare.RowCause = strSharingReason;
        objShare.UserOrGroupId = userId;
        return objShare;
    }

    /**
        * @description Method to automatically program the batch
    **/
    public void programBatch(){

        IREP_BatchControlExecution__mdt conf = [
            Select Id, IREP_EndHour__c, IREP_Interval__c, IREP_StartHour__c, IREP_Active__c
            From IREP_BatchControlExecution__mdt
            Where Label = 'IREP_SharingRecordsWithCarrier_bch'
            Limit 1
        ];

        Datetime dtHoraActual       =   System.now();
        Integer intInterval         =   Integer.valueOf(conf.IREP_Interval__c);
        Integer intHoraIni          =   Integer.valueOf(conf.IREP_StartHour__c);
        Integer intHoraFin          =   Integer.valueOf(conf.IREP_EndHour__c);
        Datetime dtNextExecution    =   dtHoraActual.addMinutes(intInterval);
        
        System.debug('\nInterval=>'+intInterval);
        System.debug('\nintHoraIni=>'+intHoraIni);
        System.debug('\nintHoraFin=>'+intHoraFin);
        System.debug('\ndtNextExecution=>'+dtNextExecution.hour());
        System.debug('\ndtNextExecutionGMT=>'+dtNextExecution.hourGmt());

        if(conf.IREP_Active__c && dtHoraActual.hour() >= intHoraIni && dtHoraActual.hour() <= intHoraFin){
        
            String strTime  =   dtNextExecution.second() +' ';
            strTime         +=  dtNextExecution.minute() +' ';
            strTime         +=  dtNextExecution.hour() +' ';
            strTime         +=  dtNextExecution.day() +' ';
            strTime         +=  dtNextExecution.month()+' ';
            strTime         +=  '? ';
            strTime         +=  dtNextExecution.year()+' ';

            String strSharingRecords    =   'IREP_SharingRecordsWithCarrier-'+dtNextExecution.hour();
            
            try{
                    
                for ( CronTrigger ct : [SELECT  Id
                    FROM    CronTrigger
                    WHERE   CronJobDetail.Name =: strSharingRecords] ) {
                    System.abortJob(ct.Id);
                }
                if(!Test.isRunningTest()) String jobId = System.schedule(strSharingRecords, strTime, new IREP_SharingRecordsWithCarrier_sch());

            }catch(Exception e){
                System.debug('\n ERROR SHARING RECORDS ===========>>>> '+e.getMessage());
            }
        }
    }   
}