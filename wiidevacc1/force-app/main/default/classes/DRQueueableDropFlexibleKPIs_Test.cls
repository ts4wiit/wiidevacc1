@isTest
public class DRQueueableDropFlexibleKPIs_Test {
    
    @isTest
    public static void testDropKPIs(){
        
         Account account = DRFixtureFactory.newDRAccount();
         User bdr = DRFixtureFactory.newBDRUser( 'bdr@xyz.com.do', 'bdrtest' );
         List<Flexible_KPI__c> kpis = new List<Flexible_KPI__c>();
 
         Test.startTest(); 

         for ( Integer i = 0; i < 200; i++ ){
             
            Flexible_KPI__c flexKPI = new Flexible_KPI__c( Account__c = account.Id, Country_Code__c = 'DO', Name = 'Test ' + i, Value__c = 'Value ' + i , Route__c = bdr.User_Route__c);
            kpis.add( flexKPI );
            
         }
              
        insert kpis;
        
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();

        RestContext.request = request;
        RestContext.response = response;
        
        DRDeleteFlexibleKPIs.deleteAll();
        
        Test.stopTest();    
        
        kpis = [ SELECT Id FROM Flexible_KPI__c ];
        
        System.assertEquals(0, kpis.size(), 'Something went wrong :( assertion failed!');
        
        
    }

}