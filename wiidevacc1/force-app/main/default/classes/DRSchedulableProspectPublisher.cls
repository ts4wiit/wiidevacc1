/**
 * Created by Dejair.Junior on 9/23/2019.
 */

global class DRSchedulableProspectPublisher implements Schedulable {
    global void execute(SchedulableContext sc) {
        DRBatchableProspectPublisher batchProspects = new DRBatchableProspectPublisher();
        Id batchId = Database.executeBatch( batchProspects, 1 );
    }
}