/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: ECPE_DeleteOpenItemsEmptiesBalance_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 09/10/2019     Carlos Alvarez Sanchez       Creation of methods.
 */
@isTest
public class ECPE_DeleteOpenItemsEmptiesBalance_Test {

    @isTest 
    static void methodOne() {
    	ECPE_DeleteOpenItemsEmptiesBalanceSch sch = new ECPE_DeleteOpenItemsEmptiesBalanceSch();
    	SchedulableContext sc;
    	sch.execute(sc);
        
        Database.BatchableContext bc;
        String queryOI = 'SELECT Id FROM ONTAP__OpenItem__c WHERE RecordType.Name IN (\'IREP_OpenItem_PE\', \'IREP_OpenItem_EC\', \'IREP_OpenItem_PA\') AND IREP_Country__c IN (\'PE\', \'EC\', \'PA\') AND LastModifiedDate < LAST_N_DAYS:30';
        ECPE_DeleteOpenItemsEmptiesBalanceBatch doi = new ECPE_DeleteOpenItemsEmptiesBalanceBatch( queryOI, 'ONTAP__OpenItem__c' );
		Database.executeBatch(doi, 200);
		List<Contact> listContact = new List<Contact>();
        Id recTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('IREP_Contact_PE').getRecordTypeId();
        listContact.add( new Contact( LastName = 'Test', RecordTypeId = recTypeId));
        insert listContact;
        doi.execute( bc, listContact);
  	}
}