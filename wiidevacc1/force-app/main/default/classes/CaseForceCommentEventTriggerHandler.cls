public class CaseForceCommentEventTriggerHandler {
    
    public static final String DR_SALES_PROCESS_RECORD_TYPE = 'DO_Sales_Process';
    public static final String DR_ASSET_INVESTIGATION_RECORD_TYPE = 'DO_Asset_Investigation';
    public static final String DR_ASSET_MOVEMENT_RECORD_TYPE = 'DO_Asset_Movement';
    public static final String DR_ASSET_MAINTENANCE_RECORD_TYPE = 'DO_Asset_Maintenance';
    private List<ONTAP__Case_Force_Comment__ChangeEvent> caseForceCommentChangeEvents = new List<ONTAP__Case_Force_Comment__ChangeEvent>();    
    private List<ONTAP__Case_Force_Comment__ChangeEvent> filteredCaseForceCommentChangeEvents = new List<ONTAP__Case_Force_Comment__ChangeEvent>(); 
    private List<DRCaseCommentOutboundMessage__e> caseForceCommentEvents = new List<DRCaseCommentOutboundMessage__e>();
    private List<Id> caseCommentIds = new List<Id>();
    private DRCaseCommentSelector caseCommentSelector;
    private Map<Id,ONTAP__Case_Force_Comment__c> caseCommentsMap = new Map<Id,ONTAP__Case_Force_Comment__c>();
    private Map<Id,ONTAP__Case_Force_Comment__c> caseCommentsFilteredMap = new Map<Id,ONTAP__Case_Force_Comment__c>();
    
    private String integrationId;
    
    public CaseForceCommentEventTriggerHandler( List<ONTAP__Case_Force_Comment__ChangeEvent> caseForceCommentChangeEvents ){
        
        this.caseForceCommentChangeEvents = caseForceCommentChangeEvents;
        
    }
    
    public void run(){
        
        this.filter();
        this.setCaseComments();
        this.setSalesProcessCases();
        this.setAssetInvestigationCases();
        this.setAssetMovementCases();
        this.setAssetMantenanceCases();
        this.publish();       
        
    }
    
    private void filter(){
        
        this.integrationId = DRUserSelector.getIntegrationId();
        Id userId = UserInfo.getUserId();
        
        for( ONTAP__Case_Force_Comment__ChangeEvent caseForceCommentChangeEvent : caseForceCommentChangeEvents ){
            if ( (caseForceCommentChangeEvent.ChangeEventHeader.getChangeType() == 'CREATE' && userId != (Id) this.integrationId )  ||  Test.isRunningTest() ){            
                filteredCaseForceCommentChangeEvents.add( caseForceCommentChangeEvent );
                caseCommentIds.add( caseForceCommentChangeEvent.ChangeEventHeader.getRecordIds().get(0) );
            }
        }
    }    
    
    private void setCaseComments(){ 
        this.caseCommentSelector = new DRCaseCommentSelector( caseCommentIds );        
    }    
    
    private void setSalesProcessCases(){
        Map<Id,ONTAP__Case_Force_Comment__c> caseCommentsLocalMap = new Map<Id,ONTAP__Case_Force_Comment__c>();
        caseCommentsLocalMap = caseCommentSelector.filterByRecordTypeName( DR_SALES_PROCESS_RECORD_TYPE );
        caseCommentsFilteredMap.putAll( caseCommentsLocalMap );
    }
    
    private void setAssetInvestigationCases(){
        Map<Id,ONTAP__Case_Force_Comment__c> caseCommentsLocalMap = new Map<Id,ONTAP__Case_Force_Comment__c>();
        caseCommentsLocalMap = caseCommentSelector.filterByRecordTypeName( DR_ASSET_INVESTIGATION_RECORD_TYPE );
        caseCommentsFilteredMap.putAll( caseCommentsLocalMap );       
    }   
    
    private void setAssetMovementCases(){
        Map<Id,ONTAP__Case_Force_Comment__c> caseCommentsLocalMap = new Map<Id,ONTAP__Case_Force_Comment__c>();
        caseCommentsLocalMap = caseCommentSelector.filterByRecordTypeName( DR_ASSET_MOVEMENT_RECORD_TYPE );
        caseCommentsFilteredMap.putAll( caseCommentsLocalMap );        
    }       
    
    private void setAssetMantenanceCases(){
        Map<Id,ONTAP__Case_Force_Comment__c> caseCommentsLocalMap = new Map<Id,ONTAP__Case_Force_Comment__c>();
        caseCommentsLocalMap = caseCommentSelector.filterByRecordTypeName( DR_ASSET_MAINTENANCE_RECORD_TYPE );
        caseCommentsFilteredMap.putAll( caseCommentsLocalMap );        
    }     

    private void mapCaseComments(){
        
        for ( Id caseCommentId : caseCommentsFilteredMap.keySet() ){
            
            ONTAP__Case_Force_Comment__c caseComment = caseCommentsFilteredMap.get( caseCommentId );
            
            DRCaseCommentOutboundMessage__e outboundMessage = new DRCaseCommentOutboundMessage__e(
            
                CaseForceId__c = caseComment.ONTAP__Case_Force__c,
                CaseForceRecordName__c = caseComment.Name,
                Comments__c = caseComment.ONTAP__Comment__c,
                CaseForceCommentId__c = caseComment.Id
 
            );           
            
        	caseForceCommentEvents.add( outboundMessage ); 
        }    
        
    }

    private void publish(){
        this.mapCaseComments();
        List<Database.SaveResult> caseCommentResults = EventBus.publish( caseForceCommentEvents );
    }
    
}