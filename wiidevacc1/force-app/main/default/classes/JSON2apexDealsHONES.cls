/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: JSON2apexDeals.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 07/01/2019           Heron Zurita           Creation of methods.
*/
public class JSON2apexDealsHONES {
    /**
    * Methods class and method for model JSON2apexDeals
    * Created By: heron.zurita@accenture.com
    * Modified By: rjimenez@ts4.mx
    * @param void
    * @return void
    */
    public String accountId;
    public String salesOrg;
    public List<Deals> deals;
    
    public class Deals {
        public String pronr;
        public Boolean proportional;
        public Boolean optional;
        public String validfrom;
        public String validto;
        public String description;
    }
    
    
    public static JSON2apexDealsHONES parse(String json) {
        return (JSON2apexDealsHONES) System.JSON.deserialize(json, JSON2apexDealsHONES.class);
    }
    
}