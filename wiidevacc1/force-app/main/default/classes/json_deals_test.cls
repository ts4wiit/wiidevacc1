/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: json_deals_test.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 15/04/2019           Ricardo Navarrete Lozano         Creation of methods.
*/

/**
* Validation of getters and setters for entity json_deals
* Created By: r.navarrete.lozano@accenture.com
*/
@isTest
public class json_deals_test {
    @isTest static void json_deals_UseCase1() {
        json_deals jd = new json_deals();
        jd.pronr = 'test';
        jd.posex = 'test';
        jd.condcode = 'test';
        jd.amount = 5.0;
        jd.percentage = 15.0;
        jd.description = 'test';
        jd.scaleid = 'test';
    }
}