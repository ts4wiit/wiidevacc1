/* ----------------------------------------------------------------------------
* AB InBev :: OnTap - OnCall
* ----------------------------------------------------------------------------
* Clase:HONES_TelecollectioncampaignBatch.apxc
* Versión: 1.0.0.0
* 
* 
* 
* Historial de Cambios
* ----------------------------------------------------------------------------
* Fecha           Usuario            Contacto      						Descripción
* 17/01/2019    Heron Zurita	  heron.zurita@accenture.com     Creación de la clase  
*/
public class HONES_TelecollectioncampaignBatch implements Database.Batchable<sObject>, Database.Stateful {
    /*
* test Method return all data which is selected in the query
* Created By:heron.zurita@accenture.com
* @params  Database.BatchableContext c
* @return List<sObject>
*/

    List<Telecollection_Campaign__c> camps;
    Telecollection_Campaign__c SVcamp = new Telecollection_Campaign__c();
    Telecollection_Campaign__c HNcamp = new Telecollection_Campaign__c();
    Integer SVcountAccount = 1;
    Integer HNcountAccount = 1;

    public List<sObject> start(Database.BatchableContext c)
        {
            List<sObject> scope = new List<sObject>();
            Date d = System.today() + 1;
            Datetime dt = (DateTime)d;
            String temp = '%'+dt.format('ddMMyyyy')+'%';
            camps = [SELECT Id FROM Telecollection_Campaign__c WHERE Name like :temp ];
            Integer campNumb =(camps.size()/2)+1;
            String campaign = 'TC' + dt.format('ddMMyyyy') + '_'+campNumb;

            SVcamp.Name = LABEL.CS_CountryCodeSV + campaign;
            SVcamp.Start_Date__c = system.today();
            SVcamp.HONES_Country__c = Label.TC_Country_ELSalvador;
            SVcamp.End_Date__c = system.today();
            SVcamp.Active__c = true;
            insert SVcamp;

            HNcamp.Name = LABEL.CS_CountryCodeHN + campaign;
            HNcamp.Start_Date__c = system.today();
            HNcamp.End_Date__c = system.today();
            HNcamp.HONES_Country__c = Label.TC_Country_Honduras;
            HNcamp.Active__c = true;
            insert HNcamp;

            scope.addAll([SELECT Id, V360_Tellecolector__c, ONTAP__ExternalKey__c, HONES_CreditRating__c
            FROM Account
            WHERE (V360_Tellecolector__c != null
            AND HONES_Telecollection_Frequency__c includes (:GlobalStrings.dayOfWeek))
            AND HONES_CreditRating__c != null ORDER BY HONES_CreditRating__c DESC NULLS LAST
            ]);

            scope.addAll([select Name,Telecollection_Campaign__c ,CreatedDate,ONCALL__Call_Status__c, ONCALL__POC__r.ONTAP__ExternalKey__c
            FROM ONCALL__Call__c
            where (ONCALL__Call_Status__c=:LABEL.PENDING_TO_CALL_HEAD
            AND CreatedDate <: system.today())
            AND (ONCALL__Call_Type__c =:LABEL.Telecollection_validation OR ONCALL__Call_Type__c =:LABEL.TELECOLLECTION)]);

            System.debug('Entri el star');
            return scope;

        }

    /*
* test Method that returns all calls of the  open campaings
* Created By:heron.zurita@accenture.com
* @params Database.BatchableContext c,List<sObject> scope
* @return void
*/
    public void execute(Database.BatchableContext c, List<sObject> scope)
        {

			List<Account> accSV=new  List<Account>();
    		List<Account> accHN=new  List<Account>();
            List<ONCALL__Call__c> yesterdayCall=new List<ONCALL__Call__c>();
            List<Account> acc=new  List<Account>();

            for(sObject obj : scope){
                switch on obj
                {
                    when Account ac
                    {
                        acc.add(ac);

                    }
                    when ONCALL__Call__c call
                    {
                        yesterdayCall.add(call);

                    }
                }
            }//END SWITCH FOR
            List<ONCALL__Call__c> yesterdaySV=new  List<ONCALL__Call__c>();
            List<ONCALL__Call__c> yesterdayHN=new  List<ONCALL__Call__c>();

            for(ONCALL__Call__c f:yesterdayCall){
                if(f.ONCALL__POC__r.ONTAP__ExternalKey__c != null && f.ONCALL__POC__r.ONTAP__ExternalKey__c.contains('SV')){
                    f.Telecollection_Campaign__c = SVcamp.id;
                    f.ISSM_CampaingName__c = SVcamp.Name;
                    f.HONES_CallSequence__c = String.valueof(SVcountAccount);
                    f.HONES_CallExecutionOrder__c = SVcountAccount;
                    f.Name = LABEL.CS_CountryCodeSV + GLobalStrings.callname + SVcountAccount;
                    SVcountAccount++;
                    yesterdaySV.add(f);
                }
                else{
                    f.Telecollection_Campaign__c = HNcamp.id;
                    f.HONES_CallSequence__c = String.valueof(HNcountAccount);
                    f.ISSM_CampaingName__c = HNcamp.Name;
                    f.HONES_CallExecutionOrder__c = HNcountAccount;
                    f.Name = LABEL.CS_CountryCodeHN + GLobalStrings.callname + HNcountAccount;
                    HNcountAccount++;
                    yesterdayHN.add(f);
                }
            }
            update yesterdaySV;
            update yesterdayHN;

            for(Account h : acc){
                if(h.ONTAP__ExternalKey__c != null && h.ONTAP__ExternalKey__c.contains('SV')){
                    accSV.add(h);
                }
                else{
                    accHN.add(h);
                }
            }
            if(accSV != null){
                Double totalAmount = 0.0;
                Set<Id> ids = new Set<Id>();
                Map<Id,Account> mapAcc = new Map<Id,Account>();
                for(Account i : accSV){
                    ids.add(i.Id);
                    mapAcc.put(i.Id,i);
                }
                
                
                Set<String> idFromAcc = new Set<String>();

                List<AggregateResult> Amou = [SELECT ONTAP__Account__c, SUM(ONTAP__Amount__c)am
                FROM ONTAP__OpenItem__c
                WHERE ONTAP__Account__c =:ids AND
                V360_ExpirationDate__c <: System.today()
                GROUP BY ONTAP__Account__c
                ORDER BY SUM(ONTAP__Amount__c) DESC NULLS LAST
                ];

                for(AggregateResult o : Amou){
                    idFromAcc.add(String.valueOf(o.get('ONTAP__Account__c')));
                }

                Schema.DescribeFieldResult Call_StatusPick = ONCALL__Call__c.ONCALL__Call_Type__c.getDescribe();
                List<Schema.PicklistEntry> Call_StatusPick_list = Call_StatusPick.getPicklistValues();
                Map<String, String> Call_StatusPick_values = new Map<String,String>();
                for( Schema.PicklistEntry v : Call_StatusPick_list) {
                    Call_StatusPick_values.put(v.getLabel(),v.getValue());
                }

                List<ONCALL__Call__c> calls = new List<ONCALL__Call__c>();
                for(Id cs : idFromAcc){
                    ONCALL__Call__c call = new ONCALL__Call__c();
                    call.Name = LABEL.CS_CountryCodeSV + GLobalStrings.callname + SVcountAccount;
                    call.ONCALL__POC__c = cs;
                    call.Telecollection_Campaign__c = SVcamp.Id;
                    call.ISSM_CampaingName__c = SVcamp.Name;
                    call.HONES_CallSequence__c = String.valueof(SVcountAccount);
                    call.HONES_CallExecutionOrder__c = SVcountAccount;
                    call.OwnerId = mapAcc.get(cs).V360_Tellecolector__c;
                    call.ONCALL__Call_Status__c = LABEL.PENDING_TO_CALL_HEAD;
                    if(Call_StatusPick_values.get(Label.Telecollection.touppercase())!=null){
                        call.ONCALL__Call_Type__c = Call_StatusPick_values.get(Label.Telecollection.touppercase());
                    }else{
                        call.ONCALL__Call_Type__c = Label.Telecollection;
                    }
                    call.recordTypeId = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByDeveloperName().get(LABEL.ISSM).getRecordTypeId();
                    calls.add(call);

                    SVcountAccount++;
                }
                insert calls;
            }

            if(accHN != null){

                Set<Id> idsH = new Set<Id>();
                Map<Id,Account> mapAcc = new Map<Id,Account>();
                for(Account i : accHN){
                    idsH.add(i.Id);
                    mapAcc.put(i.Id,i);
                }
                Set<Id> idFromAccH = new Set<Id>();
                List<ONTAP__OpenItem__c> oiH = [SELECT ONTAP__Account__c
                FROM ONTAP__OpenItem__c
                WHERE ONTAP__Account__c =: idsH
                AND V360_ExpirationDate__c < Today];
                System.debug('Valores'+oiH.size());
                for(ONTAP__OpenItem__c o : oiH){
                    idFromAccH.add(o.ONTAP__Account__c);
                }

                Schema.DescribeFieldResult Call_StatusPick = ONCALL__Call__c.ONCALL__Call_Type__c.getDescribe();
                List<Schema.PicklistEntry> Call_StatusPick_list = Call_StatusPick.getPicklistValues();
                Map<String, String> Call_StatusPick_values = new Map<String,String>();
                for( Schema.PicklistEntry v : Call_StatusPick_list) {
                    Call_StatusPick_values.put(v.getLabel(),v.getValue());
                }

                List<ONCALL__Call__c> calls = new List<ONCALL__Call__c>();
                for(Id cs : idFromAccH){
                    ONCALL__Call__c call = new ONCALL__Call__c();
                    call.Name = LABEL.CS_CountryCodeHN + GLobalStrings.callname + HNcountAccount;
                    call.ONCALL__POC__c = cs;
                    call.Telecollection_Campaign__c = HNcamp.Id;
                    call.ISSM_CampaingName__c = HNcamp.Name;
                    call.HONES_CallSequence__c = String.valueof(HNcountAccount);
                    call.HONES_CallExecutionOrder__c = HNcountAccount;
                    call.OwnerId = mapAcc.get(cs).V360_Tellecolector__c;
                    call.ONCALL__Call_Status__c = LABEL.PENDING_TO_CALL_HEAD;
                    call.ONCALL__Call_Type__c = Call_StatusPick_values.get(Label.Telecollection.touppercase()) != null ? Call_StatusPick_values.get(Label.Telecollection.touppercase()) : Label.Telecollection;
                    if(Call_StatusPick_values.get(Label.Telecollection.touppercase())!=null){
                        call.ONCALL__Call_Type__c = Call_StatusPick_values.get(Label.Telecollection.touppercase());
                    }else{
                        call.ONCALL__Call_Type__c = Label.Telecollection;
                    }
                    call.recordTypeId = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByDeveloperName().get(LABEL.ISSM).getRecordTypeId();
                    calls.add(call);

                    HNcountAccount++;
                }
                insert calls;

            }
        }
    /*
* test Method that finalize the batch
* Created By:heron.zurita@accenture.com
* @params Database.BatchableContext c
* @return void 
*/

    public void finish(Database.BatchableContext c)
        {

            SVcamp.Number_of_Call__c = SVcountAccount;
            update SVcamp;

            HNcamp.Number_of_Call__c = HNcountAccount;
            update HNcamp;
        }
}