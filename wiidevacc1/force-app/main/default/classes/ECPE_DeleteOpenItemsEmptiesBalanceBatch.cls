/* ----------------------------------------------------------------------------
* AB InBev :: iRep
* ----------------------------------------------------------------------------
* Clase: ECPE_DeleteOpenItemsEmptiesBalanceBatch.apxc
* Versión: 1.0.0.0
* Descripción: Batch que obtiene registros de los objetos ONTAP__OpenItem__c y ONTAP__EmptyBalance__c para ser borrados
* 
* 
* Historial de Cambios
* ----------------------------------------------------------------------------
* Fecha           Usuario            Contacto      						Descripción
* 25/09/2019    Carlos Álvarez	  carlos.alvarezs@gmodelo.com.mx     Creación de la clase  
*/
public class ECPE_DeleteOpenItemsEmptiesBalanceBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    String query;
    String objectName;
    Integer records = 0;
	IREP_BatchControlExecution__mdt ibc;
    
    public ECPE_DeleteOpenItemsEmptiesBalanceBatch( String query, String objectName ) {
        this.query = query;
        this.objectName = objectName;
        List<IREP_BatchControlExecution__mdt> listIB = [SELECT Id, DeveloperName, MasterLabel, Label, IREP_BatchSize__c FROM IREP_BatchControlExecution__mdt WHERE DeveloperName = 'ECPE_DeleteOpenItemsEmptiesBalanceBatch'];
        ibc = listIB != null && listIB.size() > 0 ? listIB[0] : new IREP_BatchControlExecution__mdt(Label = 'carlos.alvarezs@ab-inbev.com', IREP_BatchSize__c = 200);
    }

    public List<sObject> start(Database.BatchableContext c) {
        return Database.query(this.query);
    }
    
    public void execute(Database.BatchableContext c, List<sObject> scope) {
        // System.debug('scope: ' + scope);
        this.records = this.records + scope.size();
        delete scope;
        DataBase.emptyRecycleBin(scope);
    }
    
    public void finish(Database.BatchableContext c) {
        AsyncApexJob a = [SELECT Id, CreatedDate, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id =: c.getJobId()];

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        mail.setToAddresses(ibc.Label.split(','));
        mail.setSubject('Resúmen de ejecución de borrado de ' + this.objectName);
        mail.setPlainTextBody('Registros procesados: ' + this.records
                              + '\n\nNúmero de lotes: '+ a.TotalJobItems +', errores: '+ a.NumberOfErrors 
                              + '\n\n Hora de inicio:     ' + a.CreatedDate.format('dd-MM-YYYY HH:mm ss') 
                              + '\n Hora de término: ' + DateTime.now().format('dd-MM-YYYY HH:mm ss'));
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}