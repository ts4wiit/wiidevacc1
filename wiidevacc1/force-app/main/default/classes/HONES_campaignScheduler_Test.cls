/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: HONES_campaignScheduler_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 05/03/2018     Carlos Leal           	Creation of methods.
 */
@isTest
private class HONES_campaignScheduler_Test {
	
    /**
   * Method for test theSchudeler for coverage.
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
  static testMethod void test_execute_Campaing_Batch(){
    HONES_campaignSchedule obj01 = new HONES_campaignSchedule();
    SchedulableContext sc;
    obj01.execute(sc);
  }
}