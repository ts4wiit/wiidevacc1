/* ----------------------------------------------------------------------------
* AB InBev :: OnTap - OnCall
* ----------------------------------------------------------------------------
* Clase: ONTAP_OrderTriggerHandler.apxc
* Versión: 1.0.0.0
*  
* Historial de Cambios
* ----------------------------------------------------------------------------
* Fecha           Usuario            Contacto                           Descripción
* 18/12/ 2018     Fernando Engel      fernando.engel.funes@accenture.com    Creación de la clase 
* 10/12/ 2019     TS4      f          rjimenez@ts4.mx                       Se agrega método linkDeliveryToSalesOrder
*/

public class ONTAP_OrderTriggerHandler extends TriggerHandlerCustom{
    private Map<Id, ONTAP__Order__c> newMap;
    private Map<Id, ONTAP__Order__c> oldMap;
    private List<ONTAP__Order__c> newList;
    private List<ONTAP__Order__c> oldList;
    
    /**
* Constructor of the class
* Created By: fernando.engel.funes@accenture.com 
* @param void
* @return void
*/
    public ONTAP_OrderTriggerHandler() {
        this.newMap = (Map<Id, ONTAP__Order__c>) Trigger.newMap;
        this.oldMap = (Map<Id, ONTAP__Order__c>) Trigger.oldMap;
        this.newList = (List<ONTAP__Order__c>) Trigger.new;
        this.oldList = (List<ONTAP__Order__c>) Trigger.old;
        
    }

     /**
    * Method that executes after insert
    * Created By: TS4
    * Modify By: TS4
    * Modify Date:
    * @override
    * @param void
    * @return void
    */
    public override void afterInsert(){
        linkDeliveryToSalesOrder(this.newList);
    }
    
    /**
    * Method that executes after update
    * Created By: fernando.engel.funes@accenture.com 
    * Modify By: gabriel.e.garcia@accenture.com
    * Modify Date 2019-03-20
    * @override
    * @param void
    * @return void
    */
    public override void afterUpdate()
    {   
       
        this.newMap = (Map<Id, ONTAP__Order__c>) Trigger.newMap;
        SettingsBatchSendOrder__c sbs = SettingsBatchSendOrder__c.getOrgDefaults();   
        Set<Id> newIds = new Set<Id>();
        if(Test.isRunningTest()){
            this.newMap = new Map<Id, ONTAP__Order__c>();
            Account acc = new Account(Name = 'Test Acc',ONCALL__Country__c ='SV', ONTAP__SalesOgId__c='IS');
        	insert acc;
            
            ONTAP__Order__c order = new ONTAP__Order__c(ONTAP__OrderAccount__c = acc.id);
            order.ONCALL__SAP_Order_Item_Counter__c=1;
            order.ONCALL__Total_Order_Item_Quantity__c=1;
            order.ONTAP__DocumentationType__c='test';
            order.ISSM_PaymentMethod__c='Cash';
            order.ONTAP__DeliveryDate__c=System.today();
        	insert order;
            this.newMap.put(order.Id,order);
        }
        for(ID id : newMap.keySet())
        {
            System.debug('newMap.get(Id).ISSM_OriginText_c ' + newMap.get(Id).ISSM_OriginText__c);
            if((newMap.get(Id).ONCALL__SAP_Order_Number__c == null || String.isEmpty(newMap.get(Id).ONCALL__SAP_Order_Number__c)) && newMap.get(Id).ONCALL__OnCall_Status__c == 'Closed' && newMap.get(Id).ISSM_OriginText__c != 'OCAL' && sbs.ActiveSendByTrigger__c)
            {
                newIds.add(id);
            }
        }
        
        if(!ISSM_TriggerManager_cls.isInactive('CS_TRIGGER_PEDIDO'))
        {
            if(newIds.size() > 0 && !System.isFuture() && !Test.isRunningTest())
            {
                queryOrdersToInsert(newIds); // 11-05-19
                //queryOrdersToInsert(this.newMap); // 01-04-19
            }
        }
        
    }
    
    /**
* Method that executes before insert
* Created By: fernando.engel.funes@accenture.com 
* @param void
* @return void
*/
    public override void beforeInsert(){
        //List<ONTAP__Order__c> orders = PreventExecutionUtil.validateOrderByUser(this.newlist);
        //relateOrderTypeToOrder(orders);   
        System.debug('beforeInsert');
        relateOrderTypeToOrder(this.newlist);
        generateTicketForOrders(this.newlist, this.oldMap); //PANAMA
    }
    
    /**
* Method that executes before update
* Created By: fernando.engel.funes@accenture.com 
* @param void
* @return void
*/
    public override void beforeUpdate(){
        System.debug('beforeUpdate');
        relateOrderTypeToOrder(this.newlist);
        //updateDeliveryDateHolidays(this.newlist);
        updateDeliveryDateHolidays(this.newlist, this.oldMap);
        generateTicketForOrders(this.newlist, this.oldMap);
    }
    
     /**
    * Method that get the Orders that are ready to send
    * Created By: fernando.engel.funes@accenture.com 
    * Modify By: gabriel.e.garcia@accenture.com
    * Modify Date 2019-05-15
    * @param Map<Id, ONTAP__Order__c> newMap
    * @return void
    */    
    //PANAMA
    @future(callout=true)
    public static void queryOrdersToInsert(Set<Id> newIds){        
        List<ONTAP__Order__c> OrdersReadyToSend = new List<ONTAP__Order__c>();       
        Set<Id> setIdOrders = new Set<Id>();
        for(List<ONTAP__Order__c> lstOrders :[SELECT Id,ONCALL__SAP_Order_Item_Counter__c,ONCALL__Total_Order_Item_Quantity__c,
                                              ONTAP__DocumentationType__c,ONTAP__OrderAccount__r.ONTAP__SalesOgId__c,ONTAP__DeliveryDate__c,
                                              ISSM_PaymentMethod__c,Order_Reason__c,ISSM_OriginText__c
                                              FROM ONTAP__Order__c WHERE Id IN:newIds AND ONCALL__SAP_Order_Item_Counter__c>0 AND ONCALL__Total_Order_Item_Quantity__c>0 AND ONCALL__SAP_Order_Number__c=NULL
                                              AND ONCALL__OnCall_Status__c = 'Closed' FOR UPDATE]){
                                                  for(ONTAP__Order__c order: lstOrders){                                                      
                                                      if(order.ONTAP__DocumentationType__c!=NULL && order.ONTAP__OrderAccount__c!=NULL && order.ONTAP__OrderAccount__r.ONTAP__SalesOgId__c != NULL
                                                         && order.ONTAP__DeliveryDate__c!=NULL && order.ONCALL__Total_Order_Item_Quantity__c!=NULL && order.ONCALL__SAP_Order_Item_Counter__c!=NULL 
                                                         && order.ONCALL__SAP_Order_Item_Counter__c==order.ONCALL__Total_Order_Item_Quantity__c && order.ISSM_PaymentMethod__c !=NULL && order.ISSM_OriginText__c!=NULL){
                                                             OrdersReadyToSend.add(order);
                                                             setIdOrders.add(order.Id);
                                                         }
                                                  }
                                              }
        
        System.debug('Order Ready: '+OrdersReadyToSend);
        if(setIdOrders.size() > 0 && !Test.isRunningTest())
        {
            System.debug('Total Ids : ' + setIdOrders.size());
            ONCALL_orderToJSON sjsn = new ONCALL_orderToJSON();
            sjsn.createJson(setIdOrders);  
        }
    } 

    /* Method to save the relationship between Order and the Order type
    * Created By: fernando.engel.funes@accenture.com 
    * @param List<ONTAP__Order__c> newlist
    * @return void
    */
    //PANAMA
    public static void relateOrderTypeToOrder(List<ONTAP__Order__c> newlist){
        set<ID> orderRecordTypes = new set<ID>();
        set<ID> orderId = new set<ID>();
        set<String> recordTypeNames = New set<String>();
        
        Map<String,Order_Types__mdt> mapOrder_Types_mdt = New Map<String,Order_Types__mdt>();
        
        
        for(ONTAP__Order__c Order: newlist){
            orderRecordTypes.add(Order.RecordTypeId);
            orderId.add(Order.ONTAP__OrderAccount__c);
        }
        List<RecordType> lstRecordType = New List<RecordType>([SELECT Id, Name, DeveloperName 
                                                               FROM RecordType 
                                                               WHERE SObjectType = 'ONTAP__Order__c' 
                                                               AND Id IN: orderRecordTypes 
                                                               LIMIT 200]);
        
        Map<id,RecordType> mapRecordType = New Map<id,RecordType>(lstRecordType);
        
        for(RecordType RT : lstRecordType){
            recordTypeNames.add(RT.DeveloperName);
        }
        system.debug('>>> recordTypeNames: '+recordTypeNames);
        Map<id,Account> mapAcc = New Map<id,Account>([SELECT id, ONCALL__KATR4__c 
                                                      FROM Account WHERE ID IN: orderId LIMIT 200]);
        
        List<Order_Types__mdt> lstOrder_Types_mdt = new List<Order_Types__mdt>([SELECT id, Atr_4__c,Order_Record_Type__c, ID_SAP__c FROM Order_Types__mdt 
                                                                                WHERE Order_Record_Type__c IN :recordTypeNames 
                                                                                LIMIT 200]);
        
        if(lstOrder_Types_mdt.size() >= 1 && !lstOrder_Types_mdt.isEmpty() ){
            for(Order_Types__mdt Order_Types_mdt: lstOrder_Types_mdt){ 
                if( Order_Types_mdt.Order_Record_Type__c != null && Order_Types_mdt.Atr_4__c != null){
                    String key = Order_Types_mdt.Order_Record_Type__c + Order_Types_mdt.Atr_4__c;
                    mapOrder_Types_mdt.put( Key , Order_Types_mdt);
                }
            }
            
            for(ONTAP__Order__c Order: newlist){
                if(  Order.RecordTypeId != null && mapAcc!=NULL&& Order.ONTAP__OrderAccount__c!=NULL && mapAcc.get(Order.ONTAP__OrderAccount__c)!=NULL && mapAcc.get(Order.ONTAP__OrderAccount__c).ONCALL__KATR4__c != null){ 
                    String KATR4 = mapAcc.get(Order.ONTAP__OrderAccount__c).ONCALL__KATR4__c;
                    String KATR4Code= KATR4.left(2);
                    String key = mapRecordType.get(Order.RecordTypeId).DeveloperName  + KATR4Code;
                    if(key!=NULL&&mapOrder_Types_mdt!=NULL && mapOrder_Types_mdt.containsKey(key) && mapOrder_Types_mdt.get(key).ID_SAP__c != null){
                        Order.ONTAP__DocumentationType__c = mapOrder_Types_mdt.get(key).ID_SAP__c;
                    }
                }
            }
        }
    }

    /**
    * Method that link order of IREP to Sales Order by Number of IREP
    * Created By: TS4
    * Date: 2019-12-01
    **/
    public static void linkDeliveryToSalesOrder(list<ONTAP__Order__c> orders){
        System.debug('linkDeliveryToSalesOrder::inicio');
        list<ONTAP__Order__c> deliveryOrders = new list<ONTAP__Order__c>();
        list<ONTAP__Order__c> salesOrders = new list<ONTAP__Order__c>();
        set<String> numberSapIREP =  new set<String>();
        map<String, ONTAP__Order__c> mapSapDeliveryOrder = new  map<String, ONTAP__Order__c>();
        set<Id> setRecordType = new set<Id>();
        for(ONTAP__Order__c deliveryOrder : orders){
            setRecordType.add(deliveryOrder.RecordTypeId);
        }

        Map<ID, RecordType> mapRecordType = new Map<ID, RecordType>([SELECT Id, Name, DeveloperName  
                                                        FROM RecordType 
                                                        WHERE SObjectType = 'ONTAP__Order__c' 
                                                                AND Id IN: setRecordType ]);
    

        for(ONTAP__Order__c deliveryOrder : orders){
            System.debug('linkDeliveryToSalesOrder::deliveryOrder.RecordType.Name::' + mapRecordType.get(deliveryOrder.RecordTypeId).DeveloperName);
            System.debug('linkDeliveryToSalesOrder::deliveryOrder.IREP_SAP_Order__c::' + deliveryOrder.IREP_SAP_Order__c);
            System.debug('linkDeliveryToSalesOrder::deliveryOrder.ONTAP__TourVisit__c::' + deliveryOrder.ONTAP__TourVisit__c);
                
            if((mapRecordType.get(deliveryOrder.RecordTypeId).DeveloperName == 'IREP_Order_SV' || 
                mapRecordType.get(deliveryOrder.RecordTypeId).DeveloperName == 'IREP_Order_HN') && 
                    deliveryOrder.IREP_SAP_Order__c != null && deliveryOrder.ONTAP__TourVisit__c != null ){
                        deliveryOrders.add(deliveryOrder);
                        numberSapIREP.add(deliveryOrder.IREP_SAP_Order__c);
                        mapSapDeliveryOrder.put(deliveryOrder.IREP_SAP_Order__c, deliveryOrder);
            }
        }
        System.debug('linkDeliveryToSalesOrder::mapSapDeliveryOrder::' + mapSapDeliveryOrder);

        for (ONTAP__Order__c salesOrder : [SELECT Id, ONTAP__TourVisit__c,ONTAP__SAP_Order_Number__c 
                                            FROM ONTAP__Order__c WHERE ONTAP__SAP_Order_Number__c IN : mapSapDeliveryOrder.keySet()]){
            salesOrder.ONTAP__TourVisit__c = mapSapDeliveryOrder.get(salesOrder.ONTAP__SAP_Order_Number__c).ONTAP__TourVisit__c;
            System.debug('linkDeliveryToSalesOrder:: salesOrder.ONTAP__TourVisit__c::' +  salesOrder.ONTAP__TourVisit__c);
            salesOrders.add(salesOrder);
        }
        ISSM_TriggerManager_cls.inactivate('ONTAP_OrderTriggerHandler');
        System.debug('linkDeliveryToSalesOrder:: salesOrders::' +  salesOrders);
        if(salesOrders.size() > 0){
         update salesOrders;
        }
    }

    /**
    * Method that get the Orders that are ready to send
    * Created By: fernando.engel.funes@accenture.com 
    * Modify By: gabriel.e.garcia@accenture.com
    * Modify Date 2019-05-15
    * @param Map<Id, ONTAP__Order__c> newMap
    * @return void
    */    
    //TS4
    /*@future(callout=true)
    public static void queryOrdersToInsert(Set<Id> newIds){        
        List<ONTAP__Order__c> OrdersReadyToSend = new List<ONTAP__Order__c>();       
        Set<Id> setIdOrders = new Set<Id>();
        for(List<ONTAP__Order__c> lstOrders :[SELECT Id,ONCALL__SAP_Order_Item_Counter__c,ONCALL__Total_Order_Item_Quantity__c,
                                              ONTAP__DocumentationType__c,ONTAP__OrderAccount__r.ONTAP__SalesOgId__c,ONTAP__DeliveryDate__c,
                                              ISSM_PaymentMethod__c,Order_Reason__c,ISSM_OriginText__c
                                              FROM ONTAP__Order__c WHERE Id IN:newIds AND ONCALL__SAP_Order_Item_Counter__c>0 AND ONCALL__Total_Order_Item_Quantity__c>0 AND ONCALL__SAP_Order_Number__c=NULL
                                              AND ONCALL__OnCall_Status__c = 'Closed' FOR UPDATE]){
                                                  for(ONTAP__Order__c order: lstOrders){                                                      
                                                      if(order.ONTAP__DocumentationType__c!=NULL && order.ONTAP__OrderAccount__c!=NULL && order.ONTAP__OrderAccount__r.ONTAP__SalesOgId__c != NULL
                                                         && order.ONTAP__DeliveryDate__c!=NULL && order.ONCALL__Total_Order_Item_Quantity__c!=NULL && order.ONCALL__SAP_Order_Item_Counter__c!=NULL 
                                                         && order.ONCALL__SAP_Order_Item_Counter__c==order.ONCALL__Total_Order_Item_Quantity__c && order.ISSM_PaymentMethod__c !=NULL && order.ISSM_OriginText__c!=NULL){
                                                             OrdersReadyToSend.add(order);
                                                             setIdOrders.add(order.Id);
                                                         }
                                                  }
                                              }
        
        System.debug('Order Ready: '+OrdersReadyToSend);
        if(setIdOrders.size() > 0)
        {
            System.debug('Total Ids : ' + setIdOrders.size());
            ONCALL_orderToJSON sjsn = new ONCALL_orderToJSON();
            sjsn.createJson(setIdOrders);  
        }
    } */
    
    
    /**
* Method to save the relationship between Order and the Order type
* Created By: fernando.engel.funes@accenture.com 
* @param List<ONTAP__Order__c> newlist
* @return void
*/
//TS4
 /* public static void relateOrderTypeToOrder(List<ONTAP__Order__c> newlist){
        if(!ISSM_TriggerManager_cls.isInactive('CS_TRIGGER_PEDIDO')){
            set<ID> orderRecordTypes = new set<ID>();
            set<ID> orderId = new set<ID>();
            set<String> recordTypeNames = New set<String>();
            
            Map<String,Order_Types__mdt> mapOrder_Types_mdt = New Map<String,Order_Types__mdt>();
            
            
            for(ONTAP__Order__c Order: newlist){
                orderRecordTypes.add(Order.RecordTypeId);
                orderId.add(Order.ONTAP__OrderAccount__c);
            }
            List<RecordType> lstRecordType = New List<RecordType>([SELECT Id, Name, DeveloperName 
                                                                FROM RecordType 
                                                                WHERE SObjectType = 'ONTAP__Order__c' 
                                                                AND Id IN: orderRecordTypes 
                                                                LIMIT 200]);
            
            Map<id,RecordType> mapRecordType = New Map<id,RecordType>(lstRecordType);
            
            for(RecordType RT : lstRecordType){
                recordTypeNames.add(RT.DeveloperName);
            }
            system.debug('>>> recordTypeNames: '+recordTypeNames);
            Map<id,Account> mapAcc = New Map<id,Account>([SELECT id, ONCALL__KATR4__c 
                                                        FROM Account WHERE ID IN: orderId LIMIT 200]);
            
            List<Order_Types__mdt> lstOrder_Types_mdt = new List<Order_Types__mdt>([SELECT id, Atr_4__c,Order_Record_Type__c, ID_SAP__c FROM Order_Types__mdt 
                                                                                    WHERE Order_Record_Type__c IN :recordTypeNames 
                                                                                    LIMIT 200]);
            
            if(lstOrder_Types_mdt.size() >= 1 && !lstOrder_Types_mdt.isEmpty() ){
                for(Order_Types__mdt Order_Types_mdt: lstOrder_Types_mdt){ 
                    if( Order_Types_mdt.Order_Record_Type__c != null && Order_Types_mdt.Atr_4__c != null){
                        String key = Order_Types_mdt.Order_Record_Type__c + Order_Types_mdt.Atr_4__c;
                        mapOrder_Types_mdt.put( Key , Order_Types_mdt);
                    }
                }
                
                for(ONTAP__Order__c Order: newlist){
                    if(  Order.RecordTypeId != null && mapAcc!=NULL&& Order.ONTAP__OrderAccount__c!=NULL && mapAcc.get(Order.ONTAP__OrderAccount__c)!=NULL && mapAcc.get(Order.ONTAP__OrderAccount__c).ONCALL__KATR4__c != null){ 
                        String KATR4 = mapAcc.get(Order.ONTAP__OrderAccount__c).ONCALL__KATR4__c;
                        String KATR4Code= KATR4.left(2);
                        String key = mapRecordType.get(Order.RecordTypeId).DeveloperName  + KATR4Code;
                        if(key!=NULL&&mapOrder_Types_mdt!=NULL && mapOrder_Types_mdt.containsKey(key) && mapOrder_Types_mdt.get(key).ID_SAP__c != null){
                            Order.ONTAP__DocumentationType__c = mapOrder_Types_mdt.get(key).ID_SAP__c;
                        }
                    }
                }
            }
        }
    }*/

 /**
* Method to calculate the delivery day considering holidays
* Modified by: rjimenez@ts4.mx; smonroy@ts4.mx
* Last Date: 22-08-2019
* @param List<ONTAP__Order__c>
* @return void
*/

    public static void updateDeliveryDateHolidays(List<ONTAP__Order__c> lst, map<Id, ONTAP__Order__c> oldMap) {
    Date fecha;
    for(ONTAP__Order__c ord: lst ) {
        
        if(Test.isRunningTest()){
            ord.ONTAP__DeliveryDate__c = System.today(); 
        }

        if(ord.ONTAP__DeliveryDate__c != null && ord.ONTAP__DeliveryDate__c !=  oldMap.get(ord.Id).ONTAP__DeliveryDate__c) {
        	System.debug('updateDeliveryDateHolidays::ONTAP__DeliveryDate__c:' + ord.ONTAP__DeliveryDate__c.date());
            //En caso de que mas de un dia no habil coincida se agrega un ciclo repetitivo para evaluarlos 30/09/2019
            while(!LookUpAccountCloseSale_HONES.checkifItisWorkingDay(ord.ONTAP__DeliveryDate__c.date())){
                fecha = ord.ONTAP__DeliveryDate__c.date();
                fecha = fecha.addDays(1);
                ord.ONTAP__DeliveryDate__c = DateTime.newInstance(fecha, Time.newInstance(0, 0, 0, 0));
            }
    
            System.debug('fecha :' + fecha);
            System.debug(ord);
            //System.debug('EstadoHoliday::'+LookUpAccountCloseSale.checkifItisWorkingDay(ord.ONTAP__DeliveryDate__c.date()));
            }
        }
    }

    //PANAMA
    /*Comment By TS4
    public static void updateDeliveryDateHolidays(List<ONTAP__Order__c> lst) {
        List<Holiday> lstHdy = [SELECT ActivityDate,Id,Name FROM Holiday];
        Map<Date,String> mapHdy = new Map<Date,String>();
        Boolean hdyNotFound = true;
        for(Holiday hy:lstHdy){
            mapHdy.put(hy.ActivityDate, hy.Name);
        }
        System.debug('Mapa :' +mapHdy);
        
        for(ONTAP__Order__c ord: lst){
            if(ord.ONTAP__DeliveryDate__c != null){
                Date fecha = ord.ONTAP__DeliveryDate__c.date();
                while(hdyNotFound){
                    if(mapHdy.containsKey(fecha)){
                        fecha = fecha.addDays(1);
                    } else{
                        hdyNotFound = false;
                    }
                }
                System.debug('fecha :' + fecha);
                ord.ONTAP__DeliveryDate__c = DateTime.newInstance(fecha, Time.newInstance(0, 0, 0, 0));
                System.debug(ord);
            }
         } 
    }*/

     //PANAMA
    public static void generateTicketForOrders(List<ONTAP__Order__c> newlist, Map<Id, ONTAP__Order__c> oldmap){
        
        Set<String> orderTypesForTicket = new set<String>();
        Set<Id> orderRecordTypedIdForTicket = new Set<Id>();
        Set<Id> orderIdsToGenerateTicketOnStatusClose = new Set<Id>();
        
        for (Order_Types__mdt ot : [SELECT Order_Record_Type__c FROM Order_Types__mdt WHERE Generate_Ticket__c = TRUE]){ //Order types that must generate a PDF ticket (from custom metadata)
            orderTypesForTicket.add(ot.Order_Record_Type__c);
        }
        for(RecordType rt: [SELECT Id FROM RecordType WHERE SObjectType = 'ONTAP__Order__c' AND DeveloperName IN :orderTypesForTicket ]){ // RecordtypeId for this ONTAP__Order__c
            orderRecordTypedIdForTicket.add(rt.Id);
		}
        for (ONTAP__Order__c order: newlist) {
			if (orderRecordTypedIdForTicket.contains(order.RecordTypeId)  && order.ONCALL__OnCall_Status__c == 'Closed' && (Trigger.isInsert || oldMap.get(order.Id).ONCALL__OnCall_Status__c != 'Closed')) {                
                orderIdsToGenerateTicketOnStatusClose.add(order.Id);
            }
        }
        if (orderIdsToGenerateTicketOnStatusClose.size() > 0) {
        	GenerateTicketController.createTicketsForOrders(orderIdsToGenerateTicketOnStatusClose);
        }
    }


}