/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap - OnCall
 * ----------------------------------------------------------------------------
 * Clase: orderClassCont.apxc
 * Versión: 1.0.0.0
 *  
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario            Contacto      						Descripción
 * 03/01/ 2019     Herón Zurita		   heron.zurita@accenture.com            Creación
 */
public class orderClassCont {
    /*
     *Method that search and  agree all matches with parameter 
     * Created By:heron.zurita@accenture.com 
     * @params String searchText
     * @return List<String>
	
*/ 
  @AuraEnabled
    public static List<String> searchForIds(String searchText){
    	List<List<SObject>> results = [FIND :searchText IN ALL FIELDS  RETURNING Account(Id)];
    	List<String> ids = new List<String>();
    	for (List<SObject> sobjs : results) {
      		for (SObject sobj : sobjs) {
        		ids.add(sobj.Id);
      		}
    	}
    	return ids;
        
	}
}