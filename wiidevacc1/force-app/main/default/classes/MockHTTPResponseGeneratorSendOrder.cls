/* ----------------------------------------------------------------------------
* AB InBev :: Oncall
* ----------------------------------------------------------------------------
* Clase: MockHTTPResponseGeneratorSendOrder.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 23/01/2019        Oscar Garcia          Creation of methods.
*/
@isTest
global class MockHTTPResponseGeneratorSendOrder implements HttpCalloutMock {    
    /**
    * Test method for create mock for generate order call out
    * Created By: o.a.garcia.martinez@accenture.com@accenture.com
    * @param void
    * @return void
    */
    global HTTPResponse respond(HTTPRequest req) {
       
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatus('OK');
        res.setStatusCode(200);
        //New response as etReturn is now an array
        res.setBody('{"orderId": "0225000667", "etReturn": [ {"type" : "S", "number" : "233", "message" : "SALES_HEADER_IN procesado con éxito"}, {"type" : "S", "number" : "433", "message" : "PA-Pd PPT Estandar 0225000667 se ha grabado"} ] }');
        //res.setBody('{"id": "0225000667","etReturn": {"item": {"type": "S","number": "233","message": "SALES_HEADER_IN procesado con éxito","messageV1": null,"messageV2": null,"messageV3": null,"messageV4": null}}}');        
        System.debug('Respuesta'+res);
        return res;
    }  
}