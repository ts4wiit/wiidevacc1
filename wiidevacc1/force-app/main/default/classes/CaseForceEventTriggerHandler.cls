public class CaseForceEventTriggerHandler {
    
    public static final String DR_SALES_PROCESS_RECORD_TYPE = 'DO_Sales_Process';
    public static final String DR_ASSET_INVESTIGATION_RECORD_TYPE = 'DO_Asset_Investigation';
    public static final String DR_ASSET_MOVEMENT_RECORD_TYPE = 'DO_Asset_Movement';
    public static final String DR_ASSET_MAINTENANCE_RECORD_TYPE = 'DO_Asset_Maintenance';
    public static final String DR_PAYMENT_METHOD_CHANGE_RECORD_TYPE = 'DO_Payment_Methods_Change_Request';
    public static final String PAYMENT_CHECK = 'Pay In Cheque';
    public static final String PAYMENT_CASH = 'Pay In Cash';
    public static final String PAYMENT_TRANSFER = 'Pay In Transfer';
    
    private List<ONTAP__Case_Force__ChangeEvent> caseForceChangeEvents = new List<ONTAP__Case_Force__ChangeEvent>();
    private List<ONTAP__Case_Force__ChangeEvent> filteredCaseForceChangeEvents = new List<ONTAP__Case_Force__ChangeEvent>(); 
    private List<Id> caseIds = new List<Id>();
    private DRCaseSelector caseSelector;
    private Map<Id,ONTAP__Case_Force__c> salesProcessCasesMap = new Map<Id,ONTAP__Case_Force__c>();
    private Map<Id,ONTAP__Case_Force__c> assetInvestigationCasesMap = new Map<Id,ONTAP__Case_Force__c>();
    private Map<Id,ONTAP__Case_Force__c> assetMovementCasesMap = new Map<Id,ONTAP__Case_Force__c>();
    private Map<Id,ONTAP__Case_Force__c> assetMaintenanceCasesMap = new Map<Id,ONTAP__Case_Force__c>();   
    private Map<Id,ONTAP__Case_Force__c> paymentMethodCasesMap = new Map<Id,ONTAP__Case_Force__c>();   
    public Map<String,String> caseIdToRouteMap = new Map<String,String>();
    private List<DRCaseSalesProcessOutboundMessages__e> caseSalesProcessOutboundMessages = new List<DRCaseSalesProcessOutboundMessages__e>();
    private List<DRCaseAssetOutboundMessage__e> assetInvestigationOutboundMessages = new List<DRCaseAssetOutboundMessage__e>();
    private List<DRCaseAssetMoveOutboundMessage__e> assetMovementOutboundMessages = new List<DRCaseAssetMoveOutboundMessage__e>();
    private List<DRCaseAssetMaintenanceOutboundMessage__e> assetMaintenanceOutboundMessages = new List<DRCaseAssetMaintenanceOutboundMessage__e>();
    private List<DRCasePaymentChangeOutboundMessage__e> paymentMethodOutboundMessages = new List<DRCasePaymentChangeOutboundMessage__e>();  
    private String integrationId;
    
    // info to verify if user is allowed to publish changes
    private Map<Id,Boolean> isUserAllowedMap = new Map<Id,Boolean>();
    private List<Id> transactionUserSet = new List<Id>();

    public CaseForceEventTriggerHandler( List<ONTAP__Case_Force__ChangeEvent> caseForceChangeEvents ){
        
        this.caseForceChangeEvents = caseForceChangeEvents;
        
    }
    
    public void run(){
        
        this.filter();
        this.setCases();
        this.setSalesProcessCases();
        this.setAssetInvestigationCases();
        this.setAssetMovementCases();
        this.setAssetMantenanceCases();
        this.setPaymentMethodCases();
        this.publish();
      //  this.share();
    }
    

    private void filter(){
        
        this.integrationId = DRUserSelector.getIntegrationId();
        
        for( ONTAP__Case_Force__ChangeEvent caseForceChangeEvent : caseForceChangeEvents ){
            if (( caseForceChangeEvent.ChangeEventHeader.getChangeType() == 'CREATE' &&  caseForceChangeEvent.ChangeEventHeader.getCommitUser() != (Id) this.integrationId ) || Test.isRunningTest() ){            
                filteredCaseForceChangeEvents.add( caseForceChangeEvent );
                caseIds.add( caseForceChangeEvent.ChangeEventHeader.getRecordIds().get(0) );
            }
        }
    }
    
    private void setCases(){      
        this.caseSelector = new DRCaseSelector( caseIds );   
        this.caseIdToRouteMap = caseSelector.getCaseIdToRouteMap();
    }
    
    private void setSalesProcessCases(){
        this.salesProcessCasesMap = caseSelector.filterByRecordTypeName( DR_SALES_PROCESS_RECORD_TYPE );
    }
    
    private void setAssetInvestigationCases(){
        this.assetInvestigationCasesMap = caseSelector.filterByRecordTypeName( DR_ASSET_INVESTIGATION_RECORD_TYPE );
    }   
    
    private void setAssetMovementCases(){
        this.assetMovementCasesMap = caseSelector.filterByRecordTypeName( DR_ASSET_MOVEMENT_RECORD_TYPE );
    }       
    
    private void setAssetMantenanceCases(){
        this.assetMaintenanceCasesMap = caseSelector.filterByRecordTypeName( DR_ASSET_MAINTENANCE_RECORD_TYPE );
    }     
    
    private void setPaymentMethodCases(){
        this.paymentMethodCasesMap = caseSelector.filterByRecordTypeName( DR_PAYMENT_METHOD_CHANGE_RECORD_TYPE );
    }       
    
    private void publish(){
        this.mapSalesProcessCases();
        this.mapAssetInvestigationCases();
        this.mapAssetMovementCases();
        this.mapAssetMaintenanceCases();
        this.mapPaymentMethodCases();
        
        System.debug('>>> Case Sales Process');
        System.debug( caseSalesProcessOutboundMessages );
        System.debug('>>> Case Asset Investigation');
        System.debug( assetInvestigationOutboundMessages );    
        System.debug('>>> Case Asset Movement');
        System.debug( assetMovementOutboundMessages );
        System.debug('>>> Case Asset Maintenance');
        System.debug( assetMaintenanceOutboundMessages );         
        System.debug('>>> Case Payment Method');
        System.debug( paymentMethodOutboundMessages );           
        
        List<Database.SaveResult> salesProcessCaseResults = EventBus.publish( caseSalesProcessOutboundMessages );
        List<Database.SaveResult> assetInvestigationCaseResults = EventBus.publish( assetInvestigationOutboundMessages );
        List<Database.SaveResult> assetMovementCaseResults = EventBus.publish( assetMovementOutboundMessages );
        List<Database.SaveResult> assetMaintenanceCaseResults = EventBus.publish( assetMaintenanceOutboundMessages );
        List<Database.SaveResult> paymentMethodCaseResults = EventBus.publish( paymentMethodOutboundMessages );
    }
    
    private void mapSalesProcessCases(){
        
        for ( Id caseId : salesProcessCasesMap.keySet() ){
            
            ONTAP__Case_Force__c caseForce = salesProcessCasesMap.get( caseId );
            
            DRCaseSalesProcessOutboundMessages__e caseSalesProcessOutboundMessage = new DRCaseSalesProcessOutboundMessages__e(
            
                CaseForceId__c = caseForce.Id,
                CaseNumber__c = caseForce.ONTAP__Case_Number__c,
                CaseOrigin__c = caseForce.ONTAP__Case_Origin__c,
                CreatedDate__c = caseForce.CreatedDate,
                Description__c = caseForce.ONTAP__Description__c,
                OwnerId__c = caseIdToRouteMap.get( caseForce.Id ),
                SAPCustomerId__c = caseForce.ONTAP__Account__r.ONTAP__SAPCustomerId__c,
                Status__c = caseForce.ONTAP__Status__c,
                Type__c = caseForce.Case_Type__c,
                LastModifiedDate__c = caseForce.LastModifiedDate
                
            );           
            
        	caseSalesProcessOutboundMessages.add( caseSalesProcessOutboundMessage ); 
        }    
        
    }
    
    private void mapAssetInvestigationCases(){
        
        for ( Id caseId : assetInvestigationCasesMap.keySet() ){
            
            ONTAP__Case_Force__c caseForce = assetInvestigationCasesMap.get( caseId );
            
            DRCaseAssetOutboundMessage__e assetInvestigationOutboundMessage = new DRCaseAssetOutboundMessage__e(
                SAPCustomerId__c = caseForce.ONTAP__Account__r.ONTAP__SAPCustomerId__c,
                AssetCode__c = caseForce.Account_Asset__r.ONTAP__Asset_Code__c,
                Description__c = caseForce.ONTAP__Description__c,
                CreatedDate__c = caseForce.CreatedDate,
                CaseForceId__c = caseForce.Id,
                OwnerId__c = caseIdToRouteMap.get( caseForce.Id ),
                CaseOrigin__c = caseForce.ONTAP__Case_Origin__c,
                Type__c = caseForce.Case_Type__c,
                Status__c = caseForce.ONTAP__Status__c,
                CaseNumber__c = caseForce.ONTAP__Case_Number__c,
                LastModifiedDate__c = caseForce.LastModifiedDate
            );           
            
        	assetInvestigationOutboundMessages.add( assetInvestigationOutboundMessage ); 
        }    
        
    }    

    private void mapAssetMovementCases(){
        
        for ( Id caseId : assetMovementCasesMap.keySet() ){
            
            ONTAP__Case_Force__c caseForce = assetMovementCasesMap.get( caseId );
            
            DRCaseAssetMoveOutboundMessage__e assetMovementOutboundMessage = new DRCaseAssetMoveOutboundMessage__e(
                SAPCustomerId__c = caseForce.ONTAP__Account__r.ONTAP__SAPCustomerId__c,
                AssetCode__c = caseForce.Account_Asset__r.ONTAP__Asset_Code__c,
                Description__c = caseForce.ONTAP__Description__c,
                CreatedDate__c = caseForce.CreatedDate,
                CaseForceId__c = caseForce.Id,
                OwnerId__c = caseIdToRouteMap.get( caseForce.Id ),
                CaseOrigin__c = caseForce.ONTAP__Case_Origin__c,
                Type__c = caseForce.Case_Type__c,
                Status__c = caseForce.ONTAP__Status__c,
                CaseNumber__c = caseForce.ONTAP__Case_Number__c,
                LastModifiedDate__c = caseForce.LastModifiedDate
            );           
            
        	assetMovementOutboundMessages.add( assetMovementOutboundMessage ); 
        }    
        
    }   

    private void mapAssetMaintenanceCases(){
        
        for ( Id caseId : assetMaintenanceCasesMap.keySet() ){
            
            ONTAP__Case_Force__c caseForce = assetMaintenanceCasesMap.get( caseId );
            
            System.debug('>>> Case Force:');
            System.debug( caseForce );
            System.debug( '>>> Status: ' +  caseForce.ONTAP__Status__c );
            
            DRCaseAssetMaintenanceOutboundMessage__e assetMaintenanceOutboundMessage = new DRCaseAssetMaintenanceOutboundMessage__e(
                SAPCustomerId__c = caseForce.ONTAP__Account__r.ONTAP__SAPCustomerId__c,
                AssetCode__c = caseForce.Account_Asset__r.ONTAP__Asset_Code__c,
                Description__c = caseForce.ONTAP__Description__c,
                CreatedDate__c = caseForce.CreatedDate,
                CaseForceId__c = caseForce.Id,
                OwnerId__c = caseIdToRouteMap.get( caseForce.Id ),
                CaseOrigin__c = caseForce.ONTAP__Case_Origin__c,
                Type__c = caseForce.Case_Type__c,
                Status__c = caseForce.ONTAP__Status__c,
                CaseNumber__c = caseForce.ONTAP__Case_Number__c,
                LastModifiedDate__c = caseForce.LastModifiedDate
            );           
            
        	assetMaintenanceOutboundMessages.add( assetMaintenanceOutboundMessage ); 
        }    
        
    }  

    private void mapPaymentMethodCases(){
        
        for ( Id caseId : paymentMethodCasesMap.keySet() ){
            
            ONTAP__Case_Force__c caseForce = paymentMethodCasesMap.get( caseId );
            
            Boolean payCash = false;
            Boolean payCheck = false;
            Boolean payTransfer = false;            
            
            if ( caseForce.Payment_Method__c != null ) {
                payCash = caseForce.Payment_Method__c.contains( PAYMENT_CASH );
                payTransfer = caseForce.Payment_Method__c.contains( PAYMENT_TRANSFER );
                payCheck = caseForce.Payment_Method__c.contains( PAYMENT_CHECK );
            }
            
            DRCasePaymentChangeOutboundMessage__e paymentMethodOutboundMessage = new DRCasePaymentChangeOutboundMessage__e(

                CaseForceId__c = caseForce.Id,
                CreatedDate__c = caseForce.CreatedDate,
                Description__c = caseForce.ONTAP__Description__c,
                Origin__c = caseForce.ONTAP__Case_Origin__c,
                OwnerId__c = caseIdToRouteMap.get( caseForce.Id ),
                PayCash__c = payCash,
                PayCheck__c = payCheck,
                PaymentTerms__c = caseForce.Payment_Terms__c,
                PayTransfer__c = payTransfer,
                SAPCustomerId__c = caseForce.ONTAP__Account__r.ONTAP__SAPCustomerId__c,
                Type__c = caseForce.Case_Type__c,
                LastModifiedDate__c = caseForce.LastModifiedDate,
                Status__c = caseForce.ONTAP__Status__c
           
            );           
            
        	paymentMethodOutboundMessages.add( paymentMethodOutboundMessage ); 
        }    
        
    }
    
    private void share(){
        
        System.debug('>>> Executing Share...');
        Map<String,String> caseIdToBDRUserIdMap = this.caseSelector.getCaseIdToBRDIdMap();
        ApexSharingUtils sharing = new ApexSharingUtils( 'ONTAP__Case_Force__Share', 'ParentId', caseIdToBDRUserIdMap);
        sharing.share();
    }
    
}