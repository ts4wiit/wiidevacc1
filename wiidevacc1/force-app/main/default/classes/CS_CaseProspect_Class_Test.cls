/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CaseProspect_Class_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User               Description
 * 24/04/2019      Jose Luis Vargas        Creation of the class for test CS_CaseProspect_Class class
 *                                             
*/

@isTest
public class CS_CaseProspect_Class_Test 
{
    /**
    * Method for insert data
    * @author: jose.l.vargas.lara@accenture.com
    * @param void
    * @return void
    */
    @testsetup
    public static void InsertData()
    {
        Id RecordTypeProspect = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(Label.CS_RT_Prospectos_HN).getRecordTypeId();
        /* Se inserta una cuenta */
        Account oAccount = new Account( Name='AccountTest', ONTAP__Latitude__c= '25.65653', ONTAP__Longitude__c='-100.39177');
        Insert oAccount;
        
        /* Se inserta el prospecto */
        Account oProspect = new Account();
        oProspect.Name = 'Prospect Test';
        oProspect.CS_Last_Name__c = 'Test';
        oProspect.CS_Second_Last_Name__c = 'Test';
        oProspect.ONTAP__LegalName__c = 'Tiendas Unidas';
        oProspect.ISSM_ProspectType__c = 'PN';
        oProspect.CS_Tipo_Identificacion__c = '00';
        oProspect.HONES_IdentificationNumber__c = '90181891';
        oProspect.ONTAP__Mobile_Phone__c = '5549001010';
        oProspect.CS_Tipo_Via__c = 'CL';
        oProspect.ONTAP__Street__c = 'Romero';
        oProspect.ONTAP__Street_Number__c = '12';
        oProspect.CS_Tipo_Poblacion__c = 'Urbano';
        oProspect.CS_Poblacion__c = '06 - Choluteca';
        oProspect.CS_Distrito__c = 'ALUBAREN';
        oProspect.V360_ReferenceClient__c = oAccount.Id;
        oProspect.RecordTypeId = RecordTypeProspect;
        Insert oProspect;
       
        Case oCase = new Case();
        oCase.AccountId = oProspect.Id;   
        Insert oCase;
    }
    
    /**
    * Method for test SendCaseProspect method
    * @author: jose.l.vargas.lara@accenture.com
    * @param void
    * @return void
    */
    @isTest
    public static void test_SendCaseProspect()
    {
        Account oProspect = [SELECT Id FROM Account WHERE Name = 'Prospect Test' LIMIT 1];
        Case oCase = [SELECT Id FROM Case WHERE AccountId =: oProspect.Id];
        CS_CaseProspect_Class.SendCaseProspect(oCase.Id);
    }
    
    /**
    * Method for test SendCaseProspect method with error
    * @author: jose.l.vargas.lara@accenture.com
    * @param void
    * @return void
    */
    @isTest
    public static void test_SendCaseProspectError()
    {
        CS_CaseProspect_Class.SendCaseProspect('183982920');
    }
    
    /**
    * Method for test SendProspectMdWeb method
    * @author: jose.l.vargas.lara@accenture.com
    * @param void
    * @return void
    */
    @isTest
    public static void test_SendProspectMdWeb()
    {
        CS_CaseProspect_Class.SendProspectMdWeb();
    }
}