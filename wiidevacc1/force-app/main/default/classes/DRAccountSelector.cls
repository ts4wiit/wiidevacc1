public class DRAccountSelector {
    
    public static Map<String,String> getAccountIdRecordTypeDeveloperNameMap( List<Id> accountIds ){
        
        Map<String,String> accountIdRecordTypeDeveloperNameMap = new Map<String,String>();
         
        List<Account> accounts = [ SELECT Id, RecordType.DeveloperName FROM Account WHERE Id in : accountIds ];
        
        for( Account account : accounts ){
            
            accountIdRecordTypeDeveloperNameMap.put( account.Id, account.RecordType.DeveloperName );
            
        }
        
        return accountIdRecordTypeDeveloperNameMap;
        
    }

    public static Map<String,String> getAccountIdSAPCustomerIdMap( List<Id> accountIds ){
        
        Map<String,String> accountIdSAPCustomerIdMap = new Map<String,String>();
         
        List<Account> accounts = [ SELECT Id, ONTAP__SAPCustomerId__c FROM Account WHERE Id in : accountIds ];
        
        for( Account account : accounts ){
            
            accountIdSAPCustomerIdMap.put( account.Id, account.ONTAP__SAPCustomerId__c );
            
        }
        
        return accountIdSAPCustomerIdMap;
        
    }
    
    public static List<Account> getAccountsByIds( List<Id> accountIds ){
        
        return [ SELECT Id, Name,ONTAP__Secondary_Phone__c,ONTAP__Neighborhood__c,ONTAP__Email__c,ONTAP__SAPCustomerId__c,Closest_POC_Code__r.ONTAP__SAPCustomerId__c,ONTAP__LegalName__c,
                 ShippingAddress,ONTAP__Latitude__c,ONTAP__Longitude__c,ONTAP__Province__c,ONTAP__District__c,Phone, RecordTypeId, 
                 ONTAP__Contact_First_Name__c,ONTAP__Contact_Last_Name__c,RNC_Number__c,Sic,OwnerId
                FROM Account WHERE Id in : accountIds ];
        
    }
    
    public static Map<Id,Account> getContactIdToAccountMap( List<Id> contactIds ){
        
        Set<Id> accountIds = new Set<Id>();
        Map<Id,Account> accountIdToAccountMap = new Map<Id,Account>();
        Map<Id,Id> accountIdToContactIdMap = new Map<Id,Id>();
        Map<Id,Account> contactIdToAccountMap = new Map<Id,Account>();
        
        List<Contact> contacts = [ SELECT Id, AccountId FROM Contact WHERE Id in : contactIds ];
        
        for ( Contact contact : contacts ){
            
            accountIds.add( contact.AccountId );
            accountIdTocontactIdMap.put( contact.AccountId, contact.Id );
            
        }
        
        List<Account> accounts = [ SELECT Id, RecordType.DeveloperName, ONTAP__SAPCustomerId__c FROM Account WHERE Id in : accountIds ];
        
        for ( Account account : accounts ){
            
            accountIdToAccountMap.put( account.Id, account );
            
        }
        
        for ( Id accountId : accountIdToAccountMap.keySet() ){
            
            contactIdToAccountMap.put( accountIdTocontactIdMap.get( accountId ), accountIdToAccountMap.get( accountId ) );
            
        }
        
        return contactIdToAccountMap;
        
    } 
    
    public static Map<String,String> getAccountIdToCountryMap( List<Id> accountIds ){
        
        Map<String,String> accountIdToCountryMap = new Map<String,String>();
         
        List<Account> accounts = [ SELECT Id, ONTAP__Country__c FROM Account WHERE Id in : accountIds ];
        
        for( Account account : accounts ){
            
            accountIdToCountryMap.put( account.Id, account.ONTAP__Country__c );
            
        }
        
        return accountIdToCountryMap;
        
    }    
    
}