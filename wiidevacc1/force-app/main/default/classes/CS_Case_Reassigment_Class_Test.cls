/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CASE_REASSIGMENT_CLASS.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 28/01/2019        Jose Luis Vargas          Crecion de la clase de testing para CS_CASE_REASSIGMENT_CLASS
 */

@isTest
private class CS_Case_Reassigment_Class_Test
{
    
    /* * Test method to fetch de values of the User
    *Created By: Jose Luis Vargas 
    * Suport contact:  g.martinez.cabral@accenture.com 
    * @param void
    * @return void
    */
    @isTest static void test_fetchLookUpValues()
    {
        CS_CASE_REASSIGMENT_CLASS obj01 = new CS_CASE_REASSIGMENT_CLASS();
        User oNewUser = new User();
        oNewUser.Country__c = 'El Salvador';
        oNewUser.Username= 'juan@acc.com'; 
        oNewUser.LastName= 'Sawuan'; 
        oNewUser.Email= 'juan@acc.com'; 
        oNewUser.Alias= 'Juanis'; 
        oNewUser.CommunityNickname = 'jusuw';
        oNewUser.TimeZoneSidKey= 'America/Bogota'; 
        oNewUser.LocaleSidKey= 'es';
        oNewUser.EmailEncodingKey = 'ISO-8859-1';
        oNewUser.AboutMe = 'yas';
        oNewUser.LanguageLocaleKey = 'es';
        oNewUser.ProfileId= userinfo.getProfileId();
        insert oNewUser;
        
        CS_CASE_REASSIGMENT_CLASS.fetchLookUpValues('Juan', 'User');
    }
    
    /* * Test method to get the info of the Case object
    *Created By: Jose Luis Vargas 
    * Suport contact:  g.martinez.cabral@accenture.com 
    * @param void
    * @return void
    */
    @isTest static void GetCaseInfo()
    {
        Case oNewCase = new Case();
        Insert oNewCase;
        
        CS_CASE_REASSIGMENT_CLASS.GetCaseInfo(oNewCase.Id);
    }
    
    /* * Test method to get the Details of the account
    *Created By: Jose Luis Vargas 
    * Suport contact:  g.martinez.cabral@accenture.com 
    * @param void
    * @return void
    */
    @isTest static void GetDetailAccount()
    {
        Account oNewAccount = new Account();
        oNewAccount.Name = 'Nueva Cuenta';
        Insert oNewAccount;
        
        Case oNewCase = new Case();
        oNewCase.AccountId = oNewAccount.Id;
        Insert oNewCase;
        
        CS_CASE_REASSIGMENT_CLASS.GetDetailAccount(oNewCase.Id);
    }
    
    
    /* * Test method to retrieve the assigned cases
    *Created By: Jose Luis Vargas 
    * Suport contact:  g.martinez.cabral@accenture.com 
    * @param void
    * @return void
    */
    @isTest static void GetCaseAssigned()
    {
       CS_CASE_REASSIGMENT_CLASS.GetCaseAssigned();
    }
    
    /* * Test method to get the info of the interlocutor
    *Created By: Jose Luis Vargas 
    * Suport contact:  g.martinez.cabral@accenture.com 
    * @param void
    * @return void
    */
    @isTest static void GetInterlocutorName()
    {
       Case oNewCase = new Case();
       Insert oNewCase;
        
       CS_CASE_REASSIGMENT_CLASS.GetInterlocutorName('Agente Telventa', oNewCase.Id);
       CS_CASE_REASSIGMENT_CLASS.GetInterlocutorName('Telecobrador', oNewCase.Id);
       CS_CASE_REASSIGMENT_CLASS.GetInterlocutorName('Agente Credito', oNewCase.Id);
       CS_CASE_REASSIGMENT_CLASS.GetInterlocutorName('Representante de Ventas', oNewCase.Id);
       CS_CASE_REASSIGMENT_CLASS.GetInterlocutorName('Jefe Equipo Frio', oNewCase.Id);
       CS_CASE_REASSIGMENT_CLASS.GetInterlocutorName('Jefe Equipo Frio', '02093NDN');
    }
    
    /* * Test method to update the Owner of the Case
    *Created By: Jose Luis Vargas 
    * Suport contact:  g.martinez.cabral@accenture.com 
    * @param void
    * @return void
    */
    @isTest static void UpdateOwnerCase()
    {
        User oNewUser = new User();
        oNewUser.Country__c = 'El Salvador';
        oNewUser.Username= 'juan@acc.com'; 
        oNewUser.LastName= 'Sawuan'; 
        oNewUser.Email= 'juan@acc.com'; 
        oNewUser.Alias= 'Juanis'; 
        oNewUser.CommunityNickname = 'jusuw';
        oNewUser.TimeZoneSidKey= 'America/Bogota'; 
        oNewUser.LocaleSidKey= 'es';
        oNewUser.EmailEncodingKey = 'ISO-8859-1';
        oNewUser.AboutMe = 'yas';
        oNewUser.LanguageLocaleKey = 'es';
        oNewUser.ProfileId= userinfo.getProfileId();
        insert oNewUser;
        
        ISSM_TypificationMatrix__c oNewTypification = new ISSM_TypificationMatrix__c();
        oNewTypification.ISSM_TypificationLevel1__c = 'SAC';
        oNewTypification.ISSM_TypificationLevel2__c = 'Solicitudes';
        oNewTypification.ISSM_TypificationLevel3__c = 'Equipo Frío';
        oNewTypification.ISSM_TypificationLevel4__c = 'Reparación';
        oNewTypification.ISSM_Countries_ABInBev__c = 'Honduras';
        oNewTypification.CS_Dias_Primer_Contacto__c = 2;
        oNewTypification.ISSM_CaseRecordType__c = 'ISSM_GeneralCase';
        oNewTypification.ISSM_AssignedTo__c = 'User';
        oNewTypification.ISSM_OwnerUser__c =oNewUser.Id ;
        oNewTypification.ISSM_Priority__c = 'Medium';
        oNewTypification.CS_Automatic_Closing_Case__c = true;
        oNewTypification.ISSM_OwnerQueue__c = 'WithOut Owner';
        oNewTypification.ISSM_IdQueue__c = '';         
        oNewTypification.ISSM_Email1CommunicationLevel3__c = '';
        oNewTypification.ISSM_Email1CommunicationLevel4__c = '';
        oNewTypification.ISSM_Email2CommunicationLevel3__c = '';
        oNewTypification.ISSM_Email2CommunicationLevel4__c = '';
        oNewTypification.ISSM_Email3CommunicationLevel3__c = '';
        oNewTypification.ISSM_Email3CommunicationLevel4__c = '';
        oNewTypification.ISSM_Email4CommunicationLevel4__c = '';
        oNewTypification.ISSM_CaseClosedSendLetter__c = 'No';
        oNewTypification.CS_Days_to_End__c=3;
        Insert oNewTypification;
        
       Case oNewCase = new Case();
       oNewCase.HONES_Case_Country__c = 'Honduras';
       oNewCase.ISSM_TypificationNumber__c = oNewTypification.Id;
       Insert oNewCase;
       
       ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
       Insert oNewCaseForce;
        
       CS_CASE_REASSIGMENT_CLASS.UpdateOwnerCase(oNewCase.Id, oNewCaseForce.Id, oNewUser.Id, 'Agente Telventa');
       CS_CASE_REASSIGMENT_CLASS.UpdateOwnerCase(oNewCase.Id, oNewCaseForce.Id, oNewUser.Id, 'Telecobrador');
       CS_CASE_REASSIGMENT_CLASS.UpdateOwnerCase(oNewCase.Id, oNewCaseForce.Id, oNewUser.Id, 'Agente Credito');
       CS_CASE_REASSIGMENT_CLASS.UpdateOwnerCase(oNewCase.Id, oNewCaseForce.Id, oNewUser.Id, 'Representante de Ventas');
       CS_CASE_REASSIGMENT_CLASS.UpdateOwnerCase(oNewCase.Id, oNewCaseForce.Id, oNewUser.Id, 'Jefe Equipo Frio');
       CS_CASE_REASSIGMENT_CLASS.UpdateOwnerCase(oNewCase.Id, oNewCaseForce.Id, oNewUser.Id, 'Usuario');
    }
}