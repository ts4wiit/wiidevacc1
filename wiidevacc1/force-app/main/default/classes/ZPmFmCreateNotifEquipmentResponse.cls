public class ZPmFmCreateNotifEquipmentResponse {
  
    public class ItemAtAviso {
    public String NotifNo {get;set;}   
    public String Equipment {get;set;}
      public ItemAtAviso(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'NotifNo') {
                            NotifNo = parser.getText() ;
                        }
                        if (text == 'Equipment') {
                            Equipment = parser.getText() ;
                        }
                    }
                }
            }
        }
    }

public EtAvisos EtAvisos {get;set;}    

    public ZPmFmCreateNotifEquipmentResponse(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                System.debug('texto' + text);
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'EtAvisos') {
                        EtAvisos = new EtAvisos(parser);
                    }
                }
            }
        }
    }

    public static ZPmFmCreateNotifEquipmentResponse parse(String json) {
        System.debug(json);
        System.JSONParser parser = System.JSON.createParser(json);
        return new ZPmFmCreateNotifEquipmentResponse(parser);
    }
   /**
   * Method to consume the objects inside de Json (token iteration)
   * Created By: heron.zurita@accenture.com
   * @param JSONParser json
   * @return void
   */
    public class EtAvisos {
        public ItemAtAviso ItemAtAviso {get;set;}
        public EtAvisos(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'item') {
                            ItemAtAviso = new ItemAtAviso(parser);
                        }
                    }
                }
            }
        }
    }
}