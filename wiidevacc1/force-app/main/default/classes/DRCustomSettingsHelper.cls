public class DRCustomSettingsHelper {
    
    
    public static String getIntegrationUserId(){
        
        String userId; 
        List<DRIntegrationUser__c> users = new List<DRIntegrationUser__c>();
        users = [ SELECT UserId__c FROM DRIntegrationUser__c ];
            
        if ( users.size() > 0 ){  
                
                return users.get(0).UserId__c;
                
        } else {
                
                return null;
                
        }           
        
    }

}