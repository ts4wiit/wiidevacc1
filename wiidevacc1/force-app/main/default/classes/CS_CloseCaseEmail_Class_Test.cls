/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CLOSECASEEMAIL_CLASS_Test.apxc
 * Version: 1.0.0.0
 *  
 * 
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 12/02/2019           Gabriel E Garcia       Test class of CS_CLOSECASEEMAIL_CLASS 
 */
@isTest
private class CS_CloseCaseEmail_Class_Test 
{
     /**
    * Method Config Setup
    * @author: gabriel.e.garcia@accenture.com
    * @param Void
    * @return Void
    */
    @testSetup static void setup() {
        Account accountTest = new Account(Name = 'Account Test');   
        insert accountTest;                     
            
        Case caseTest = new Case();
        caseTest.Accountid = accountTest.Id;
        caseTest.Description = 'Test Description';
        caseTest.Status = 'New';
        caseTest.Subject = 'Test Subject';
        insert caseTest;
    }
	/**
    * Method for test the method handleInboundEmail success case
    * @author: gabriel.e.garcia@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_handleInboundEmail_Succes()
    {
        Case caseTest = [SELECT Id, CaseNumber FROM Case LIMIT 1];
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        
        email.subject = 'Email Close case test';
        email.fromName = 'test name';
        email.plainTextBody = 'Case:#' + caseTest.CaseNumber + '\nSolution: Close test';
        envelope.fromAddress = 'emailsupport@test.com';
               
        CS_CloseCaseEmail_Class cce = new CS_CloseCaseEmail_Class();
        Messaging.InboundEmailResult result = cce.handleInboundEmail(email, envelope);
        System.assertEquals(result.success  ,true);    
    }
    
    /**
    * Method for test the method handleInboundEmail fail case 1
    * @author: gabriel.e.garcia@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_handleInboundEmail_Fail1()
    {
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        
        email.subject = 'Email Close case test';
        email.fromName = 'test name';
        email.plainTextBody = 'Solution: Close test';
        envelope.fromAddress = 'emailsupport@test.com';
               
        CS_CloseCaseEmail_Class cce = new CS_CloseCaseEmail_Class();
        Messaging.InboundEmailResult result = cce.handleInboundEmail(email, envelope);
        System.assertEquals(result.success, false);    
    }
    
    /**
    * Method for test the method handleInboundEmail fail case 2
    * @author: gabriel.e.garcia@accenture.com
    * @param Void
    * @return Void
    */
    @isTest static void test_handleInboundEmail_Fail2()
    {
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        
        email.subject = 'Email Close case test';
        email.fromName = 'test name';
        email.plainTextBody = 'Case:#0000X000\nSolution: Close test';
        envelope.fromAddress = 'emailsupport@test.com';
               
        CS_CloseCaseEmail_Class cce = new CS_CloseCaseEmail_Class();
        Messaging.InboundEmailResult result = cce.handleInboundEmail(email, envelope);
        System.assertEquals(result.success, false);    
    }        
}