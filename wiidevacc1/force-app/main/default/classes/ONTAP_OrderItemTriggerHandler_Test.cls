/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap - OnCall
 * ----------------------------------------------------------------------------
 * Clase: ONTAP_OrderItemTriggerHandler_Test.apxc
 * Versión: 1.0.0.0
 *  
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario            Contacto      						Descripción
 * 11/01/ 2019     Oscar Garcia		  o.a.garcia.martinez_accenture.com     Test methods for ONTAP_OrderItemTriggerHandler
 */

@isTest
private class ONTAP_OrderItemTriggerHandler_Test{
    
    /**
    * test method which test the trigger after insert an Order Item
    * Created By: fernando.engel.funes@accenture.com 
    * @param void
    * @return void
    */
  @isTest static void test_afterInsert_UseCase1(){
    List<ONTAP__Order_Item__c> orderItems = new List<ONTAP__Order_Item__c>();
    ONTAP__Order_Item__c orderItem = new ONTAP__Order_Item__c();
    orderItems.add(orderItem);
    insert orderItems;
    ONTAP_OrderItemTriggerHandler obj01 = new ONTAP_OrderItemTriggerHandler();
  }
    
    /**
    * test method which test the trigger after update an Order Item
    * Created By: fernando.engel.funes@accenture.com 
    * @param void
    * @return void
    */
  @isTest static void test_afterUpdate_UseCase1(){
    ONTAP_OrderItemTriggerHandler obj01 = new ONTAP_OrderItemTriggerHandler();
    obj01.afterUpdate();
  }
    
    /**
    * test method which test the trigger before insert an Order Item
    * Created By: fernando.engel.funes@accenture.com 
    * @param void
    * @return void
    */
  @isTest static void test_beforeInsert_UseCase1(){
    ONTAP_OrderItemTriggerHandler obj01 = new ONTAP_OrderItemTriggerHandler();
    obj01.beforeInsert();
  }
    
    /**
    * test method which test the trigger before update an Order Item
    * Created By: fernando.engel.funes@accenture.com 
    * @param void
    * @return void
    */
  @isTest static void test_beforeUpdate_UseCase1(){
    ONTAP_OrderItemTriggerHandler obj01 = new ONTAP_OrderItemTriggerHandler();
    obj01.beforeUpdate();
  }
}