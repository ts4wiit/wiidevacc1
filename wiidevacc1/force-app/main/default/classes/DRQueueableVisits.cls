/**
 * Created by Dejair.Junior on 10/3/2019.
 */

public with sharing class DRQueueableVisits implements Queueable {

    private static Time START_TIME = Time.newInstance(7, 0, 0, 0);
    private static Integer VISIT_DURATION = 15;
    private static final String EVENT_TYPE = 'Visita Planificada';
    private static Integer NUMBER_OF_DAYS_UPFRONT = 7;
    private static final String EVENT_STATUS = 'Abierto';

    private List<DOVisitPlan__c> visitPlans = new List<DOVisitPlan__c>();
    private List<String> weekdays = new List<String>{'Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'};
    private List<Integer> weekdayNumbers = new List<Integer>{1,2,4,8,16,32,64};
    private Set<String> sapIdsSet = new Set<String>();
    private User executionUser = new User();
    public Map<Integer,String> weekdaysMap = new Map<Integer,String>();
    private List<Event> events = new List<Event>();
    private Date currentDate;
    private Map<String,String> sapToAccountNameMap = new Map<String,String>();
    private Map<String,Id> sapToIdMap = new Map<String,Id>();
    private List<Account> accounts = new List<Account>();
    private Map<String,List<DOVisitPlanWrapper>> weekdayToVisitPlansMap = new Map<String,List<DOVisitPlanWrapper>>();
    private List<VisitDate> visitDates = new List<VisitDate>();
    private List<Event> eventsToDelete = new List<Event>();
    private Boolean isToggleOn;
    private List<DOVisit__c> dummyVisits = new List<DOVisit__c>();
    private List<DOVisit__c> dummyVisitsToDelete = new List<DOVisit__c>();

    public DRQueueableVisits( User executionUser ){
        this.executionUser = executionUser;
        currentDate = Date.today();
        isToggleOn = isToggleOn();
    }

    public void execute( QueueableContext context ){

        if(isToggleOn){
            clearVisits();
        } else {
            clearDummyVisits();
        }
        setWeekdaysMap();
        setVisitDates();
        selectVisitPlans();
        setupAccounts();
        mapVisitPlans();
        prepareVisits();
        createVisits();
        if (isToggleOn){
            insertVisits();
        } else {
            insertDummyVisits();
        }

    }

    private class DOVisitPlanWrapper {

        Id visitPlanId;
        Date startDate;
        Date endDate;
        Integer frequency;
        Id userId;
        Id accountId;
        Integer sequence;
        String weekday;
        String pocName;
        Integer weekNumber;

    }

    private class VisitDate {

        String weekday;
        Date visitDate;
        Integer weekNumber;

        public VisitDate( Date visitDate ){

            Datetime visitDatetime = visitDate;
            if(Test.isRunningTest()){
                visitDatetime = visitDatetime;
            } else {
                visitDatetime = visitDatetime.addHours(12);
            }

            System.debug('>>> Visit datetime ' + visitDatetime );
            this.visitDate = visitDate;
            this.weekday = visitDatetime.format('EEEE');
            System.debug('>>> Visit weekday ' + weekday );
            this.weekNumber = DRVisitHelper.getWeekNumber( visitDate );

        }

    }

    private DOVisitPlanWrapper newVisitPlanWrapper( DOVisitPlan__c visitPlan ){

        DOVisitPlanWrapper visitPlanWrapper = new DOVisitPlanWrapper();

        visitPlanWrapper.visitPlanId = visitPlan.Id;
        visitPlanWrapper.startDate = visitPlan.StartDate__c;
        visitPlanWrapper.endDate = visitPlan.EndDate__c;
        visitPlanWrapper.frequency = (Integer) visitPlan.Recurrence__c;
        visitPlanWrapper.sequence = (Integer) visitPlan.Sequence__c;
        visitPlanWrapper.weekday = weekdaysMap.get( (Integer) visitPlan.Weekday__c );
        visitPlanWrapper.userId = executionUser.Id;
        visitPlanWrapper.accountId = sapToIdMap.get( visitPlan.SAPCustomerID__c );
        visitPlanWrapper.pocName = sapToAccountNameMap.get( visitPlan.SAPCustomerID__c );
        visitPlanWrapper.weekNumber = DRVisitHelper.getWeekNumber( visitPlan.StartDate__c );

        return visitPlanWrapper;

    }

    private void setWeekdaysMap(){

        Integer i = 0;

        for( String weekday : weekdays ){

            weekdaysMap.put( weekdayNumbers.get( i ), weekday );
            i++;

        }

    }

    private void selectVisitPlans(){

        visitPlans =   [ SELECT CreatedById
                    ,CreatedDate
                    ,EndDate__c
                    ,External_ID__c
                    ,Id
                    ,IsActive__c
                    ,Name
                    ,OwnerId
                    ,Recurrence__c
                    ,Route__c
                    ,SAPCustomerID__c
                    ,Sequence__c
                    ,StartDate__c
                    ,SystemModstamp
                    ,Weekday__c
            FROM DOVisitPlan__c
            WHERE IsActive__c = true
            AND Route__c = : this.executionUser.User_Route__c
            ORDER BY Weekday__c,Sequence__c ];


    }

    private void prepareVisits(){

        for( VisitDate visitDate : visitDates ){

            System.debug('*************************************************************************************');
            System.debug('>>> Processing ' + visitDate.weekday + '  day ' +  visitDate.visitDate);
            List<DOVisitPlanWrapper> weekdayItineraries =  weekdayToVisitPlansMap.get( visitDate.weekday );
            List<DOVisitPlanWrapper> weekdayItinerariesToRemove =  new List<DOVisitPlanWrapper>();
            System.debug('>>> Initial list size ' + weekdayItineraries.size());

            //Iterates to identify items tha must be ignored
            for( DOVisitPlanWrapper weekdayItinerary :  weekdayItineraries ){

                Boolean isVisitOk = checkVisit( weekdayItinerary,visitDate );

                if ( !isVisitOk ){

                    weekdayItinerariesToRemove.add( weekdayItinerary );

                }

            }

            //Iterates to remove items
            for( DOVisitPlanWrapper itineraryToRemove :  weekdayItinerariesToRemove ){

                weekdayItineraries.remove( weekdayItineraries.indexOf( itineraryToRemove ) );

            }

            System.debug('>>> Final list size ' + weekdayItineraries.size());

        }

    }

    private void createVisits(){

        for( VisitDate visitDate : visitDates ){

            List<DOVisitPlanWrapper> weekdayItineraries =  weekdayToVisitPlansMap.get( visitDate.weekday );

            DateTime startDateTime =  Datetime.newInstance( visitDate.visitDate , START_TIME );

            for( DOVisitPlanWrapper weekdayItinerary :  weekdayItineraries ){

                if (isToggleOn){
                    Event visit = createVisitFromItinerary( weekdayItinerary, startDateTime );
                    System.debug('>>> Visit: ' + visit );
                    events.add( visit );
                } else {
                    DOVisit__c visit = createDummyVisitFromItinerary( weekdayItinerary, startDateTime );
                    System.debug('>>> Visit: ' + visit );
                    dummyVisits.add( visit );
                }

                startDateTime = startDateTime.addMinutes( VISIT_DURATION );

            }

        }

    }

    private  void insertDummyVisits(){

        Database.SaveResult[] results = Database.insert(dummyVisits, false);

        for (Database.SaveResult result : results) {
            if (result.isSuccess()) {
                System.debug('>>> Dummy visit created: ' + result.getId());
            } else {
                for (Database.Error err : result.getErrors()) {
                    System.debug('>>> The following error has occurred:');
                    System.debug('>>> ' + err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('>>> Fields: ' + err.getFields());
                }
            }
        }

    }

    private void insertVisits() {

        Database.SaveResult[] results = Database.insert(events, false);

        for (Database.SaveResult result : results) {
            if (result.isSuccess()) {
                System.debug('>>> Visit created: ' + result.getId());
            } else {
                for (Database.Error err : result.getErrors()) {
                    System.debug('>>> The following error has occurred:');
                    System.debug('>>> ' + err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('>>> Fields: ' + err.getFields());
                }
            }
        }
    }

    private Event createVisitFromItinerary( DOVisitPlanWrapper weekdayItinerary, Datetime visitDateTime ){

        Event visit = new Event(

                WhatId = weekdayItinerary.accountId,
                OwnerId = weekdayItinerary.userId,
                ActivityDateTime = visitDateTime,
                DurationInMinutes = VISIT_DURATION,
                IsRecurrence = false,
                ONTAP__Sequence__c = weekdayItinerary.sequence,
                Subject = weekdayItinerary.pocName,
                IsAllDayEvent = false,
                ONTAP__Event_Type__c = EVENT_TYPE
        );

        return visit;

    }

    private DOVisit__c createDummyVisitFromItinerary( DOVisitPlanWrapper weekdayItinerary, Datetime visitDateTime ){

        DOVisit__c visit = new DOVisit__c (

                BDR__c = weekdayItinerary.userId,
                DOVisitPlan__c = weekdayItinerary.visitPlanId,
                POC__c = weekdayItinerary.accountId,
                Sequence__c = weekdayItinerary.sequence,
                VisitDateTime__c = visitDateTime,
                Name = weekdayItinerary.pocName

        );

        return visit;

    }

    private void mapVisitPlans(){

        List<DOVisitPlanWrapper> sundayWrappers = new List<DOVisitPlanWrapper>();
        List<DOVisitPlanWrapper> mondayWrappers = new List<DOVisitPlanWrapper>();
        List<DOVisitPlanWrapper> tuesdayWrappers = new List<DOVisitPlanWrapper>();
        List<DOVisitPlanWrapper> wednesdayWrappers = new List<DOVisitPlanWrapper>();
        List<DOVisitPlanWrapper> thursdayWrappers = new List<DOVisitPlanWrapper>();
        List<DOVisitPlanWrapper> fridayWrappers = new List<DOVisitPlanWrapper>();
        List<DOVisitPlanWrapper> saturdayWrappers = new List<DOVisitPlanWrapper>();

        for ( DOVisitPlan__c visitPlan : visitPlans ){

            DOVisitPlanWrapper visitPlanWrapper = new DOVisitPlanWrapper();
            visitPlanWrapper = newVisitPlanWrapper( visitPlan );

            switch on visitPlanWrapper.weekday {
                when 'Sunday' {
                    sundayWrappers.add( visitPlanWrapper );
                }
                when 'Monday' {
                    mondayWrappers.add( visitPlanWrapper );
                }
                when 'Tuesday' {
                    tuesdayWrappers.add( visitPlanWrapper );
                }
                when 'Wednesday' {
                    wednesdayWrappers.add( visitPlanWrapper );
                }
                when 'Thursday' {
                    thursdayWrappers.add( visitPlanWrapper );
                }
                when 'Friday' {
                    fridayWrappers.add( visitPlanWrapper );
                }
                when 'Saturday'  {
                    saturdayWrappers.add( visitPlanWrapper );
                }
            }

        }

        weekdayToVisitPlansMap.put( 'Sunday',sundayWrappers );
        weekdayToVisitPlansMap.put( 'Monday',mondayWrappers );
        weekdayToVisitPlansMap.put( 'Tuesday',tuesdayWrappers );
        weekdayToVisitPlansMap.put( 'Wednesday',wednesdayWrappers );
        weekdayToVisitPlansMap.put( 'Thursday',thursdayWrappers );
        weekdayToVisitPlansMap.put( 'Friday',fridayWrappers );
        weekdayToVisitPlansMap.put( 'Saturday',saturdayWrappers );

    }

    private void setVisitDates(){

        Date today = Date.today();

        Date myDate;

        for( Integer i = 0; i < NUMBER_OF_DAYS_UPFRONT ; i++  ){

            myDate = today.addDays( i );
            visitDates.add( new VisitDate( myDate ) );

        }

    }

    private void setupAccounts(){

        getSets();
        getAccounts();
        getAccountsMap();

    }

    private void getSets(){

        for( DOVisitPlan__c visitPlan : visitPlans ){

            sapIdsSet.add( visitPlan.SAPCustomerID__c );

        }

    }

    private void getAccounts(){

        accounts = [ SELECT Id,ONTAP__SAPCustomerId__c,Name FROM Account WHERE ONTAP__SAPCustomerId__c in : sapIdsSet  ];

    }

    private void getAccountsMap(){

        for( Account account : accounts ){

            sapToAccountNameMap.put( account.ONTAP__SAPCustomerId__c,account.Name );
            sapToIdMap.put( account.ONTAP__SAPCustomerId__c,account.Id );

        }

    }

    private Boolean checkVisit( DOVisitPlanWrapper visitPlanWrapper, VisitDate visitDate ){

        if ( visitPlanWrapper.frequency == 1 ){

            return true;

        } else {

            if( ( DRVisitHelper.isEven(visitPlanWrapper.weekNumber) && DRVisitHelper.isEven( visitDate.weekNumber ) ) ||
                    (!DRVisitHelper.isEven(visitPlanWrapper.weekNumber) && !DRVisitHelper.isEven( visitDate.weekNumber )) ){

                return true;

            } else {

                return false;

            }

        }

    }

    private void clearVisits(){

        selectFutureVisits();
        deleteFutureVisits();

    }

    private void selectFutureVisits(){

        eventsToDelete = [ SELECT Id
        FROM Event
        WHERE ONTAP__Event_Type__c = : EVENT_TYPE
        AND ActivityDate >= TODAY
        AND ONTAP__Estado_de_visita__c  = : EVENT_STATUS
        AND OwnerId = : executionUser.Id ];

    }

    private void deleteFutureVisits(){

        Database.DeleteResult[] results = Database.delete( eventsToDelete );

        for ( Database.DeleteResult result : results ) {
            if (result.isSuccess()) {
                System.debug('>>> Visit removed: ' + result.getId());
            } else {
                for ( Database.Error err : result.getErrors() ) {
                    System.debug('>>> The following error has occurred:');
                    System.debug('>>> ' + err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('>>> Fields: ' + err.getFields());
                }
            }

        }

    }

    private void clearDummyVisits(){
        selectDummyVisits();
        deleteDummyVisits();
    }

    private void selectDummyVisits(){
        dummyVisitsToDelete = [SELECT Id FROM DOVisit__c WHERE BDR__c =: executionUser.Id];
    }

    private void deleteDummyVisits(){

        Database.DeleteResult[] results = Database.delete( dummyVisitsToDelete );

        for ( Database.DeleteResult result : results ) {
            if (result.isSuccess()) {
                System.debug('>>> Dummy visit removed: ' + result.getId());
            } else {
                for ( Database.Error err : result.getErrors() ) {
                    System.debug('>>> The following error has occurred:');
                    System.debug('>>> ' + err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('>>> Fields: ' + err.getFields());
                }
            }

        }

    }

    private Boolean isToggleOn(){

        try{
            return [ SELECT IsActive__c FROM DR_Visit_Toggle__mdt WHERE DeveloperName = 'DO'].get(0).IsActive__c;
        } catch (Exception e){
            return false;
        }

    }

}