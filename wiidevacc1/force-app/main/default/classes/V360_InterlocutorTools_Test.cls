/* ----------------------------------------------------------------------------
* AB InBev :: 360 View
* ----------------------------------------------------------------------------
* Clase: V360_InterlocutorTools.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 12/02/2018     Gerardo Martinez        Creation of methods.
*/
@isTest public class V360_InterlocutorTools_Test {
	
    
  /**
    * Method wich test the asgination of interlocutor assignation by customer
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest public static void AssignInterlocutorBeforeUpdate(){
        User u = [SELECT Id from User LIMIT 1];
        
        Id salesOrgRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ORG_RECORDTYPE_NAME).getRecordTypeId();
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_OFFICE_RECORDTYPE_NAME).getRecordTypeId();
        Id clientGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.CLIENT_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
        
        Account og =  new Account(Name='OG',ONTAP__ExternalKey__c='OG',RecordTypeId=salesOrgRecordTypeId);
        insert og;
        Account off =  new Account(Name='OF',ONTAP__ExternalKey__c='OF',RecordTypeId=salesOfficeRecordTypeId, ParentId=og.Id, V360_Director__c=u.Id);
        insert off;
        Account cg =  new Account(Name='CG',ONTAP__ExternalKey__c='CG',RecordTypeId=clientGroupdevRecordTypeId, ParentId=off.Id,V360_Manager__c=u.Id);
        insert cg;
        Account sg =  new Account(Name='SG',ONTAP__ExternalKey__c='SG',RecordTypeId=salesGroupdevRecordTypeId, ParentId=cg.Id,V360_TeamLead__c=u.Id);
        insert sg;
        Account szz =  new Account(Name='SZ',ONTAP__ExternalKey__c='SZ',RecordTypeId=salesZonedevRecordTypeId, ParentId=sg.Id);
        insert szz;

		V360_SalerPerZone__c sz = new V360_SalerPerZone__c(V360_SalesZone__c=szz.id,V360_User__c=u.Id);
        insert sz;
        
        Account acc = new Account(Name='Test',ONTAP__SAP_Number__c='1234567890',ONTAP__SalesOgId__c = GlobalStrings.HONDURAS_ORG_CODE, V360_SalesZoneAssignedBDR__c=sz.Id, V360_SalesZoneAssignedTelesaler__c=sz.Id,V360_SalesZoneAssignedPresaler__c = sz.Id, V360_SalesZoneAssignedCoolers__c = sz.Id, V360_SalesZoneAssignedCredit__c = sz.Id,V360_SalesZoneAssignedTellecolector__c = sz.Id);

        insert acc;
        
        Map<Id,Account> accMap = new  Map<Id,Account>();
        accMap.put(acc.Id,acc);
        
        V360_InterlocutorTools.AssignInterlocutorBeforeUpdate(accMap.values());
        
    }
    
    /**
    * Method wich test the asgination of interlocutor when no zone is assigned to a customer it suppoused to do nothing
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest public static void NoAssignInterlocutorBeforeUpdate(){
        User u = [SELECT Id from User LIMIT 1];
        
        Id salesOrgRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ORG_RECORDTYPE_NAME).getRecordTypeId();
        Id salesOfficeRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_OFFICE_RECORDTYPE_NAME).getRecordTypeId();
        Id clientGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.CLIENT_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesGroupdevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_GROUP_RECORDTYPE_NAME).getRecordTypeId();
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
        
        Account og =  new Account(Name='OG',ONTAP__ExternalKey__c='OG',RecordTypeId=salesOrgRecordTypeId);
        insert og;
        Account off =  new Account(Name='OF',ONTAP__ExternalKey__c='OF',RecordTypeId=salesOfficeRecordTypeId, ParentId=og.Id, V360_Director__c=u.Id);
        insert off;
        Account cg =  new Account(Name='CG',ONTAP__ExternalKey__c='CG',RecordTypeId=clientGroupdevRecordTypeId, ParentId=off.Id,V360_Manager__c=u.Id);
        insert cg;
        Account sg =  new Account(Name='SG',ONTAP__ExternalKey__c='SG',RecordTypeId=salesGroupdevRecordTypeId, ParentId=cg.Id,V360_TeamLead__c=u.Id);
        insert sg;
        Account szz =  new Account(Name='SZ',ONTAP__ExternalKey__c='SZ',RecordTypeId=salesZonedevRecordTypeId, ParentId=sg.Id);
        insert szz;

		V360_SalerPerZone__c sz = new V360_SalerPerZone__c(V360_SalesZone__c=szz.id,V360_User__c=u.Id);
        insert sz;
        
        Account acc = new Account(Name='Test',ONTAP__SAP_Number__c='1234567890',ONTAP__SalesOgId__c = GlobalStrings.HONDURAS_ORG_CODE);

        insert acc;
        
        Map<Id,Account> accMap = new  Map<Id,Account>();
        accMap.put(acc.Id,acc);
        
        V360_InterlocutorTools.AssignInterlocutorBeforeUpdate(accMap.values());
        
    }
}