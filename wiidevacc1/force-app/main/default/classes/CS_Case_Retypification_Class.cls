/* -----------------------------------------------------------------------------------------------
* AB InBev :: Customer Service
* -----------------------------------------------------------------------------------------------
* Class: CS_COOLERS_CALLOUT_cls.apxc
* Version: 1.0.0.0
*  
* Change History
* -----------------------------------------------------------------------------------------------
* Date                 User                   Description
* 22/02/2019           Debbie Zacarias        Creation of the class for lightning component control
*/

public class CS_Case_Retypification_Class 
{
    public CS_Case_Retypification_Class(){}
    
    /**
    * Method for consulting the case details
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return Case with typifications
    */
    @AuraEnabled
    public static Case GetCaseTypification(String idCase)
    {
        
        Case caso = ([SELECT id, CaseNumber, ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_TypificationLevel3__c, ISSM_TypificationLevel4__c 
                      FROM Case 
                      WHERE Id=:idCase]);    
        return caso;
    }
    
    /** 
    * Method for consulting the level 1 for typification for country
    * @author: d.zacarias.cantillo@accenture.com
    * @param Void
    * @return List with the typification level 1 for country
    */
    @AuraEnabled
    public static List<CS_CASETYPIFICATION_CLASS.PicklistValues> GetTypificationN1()
    {
        return CS_CASETYPIFICATION_CLASS.GetTypificationN1();
    }
    
    /**
    * Method for consulting the level 2 for typification for country
    * @author: d.zacarias.cantillo@accenture.com
    * @param Typification selected for level 1
    * @return List with the typification level 2 for country
    */
    @AuraEnabled
    public static List<CS_CASETYPIFICATION_CLASS.PicklistValues> GetTypificationN2(string typificationNivel1)
    {
        return CS_CASETYPIFICATION_CLASS.GetTypificationN2(typificationNivel1);
    }
    
    /**
    * Method for consulting the level 3 for typification for country
    * @author: d.zacarias.cantillo@accenture.com
    * @param Typification selected for level 1, typification selected for level 2
    * @return List with the typification level 3 for country
    */
    @AuraEnabled
    public static List<CS_CASETYPIFICATION_CLASS.PicklistValues> GetTypificationN3(string typificationNivel1, string typificationNivel2)
    {
        return CS_CASETYPIFICATION_CLASS.GetTypificationN3(typificationNivel1, typificationNivel2);
    }
    
    /**
    * Method for consulting the level 4 for typification for country
    * @author: d.zacarias.cantillo@accenture.com
    * @param Typification selected for level 1, typification selected for level 2, typification selected for level 3
    * @return List with the typification level 4 for country
    */
    @AuraEnabled
    public static List<CS_CASETYPIFICATION_CLASS.PicklistValues> GetTypificationN4(string typificationNivel1, string typificationNivel2, string typificationNivel3)
    {
        return CS_CASETYPIFICATION_CLASS.GetTypificationN4(typificationNivel1, typificationNivel2, typificationNivel3);
    }
    
    /**
    * Method for update the typification case
    * @author: d.zacarias.cantillo@accenture.com
    * @param Typifications selected for each level 
    * @return Boolean to know if the process was successfully
    */
    @AuraEnabled
    public static Boolean UpdateTypificationCase(String idCase,String typificationN1, String typificationN2, String typificationN3, String typificationN4)
    {
        Boolean updated = true;
        
        try
        {  
            User userCount = [SELECT Country__c 
                              FROM USER
                              WHERE Id =: UserInfo.getUserId()];
            
            ISSM_TypificationMatrix__c typificationDetail = new ISSM_TypificationMatrix__c();
            CS_CASEGENERATION_CLASS csGen = new CS_CASEGENERATION_CLASS();
            String userCountry = userCount.Country__c;
            string idRecordType = '';
            DateTime fechaCierre = Date.today();          
            
            typificationDetail = csGen.GetTypificationDetail(typificationN1, typificationN2, typificationN3, typificationN4, userCountry);
            
            String recordTypeName = typificationDetail.ISSM_CaseRecordType__c;
            System.debug(recordTypeName);
            idRecordType =csGen.GetRecordTypeId(recordTypeName);
            
            integer daysToAdd = (Integer)typificationDetail.CS_Days_to_End__c;
            fechaCierre = fechaCierre.addDays(daysToAdd);
            
            Case caseToUpdate = ([SELECT Id, ISSM_TypificationLevel1__c, ISSM_TypificationLevel2__c, ISSM_TypificationLevel3__c, ISSM_TypificationLevel4__c, RecordTypeID, ISSM_TypificationNumber__c,
                                  EntitlementId, Priority, OwnerId, ISSM_OwnerCaseForce__c, ISSM_FirstUserOwner__c, ISSM_TypeSponsorship__c, ISSM_CaseClosedSendLetter__c, CS_Automatic_Closing_Case__c,
                                  Expected_Completion_Date__c, Aplica_encuesta__c, AccountId
                                  FROM Case
                                  WHERE Id=:idCase]);
                        
            Account caseAccount = [SELECT ONTAP__Email__c, OwnerId, ONTAP__Street__c, ONTAP__Neighborhood__c, ONTAP__Municipality__c
                                    FROM Account
                                    WHERE Id =: caseToUpdate.AccountId
                                    LIMIT 1]; 
            csGen.GetOwnerCase(typificationDetail, caseAccount);
            
            caseToUpdate.ISSM_TypificationLevel1__c = typificationN1;
            caseToUpdate.ISSM_TypificationLevel2__c = typificationN2;
            caseToUpdate.ISSM_TypificationLevel3__c = typificationN3;
            caseToUpdate.ISSM_TypificationLevel4__c = typificationN4;
            caseToUpdate.RecordTypeID = idRecordType;
            caseToUpdate.ISSM_TypificationNumber__c = typificationDetail.Id;
            caseToUpdate.EntitlementId = typificationDetail.ISSM_Entitlement__c;
            caseToUpdate.Priority = typificationDetail.ISSM_Priority__c;
            caseToUpdate.OwnerId = csGen.IdOwnerStd;
            caseToUpdate.ISSM_OwnerCaseForce__c = csGen.IdOwnerForce;
            caseToUpdate.ISSM_FirstUserOwner__c = csGen.IdOwnerForce;
            caseToUpdate.ISSM_TypeSponsorship__c = typificationDetail.ISSM_AssignedTo__c;
            caseToUpdate.Subject = 'Caso: ' + typificationN1 + ' | ' + typificationN2 + ' | ' + typificationN3 + ' | ' + typificationN4;
            caseToUpdate.ISSM_CaseClosedSendLetter__c = typificationDetail.ISSM_CaseClosedSendLetter__c;
            caseToUpdate.CS_Automatic_Closing_Case__c = typificationDetail.CS_Automatic_Closing_Case__c;
            caseToUpdate.Expected_Completion_Date__c = fechaCierre;
            caseToUpdate.Aplica_encuesta__c = typificationDetail.CS_Apply_Survey__c;
            ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
            update caseToUpdate;
            
            System.debug(caseToUpdate);
            /* Asignacion de escalamiento a los usuarios */
            if(csGen.isUser)
                csGen.oAssignmentUser.AssignEscalationUser(caseToUpdate.Id, csGen.IdOwnerStd);
            /* Asignacion de escalamientos para interlocutores */
            if(test.isRunningTest()){
	            csGen.isInterlocutor = true;
                csGen.filteredQuery = ISSM_Constants_cls.TELVENTA;
                system.debug('typificationN3' + typificationN3);
                if(typificationN3 == 'PREVENTA'){
                    csGen.filteredQuery = ISSM_Constants_cls.PREVENTA;
                }
                if(typificationN3 == 'CREDITO'){
                    csGen.filteredQuery = ISSM_Constants_cls.CREDITO;
                }
                if(typificationN3 == 'EQUIPO_FRIO'){
                    csGen.filteredQuery = ISSM_Constants_cls.EQUIPO_FRIO;
                }
                if(typificationN3 == 'TELECOBRANZA'){
                    csGen.filteredQuery = ISSM_Constants_cls.TELECOBRANZA;
                }
                
                //csGen.filteredQuery = ISSM_Constants_cls.TELVENTA;
			}
            if(csGen.isInterlocutor)
            {
                if(csGen.filteredQuery == ISSM_Constants_cls.TELVENTA)
                    csGen.oAssignmentInterlocutor.AssignEscalationTelventa(caseToUpdate.Id, csGen.filteredQuery);
                else if(csGen.filteredQuery == ISSM_Constants_cls.PREVENTA)
                    csGen.oAssignmentInterlocutor.AssignEscalationPreVenta(caseToUpdate.Id, csGen.filteredQuery);
                else if(csGen.filteredQuery == ISSM_Constants_cls.CREDITO)
                    csGen.oAssignmentInterlocutor.AssignEscalationCredit(caseToUpdate.Id, csGen.filteredQuery);
                else if(csGen.filteredQuery == ISSM_Constants_cls.EQUIPO_FRIO)
                    csGen.oAssignmentInterlocutor.AssignEscalationCoolers(caseToUpdate.Id, csGen.filteredQuery, csGen.IdOwnerStd);
                else if (csGen.filteredQuery == ISSM_Constants_cls.TELECOBRANZA)
                    csGen.oAssignmentInterlocutor.AssignEscalationTellecolection(caseToUpdate.Id, csGen.filteredQuery);    
            }
        }
        catch(Exception ex)
        {
            updated = false;
            System.debug('HONES_CASEGENERATION_CS_CLASS.UpdateTypificationCase Message: ' + ex.getMessage());   
            System.debug('HONES_CASEGENERATION_CS_CLASS.UpdateTypificationCase Cause: ' + ex.getCause());   
            System.debug('HONES_CASEGENERATION_CS_CLASS.UpdateTypificationCase Line number: ' + ex.getLineNumber());   
            System.debug('HONES_CASEGENERATION_CS_CLASS.UpdateTypificationCase Stack trace: ' + ex.getStackTraceString());
        }
        
        return updated;
    }    
}