/****************************************************************************************************
   General Information
   -------------------
   author: Nelson Sáenz Leal
   email: nsaenz@avanxo.com
   company: Avanxo Colombia
   Project: ISSM DSD
   Customer: AbInBev Grupo Modelo
   Description: Test Class for AccountByVisitPlanOperations_cls

   Information about changes (versions)
   -------------------------------------
   Number    Dates             Author                       Description
   ------    --------          --------------------------   -----------
   1.0       10-08-2017        Nelson Sáenz Leal (NSL)      Creation Class
****************************************************************************************************/
@isTest
private class AccountByVisitPlanOperations_tst
{

  public static ONTAP__Route__c objONTAPRoute;
  public static ONTAP__Route__c objONTAPRoute2;
  public static VisitPlan__c objISSMVisitPlan;
  public static VisitPlan__c objISSMVisitPlan2;
  public static Account objAccount;
  public static Account objAccount3;
  public static   ONTAP__Vehicle__c objONTAPVehicle2;
  public static VisitPlan__c objISSMVisitPlan3;
  
  public static AccountByRoute__c objAccxRoute;

  static void createData()
    {
      User userTest = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = :TestUtils_tst.PROFILE_NAME_ADMIN].Id,
         LastName = 'Test',
         Email = 'asdasdasd@amamama.com',
         Username = 'asdasd@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST2',
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US'
        );
        insert userTest;
        
         User u = new User(
         ProfileId = [SELECT Id FROM Profile WHERE Name = :TestUtils_tst.PROFILE_NAME_ADMIN].Id,
         LastName = 'last',
         Email = 'puser000@amamama.com',
         Username = 'puser000@amamama.com' + System.currentTimeMillis(),
         CompanyName = 'TEST',
         Title = 'title',
         Alias = 'alias',
         TimeZoneSidKey = 'America/Los_Angeles',
         EmailEncodingKey = 'UTF-8',
         LanguageLocaleKey = 'en_US',
         LocaleSidKey = 'en_US',
         ManagerId  = userTest.Id
        );
        insert u;
        
        list<RecordType> lstRT = [SELECT Description,DeveloperName,Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Account' OR SobjectType = 'ONTAP__Route__c'];
        map<String, RecordType> mapRT = new map<String, RecordType>();
        for(RecordType rt : lstRT)
          mapRT.put(rt.DeveloperName, rt);

        objAccount3                         = new Account();
        objAccount3.RecordTypeId            = mapRT.get('SalesOrg').Id;
        objAccount3.Name                    = 'CMM Test3';
        objAccount3.StartTime__c            = '02:00';
        objAccount3.EndTime__c              = '01:00';
        objAccount3.ONTAP__SalesOgId__c    = '3117';
        insert objAccount3;

        Account objAccount2                   = new Account();
        objAccount2.RecordTypeId         = mapRT.get('SalesOffice').Id;
        objAccount2.Name                 = 'CMM Test';
        objAccount2.StartTime__c         = '02:00';
        objAccount2.EndTime__c           = '01:00';
        objAccount2.ONTAP__SalesOffId__c = 'FZ08';
        insert objAccount2;

        objAccount                                = new Account();
        objAccount.RecordTypeId                   = mapRT.get('Account').Id;
        objAccount.Name                           = 'CMM Dolores FZ08';
        objAccount.StartTime__c                   = '01:00';
        objAccount.EndTime__c                     = '01:00';
        objAccount.ISSM_SalesOffice__c            = objAccount2.Id;
        objAccount.ONCALL__Order_Block__c         = null;
        objAccount.ONCALL__SAP_Deleted_Flag__c    = null;
        objAccount.ONTAP__SalesOgId__c        = '3117';
        objAccount.ONTAP__SalesOffId__c        = 'FZ08';
        insert objAccount;
        
        Account objAccount4                        = new Account();
        objAccount4.RecordTypeId                   = mapRT.get('Account').Id;
        objAccount4.Name                           = 'CMM Dolores FZ09';
        objAccount4.StartTime__c                   = '01:00';
        objAccount4.EndTime__c                     = '01:00';
        objAccount4.ISSM_SalesOffice__c            = objAccount2.Id;
        objAccount4.ONCALL__Order_Block__c         = null;
        objAccount4.ONCALL__SAP_Deleted_Flag__c    = null;
      objAccount4.ONTAP__SalesOgId__c         = '3117';
        objAccount4.ONTAP__SalesOffId__c       = 'FZ08';
        insert objAccount4;

        AccountTeamMember objAccountTeamMember  = new AccountTeamMember();
        objAccountTeamMember.AccountId          = objAccount.Id;
        objAccountTeamMember.TeamMemberRole     = 'Preventa';
        objAccountTeamMember.UserId             = u.Id;
        insert objAccountTeamMember;

        AccountTeamMember objAccountTeamMember2     = new AccountTeamMember();
        objAccountTeamMember2.AccountId             = objAccount.Id;
        objAccountTeamMember2.TeamMemberRole        = 'Supervisor de Ventas';
        objAccountTeamMember2.UserId                = u.Id;
        insert objAccountTeamMember2;


        ONTAP__Vehicle__c objONTAPVehicle       = new ONTAP__Vehicle__c();
        objONTAPVehicle.ONTAP__VehicleId__c     = '112345-23';
        objONTAPVehicle.ONTAP__VehicleName__c   = 'Zurdo Movil';
        objONTAPVehicle.ONTAP__SalesOffice__c = objAccount2.id;
        insert objONTAPVehicle;

        objONTAPVehicle2       = new ONTAP__Vehicle__c();
        objONTAPVehicle2.ONTAP__VehicleId__c     = '112345-23';
        objONTAPVehicle2.ONTAP__VehicleName__c   = 'Zurdo Movil';
        objONTAPVehicle2.ONTAP__SalesOffice__c = objAccount3.id;
        insert objONTAPVehicle2;



        objONTAPRoute                       = new ONTAP__Route__c();
        objONTAPRoute.RecordTypeId      = mapRT.get('Sales').Id;
        objONTAPRoute.ServiceModel__c       = 'Presales';
        objONTAPRoute.ONTAP__SalesOffice__c = objAccount2.Id;
        objONTAPRoute.RouteManager__c       = u.Id;
        objONTAPRoute.Supervisor__c         = u.id;
        objONTAPRoute.Vehicle__c            = objONTAPVehicle.id;
        insert objONTAPRoute;

        objAccxRoute                        = new AccountByRoute__c();
        objAccxRoute.Route__c               = objONTAPRoute.Id;
        objAccxRoute.Account__c             = objAccount.Id;
        objAccxRoute.Monday__c              = false;
        objAccxRoute.Thursday__c            = false;
        insert objAccxRoute;

        objONTAPRoute2                       = new ONTAP__Route__c();
        objONTAPRoute2.RecordTypeId       = mapRT.get('Sales').Id;
        objONTAPRoute2.ServiceModel__c       = 'Telesales';
        objONTAPRoute2.ONTAP__SalesOffice__c = objAccount3.Id;
        objONTAPRoute2.RouteManager__c       = userTest.Id;
        objONTAPRoute2.Supervisor__c         = userTest.id;
        objONTAPRoute2.Vehicle__c            = objONTAPVehicle2.id;
        insert objONTAPRoute2;

        objISSMVisitPlan                    =   new VisitPlan__c();
        objISSMVisitPlan.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan.Saturday__c        =   true;
        objISSMVisitPlan.Wednesday__c       =   true;
        objISSMVisitPlan.Thursday__c        =   true;
        objISSMVisitPlan.Tuesday__c         =   true;
        objISSMVisitPlan.Friday__c          =   true;
        objISSMVisitPlan.Monday__c          =   true;
        objISSMVisitPlan.Route__c           =   objONTAPRoute.Id;
        insert objISSMVisitPlan;

        objISSMVisitPlan3                    =   new VisitPlan__c();
        objISSMVisitPlan3.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan3.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan3.Saturday__c        =   false;
        objISSMVisitPlan3.Wednesday__c       =   false;
        objISSMVisitPlan3.Thursday__c        =   false;
        objISSMVisitPlan3.Tuesday__c         =   false;
        objISSMVisitPlan3.Friday__c          =   false;
        objISSMVisitPlan3.Monday__c          =   false;
        objISSMVisitPlan3.Route__c           =   objONTAPRoute.Id;
        insert objISSMVisitPlan3;

        objISSMVisitPlan2                    =   new VisitPlan__c();
        objISSMVisitPlan2.EffectiveDate__c   =   System.today().addDays(25);
        objISSMVisitPlan2.ExecutionDate__c   =   System.today().addDays(-15);
        objISSMVisitPlan2.Saturday__c        =   false;
        objISSMVisitPlan2.Wednesday__c       =   false;
        objISSMVisitPlan2.Thursday__c        =   false;
        objISSMVisitPlan2.Tuesday__c         =   false;
        objISSMVisitPlan2.Friday__c          =   false;
        objISSMVisitPlan2.Monday__c          =   false;
        objISSMVisitPlan2.Route__c           =   objONTAPRoute2.Id;
        insert objISSMVisitPlan2;
  }
  @isTest static void test_method_one()
  {
    createData();
    Test.startTest();
      AccountByVisitPlan__c objISSMAccByVisitPlan =   new AccountByVisitPlan__c();
      objISSMAccByVisitPlan.Account__c            =   objAccount.Id;
      objISSMAccByVisitPlan.VisitPlan__c          =   objISSMVisitPlan3.Id;
      objISSMAccByVisitPlan.Sequence__c           =   1;
      objISSMAccByVisitPlan.WeeklyPeriod__c       =   '1';
      objISSMAccByVisitPlan.LastVisitDate__c      =   null;
      objISSMAccByVisitPlan.Saturday__c           =   false;
      objISSMAccByVisitPlan.Thursday__c           =   false;
      objISSMAccByVisitPlan.Tuesday__c            =   false;
      objISSMAccByVisitPlan.Friday__c             =   false;
      objISSMAccByVisitPlan.Monday__c             =   false;
      objISSMAccByVisitPlan.Wednesday__c          =   false;
      insert objISSMAccByVisitPlan;

      objISSMAccByVisitPlan.WeeklyPeriod__c       =   '3';
      delete objISSMAccByVisitPlan;
    Test.stopTest();
  }

  @isTest static void test_method_two()
  {
    createData();
    Test.startTest();
      AccountByVisitPlan__c objISSMAccByVisitPlan2 =   new AccountByVisitPlan__c();
      objISSMAccByVisitPlan2.Account__c            =   objAccount.Id;
      objISSMAccByVisitPlan2.VisitPlan__c          =   objISSMVisitPlan3.Id;
      objISSMAccByVisitPlan2.Sequence__c           =   1;
      objISSMAccByVisitPlan2.WeeklyPeriod__c       =   '1';
      objISSMAccByVisitPlan2.LastVisitDate__c      =   null;
      objISSMAccByVisitPlan2.Saturday__c           =   false;
      objISSMAccByVisitPlan2.Thursday__c           =   false;
      objISSMAccByVisitPlan2.Tuesday__c            =   false;
      objISSMAccByVisitPlan2.Friday__c             =   false;
      objISSMAccByVisitPlan2.Monday__c             =   false;
      objISSMAccByVisitPlan2.Wednesday__c          =   false;

      insert objISSMAccByVisitPlan2;
      try {
            objISSMAccByVisitPlan2.Saturday__c           =   false;
            objISSMAccByVisitPlan2.Thursday__c           =   false;
            objISSMAccByVisitPlan2.Tuesday__c            =   false;
            objISSMAccByVisitPlan2.Friday__c             =   false;
            objISSMAccByVisitPlan2.Monday__c             =   false;
            objISSMAccByVisitPlan2.Wednesday__c          =   false;
            objISSMAccByVisitPlan2.Monday__c             =   false;
            update objISSMAccByVisitPlan2;

      } catch(Exception ex) {
        System.debug(ex.getMessage());
      }
    Test.stopTest();
  }
  @isTest static void test_method_three()
  {
    createData();
    Test.startTest();
      AccountByVisitPlan__c objISSMAccByVisitPlan2 =   new AccountByVisitPlan__c();
      objISSMAccByVisitPlan2.Account__c            =   objAccount.Id;
      objISSMAccByVisitPlan2.VisitPlan__c          =   objISSMVisitPlan3.Id;
      objISSMAccByVisitPlan2.Sequence__c           =   1;
      objISSMAccByVisitPlan2.WeeklyPeriod__c       =   '1';
      objISSMAccByVisitPlan2.LastVisitDate__c      =   null;
      objISSMAccByVisitPlan2.Saturday__c           =   false;
      objISSMAccByVisitPlan2.Thursday__c           =   false;
      objISSMAccByVisitPlan2.Tuesday__c            =   false;
      objISSMAccByVisitPlan2.Friday__c             =   false;
      objISSMAccByVisitPlan2.Monday__c             =   false;
      objISSMAccByVisitPlan2.Wednesday__c          =   false;

      insert objISSMAccByVisitPlan2;
      try {
            objISSMAccByVisitPlan2.Saturday__c           =   true;
            objISSMAccByVisitPlan2.Thursday__c           =   true;
            objISSMAccByVisitPlan2.Tuesday__c            =   true;
            objISSMAccByVisitPlan2.Friday__c             =   true;
            objISSMAccByVisitPlan2.Monday__c             =   true;
            objISSMAccByVisitPlan2.Wednesday__c          =   true;
            objISSMAccByVisitPlan2.Monday__c             =   true;
            update objISSMAccByVisitPlan2;

      } catch(Exception ex) {
        System.debug(ex.getMessage());
      }
    Test.stopTest();
  }


  @isTest static void test_method_four()
  {
    createData();
    Test.startTest();
      AccountByVisitPlan__c objISSMAccByVisitPlan2 =   new AccountByVisitPlan__c();
      objISSMAccByVisitPlan2.Account__c            =   objAccount.Id;
      objISSMAccByVisitPlan2.VisitPlan__c          =   objISSMVisitPlan3.Id;
      objISSMAccByVisitPlan2.Sequence__c           =   1;
      objISSMAccByVisitPlan2.WeeklyPeriod__c       =   '1';
      objISSMAccByVisitPlan2.LastVisitDate__c      =   null;
      objISSMAccByVisitPlan2.Saturday__c           =   false;
      objISSMAccByVisitPlan2.Thursday__c           =   false;
      objISSMAccByVisitPlan2.Tuesday__c            =   false;
      objISSMAccByVisitPlan2.Friday__c             =   false;
      objISSMAccByVisitPlan2.Monday__c             =   false;
      objISSMAccByVisitPlan2.Wednesday__c          =   false;

      insert objISSMAccByVisitPlan2;
      try {
            objISSMAccByVisitPlan2.Saturday__c           =   true;
            objISSMAccByVisitPlan2.Thursday__c           =   true;
            objISSMAccByVisitPlan2.Tuesday__c            =   true;
            objISSMAccByVisitPlan2.Friday__c             =   true;
            objISSMAccByVisitPlan2.Monday__c             =   true;
            objISSMAccByVisitPlan2.Wednesday__c          =   true;
            objISSMAccByVisitPlan2.Monday__c             =   true;
            update objISSMAccByVisitPlan2;

      } catch(Exception ex) {
        System.debug(ex.getMessage());
      }
      List<AccountByVisitPlan__c> lstAccountByVisitPlan = [
        Select id, Account__c, VisitPlan__c, Sequence__c from AccountByVisitPlan__c
      ];
      map<Id, AccountByVisitPlan__c> mapVisitPlan = new map<Id, AccountByVisitPlan__c>([
        Select  Id, VisitPlan__c
        From  AccountByVisitPlan__c
      ]);
      AccountByVisitPlanOperations_cls.manageAggregateAccount(mapVisitPlan, false);
      AccountByVisitPlanOperations_cls.sortListBySequence(lstAccountByVisitPlan);
      AccountByVisitPlanOperations_cls.validateServiceModelConfig(lstAccountByVisitPlan);
      AccountByVisitPlanOperations_cls.deleteVisits(lstAccountByVisitPlan);

    Test.stopTest();
  }
}