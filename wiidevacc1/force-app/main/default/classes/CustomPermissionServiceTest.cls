/**
* @author Marcilio Souza
* @date 28/05/19
* @description Test Class for CustomPermissionService
*/
@IsTest
public with sharing class CustomPermissionServiceTest {
   
   //check permission created on setup
   static testMethod void test_user_with_custom_permission(){
        User u = [SELECT id FROM User WHERE UserName='custompermissionservicetest@custompermissionservicetest.com'];
        CustomPermission cp = [SELECT Id, DeveloperName FROM CustomPermission ORDER BY DeveloperName DESC LIMIT 1][0];
        
        Set<Id> setIdUser = new Set<Id>{u.Id};
            
        Test.startTest();
        
        Map<Id,User> usersPermissions = CustomPermissionService.getUsersWithCustomPermission(setIdUser, cp.DeveloperName);
        System.assertEquals(true, usersPermissions.containsKey(u.Id));
        
        Test.stopTest();
    }

    @TestSetup
    static void setup(){
        PermissionSet ps = new PermissionSet();
        ps.Name = 'TestPermissionChatter';
        ps.Label = 'TestPermissionChatter';
        insert ps;
        
        SetupEntityAccess sea = new SetupEntityAccess();
        sea.ParentId = ps.Id;
        sea.SetupEntityId = [SELECT Id FROM CustomPermission ORDER BY DeveloperName DESC LIMIT 1][0].Id;
        insert sea;
        
        Profile p = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
        
        User u = new User(Alias = 'talias', Email='custompermissionservicetest@custompermissionservicetest.com',
                          EmailEncodingKey='UTF-8', LastName='testlastname', LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US', ProfileId = p.Id,
                          TimeZoneSidKey='America/Los_Angeles', 
                          UserName='custompermissionservicetest@custompermissionservicetest.com');
        insert u;
        
        PermissionSetAssignment psa = new PermissionSetAssignment();
        psa.AssigneeId = u.Id;
        psa.PermissionSetId = ps.Id;
        insert psa;
    }
}