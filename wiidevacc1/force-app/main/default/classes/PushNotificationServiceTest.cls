/**
* @author Marcilio Souza
* @date 27/09/2019
* @description Class Coverage PushNotificationService
*  Its not possible to asserts because send a mobile message outside the salesforce
*/
@isTest
public class PushNotificationServiceTest{
	
    @testSetup 
    static void setup(){
        
        ONTAP__OnTapSettings__c ontapSettings = new ONTAP__OnTapSettings__c();
        ontapSettings.ONTAP__Connected_APP_for_ANDROID__c = 'TestAndroid';
        ontapSettings.ONTAP__Connected_APP_for_IOS__c = 'TestIOS';
        insert ontapSettings;
        
        ONTAP__Push_Notifications__c pushNotification = new ONTAP__Push_Notifications__c();
        pushNotification.Name = 'Test';
        pushNotification.ONTAP__Android_Enabled__c = true;
        pushNotification.ONTAP__iOS_Enabled__c = true;
        insert pushNotification;
     
    }
    
    @isTest
    static void test_send_push_notification_service(){
        ONTAP__OnTapSettings__c lOnTapSettings = ONTAP__OnTapSettings__c.getInstance();
        
        Map<Id, Map<String, Object>> mapIdUserByPayLoad  = new Map<Id, Map<String, Object>>();
         
        Map<String, Object> mapStrObj = new Map<String, Object>();
        mapStrObj.put('attribute', 'value');
        
        mapIdUserByPayLoad.put(lOnTapSettings.Id, mapStrObj);
        
        PushNotificationService.sendPushNotifications(mapIdUserByPayLoad, 'Test');
    }
    
}