@isTest
public class SyrveyPublisherTest {
    
    @isTest
    static void testSurveyChangeEvent(){
        
        String recordTypeDeveloperName;
        
        Account account = DRFixtureFactory.newDRAccount();
        
        Test.startTest();
        
        ONTAP__SurveyTaker__c survey = DRFixtureFactory.newDRSurvey( account.Id );
        
        survey.ONTAP__Status__c = 'Completada';
        
        update survey;
        
        Test.getEventBus().deliver();
        
        Test.stopTest();
        
    }     
    
    @isTest
    static void testSurveyPublisher(){
        
        String recordTypeDeveloperName;
        
        Account account = DRFixtureFactory.newDRAccount();
        
        ONTAP__SurveyTaker__c survey = DRFixtureFactory.newDRSurvey( account.Id );
        
        List<ONTAP__SurveyTaker__c> surveys = new List<ONTAP__SurveyTaker__c>();
        
        surveys.add( survey );
        
        Test.startTest();
        
		SurveyPublisher publisher = new SurveyPublisher( surveys );
        publisher.run();
        
        survey.ONTAP__Status__c = 'Completada';
        
        update survey;
        
        Test.stopTest();
        
    }      

}