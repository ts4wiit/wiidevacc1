/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_HeaderList.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 01/10/2019           Heron Zurita           Creation of methods.
*/

public class HONES_ONCALL_HeaderList {
    
    /**
    * Methods getters and setters for entity HONES_ONCALL_HeaderList
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
    public String sfdcId{get; set;}
    public String deliveryDate{get; set;} 
    public String salesOrg{get; set;}
    public String paymentMethod{get; set;}
    public String orderType{get; set;}
    public String orderReason{get; set;}
    public String customerOrder{get; set;}

}