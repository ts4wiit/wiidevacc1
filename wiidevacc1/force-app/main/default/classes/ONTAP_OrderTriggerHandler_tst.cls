/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap - OnCall
 * ----------------------------------------------------------------------------
 * Clase: ONTAP_OrderTriggerHandler_tst.apxc
 * Versión: 1.0.0.0
 *  
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario            Contacto      						Descripción
 * 26/12/ 2018     Fernando Engel	  fernando.engel.funes@accenture.com    Test class of ONTAP_OrderTriggerHandler
 * 
*/

@isTest
public class ONTAP_OrderTriggerHandler_tst {
    
    /**
    * test method to set up the data and test the triggers when insert and update an order adding an order item
    * Created By: fernando.engel.funes@accenture.com 
    * @param void
    * @return void
    */
    @isTest public static void ONTAP_OrderTriggerHandler_tst(){
        Test.setMock(HttpCalloutMock.class, new MockHTTPResponseGeneratorgenOrder());
        Order_Types__mdt Order_Record_Type = [SELECT Id, Atr_4__c, Order_Record_Type__c FROM Order_Types__mdt LIMIT 1];
        
        Account acc = new Account(Name = 'Test Acc',ONCALL__Country__c ='PA' , ONCALL__KATR4__c = Order_Record_Type.Atr_4__c,ONTAP__SalesOgId__c='IS');
        insert acc;
        
        String RT = [SELECT id FROM recordType where sobjecttype = 'ONTAP__Order__c' AND DeveloperName =: Order_Record_Type.Order_Record_Type__c  LIMIT 1].id;
        test.startTest();
        
    
        
        ONTAP__Order__c order = new ONTAP__Order__c(ONTAP__OrderAccount__c = acc.id, recordTypeId = RT);
        order.ONCALL__SAP_Order_Item_Counter__c=1;
        order.ONCALL__Total_Order_Item_Quantity__c=1;
        order.ONTAP__DocumentationType__c='test';
        order.ISSM_PaymentMethod__c='Cash';
        order.ONTAP__DeliveryDate__c=System.today();
        insert order;  
        
        ONTAP__Order_Item__c orderItem = new ONTAP__Order_Item__c(ONTAP__CustomerOrder__c =order.Id);
		insert orderItem;
        
        //update order; 
        
        test.stopTest();
    }
    
    @isTest public static void queryOrdersToInsert_test() {
        Order_Types__mdt Order_Record_Type = [SELECT Id, Atr_4__c, Order_Record_Type__c FROM Order_Types__mdt LIMIT 1];
        
        System.debug('Order_Record_Type = ' + Order_Record_Type);
        
        Account acc = new Account(Name = 'Test Acc', ONCALL__Country__c ='PA', ONCALL__KATR4__c = Order_Record_Type.Atr_4__c,ONTAP__SalesOgId__c='IS');
        insert acc;
        
        System.debug('acc = ' + acc);
        
        Id RT = [SELECT id FROM recordType where sobjecttype = 'ONTAP__Order__c' AND DeveloperName =: Order_Record_Type.Order_Record_Type__c  LIMIT 1].id;
        
        System.debug('RT = ' + RT);
        
        Account accToSend = [SELECT Id, ONCALL__Country__c, ONCALL__KATR4__c, ONTAP__SalesOgId__c FROM Account WHERE Name = 'Test Acc' LIMIT 1];
        
        Test.startTest();
        
        ONTAP__Order__c oo = new ONTAP__Order__c(ONCALL__SAP_Order_Item_Counter__c=1, 
                                                 ONCALL__Total_Order_Item_Quantity__c=1, 
                                                 ONCALL__SAP_Order_Number__c=null, 
                                                 ONCALL__OnCall_Status__c = 'Closed',
                                                 ONTAP__DeliveryDate__c=System.today(),
                                                 ISSM_PaymentMethod__c='Cash',
                                                 ONTAP__OrderAccount__c = accToSend.id, 
                                                 recordTypeId = RT);
        insert oo;
        
        System.debug('oo = ' + oo);
        
        Set<Id> setIdOrders = new Set<Id>();
        setIdOrders.add(oo.Id);
        SettingsBatchSendOrder__c sbs = new SettingsBatchSendOrder__c();
        sbs.ActiveSendByTrigger__c = true;
        insert sbs;
        
        System.debug('sbs = ' + sbs);
        
        System.debug('setIdOrders = ' + setIdOrders);
        
        ONTAP_OrderTriggerHandler.queryOrdersToInsert(setIdOrders);
    }
    
    @isTest  static void updateDeliveryDateHolidays_test() {
        Map<Id,ONTAP__Order__c> oldmaptest = new Map<Id,ONTAP__Order__c>();
        Account acc = new Account(Name = 'Test Acc',ONCALL__Country__c ='PA', ONTAP__SalesOgId__c='IS');
        insert acc;
        List<ONTAP__Order__c> orders = new List<ONTAP__Order__c>();
        
        ONTAP__Order__c order = new ONTAP__Order__c(ONTAP__OrderAccount__c = acc.id);
        order.ONCALL__SAP_Order_Item_Counter__c=1;
        order.ONCALL__Total_Order_Item_Quantity__c=1;
        order.ONTAP__DocumentationType__c='test';
        order.ISSM_PaymentMethod__c='Cash';
        order.ONTAP__DeliveryDate__c=System.today();
        orders.add(order);
        
        ONTAP__Order__c order2 = new ONTAP__Order__c(ONTAP__OrderAccount__c = acc.id);
        order2.ONCALL__SAP_Order_Item_Counter__c=1;
        order2.ONCALL__Total_Order_Item_Quantity__c=1;
        order2.ONTAP__DocumentationType__c='test';
        order2.ISSM_PaymentMethod__c='Cash';
        order2.ONTAP__DeliveryDate__c= System.today()+3;
        orders.add(order2);
        
        insert orders;
        
        for(ONTAP__Order__c ordTest : orders){
            oldmaptest.put(ordTest.Id,ordTest);
        }
		

        ONTAP_OrderTriggerHandler.updateDeliveryDateHolidays(orders,oldmaptest);
    }
    
    @isTest static void testSetBatchOrderafterUpdate(){
        ONTAP_OrderTriggerHandler oBatch = new ONTAP_OrderTriggerHandler();
        
        SettingsBatchSendOrder__c sbs = new SettingsBatchSendOrder__c();
        sbs.ActiveSendByTrigger__c = true;
        insert sbs;
        oBatch.afterUpdate();
    }
    
}