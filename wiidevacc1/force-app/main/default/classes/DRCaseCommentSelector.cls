public class DRCaseCommentSelector {
    
    List<Id> caseCommentIds = new List<Id>();
    public List<ONTAP__Case_Force_Comment__c> caseComments { get; set; }
    
    public DRCaseCommentSelector( List<Id> caseCommentIds ){
        this.caseCommentIds = caseCommentIds;
        this.setCaseComments();
    }
    
    private void setCaseComments(){
        
        this.caseComments = [ SELECT Id, ONTAP__Case_Force__c, Name, ONTAP__Comment__c, RecordTypeDeveloperName__c
                                FROM ONTAP__Case_Force_Comment__c
                               WHERE Id in : caseCommentIds ];
                  
    }
    
    public Map<Id,ONTAP__Case_Force_Comment__c> filterByRecordTypeName( String recordTypeDeveloperName ){
        
        Map<Id,ONTAP__Case_Force_Comment__c> filteredCaseCommentsMap = new Map<Id,ONTAP__Case_Force_Comment__c>();
        
        for ( ONTAP__Case_Force_Comment__c forceCaseComment : caseComments ){
            
            if ( forceCaseComment.RecordTypeDeveloperName__c == recordTypeDeveloperName ){
                
                filteredCaseCommentsMap.put( forceCaseComment.Id, forceCaseComment );
                
            }
            
        }
        
        return filteredCaseCommentsMap;
        
    }
        

}