@IsTest
public with sharing class FeedPushUtil_Test {
    
    
	@isTest static void test_user_by_payload(){
        
        User bdrUser = [SELECT Id FROM User WHERE UserName = 'testbdrruta@test.com'];

        Map<Id, Set<Id>> mapIdUserByListFeedId = new Map<Id, Set<Id>>();
        mapIdUserByListFeedId.put(bdrUser.Id, new Set<Id>{bdrUser.Id}); //bdr user id as feedid

        Map<Id, String> mapIdUserByContent = new Map<Id, String>();
        mapIdUserByContent.put(bdrUser.Id, 'Chatter Message');

        Map<Id, Map<String, Object>> buildExpected = new Map<Id, Map<String, Object>>();
        Map<String, Object> mapPayload = new Map<String, Object>();

        mapPayload.put('type', 'chatter_feed');
        mapPayload.put('content', 'Chatter Message');
        mapPayload.put('extra_fields', 'user_id,feed_id');
        mapPayload.put('user_id', bdrUser.Id);								 			
        mapPayload.put('feed_id', bdrUser.Id);

        buildExpected.put(bdrUser.Id, mapPayload);
		system.debug('mapIdUserByListFeedId: ' + mapIdUserByListFeedId);
        system.debug('mapIdUserByContent: ' + mapIdUserByContent);
        FeedPushUtil feedUtil = new FeedPushUtil();
        
        Test.startTest();
        System.runAs(bdrUser){
        System.assertEquals(buildExpected, feedUtil.getUserIdbyPayload(mapIdUserByListFeedId, mapIdUserByContent));
        FeedPushUtil feedExecute = new FeedPushUtil(mapIdUserByListFeedId, mapIdUserByContent);
        feedExecute.sendPushNotfication();
        }
        Test.stopTest();
	}

    @TestSetup
    static void setup(){
        Id profileId = [ SELECT Id FROM PROFILE WHERE Name = 'System Administrator' ].get(0).Id;

		User u = new User(FirstName='BDR', LastName='testbdrruta', UserName = 'testbdrruta@test.com', Email='testbdrruta@test.com',
			 Alias='dobdr', LocaleSidKey='es', LanguageLocaleKey='es', EmailEncodingKey='ISO-8859-1',ONTAP__Country_Alias__c = 'DO',
			 ProfileId = profileId, TimeZoneSidKey='America/Puerto_Rico', Country='DO', isActive = true,User_Route__c='DO1234');

		insert u;

        List<PermissionSet> listPermission = [SELECT Id, Name, Label FROM PermissionSet WHERE Name = 'ChatterUserPushNotification'];

        PermissionSet ps;

        if(listPermission.isEmpty()){
            ps = new PermissionSet();
            ps.Name = 'ChatterUserPushNotification';
            ps.Label = 'ChatterUserPushNotification';
            insert ps;
        }else{
           ps = listPermission.get(0);
        }

        SetupEntityAccess sea = new SetupEntityAccess();
        sea.ParentId = ps.Id;
        sea.SetupEntityId = [SELECT Id FROM CustomPermission ORDER BY DeveloperName DESC LIMIT 1][0].Id;
        insert sea;

        PermissionSetAssignment psa = new PermissionSetAssignment();
        psa.AssigneeId = u.Id;
        psa.PermissionSetId = ps.Id;
        insert psa;
    }
        

}