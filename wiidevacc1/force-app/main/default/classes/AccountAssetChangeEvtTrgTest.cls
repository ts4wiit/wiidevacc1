@isTest
public class AccountAssetChangeEvtTrgTest {
    
    @isTest
    static void testAccountAssetCHangeEvent(){
        
        Account account = DRFixtureFactory.newDRAccount();
        
        Test.startTest();
        
        Test.enableChangeDataCapture();
        
        ONTAP__Account_Asset__c accountAsset = DRFixtureFactory.newDRAccountAsset( account.Id );
        
        Test.getEventBus().deliver();
        
        accountAsset.ONTAP__Asset_Status__c = 'Qualified';
        
        update accountAsset;
        
        Test.getEventBus().deliver();
        
        Test.stopTest();
        
    }

}