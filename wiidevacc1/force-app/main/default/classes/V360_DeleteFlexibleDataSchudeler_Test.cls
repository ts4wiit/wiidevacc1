/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteFlexibleDataSchudeler_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Carlos Leal        Creation of methods.
 */
@isTest
private class V360_DeleteFlexibleDataSchudeler_Test{
   /**
   * Method for test the Schudeler for flexible data.
   * @author: c.leal.beltran@accenture.com
   * @param void
   * @return void
   */
  @isTest static void test_execute_UseCase1(){
    V360_DeleteFlexibleDataSchudeler obj01 = new V360_DeleteFlexibleDataSchudeler();
    SchedulableContext sc;
    obj01.execute(sc);
  }
}