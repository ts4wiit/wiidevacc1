/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: SaleStatsController_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest(SeeAllData=true)
private class SaleStatsController_Test{
    
    /**
    * Test Method to get all the Idcalls by recordId  
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_getAccountFromCall2_UseCase1(){
     
       /* List<ONCALL__Call__c>ct= [Select ONCALL__POC__c, ONCALL__Phone__c FROM ONCALL__Call__c];
        List<V360_SalerPerZone__c> spzObj = [SELECT V360_User__c, V360_SalesZone__c, V360_Type__c, IsDeleted FROM V360_SalerPerZone__c];
          
        SaleStatsController obj01 = new SaleStatsController();
        try{
        SaleStatsController.getAccountFromCall2(ct[0].ONCALL__POC__c);
        }
        catch(System.Exception ex){
            System.debug('Error');
        }*/
        Account acc = new Account(Name='Test');
        insert acc;
        ONCALL__Call__c call = new ONCALL__Call__c();
        call.Name = 'Test';
        call.ONCALL__POC__c = acc.Id;
        insert call;
        SaleStatsController.getAccountFromCall2(call.Id);
        
    }
    
    /**
    * Test method to get all the coverage elements related to an account  
    * Created By: g.martinez.cabral@accenture.com
    * Modified By c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
  	@isTest static void test_getStats_UseCase1(){
        Account acc = new Account();
        acc.Name = 'Test';
        insert acc; 
        
        v360_Coverage__c saleStat = new v360_Coverage__c(V360_Id_client_SAP__c = 'test', V360_Brand__c = 'test', V360_Container__c = 'test',
               V360_This__c = false, V360_Last__c = false, V360_Focus_Portfolio__c = true, V360_Account__c = acc.Id, V360_Sector__c = 'test');
        insert saleStat;
        v360_Coverage__c saleStat2 = new v360_Coverage__c(V360_Id_client_SAP__c = 'test', V360_Brand__c = 'test', V360_Container__c = 'test',
               V360_This__c = false, V360_Last__c = true, V360_Focus_Portfolio__c = false, V360_Account__c = acc.Id, V360_Sector__c = 'test');
        insert saleStat2;
        ONCALL__Call__c call = new ONCALL__Call__c();
        call.Name = 'Test9999';
        call.ONCALL__POC__c = acc.Id;
        insert call;        
		        
        List<ONCALL__Call__c>ct=[Select ONCALL__POC__c FROM ONCALL__Call__c WHERE Name = 'Test9999'];
        SaleStatsController obj01 = new SaleStatsController();
        ct[0].ONCALL__POC__C=acc.Id;
        update ct[0];
        SaleStatsController.getStats(ct[0].ONCALL__POC__c);
    }
}