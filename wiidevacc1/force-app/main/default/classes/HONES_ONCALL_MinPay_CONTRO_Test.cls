/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: HONES_ONCALL_MinPay_CONTRO_Test.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Luis Arturo Parra Rosas           Creation of methods.
*/

@isTest
public class HONES_ONCALL_MinPay_CONTRO_Test {
    
    
    /**
    * Test method for set start values
    * Created By: heron.zurita@accenture.com
    * @param void
    * @return void
    */
    @testSetup
    static void setupTestData(){
        test.startTest();
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
        Id finishProduct = Schema.SObjectType.ONTAP__Order_Item__c.getRecordTypeInfosByName().get('Finished Product').getRecordTypeId();
        Id emptiesIntroduction = Schema.SObjectType.ONTAP__Order_Item__c.getRecordTypeInfosByName().get('Empties Introduction').getRecordTypeId();
        
        User u = [Select Id FROM User Limit 1];  
        Account a = New Account(ONTAP__ExternalKey__c='SV1Test',Name ='ABC', RecordTypeId=salesZonedevRecordTypeId, ISSM_CreditLimit__c = 1200.00);
        
        insert a;

              
        ONTAP__Order__c orderObj = new ONTAP__Order__c(ONCALL__OnCall_Account__c = a.Id);
        insert orderObj;
        
        ONCALL__Call__c callObj = new ONCALL__Call__c(ONCALL__POC__c = a.Id);
        insert callObj;
        
        
        ONTAP__Order_Item__c orderItemObj = new ONTAP__Order_Item__c(ISSM_TotalAmount__c = 200.00, RecordTypeId = finishProduct) ;
        insert orderItemObj;
         
        ONTAP__OpenItem__c openItObj = new ONTAP__OpenItem__c(ONTAP__Account__c = a.Id, ONTAP__Amount__c = 200.00);
        ONTAP__OpenItem__c openItObj2 = new ONTAP__OpenItem__c(ONTAP__Account__c = a.Id, ONTAP__Amount__c = 200.00, ONTAP__DueDate__c = Date.today()-3, V360_Reference__c = 'L');
        ONTAP__OpenItem__c openItObj3 = new ONTAP__OpenItem__c(ONTAP__Account__c = a.Id, ONTAP__Amount__c = 200.00, ONTAP__DueDate__c = Date.today()-3, V360_Reference__c = 'E');
        ONTAP__OpenItem__c openItObj4 = new ONTAP__OpenItem__c(ONTAP__Account__c = a.Id, ONTAP__Amount__c = 200.00, ONTAP__DueDate__c = Date.today()+3, V360_Reference__c = 'L');
        ONTAP__OpenItem__c openItObj5 = new ONTAP__OpenItem__c(ONTAP__Account__c = a.Id, ONTAP__Amount__c = 200.00, ONTAP__DueDate__c = Date.today()+3, V360_Reference__c = 'E');
        insert openItObj;
        insert openItObj2;
        insert openItObj3;
        insert openItObj4;
        insert openItObj5;
        
        test.stopTest();
    }
    
    /**
    * test method to get the credit limit of an account
    * Created By: luis.arturo.parra@accenture.com
    * Modified By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest static void creditLimit_test(){
        
        List<Account> lstAcc = [SELECT Id, ONTAP__Credit_Amount__c, ISSM_CreditLimit__c FROM Account];
        HONES_ONCALL_MinPay_CONTRO.creditLimit(lstAcc[0].Id);
        //System.assert(lstAcc.size()>0);
    }
    
     /**
    * test method to get the account id of an Account object
    * Created By: heron.zurita@accenture.com  
    * @param void
    * @return void
    */
    
    @isTest static void getById_test(){
        
        List<Account> lstAcc = [SELECT Id, ONTAP__Credit_Amount__c, ISSM_CreditLimit__c FROM Account];
        HONES_ONCALL_MinPay_CONTRO.getById(lstAcc[0].Id);
        
    }
    
    
    /**
    * test method to get the account id of a Call 
    * Created By: heron.zurita@accenture.com  
    * @param void
    * @return void
    */
    @isTest static void getFlexibleDataById_test(){
        
        List<ONCALL__Call__c> lstCall = [SELECT ONCALL__POC__c FROM ONCALL__Call__c];
        HONES_ONCALL_MinPay_CONTRO.getFlexibleDataById(lstCall[0].ONCALL__POC__c);
    }
    

    /**
    * test method to get the account id of an Open Item
    * Created By: heron.zurita@accenture.com  
    * @param void
    * @return void
    */
    @isTest static void getFlexibleDataById2_test(){
        
        List<ONTAP__OpenItem__c> lstOpen = [SELECT ONTAP__Account__c FROM ONTAP__OpenItem__c];
        HONES_ONCALL_MinPay_CONTRO.getFlexibleDataById2(lstOpen[0].ONTAP__Account__c);
    }
    
    /**
    * test method to get the available balance of an Account
    * Created By: heron.zurita@accenture.com  
    * @param void
    * @return void
    */
    @isTest static void disponible_test(){
        test.startTest();
        List<Account> lstAcc = [SELECT Id, ONTAP__Credit_Amount__c, ISSM_CreditLimit__c FROM Account WHERE Name='ABC'];
        
        //System.assertEquals(HONES_ONCALL_MinPay_CONTRO.disponible(lstAcc[0].Id), 200.00);

        Account acc = new Account(Name='Test1', ONTAP__ExternalKey__c = 'SVHVF');
        insert acc;
        
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        ONTAP__OpenItem__c oi1 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,ONTAP__Account__c=acc.Id, RecordTypeId=rt_open_items,ISSM_Debit_Credit__c='S');
        ONTAP__OpenItem__c oi2 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,ONTAP__Account__c=acc.Id, RecordTypeId=rt_open_items,ISSM_Debit_Credit__c='H');
        account_open_items.add(oi1);
        account_open_items.add(oi2);
        
        insert account_open_items;
        test.stopTest();
        
        HONES_ONCALL_MinPay_CONTRO.disponible(acc.Id);
        
    }
    
    /**
    * test method to get the total amount of all Open Items of an Account and the Due Date is set before today 
    * Created By: heron.zurita@accenture.com  
    * @param void
    * @return void
    */
    @isTest static void outOfDateCredits_test(){

        //List<Account> lstAcc = [SELECT Id, ONTAP__Credit_Amount__c, ISSM_CreditLimit__c FROM Account WHERE Name='ABC'];
        //HONES_ONCALL_MinPay_CONTRO.outOfDateCredits(lstAcc[0].Id);
        //system.assertEquals(HONES_ONCALL_MinPay_CONTRO.outOfDateCredits(lstAcc[0].Id), 400.00);
        
        test.startTest();
        Account acc = new Account(Name='Test1', ONTAP__ExternalKey__c = 'SVHVF');
        insert acc;
        
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        ONTAP__OpenItem__c oi1 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,
                                                        ONTAP__Account__c=acc.Id, 
                                                        RecordTypeId=rt_open_items,
                                                        ONTAP__DueDate__c=System.today()-15, 
                                                        ISSM_Debit_Credit__c='S');
        ONTAP__OpenItem__c oi2 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,
                                                        ONTAP__Account__c=acc.Id, 
                                                        RecordTypeId=rt_open_items,
                                                        ONTAP__DueDate__c=System.today()-15, 
                                                        ISSM_Debit_Credit__c='H');
        
        account_open_items.add(oi1);
        account_open_items.add(oi2);
        insert account_open_items;
        test.stopTest();
        HONES_ONCALL_MinPay_CONTRO.outOfDateCredits(acc.Id);
    }
    
    /**
    * test method to get the total amount of all Open Items of an Account and the Due Date is set before today and the Reference is Liquid 
    * Created By: heron.zurita@accenture.com  
    * @param void
    * @return void
    */
    @isTest static void outOfDateCreditsLiquid_test(){
        //List<Account> lstAcc = [SELECT Id, ONTAP__Credit_Amount__c, ISSM_CreditLimit__c FROM Account WHERE Name='ABC'];
        //HONES_ONCALL_MinPay_CONTRO.outOfDateCreditsLiquid(lstAcc[0].Id);
        //system.assertEquals(HONES_ONCALL_MinPay_CONTRO.outOfDateCreditsLiquid(lstAcc[0].Id), 200.00);
        test.startTest();
        List<Account> lstacc = new List<Account>();
        Account acc = new Account(Name='Test1', ONTAP__ExternalKey__c = 'SVHVF');
        lstacc.add(acc);
        insert lstacc;
        //insert acc;
        
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        List<Open_Items_Identifier__mdt> openItems_identifier = new List<Open_Items_Identifier__mdt>();
        //Open_Items_Identifier__mdt oii = new Open_Items_Identifier__mdt(DeveloperName = GlobalStrings.OPEN_ITEMS_IDENTIFIER, Liquid__c=);
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        ONTAP__OpenItem__c oi1 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,
                                                        ONTAP__Account__c=acc.Id, 
                                                        RecordTypeId=rt_open_items,
                                                        ONTAP__DueDate__c=System.today()-15, 
                                                        ISSM_Debit_Credit__c='S',
                                                        V360_Reference__c= 'L');
        ONTAP__OpenItem__c oi2= new ONTAP__OpenItem__c(ONTAP__Amount__c=15,
                                                        ONTAP__Account__c=acc.Id, 
                                                        RecordTypeId=rt_open_items,
                                                        ONTAP__DueDate__c=System.today()-15, 
                                                        ISSM_Debit_Credit__c='H',
                                                        V360_Reference__c= 'L');
        
        account_open_items.add(oi1);
        account_open_items.add(oi2);
        insert account_open_items;
        test.stopTest();
        
        //HONES_ONCALL_MinPay_CONTRO.outOfDateCreditsLiquid(acc.Id);
        HONES_ONCALL_MinPay_CONTRO.outOfDateCreditsLiquid(lstacc[0].Id);
    }
    
    /**
    * test method to get the total amount of all Open Items of an Account and the Due Date is set before today and the Reference is Container 
    * Created By: heron.zurita@accenture.com  
    * @param void
    * @return void
    */
    @isTest static void outOfDateCreditsContainer_test(){
        
        //List<Account> lstAcc = [SELECT Id, ONTAP__Credit_Amount__c, ISSM_CreditLimit__c FROM Account WHERE Name='ABC'];
        //HONES_ONCALL_MinPay_CONTRO.outOfDateCreditsContainer(lstAcc[0].Id);
        //system.assertEquals(HONES_ONCALL_MinPay_CONTRO.outOfDateCreditsContainer(lstAcc[0].Id), 200.00);
        
        test.startTest();
        List<Account> lstacc = new List<Account>();
        Account acc = new Account(Name='Test1', ONTAP__ExternalKey__c = 'SVHVF');
        lstacc.add(acc);
        insert lstacc;
        //insert acc;
        
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        List<Open_Items_Identifier__mdt> openItems_identifier = new List<Open_Items_Identifier__mdt>();
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        ONTAP__OpenItem__c oi1 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,
                                                        ONTAP__Account__c=acc.Id, 
                                                        RecordTypeId=rt_open_items,
                                                        ONTAP__DueDate__c=System.today()-15, 
                                                        ISSM_Debit_Credit__c='S',
                                                        V360_Reference__c= 'E');
        ONTAP__OpenItem__c oi2 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,
                                                        ONTAP__Account__c=acc.Id, 
                                                        RecordTypeId=rt_open_items,
                                                        ONTAP__DueDate__c=System.today()-15, 
                                                        ISSM_Debit_Credit__c='H',
                                                        V360_Reference__c= 'E');
        account_open_items.add(oi1);
        account_open_items.add(oi2);
        insert account_open_items;
        test.stopTest();
        
        //HONES_ONCALL_MinPay_CONTRO.outOfDateCreditsContainer(acc.Id);
        HONES_ONCALL_MinPay_CONTRO.outOfDateCreditsContainer(lstacc[0].Id);
    }
    
    
    /**
    * test method to obtain the total amount of the Open Items of an Account where the Due Date is set today  or days after today
    * Created By: heron.zurita@accenture.com  
    * @param void
    * @return void
    */
    @isTest static void dateCredits_test(){
        
        //List<Account> lstAcc = [SELECT Id, ONTAP__Credit_Amount__c, ISSM_CreditLimit__c FROM Account WHERE Name='ABC'];
        //HONES_ONCALL_MinPay_CONTRO.dateCredits(lstAcc[0].Id);
        //system.assertEquals(HONES_ONCALL_MinPay_CONTRO.dateCredits(lstAcc[0].Id), 400.00);
        
        test.startTest();
        Account acc = new Account(Name='Test1', ONTAP__ExternalKey__c = 'SVHVF');
        insert acc;
        
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        ONTAP__OpenItem__c oi1 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,
                                                        ONTAP__Account__c=acc.Id, 
                                                        RecordTypeId=rt_open_items,
                                                        ONTAP__DueDate__c=System.today()+15, 
                                                        ISSM_Debit_Credit__c='S');
        ONTAP__OpenItem__c oi2 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,
                                                        ONTAP__Account__c=acc.Id, 
                                                        RecordTypeId=rt_open_items,
                                                        ONTAP__DueDate__c=System.today()+15, 
                                                        ISSM_Debit_Credit__c='H');
        
        account_open_items.add(oi1);
        account_open_items.add(oi2);
        insert account_open_items;
        test.stopTest();
        
        HONES_ONCALL_MinPay_CONTRO.dateCredits(acc.Id);
    }

    /**
    * test method to obtain the total amount of the Open Items of an Account where the Due Date is set today  or days after today and the Reference is Liquid
    * Created By: heron.zurita@accenture.com  
    * @param void
    * @return void
    */
    @isTest static void onTimeDateCreditsLiquid_test(){
        
        //List<Account> lstAcc = [SELECT Id, ONTAP__Credit_Amount__c, ISSM_CreditLimit__c FROM Account WHERE Name='ABC'];
        //HONES_ONCALL_MinPay_CONTRO.onTimeDateCreditsLiquid(lstAcc[0].Id);
        //system.assertEquals(HONES_ONCALL_MinPay_CONTRO.onTimeDateCreditsLiquid(lstAcc[0].Id), 200.00);
        
        List<Account> lstAcc = new List<Account>();
        Account acc = new Account(Name='Test1', ONTAP__ExternalKey__c = 'SVHVF');
        lstAcc.add(acc);
        insert lstAcc;
        //insert acc;
        
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        ONTAP__OpenItem__c oi1 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,
                                                        ONTAP__Account__c=acc.Id, 
                                                        RecordTypeId=rt_open_items,
                                                        ONTAP__DueDate__c=System.today()+15, 
                                                        ISSM_Debit_Credit__c='S',
                                                        V360_Reference__c ='L');
        ONTAP__OpenItem__c oi2 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,
                                                        ONTAP__Account__c=acc.Id, 
                                                        RecordTypeId=rt_open_items,
                                                        ONTAP__DueDate__c=System.today()+15, 
                                                        ISSM_Debit_Credit__c='H',
                                                        V360_Reference__c ='L');
        account_open_items.add(oi1);
        account_open_items.add(oi2);
        insert account_open_items;
        
        test.startTest();
        //HONES_ONCALL_MinPay_CONTRO.onTimeDateCreditsLiquid(acc.Id);
        HONES_ONCALL_MinPay_CONTRO.onTimeDateCreditsLiquid(lstAcc[0].Id);
        test.stopTest();
    }
    
     /**
    * test method to obtain the total amount of the Open Items of an Account where the Due Date is set today  or days after today and the Reference is container
    * Created By: heron.zurita@accenture.com  
    * @param void
    * @return void
    */
    @isTest static void onTimeDateCreditsContainer_test(){
        
        //List<Account> lstAcc = [SELECT Id, ONTAP__Credit_Amount__c, ISSM_CreditLimit__c FROM Account WHERE Name='ABC'];
        //HONES_ONCALL_MinPay_CONTRO.onTimeDateCreditsContainer(lstAcc[0].Id);
        //system.assertEquals(HONES_ONCALL_MinPay_CONTRO.onTimeDateCreditsContainer(lstAcc[0].Id), 200.00);
        
        test.startTest();
        List<Account> lstacc = new List<Account>();
        Account acc = new Account(Name='Test1', ONTAP__ExternalKey__c = 'SVHVF');
        //insert acc;
        lstacc.add(acc);
        insert lstacc;
        
		String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        
        List<Open_Items_Identifier__mdt> openItems_identifier = new List<Open_Items_Identifier__mdt>();
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        ONTAP__OpenItem__c oi1 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,
                                                        ONTAP__Account__c=acc.Id, 
                                                        RecordTypeId=rt_open_items,
                                                        ONTAP__DueDate__c=System.today()+15, 
                                                        ISSM_Debit_Credit__c='S',
                                                        V360_Reference__c= 'E');
        ONTAP__OpenItem__c oi2 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,
                                                        ONTAP__Account__c=acc.Id, 
                                                        RecordTypeId=rt_open_items,
                                                        ONTAP__DueDate__c=System.today()+15, 
                                                        ISSM_Debit_Credit__c='H',
                                                        V360_Reference__c= 'E');
        account_open_items.add(oi1);
        account_open_items.add(oi2);
        insert account_open_items;
        test.stopTest();
        
        //HONES_ONCALL_MinPay_CONTRO.onTimeDateCreditsContainer(acc.Id);
        HONES_ONCALL_MinPay_CONTRO.onTimeDateCreditsContainer(lstacc[0].Id);
    }
    
     /**
    * test method to get the minimum payment of an order
    * Created By: heron.zurita@accenture.com  
    * @param void
    * @return void
    */
    @isTest static void total_test(){
        /*
		Account acc = new Account();
        acc.Name = 'Test9999';
        acc.ONTAP__Credit_Condition__c = 'Credit';
        acc.ISSM_CreditLimit__c = Double.valueof(1200.00);
        acc.ONTAP__ExternalKey__c = 'SVHDF';
        insert acc;
        
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        
        ONTAP__OpenItem__c oi = new ONTAP__OpenItem__c();
        oi.ONTAP__DueDate__c = Date.newInstance(2018, 11,4);
        insert oi;
        
        List<Account> lstAcc = [SELECT Id, ONTAP__Credit_Amount__c, ISSM_CreditLimit__c FROM Account WHERE ONTAP__Credit_Condition__c ='Credit'];
        HONES_ONCALL_MinPay_CONTRO.total(lstAcc[0].Id,'Credit',Double.valueof(acc.ISSM_CreditLimit__c));
        */
        
        test.startTest();
        Double limitExc = 0.0;
        Double outAmount = 0.0;
        
        Account acc = new Account(Name='Test1', ONTAP__ExternalKey__c = 'SVHVF',ISSM_CreditLimit__c = Double.valueof(1200.00),ONTAP__Credit_Condition__c = 'Credit');
        insert acc;
        
        String rt_open_items = Schema.SObjectType.ONTAP__OpenItem__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.OPEN_ITEMS_RT).getRecordTypeId();
        
        List<ONTAP__OpenItem__c> account_open_items = new List<ONTAP__OpenItem__c>();
        ONTAP__OpenItem__c oi1 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,
                                                        ONTAP__Account__c=acc.Id, 
                                                        RecordTypeId=rt_open_items,
                                                        ONTAP__DueDate__c=System.today()+15, 
                                                        ISSM_Debit_Credit__c='S');
        ONTAP__OpenItem__c oi2 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,
                                                        ONTAP__Account__c=acc.Id, 
                                                        RecordTypeId=rt_open_items,
                                                        ONTAP__DueDate__c=System.today()+15, 
                                                        ISSM_Debit_Credit__c='H');
        ONTAP__OpenItem__c oi3 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,
                                                        ONTAP__Account__c=acc.Id, 
                                                        RecordTypeId=rt_open_items,
                                                        ONTAP__DueDate__c=System.today()-15, 
                                                        ISSM_Debit_Credit__c='S');
        ONTAP__OpenItem__c oi4 = new ONTAP__OpenItem__c(ONTAP__Amount__c=15,
                                                        ONTAP__Account__c=acc.Id, 
                                                        RecordTypeId=rt_open_items,
                                                        ONTAP__DueDate__c=System.today()-15, 
                                                        ISSM_Debit_Credit__c='H');
        
        account_open_items.add(oi1);
        account_open_items.add(oi2);
        account_open_items.add(oi3);
        account_open_items.add(oi4);
        insert account_open_items;
        test.stopTest();
        
        HONES_ONCALL_MinPay_CONTRO.total(acc.Id,'Credit',Double.valueof(acc.ISSM_CreditLimit__c));
    }
    
}