public class FlexibleKPITriggerHandler {
    
    private List<Flexible_KPI__c> flexibleKPIs = new List<Flexible_KPI__c>();
    private Map<String,String> routeToUserIdMap = new Map<String,String>();
    private List<String> routes = new List<String>();
    
    public FlexibleKPITriggerHandler( List<Flexible_KPI__c> flexibleKPIs ){
        
        this.flexibleKPIs = flexibleKPIs;
        
    }
    
    public void run(){
        
        this.getRoutes();
        this.getRouteToUserIdMap();
        this.setBDRRoutes();
        
    }
    
    private void getRoutes(){
        
        for ( Flexible_KPI__c kpi : flexibleKPIs ){
            
            routes.add( kpi.Route__c );
            
        }
        
    }
    
    private void getRouteToUserIdMap(){
        
        this.routeToUserIdMap = DRUserSelector.getRouteToUserIdMap( routes );
        
    }
    
    private void setBDRRoutes(){
        
        for ( Flexible_KPI__c kpi : flexibleKPIs ){
            
            Id ownerId;
            
            ownerId =  routeToUserIdMap.get( kpi.Route__c );
                
            
            if ( ownerId == null ) {
                
				ownerId = UserInfo.getUserId();
                                   
            } 

            kpi.BDR_Route__c = ownerId; 
     		kpi.OwnerId = ownerId;
            
        }        
        
    }

}