public class AccountChangeEventTriggerHandler {
    
    public static final String DR_ACCOUNT_RECORD_TYPE = 'Account_DO';
    
    List<AccountChangeEvent> accountChangeEvents = new List<AccountChangeEvent>();
    List<Account> accounts = new List<Account>();
    List<Id> accountIds = new List<Id>();
    List<String> prospectIds = new List<String>();
    Map<Id,Account> idToAccountMap = new Map<Id,Account>();
    List<DRAccountOutboundMessage__e> accountOutboundMessages = new List<DRAccountOutboundMessage__e>();
    private String integrationId;
    
    private String accountRecordTypeId;

    public AccountChangeEventTriggerHandler( List<AccountChangeEvent> accountChangeEvents ){
        
        this.accountChangeEvents = accountChangeEvents;
        
    }
    
    private void setup(){

        
        this.integrationId = DRUserSelector.getIntegrationId();
        
        for ( AccountChangeEvent accountChangeEvent : accountChangeEvents ){
            
            List<String> recordIds = accountChangeEvent.ChangeEventHeader.getRecordIds();
            
            System.debug('>>> Account Change Event: ' + accountChangeEvent );
            
            if ( accountChangeEvent.ChangeEventHeader.getCommitUser() != (Id) this.integrationId ){
                
                   accountIds.addAll( recordIds );
                
            }

        }
        
        this.accounts = DRAccountSelector.getAccountsByIds( accountIds );           
        
        for ( Account account : accounts ){
           
            idToAccountMap.put( account.Id, account );
            
        }
        
    }
    
    public void run(){
             
        System.debug('>>> Running async trigger');
        this.setup();
        this.setRecordTypes();
		this.mapEvents();
        this.publish();
        
    }

    
    private void setRecordTypes(){
        
        accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get( DR_ACCOUNT_RECORD_TYPE ).getRecordTypeId();
        
    }    

    
    private void mapEvents(){
         
        for ( AccountChangeEvent accountChangeEvent : accountChangeEvents ){      

            Account account = idToAccountMap.get( accountChangeEvent.ChangeEventHeader.getRecordIds().get(0) );
                
            if ( account.RecordTypeId == accountRecordTypeId ){
                mapAccount( account );
            }

        }            
    }

    private void publish(){
        System.debug('>>> Account');
        System.debug( accountOutboundMessages );
        List<Database.SaveResult> accountResults = EventBus.publish( accountOutboundMessages );
    }
    
    private Boolean verifyAccountRecordType( AccountChangeEvent accountChangeEvent ){
        
       System.debug('>>> Change Type | Account: ' + accountChangeEvent.ChangeEventHeader.getChangeType() );
       System.debug('>>> Email: ' + accountChangeEvent.ONTAP__Email__c);
        
       if ( accountChangeEvent.ChangeEventHeader.getChangeType() != 'UPDATE' ) return false;
       if ( accountChangeEvent.ONTAP__Email__c != null ) return true;
       if ( accountChangeEvent.ONTAP__Secondary_Phone__c != null ) return true;
       if ( accountChangeEvent.ONTAP__Neighborhood__c != null ) return true;
       if ( accountChangeEvent.ShippingAddress != null ) return true;
       if ( accountChangeEvent.ONTAP__Latitude__c != null ) return true;
       if ( accountChangeEvent.ONTAP__Longitude__c != null ) return true;
       if ( accountChangeEvent.ONTAP__Province__c != null ) return true;
       if ( accountChangeEvent.ONTAP__District__c != null ) return true;
       if ( accountChangeEvent.Phone != null ) return true; 
        
       return false;       
    }

    
    private void mapAccount( Account account ){
          
        String shippingAddress = '';
        
        try {  
            shippingAddress =  account.ShippingAddress.getStreet();
        } catch(Exception e){
            System.debug('>>> Error: ' + e.getMessage());
        }
             
        DRAccountOutboundMessage__e outboundMessage = new DRAccountOutboundMessage__e(
                  District__c = account.ONTAP__District__c,
                  Email__c = account.ONTAP__Email__c,
                  Latitude__c = account.ONTAP__Latitude__c,
                  Longitude__c = account.ONTAP__Longitude__c,
                  Neighborhood__c = account.ONTAP__Neighborhood__c,
                  Phone__c = account.Phone,
                  Province__c = account.ONTAP__Province__c,
                  SecondaryPhone__c = account.ONTAP__Secondary_Phone__c,
                  ShippingAddress__c =  shippingAddress,
                  SAPCustomerId__c = account.ONTAP__SAPCustomerId__c
        );
        
        accountOutboundMessages.add( outboundMessage );           
    }   

}