@isTest
public class DRFixtureFactory {
    
    public static User newBDRUser( String userName, String alias ){
        
        Id profileId = [ SELECT Id FROM PROFILE WHERE Name = 'DO BDR' ].get(0).Id;

		User bdrUser = new User(FirstName='BDR', LastName='Agent 1', UserName = userName, Email='bdr@test.com',
				Alias=alias, LocaleSidKey='es', LanguageLocaleKey='es', EmailEncodingKey='ISO-8859-1',ONTAP__Country_Alias__c = 'DO',
				ProfileId = profileId, TimeZoneSidKey='America/Puerto_Rico', Country='DO', isActive = true,User_Route__c='DO1234');

		insert bdrUser;

        return bdrUser;        
        
    }
    
    public static Account newDRProspect(){
        
        String recordTypeDeveloperName = 'Prospect_DO';
        String recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get( recordTypeDeveloperName ).getRecordTypeId();
              
        Account prospectAccount = new Account (
             ONTAP__Contact_First_Name__c = 'Contact',
             ONTAP__Contact_Last_Name__c = 'Lastname',
             ONTAP__District__c = 'District',
             Phone = '(988) 899-9897',
             ONTAP__Province__c = 'Province',
             ONTAP__LegalName__c = 'Account Legal Name',
             Name = 'Account Name',
             RNC_Number__c = '123',
             SIC = '123',
             RecordTypeId = recordTypeId
         );
        
        insert prospectAccount;
        
        return prospectAccount;
    }

    public static Account newDRAccount(){
        
        String recordTypeDeveloperName = 'Account_DO';
        String recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get( recordTypeDeveloperName ).getRecordTypeId();

		User bdrUser = newBDRUser( 'bdr1@oncall.devpro', 'bdr1' );
        
        Account account = new Account (
             ONTAP__Contact_First_Name__c = 'Contact',
             ONTAP__Contact_Last_Name__c = 'Lastname',
             ONTAP__District__c = 'District',
             Phone = '(988) 899-9897',
             ONTAP__Province__c = 'Province',
             ONTAP__LegalName__c = 'Account Legal Name',
             Name = 'Account Name',
             RNC_Number__c = '123',
             SIC = '123',
             RecordTypeId = recordTypeId,
             ONTAP__Email__c = 'email@email.com',
             ONTAP__Secondary_Phone__c = '(232) 123-1233',
             OwnerId = bdrUser.Id
         );
        
        insert account;
        
        return account;
    }    
    
    public static Contact newDRContact( Id accountId ){
                        
        Contact contact = new Contact(
            FirstName = 'Wesley',
            LastName = 'Batista',
            ONTAP__UserType__c = 'Portero',
            Phone = '(232) 232-1233',
            AccountId = accountId
        );                        
        
        insert contact;
        
        return contact;
    }
        
    public static ONTAP__Case_Force__c newDRCase( Id accountId, String recordTypeDeveloperName, String caseType ){
        
        Id recordTypeId = Schema.SObjectType.ONTAP__Case_Force__c.getRecordTypeInfosByDeveloperName().get( recordTypeDeveloperName ).getRecordTypeId();
        
        ONTAP__Case_Force__c newCase = new ONTAP__Case_Force__c(
            ONTAP__Case_Number__c = 121221,
            ONTAP__Case_Origin__c = 'OnTap',
            ONTAP__Description__c = 'Test',
            ONTAP__Status__c = 'Abierto',
            Case_Type__c = caseType,
            RecordTypeId = recordTypeId,
            ONTAP__Account__c = accountId
        );
        
        insert newCase;
        
        return newCase;
        
    }
 
    public static ONTAP__Case_Force_Comment__c newDRCaseComment( Id caseId ){
        
        
            ONTAP__Case_Force_Comment__c caseComment = new ONTAP__Case_Force_Comment__c(
                
               ONTAP__Case_Force__c = caseId,
               ONTAP__Comment__c = 'Test comments',
               ONTAP__Public__c = true  
            );
        
        insert caseComment;
        
        return caseComment;
        
    }    
    
    public static ONTAP__Account_Asset__c newDRAccountAsset( Id accountId ){
        
        Id recordTypeId = Schema.SObjectType.ONTAP__Account_Asset__c.getRecordTypeInfosByDeveloperName().get( 'Serialized_Asset' ).getRecordTypeId();
        
        ONTAP__Account_Asset__c accountAsset = new ONTAP__Account_Asset__c(
            RecordTypeId = recordTypeId,
            ONTAP__Asset_Code__c = '123456',
            ONTAP__Asset_Status__c = 'Client',
            ONTAP__Account__c = accountId,
            ONTAP__Undetected_Reason__c = 'Device is lost',
            ONTAP__Undetected_Reason_Comment__c = 'Comments',
            ONTAP__Asset_Model__c = 'Test Asset Model',
            ONTAP__Asset_Name__c = 'Test Asset'
            
        );
        
        insert accountAsset;
        
        return accountAsset;
        
    }
    
    public static Event newDREvent( Id accountId, Id userId ){
        
        Datetime activityDate = Datetime.now();
        activityDate = activityDate.addDays(1);
        Datetime endDateTime = activityDate.addMinutes(30);
        
        
        Event event = new Event(
            ONTAP__Event_Type__c = 'Planned Visit',
            OwnerId = userId,
            WhatId = accountId,
            ActivityDateTime = activityDate,
            EndDateTime = endDateTime,
            ONTAP__Estado_de_visita__c = 'Abierto'
        );
        
        insert event;
        
        return event;
        
    }
    
    public static Task newDRTask( Id accountId, Id userId ){
        
        Date activityDate = Date.today();
        activityDate = activityDate.addDays(2);
        
        Task task = new Task(
            WhatId = accountId,
            OwnerId = userId,
        	Type = 'Ventas',
            Status = 'Open',
            Subject = 'Visita',
            ActivityDate = activityDate
            
        );
 
        insert task;

        return task;
        
    }
    
    
    public static ONTAP__SurveyTaker__c newDRSurvey( Id accountId ){
        
        String SAMPLE_HTML = '<HTML></HTML>';
        String SAMPLE_PROFILES = 'Profile';
        Id recordTypeId = Schema.SObjectType.ONTAP__Survey__c.getRecordTypeInfosByDeveloperName().get('Basic').getRecordTypeId();
        
        Date activeBeginDate = Date.today();
        Date activeEndDate = activeBeginDate.addDays( 15 );
        
        ONTAP__Survey__c survey = new ONTAP__Survey__c(
           RecordTypeId = recordTypeId, 
           ONTAP__Active_Begin_Date__c = activeBeginDate,
           ONTAP__Category__c = 'Event',
           ONTAP__Channel__c = 'AB',
           ONTAP__Active_End_Date__c = activeEndDate,
           ONTAP__HTML5_Bundle__c = SAMPLE_HTML, 
           ONTAP__Profiles__c = SAMPLE_PROFILES           
        );
        
        insert survey;
        
        ONTAP__SurveyTaker__c surveyTaker = new ONTAP__SurveyTaker__c(
        	ONTAP__Account__c = accountId,
            ONTAP__Fecha_Programcion_Encuesta__c = activeBeginDate,
            ONTAP__Survey__c = survey.Id,
            ONTAP__Total_Score__c = 125
        );
 
        insert surveyTaker;

        return surveyTaker;
        
    }    
    
}