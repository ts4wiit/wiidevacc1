/**
 * Created by Dejair.Junior on 10/3/2019.
 */

public with sharing class DRVisitHelper {

    public static Integer getWeekNumber( Date dateToVerify ){

        return( Math.ceil((Double)(dateToVerify.Day()) / 7).intValue());

    }

    public static Boolean isEven( Integer n ){

        Integer r;

        r = System.Math.mod(n, 2);

        if(r == 0) {
            return true;
        } else {
            return false;
        }

    }



}