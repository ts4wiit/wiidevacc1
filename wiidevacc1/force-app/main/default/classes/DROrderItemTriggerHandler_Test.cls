@isTest
public class DROrderItemTriggerHandler_Test {
    
    @isTest
    public static void testOrderItemSharing(){
        
       Id profileId = [ SELECT Id FROM PROFILE WHERE Name = 'DO BDR' ].get(0).Id;
        
       Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_DO').getRecordTypeId(); 
        
	   User bdrUser1 = new User(FirstName='BDR1', LastName='Agent 1', UserName = 'bdr1@test.com', Email='bdr1@test.com',
				Alias='alias1', LocaleSidKey='es', LanguageLocaleKey='es', EmailEncodingKey='ISO-8859-1',ONTAP__Country_Alias__c = 'DO',
				ProfileId = profileId, TimeZoneSidKey='America/Puerto_Rico', Country='DO', isActive = true,User_Route__c='DO9999');

	   insert bdrUser1;    
        
       Account account = new Account( Name = 'Account', ONTAP__SAPCustomerId__c = 'CID1', RecordTypeId = accountRecordTypeId, OwnerId = bdrUser1.Id,
                                           ONTAP__Contact_First_Name__c = 'Contact', ONTAP__Contact_Last_Name__c = 'Lastname', BDR_Route__c = 'DO9999' );
      
       insert account;

        String orderItemRecordType = DROrderItemTriggerHandler.DR_ORDER_ITEM_RECORD_TYPE;
        Id orderItemRecordTypeId =  Schema.SObjectType.ONTAP__Order_Item__c.getRecordTypeInfosByDeveloperName().get( orderItemRecordType ).getRecordTypeId();
        String orderRecordType = DROrderTriggerHandler.DR_ORDER_RECORD_TYPE;
        Id orderRecordTypeId =  Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get( orderRecordType ).getRecordTypeId();


        ONTAP__Order__c order = new ONTAP__Order__c(
           
       	   ONTAP__OrderAccount__c = account.Id,
           ONTAP__Route_Id__c = bdrUser1.User_Route__c,
           RecordTypeId = orderRecordTypeId
       
       );

        
       insert order;
      
       Test.startTest();

       ONTAP__Order_Item__c orderItem = new ONTAP__Order_Item__c(
       		ONTAP__CustomerOrder__c = order.Id,
            RecordTypeId = orderItemRecordTypeId
       );

       insert orderItem;
        
       ONTAP__Order_Item__c newOrderItem = [ SELECT Id,OwnerId FROM ONTAP__Order_Item__c WHERE Id = : orderItem.Id ];       
        
       System.assertEquals( bdrUser1.Id , newOrderItem.OwnerId , '>>> Something went wrong :( assertion failed!');
         
       Test.stopTest();           
        
    }

}