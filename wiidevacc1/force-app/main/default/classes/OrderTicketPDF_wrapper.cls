/**
* Company:        Avanxo México
* Author:        Carlos Pintor / cpintor@avanxo.com
* Project:        AbInbev 2019
* Description:      Apex Wrapper Class to create the attributes of the PDF File template of the Order
* Apex Test:      ISSM_OrderTicket_Wpr_tst
*
* Nu.    Date                 Author                     Description
* 1.0    June 11th, 2019     Carlos Pintor              Created
* 1.1    Octorber 30th, 2019 Marco Zúñiga               Added the total quantity of sold boxes
*                                                       and the total of discounts applied to the order
*
*/

public class OrderTicketPDF_wrapper {
	public String SAPOrderNumber {get; set;}
    public String salesOfficeName {get; set;}
    public String salesOfficeAddress {get; set;}
    public String CreatedDateFecha {get; set;}
	public String CreatedDateHora {get; set;}
    public String paymentMethod {get;set;}
    public String Visit_Transport {get;set;}
    
    public String salesOfficeId {get; set;}
    public String routeIdDescription {get; set;}
    public String customerSAPNumber {get; set;}
    public String customerName {get; set;}
    public String SectorVenta {get;set;}
    public String customerAddress {get; set;}
    
    public String ticketNumber {get; set;}
    
    public String orderCreatedDate {get; set;}
    public String vendedor {get;set;}
    public String appVersion {get; set;}
    
    public String tourName {get; set;}
    
    public Decimal totalSoldBoxes {get;set;}
    public Decimal totalDiscount {get; set;}
    public Decimal totalWOTax {get; set;}
    public Decimal totalTax {get; set;}
    public Decimal totalWTax {get; set;} 
    
    public OrderItems orderItems {get; set;}
    
    public List<OrderItem> emptyOrderItemList {get; set;}
    
    
    public OrderTicketPDF_wrapper(){
        this.SAPOrderNumber = '';
        this.salesOfficeName = '';
        this.salesOfficeId = '';
        this.CreatedDateFecha = '';
        this.CreatedDateHora = '';
        this.paymentMethod = '';
        this.routeIdDescription = '';
        this.customerSAPNumber = '';
        this.customerName = '';
        this.customerAddress = '';
        this.totalWOTax = 0.0;
        this.totalTax = 0.0;
        this.totalWTax = 0.0;
        this.ticketNumber = '';
        this.orderCreatedDate = '';
        this.vendedor = '';
        this.totalSoldBoxes = 0.0;
        this.totalDiscount = 0.0;
        
        this.appVersion = '';
        this.tourName = '';
        this.orderItems = new OrderItems();
        this.emptyOrderItemList = new List<OrderItem>();
    }
    
    public OrderTicketPDF_wrapper(
        String SAPOrderNumber,
        String salesOfficeName, 
        String salesOfficeId, 
        String CreatedDateFecha,
        String CreatedDateHora, 
        String paymentMethod,
        String routeIdDescription,
        String customerSAPNumber,
        String customerName, 
        String customerAddress, 
        Decimal totalWOTax,
        Decimal totalTax,
        Decimal totalWTax,
        Decimal totSoldBoxes,
        Decimal totalDiscount,
        String ticketNumber,
        String orderCreatedDate,
        String vendedor,
        String appVersion,
        String tourName, 
        OrderItems orderItems,
        List<OrderItem> emptyOrderItemList
    ){
        this.SAPOrderNumber = SAPOrderNumber;
        this.salesOfficeName = salesOfficeName;
        this.salesOfficeId = salesOfficeId;
        this.CreatedDateFecha = CreatedDateFecha;
        this.CreatedDateHora = CreatedDateHora;
        this.paymentMethod = paymentMethod;
        this.routeIdDescription = routeIdDescription;
        this.customerSAPNumber = customerSAPNumber;
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.totalWOTax = totalWOTax;
        this.totalTax = totalTax;
        this.totalWTax = totalWTax;
        this.totalSoldBoxes = totalSoldBoxes;
        this.totalDiscount = totalDiscount;
        this.ticketNumber = ticketNumber;
        this.orderCreatedDate = orderCreatedDate;
        this.vendedor = vendedor;
        this.appVersion = appVersion;
        this.tourName = tourName;
        this.orderItems = orderItems;
        this.emptyOrderItemList = emptyOrderItemList;
    }
    
    public class OrderItems {
        public List<OrderItem> orderItemList {get; set;}
        public OrderItems(){
            this.orderItemList = new List<OrderItem>();
        }
        public OrderItems(List<OrderItem> orderItemList){
            this.orderItemList = orderItemList;
        }
    }
    
    /*
public class EmptyBalance {
public List<EmptyBalanceB__c> emptyBalanceList {get; set;}
public Integer balance {get; set;}

public EmptyBalance(){
this.emptyBalanceList = new List<EmptyBalanceB__c>();
this.balance = 0;
}

public EmptyBalance(List<EmptyBalanceB__c> emptyBalanceList, Integer balance){
this.emptyBalanceList = emptyBalanceList;
this.balance = balance;
}
}
*/
    public class OrderItem {
        public String sku {get; set;}
        public String name {get; set;}
        public String shortName {get; set;}
        public string udm {get; set;}
        public Decimal quantity {get; set;}
        public Decimal price {get; set;}
        public Decimal discount {get; set;}
        public Decimal tax {get; set;}
        public Decimal total {get; set;}
        
        
        public OrderItem(){ // Constructor vacío
            this.sku = '';
            this.name = '';
            this.shortName = '';
            this.udm= 'CAJ';
            this.quantity = 0.0;
            this.price = 0.0;
            this.discount = 0.0;
            this.tax = 0.0;
            this.total = 0.0;
        }
        // Constructor con parámetros
        public OrderItem(String sku, String name, String shortName, String udm, Decimal quantity, Decimal price, Decimal discount, Decimal total){ 
            this.sku = sku;
            this.name = name;
            this.shortName = shortName;
            this.udm = udm;
            this.quantity = quantity;
            this.price = price;
            this.discount = discount;
            this.tax = tax;
            this.total = total;
        }
    }
}