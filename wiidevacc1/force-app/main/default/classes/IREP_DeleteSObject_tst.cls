@isTest
private class IREP_DeleteSObject_tst {

	@testSetup static void setup() {
        IREP_CreateObjects_tst.createFullTour(2, 2, 2);
    }
	
	/*@isTest static void testDeleteSObjectBatch() {
		Test.startTest();
			IREP_DeleteSObject_bch objBatch = new IREP_DeleteSObject_bch();
			Database.executebatch(objBatch);
		Test.stopTest();
	}*/
	
	@isTest static void testDeleteSObjectScheduler() {
		Test.startTest();
			IREP_DeleteSObject_sch objBatch = new IREP_DeleteSObject_sch();
			objBatch = new IREP_DeleteSObject_sch(1);
			String sch = '0 0 23 * * ?';
			system.schedule('Share Records', sch, objBatch);
		Test.stopTest();
	}
	
}