/**
* @date 28/05/2019
* @description Class - This class send case push notification to Mobile(IOS and Android).
*/
public without sharing class Local_CaseForce_SendNotification {
    
    public static void verifyStatusToSendNotifications(Map<Id, ONTAP__Case_Force__c> oldCaseMap, List<ONTAP__Case_Force__c> listNewCase){

        List<ONTAP__Push_Notification_UpdateCase_RecordTypes__mdt> listCustomMetadataPushNotificationRecordTypes = 
            												   [ SELECT ONTAP__RecordTypeName__c, ONTAP__Notification_Message__c, ONTAP__Enabled__c 
                												   FROM ONTAP__Push_Notification_UpdateCase_RecordTypes__mdt ];		
        
    	Map<String, ONTAP__Push_Notification_UpdateCase_RecordTypes__mdt> mapRecordTypesCustomMetadata = 
            										new Map<String, ONTAP__Push_Notification_UpdateCase_RecordTypes__mdt>();
		
        System.debug('listCustomMetadataPushNotificationRecordTypes => ' + listCustomMetadataPushNotificationRecordTypes);
        
		for(ONTAP__Push_Notification_UpdateCase_RecordTypes__mdt row : listCustomMetadataPushNotificationRecordTypes){
            mapRecordTypesCustomMetadata.put(row.ONTAP__RecordTypeName__c, row);
        }
        
        for(ONTAP__Case_Force__c caseForce : listNewCase){
            
            String caseStatus 		= oldCaseMap.get(caseForce.Id).ONTAP__Geolocation_Approval_Status__c;
            String caseDescription 	= oldCaseMap.get(caseForce.Id).ONTAP__Description__c;
            
            String recordTypeName 	= Schema.getGlobalDescribe().get('ONTAP__Case_Force__c').getDescribe().getRecordTypeInfosById().get(caseForce.RecordTypeId).getName();
            
            Set<String> setRecordTypesName = mapRecordTypesCustomMetadata.keySet();
            
            System.debug( ' recordTypeName 		=> ' + recordTypeName);
            System.debug( ' setRecordTypesName 	=> ' + setRecordTypesName);
            
            if( setRecordTypesName.contains(recordTypeName) ){
                
                if( mapRecordTypesCustomMetadata.get(recordTypeName).ONTAP__Enabled__c ){
            
                    if(oldCaseMap.get(caseForce.Id).ONTAP__Geolocation_Approval_Status__c != caseForce.ONTAP__Geolocation_Approval_Status__c){
                        
                        if (caseForce.ONTAP__Geolocation_Approval_Status__c == 'Approved' || caseForce.ONTAP__Geolocation_Approval_Status__c == 'Rejected'){
                            
                            ONTAP__Push_Notification_UpdateCase_RecordTypes__mdt customMestadataValue = mapRecordTypesCustomMetadata.get(recordTypeName);
                                
                            customMestadataValue.ONTAP__Notification_Message__c = customMestadataValue.ONTAP__Notification_Message__c + ' ' + caseForce.ONTAP__Geolocation_Approval_Status__c;
                                
                            mapRecordTypesCustomMetadata.put(recordTypeName, customMestadataValue);
                            
                            verifyStatusToSendNotifications(oldCaseMap, caseForce, mapRecordTypesCustomMetadata.get(recordTypeName));
                        }
                        
                    }else{
                                        
                        verifyStatusToSendNotifications(oldCaseMap, caseForce, mapRecordTypesCustomMetadata.get(recordTypeName));
                    }
                }
            }
        }  
    } 
    
    public static void verifyStatusToSendNotifications( Map<Id, ONTAP__Case_Force__c> oldCaseMap, ONTAP__Case_Force__c caseForce, 
                                                      	ONTAP__Push_Notification_UpdateCase_RecordTypes__mdt customMetadata ){
        
        ONTAP__OnTapSettings__c lOnTapSettings = ONTAP__OnTapSettings__c.getInstance();
        
        //Need to review the functionality of this
        String extraFields = getCaseFieldsJson(oldCaseMap.get(caseForce.Id), caseForce);
        
        ONTAP__Push_Notifications__c pushNotificationSettings = ONTAP__Push_Notifications__c.getValues('ChatterPushNotification');      
        
        if(pushNotificationSettings != null){
            
            if(pushNotificationSettings.ONTAP__iOS_Enabled__c){  
                
                Map<String, Object> payload = new Map<String, Object>();
                
                payload = Messaging.PushNotificationPayload.apple( customMetadata.ONTAP__Notification_Message__c + ' ' + 
                                                                   caseForce.ONTAP__Geolocation_Approval_Status__c, '', null, null);               
                              
                payload.put('type', 'caseforce_approval');
                payload.put('content', customMetadata.ONTAP__Notification_Message__c); 
                payload.put('case_id', caseForce.Id);
                payload.put('extra_fields', extraFields); 
                
                Messaging.PushNotification msgToBeSend = new Messaging.PushNotification();   
                
                try{
                    Set<String> setBDRToSend = new Set<String>();	
                    setBDRToSend.add(caseForce.OwnerId);
                    msgToBeSend.setPayload(payload);
                    msgToBeSend.send(lOnTapSettings.ONTAP__Connected_APP_for_IOS__c, setBDRToSend);
                    
                }catch(Exception e){
                    System.debug(LoggingLevel.INFO,'Local_CaseForce_SendNotification.verifyStatusToSendNotifications.Android.Exception.e.getMessage(): ' + e.getMessage());
                }   
            } 
            
            if(pushNotificationSettings.ONTAP__Android_Enabled__c){
                
                Messaging.PushNotification lMsgToBeSend = new Messaging.PushNotification();
                
                Map<String, Object> payload = new Map<String, Object>();
                
                //Need to make this feature generic
                payload.put('type', 'case_changed');
                payload.put('content', customMetadata.ONTAP__Notification_Message__c); 
                payload.put('extra_fields', 'case_id');
                payload.put('case_id', caseForce.Id);
                
                try{
                    Set<String> lSetBDRToSend = new Set<String>();
                    lSetBDRToSend.add(caseForce.OwnerId);   
                    lMsgToBeSend.setPayload(payload);
                    lMsgToBeSend.send(lOnTapSettings.ONTAP__Connected_APP_for_ANDROID__c, lSetBDRToSend);
                    
                }catch(Exception e){
                    System.debug(LoggingLevel.INFO,'Local_CaseForce_SendNotification.verifyStatusToSendNotifications.iOs.Exception.e.getMessage(): ' + e.getMessage());
                } 
            }
        }        
    }
    
    private static String getCaseFieldsJson(ONTAP__Case_Force__c oldCase, ONTAP__Case_Force__c newCase){
        
        String description = String.isBlank(newCase.ONTAP__Description__c) ? oldCase.ONTAP__Description__c : newCase.ONTAP__Description__c;
        
        String geoLocationCase = newCase.ONTAP__Geolocation_Approval_Status__c != null ? newCase.ONTAP__Geolocation_Approval_Status__c : '';
        
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('ONTAP__Geolocation_Approval_Status__c', geoLocationCase);
        gen.writeStringField('ONTAP__Description__c', description == null ? '' : description);
        gen.writeEndObject();
        return gen.getAsString();
    }
}