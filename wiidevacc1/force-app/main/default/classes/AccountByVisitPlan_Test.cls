/****************************************************************************************************
    General Information
    -------------------
    author: Gerardo Martínez
    email: g.martinez.cabral@accenture.com
    company: Accenture
    Project: WIIT
    Customer: AbInBev
    Description:this is a test class for Account By Visit Plan elimination.

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       21/01/2019        Gerardo Martínez             Creation Class
****************************************************************************************************/
@isTest public class AccountByVisitPlan_Test {

  /**
    * Method to deleteAccountByVisitPlanObject
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest public static void testDeleteAccountByVisitPlan (){
        
		ONTAP__Route__c route =  new ONTAP__Route__c();
		route.VisitControl_ByPass__c = true;        
        insert route;
        
        VisitPlan__c visitPlan =  new VisitPlan__c();
        visitPlan.Route__c= route.Id;
        insert visitPlan;
        
        AccountByVisitPlan__c newObj = new AccountByVisitPlan__c();
        newObj.VisitPlan__c=visitPlan.Id;
        insert newObj;
        
        Test.startTest();
        
        delete newObj;
        
        Test.stopTest();
        
    }
}