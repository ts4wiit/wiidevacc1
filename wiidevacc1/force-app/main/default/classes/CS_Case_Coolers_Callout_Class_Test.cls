/* -----------------------------------------------------------------------------------------------
* AB InBev :: Customer Service
* -----------------------------------------------------------------------------------------------
* Clase: CS_CASE_COOLERS_CALLOUT_cls_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* -----------------------------------------------------------------------------------------------
* Date                 User                   Description
* 19/02/2019           Debbie Zacarias       Creacion de la clase test para Callout
*/
@isTest
public class CS_Case_Coolers_Callout_Class_Test 
{
    
    /**
    * Method for create data 
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @testSetup
    static void loadDataToTest()
    {
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        ISSM_TypificationMatrix__c typMat = new ISSM_TypificationMatrix__c();
        typMat.CS_Days_to_End__c = 1;
        insert typMat;
        
        ONTAP__Product__c product = new ONTAP__Product__c(ONTAP__MaterialProduct__c = 'Product Test', ONTAP__ProductCode__c = '3005686');
        insert product;
        
        ONTAP__Product__c product1 = new ONTAP__Product__c(ONTAP__MaterialProduct__c = 'Product Test', ONTAP__ProductCode__c = '3005687');
        insert product1;
        
        String rtInstallationCooler = Label.CS_RT_Installation_Cooler;
        String rtRetirementCooler = Label.CS_RT_Retirement_Cooler;
        
        RecordType rectypeSV = ([SELECT id, DeveloperName FROM RecordType WHERE DeveloperName=:rtRetirementCooler]);        
        RecordType rectype = ([SELECT id, DeveloperName FROM RecordType WHERE DeveloperName=:rtInstallationCooler]);
        
        Case caseTest = new Case();
        caseTest.Accountid = accountTest.Id;
        caseTest.Description = 'Test Description';
        caseTest.Status = 'New';
        caseTest.Subject = 'Subject';
        caseTest.ISSM_TypificationNumber__c = typMat.Id;
        caseTest.HONES_Case_Country__c = 'El Salvador';
        caseTest.CS_Send_To_Mule__c = false;
        caseTest.RecordTypeId = rectype.id;
        caseTest.CS_skuProduct__c = '3005686';
        caseTest.ISSM_Count__c = 1;
        insert caseTest;
        
        Case caseTest1 = new Case();
        caseTest1.Accountid = accountTest.Id;
        caseTest1.Description = 'Test Description';
        caseTest1.Status = 'New';
        caseTest1.Subject = 'Subject';
        caseTest1.ISSM_TypificationNumber__c = typMat.Id;
        caseTest1.HONES_Case_Country__c = 'El Salvador';
        caseTest1.CS_Send_To_Mule__c = false;
        caseTest1.RecordTypeId = rectype.id;
        caseTest1.CS_skuProduct__c = '3005687';
        caseTest1.ISSM_Count__c = 1;
        insert caseTest1;
                
        Case caseTestE = new Case();
        caseTestE.Accountid = accountTest.Id;
        caseTestE.Description = 'Test Description';
        caseTestE.Status = 'New';
        caseTestE.Subject = 'Subject';
        caseTestE.ISSM_TypificationNumber__c = typMat.Id;
        caseTestE.HONES_Case_Country__c = 'El Salvador';
        caseTestE.CS_Send_To_Mule__c = false;
        caseTestE.RecordTypeId = rectype.id;
        caseTestE.CS_skuProduct__c = '3005688';
        caseTestE.ISSM_Count__c = 2;
        insert caseTestE;
        
        Case caseTestHN = new Case();
        caseTestHN.Accountid = accountTest.Id;
        caseTestHN.Description = 'Test Description';
        caseTestHN.Status = 'New';
        caseTestHN.Subject = 'Subject';
        caseTestHN.ISSM_TypificationNumber__c = typMat.Id;
        caseTestHN.HONES_Case_Country__c = 'Honduras';
        caseTestHN.CS_Send_To_Mule__c = false;
        caseTestHN.RecordTypeId = rectype.id;
        caseTestHN.CS_skuProduct__c = '3005686';
        caseTestHN.ISSM_Count__c = 1;
        insert caseTestHN;
        
        Case caseTest1HN = new Case();
        caseTest1HN.Accountid = accountTest.Id;
        caseTest1HN.Description = 'Test Description';
        caseTest1HN.Status = 'New';
        caseTest1HN.Subject = 'Subject';
        caseTest1HN.ISSM_TypificationNumber__c = typMat.Id;
        caseTest1HN.HONES_Case_Country__c = 'Honduras';
        caseTest1HN.CS_Send_To_Mule__c = false;
        caseTest1HN.RecordTypeId = rectype.id;
        caseTest1HN.CS_skuProduct__c = '3005687';
        caseTest1HN.ISSM_Count__c = 1;
        insert caseTest1HN;

        Case caseTestEHN = new Case();
        caseTestEHN.Accountid = accountTest.Id;
        caseTestEHN.Description = 'Test Description';
        caseTestEHN.Status = 'New';
        caseTestEHN.Subject = 'Subject';
        caseTestEHN.ISSM_TypificationNumber__c = typMat.Id;
        caseTestEHN.HONES_Case_Country__c = 'Honduras';
        caseTestEHN.CS_Send_To_Mule__c = false;
        caseTestEHN.RecordTypeId = rectype.id;
        caseTestEHN.CS_skuProduct__c = '3005688';
        caseTestEHN.ISSM_Count__c = 2;
        insert caseTestEHN;
        
        CS_CoolerSettings__c oSettings = new CS_CoolerSettings__c();
        oSettings.customOrder__c = 'CUSE';
        oSettings.measurecode__c = 'UN';
        oSettings.orderTypeESVInstall__c = 'ZSKB';
        oSettings.orderTypeESVRetire__c = 'ZSKA';
        oSettings.orderTypeHNInstall__c = 'ZHKB';
        oSettings.orderTypeHNRetire__c = 'ZHKA';
        oSettings.paymentMethod__c = '0001';
        oSettings.quantity__c = 1;
        oSettings.role__c = 'SH';
        oSettings.URL__c = 'http://wiit-hndesv-orders-sfdc-to-sap.de-c1.cloudhub.io/generateOrders';
        Insert oSettings;
    }
    
    /**
    * Method for test when get success response
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest static void testCallout()
    {
        String rtInstallationCooler = Label.CS_RT_Installation_Cooler;        
        Case caso = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'El Salvador' AND CS_skuProduct__c = '3005686']);
        Case caso1 = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'El Salvador' AND CS_skuProduct__c = '3005687']);
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CS_MockHTTPResponseCoolerCalloutSuccess());
        
        // Call method to test.
        // from the class that implements HttpCalloutMock. 
        CS_Case_Coolers_Callout_Class.sendRequest(caso.id);
        
        List<string> lstIdCase = new List<string>();
        lstIdCase.add(caso1.id);
        CS_Case_Coolers_Callout_Class.sendRequest(lstIdCase);        
    }
    
    /*
    * Method for test when get error response
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest static void testCallout_caseError()
    {        
        String rtInstallationCooler = Label.CS_RT_Installation_Cooler;        
        List<Case> caso = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'Honduras' AND CS_skuProduct__c = '3005686']);
        List<Case> caso1 = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'Honduras' AND CS_skuProduct__c = '3005687']);
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CS_MockHTTPResponseCoolerCalloutError());
        
        // Call method to test.
        // from the class that implements HttpCalloutMock. 
        if(caso.size() > 0){
        	CS_Case_Coolers_Callout_Class.sendRequest(caso[0].id);
        }
        
        List<string> lstIdCase = new List<string>();
        if(caso1.size() > 0){
        	lstIdCase.add(caso1[0].id);
        }
        CS_Case_Coolers_Callout_Class.sendRequest(lstIdCase);    } 
    

    
    @isTest static void testCallout_SV()
    {
        String rtInstallationCooler = Label.CS_RT_Installation_Cooler;        
        Case caso = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'El Salvador' AND CS_skuProduct__c = '3005686']);
        Case caso1 = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'El Salvador' AND CS_skuProduct__c = '3005687']);
        Case caso2 = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'El Salvador' AND CS_skuProduct__c = '3005688']);
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CS_MockHTTPResponseCoolerCalloutSuccess());
        Test.setMock(HttpCalloutMock.class, new CS_MockHTTPResponseCoolerCalloutSuccess());
        
        // Call method to test.
        // from the class that implements HttpCalloutMock. 
        CS_Case_Coolers_Callout_Class.sendRequest(caso.id);
        CS_Case_Coolers_Callout_Class.sendRequest(caso2.id);
        List<string> lstIdCase = new List<string>();
        lstIdCase.add(caso1.id);
        CS_Case_Coolers_Callout_Class.sendRequest(lstIdCase);
    }

    @isTest static void testCalloutHN_SV()
    {
        String rtInstallationCooler = Label.CS_RT_Installation_Cooler;        
        Case caso = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'Honduras' AND CS_skuProduct__c = '3005686']);
        Case caso1 = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'Honduras' AND CS_skuProduct__c = '3005687']);
        Case caso2 = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'Honduras' AND CS_skuProduct__c = '3005688']);
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CS_MockHTTPResponseCoolerCalloutSuccess());
        Test.setMock(HttpCalloutMock.class, new CS_MockHTTPResponseCoolerCalloutSuccess());
        
        // Call method to test.
        // from the class that implements HttpCalloutMock. 
        CS_Case_Coolers_Callout_Class.sendRequest(caso.id);
        CS_Case_Coolers_Callout_Class.sendRequest(caso2.id);
        List<string> lstIdCase = new List<string>();
        lstIdCase.add(caso1.id);
        CS_Case_Coolers_Callout_Class.sendRequest(lstIdCase);
    }
    
    
	@isTest static void testCalloutError_SV()
    {
        String rtInstallationCooler = Label.CS_RT_Installation_Cooler;        
        Case caso = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'El Salvador' AND CS_skuProduct__c = '3005686']);
        Case caso1 = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'El Salvador' AND CS_skuProduct__c = '3005687']);
        Case caso2 = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'El Salvador' AND CS_skuProduct__c = '3005688']);
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CS_MockHTTPResponseCoolerCalloutError());
        
        // Call method to test.
        // from the class that implements HttpCalloutMock. 
        CS_Case_Coolers_Callout_Class.sendRequest(caso.id);
        
        List<string> lstIdCase = new List<string>();
        lstIdCase.add(caso1.id);
        CS_Case_Coolers_Callout_Class.sendRequest(lstIdCase);        

        List<string> lstIdCase1 = new List<string>();
        lstIdCase1.add(caso2.id);
        CS_Case_Coolers_Callout_Class.sendRequest(lstIdCase1);        

    }
    
    @isTest static void testCallout_caseError_SV()
    {        
        String rtInstallationCooler = Label.CS_RT_Installation_Cooler;        
        List<Case> caso = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'Honduras' AND CS_skuProduct__c = '3005686']);
        List<Case> caso1 = ([SELECT id FROM Case WHERE HONES_Case_Country__c = 'Honduras' AND CS_skuProduct__c = '3005687']);
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CS_MockHTTPResponseCoolerCalloutError());
        
        // Call method to test.
        // from the class that implements HttpCalloutMock. 
        if(caso.size() > 0){
        	CS_Case_Coolers_Callout_Class.sendRequest(caso[0].id);
        }
        
        List<string> lstIdCase = new List<string>();
        if(caso1.size() > 0){
        	lstIdCase.add(caso1[0].id);
        }
        CS_Case_Coolers_Callout_Class.sendRequest(lstIdCase);    
    }

    @isTest static void testCalloutESV_SV() {
        Case casoESV = new Case();
        String rtInstallationCooler = 'HONES_RT_Change_Cooler';            
        casoESV.CS_skuProduct__c = '2443314';
        casoESV.ISSM_Count__c = 1;
        casoESV.ISSM_CountRetiro__c = 1;
        casoESV.HONES_Case_Country__c = 'El Salvador';
        casoESV.ISSM_TypificationLevel3__c = 'EFC - Cambio';
        insert casoESV;
        String condicionSegundoEnvio = 'Instalacion';

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CS_MockHTTPResponseCoolerCalloutSuccess());
        
        CS_Case_Coolers_Callout_Class.sendRequest(casoESV.id,condicionSegundoEnvio);

    }

    @isTest static void testCalloutESVError_SV() {
        Case casoESV = new Case();
        String rtInstallationCooler = 'HONES_RT_Change_Cooler';            
        casoESV.CS_skuProduct__c = '2443314';
        casoESV.ISSM_Count__c = 1;
        casoESV.ISSM_CountRetiro__c = 1;
        casoESV.HONES_Case_Country__c = 'El Salvador';
        casoESV.ISSM_TypificationLevel3__c = 'EFC - Cambio';
        insert casoESV;
        String condicionSegundoEnvio = 'Instalacion';

        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CS_MockHTTPResponseCoolerCalloutError());
        
        CS_Case_Coolers_Callout_Class.sendRequest(casoESV.id,condicionSegundoEnvio);

    }

    @isTest static void testCalloutESVRetiro_SV() {
        Case casoESV = new Case();
        String rtInstallationCooler = 'HONES_RT_Change_Cooler';            
        casoESV.CS_skuProduct__c = '2443314';
        casoESV.ISSM_Count__c = 1;
        casoESV.ISSM_CountRetiro__c = 1;
        casoESV.HONES_Case_Country__c = 'El Salvador';
        casoESV.ISSM_TypificationLevel3__c = 'EFC - Cambio';
        insert casoESV;
        String condicionSegundoEnvio = 'Retiro';
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CS_MockHTTPResponseCoolerCalloutError());
        CS_Case_Coolers_Callout_Class.sendRequest(casoESV.id,condicionSegundoEnvio);
    }

    @isTest static void testCalloutESVREFRetiro_SV() {
        Case casoESV = new Case();
        String rtInstallationCooler = 'HONES_RT_Change_Cooler';            
        casoESV.CS_skuProduct__c = '2443314';
        casoESV.ISSM_Count__c = 2;
        casoESV.ISSM_CountRetiro__c = 1;
        casoESV.HONES_Case_Country__c = 'El Salvador';
        casoESV.ISSM_TypificationLevel3__c = 'REF - Retiro';
        insert casoESV;
        String condicionSegundoEnvio = 'Retiro';
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CS_MockHTTPResponseCoolerCalloutError());
        CS_Case_Coolers_Callout_Class.sendRequest(casoESV.id,condicionSegundoEnvio);
    }

    @isTest static void testCalloutHON_SV() {
        Case casoHON = new Case();
        String rtInstallationCooler = 'CS_RT_Installation_Cooler';        
        casoHON.CS_skuProduct__c = '2443313';
        casoHON.ISSM_Count__c = 1;
        casoHON.HONES_Case_Country__c = 'Honduras';
        casoHON.ISSM_TypificationLevel3__c = 'EFI - Instalación';
        insert casoHON;
        String condicionSegundoEnvio = 'Instalacion';
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, new CS_MockHTTPResponseCoolerCalloutSuccess());
        
        CS_Case_Coolers_Callout_Class.sendRequest(casoHON.id,condicionSegundoEnvio);

    }

}