/* ----------------------------------------------------------------------------
* AB InBev :: TELECOLLECTION
* ----------------------------------------------------------------------------
* Clase: HONES_HeaderTelecollectionContr_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 10/01/2019     Herón Zurita       			Testing Telecollection controller methods.
*/

@isTest
private class HONES_HeaderTelecollectionContr_Test{

     /**
    * Method for test the total of calls.
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    
  @isTest static void test_callTotal_UseCase1(){
     HONES_HeaderTelecollectionContr obj01 = new HONES_HeaderTelecollectionContr(); 
     Account acc =  new Account(Name='test',ISSM_CreditLimit__c=10,ONTAP__Credit_Amount__c=10);
     insert acc;
     ONCALL__Call__c call =  new ONCALL__Call__c(ONCALL__POC__c=acc.Id);  
     call.Name = 'Test';
     call.ONCALL__Call_Type__c = 'Telecollection';
     call.ONCALL__Call_Status__c = 'Complete';
     call.RecordTypeId = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByName().get('ISSM_Telecollection').getRecordTypeId();
     insert call;
      
   
    HONES_HeaderTelecollectionContr.callTotal();
  }
    
     /**
    * Method for test calls Pending.
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    
  @isTest static void test_callPending_UseCase1(){
     HONES_HeaderTelecollectionContr obj01 = new HONES_HeaderTelecollectionContr(); 
     Account acc =  new Account(Name='test',ISSM_CreditLimit__c=10,ONTAP__Credit_Amount__c=10);
     insert acc;
     ONCALL__Call__c call =  new ONCALL__Call__c(ONCALL__POC__c=acc.Id);
     call.Name = 'Test';
     call.ONCALL__Call_Type__c = 'Telecollection';
     call.ONCALL__Call_Status__c = 'Pending to call';
     call.RecordTypeId = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByName().get('ISSM_Telecollection').getRecordTypeId();
     insert call;
   
    HONES_HeaderTelecollectionContr.callPending();
  }
    
    /**
    * Method for test calls Incomplete.
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    
    
  @isTest static void test_callIncomplete_UseCase1(){
     HONES_HeaderTelecollectionContr obj01 = new HONES_HeaderTelecollectionContr(); 
     Account acc =  new Account(Name='test',ISSM_CreditLimit__c=10,ONTAP__Credit_Amount__c=10);
     insert acc;
     ONCALL__Call__c call =  new ONCALL__Call__c(ONCALL__POC__c=acc.Id); 
   
     call.Name = 'Test';
     call.ONCALL__Call_Type__c = 'Telecollection';
     call.ONCALL__Call_Status__c = 'Incomplete';
     call.RecordTypeId = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByName().get('ISSM_Telecollection').getRecordTypeId();
     insert call;
   
    HONES_HeaderTelecollectionContr.callIncomplete();
  }
    
    /**
    * Method for test calls Complete.
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    
  @isTest static void test_callComplete_UseCase1(){
	 HONES_HeaderTelecollectionContr obj01 = new HONES_HeaderTelecollectionContr(); 
     Account acc =  new Account(Name='test',ISSM_CreditLimit__c=10,ONTAP__Credit_Amount__c=10);
     insert acc;
     ONCALL__Call__c call =  new ONCALL__Call__c(ONCALL__POC__c=acc.Id);     
     
     call.Name = 'Test';
     call.ONCALL__Call_Type__c = 'Telecollection';
     call.ONCALL__Call_Status__c = 'Complete';
     call.RecordTypeId = Schema.SObjectType.ONCALL__Call__c.getRecordTypeInfosByName().get('ISSM_Telecollection').getRecordTypeId();
     insert call;

    HONES_HeaderTelecollectionContr.callComplete();
  }
}