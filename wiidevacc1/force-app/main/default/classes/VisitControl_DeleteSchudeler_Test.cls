/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: VisitControl_DeleteSchudeler_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest global class VisitControl_DeleteSchudeler_Test {
    
    /**
   * Method for test theSchudeler for routes.
   * @author: g.martinez.cabral@accenture.com
   * @param void
   * @return void
   */
    @isTest static void testExecute(){
        VisitControl_DeleteSchudeler obj01 = new VisitControl_DeleteSchudeler();
        SchedulableContext sc;
        obj01.execute(sc);
    }
    
}