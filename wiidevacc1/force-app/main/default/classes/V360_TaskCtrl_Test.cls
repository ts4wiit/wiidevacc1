/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_TaskCtrl_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest
public class V360_TaskCtrl_Test {

    /**
    * Method to set data parameters
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @testSetup
  	static void setupTestData(){
        test.startTest();
        Id salesZonedevRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(GlobalStrings.SALES_ZONE_RECORDTYPE_NAME).getRecordTypeId();
          
        User u = [Select Id FROM User Limit 1];  
        Account a = New Account(ONTAP__ExternalKey__c='ABC',Name ='ABC', RecordTypeId=salesZonedevRecordTypeId);
        insert a;
          
        V360_SalerPerZone__c ae = New V360_SalerPerZone__c(V360_SalesZone__c =a.Id,V360_User__c=u.Id, V360_Type__c ='Presales');
        insert ae;
   
        Task ta = new Task();
        insert ta;
        
        test.stopTest();
	}
    
    /**
    * Test method to get all the tasks related to an account
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return Void
    */
    @isTest static void test_getTasksByAccount_UseCase1(){
   
        V360_TaskCtrl tsk = new V360_TaskCtrl();
        V360_TaskCtrl.getTasksByAccount('test');
   
    } 
}