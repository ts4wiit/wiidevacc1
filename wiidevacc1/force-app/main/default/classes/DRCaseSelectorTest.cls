@isTest
public class DRCaseSelectorTest {
    
    @isTest
    static void testCaseSelectorMethods(){
        
        String recordTypeDeveloperName;
        
        Account account = DRFixtureFactory.newDRAccount();
        List<Id> caseIds = new List<Id>();
        
        //Create cases
 
        recordTypeDeveloperName = CaseForceEventTriggerHandler.DR_SALES_PROCESS_RECORD_TYPE;
        ONTAP__Case_Force__c caseSales = DRFixtureFactory.newDRCase( account.Id, recordTypeDeveloperName , 'No visita vendedor' );
        caseIds.add( caseSales.Id );
        
        recordTypeDeveloperName = CaseForceEventTriggerHandler.DR_ASSET_INVESTIGATION_RECORD_TYPE;
        ONTAP__Case_Force__c caseAssetInvestigation = DRFixtureFactory.newDRCase( account.Id, recordTypeDeveloperName , 'Asset investigation' );
        caseIds.add( caseAssetInvestigation.Id );
        
        recordTypeDeveloperName = CaseForceEventTriggerHandler.DR_ASSET_MOVEMENT_RECORD_TYPE;
        ONTAP__Case_Force__c caseMovement = DRFixtureFactory.newDRCase( account.Id, recordTypeDeveloperName , 'Movimiento' );
        caseIds.add( caseMovement.Id );
        
        recordTypeDeveloperName = CaseForceEventTriggerHandler.DR_ASSET_MAINTENANCE_RECORD_TYPE;
        ONTAP__Case_Force__c caseMaintenance = DRFixtureFactory.newDRCase( account.Id, recordTypeDeveloperName , 'Asset Maintenance' );         
        caseIds.add( caseMaintenance.Id );
        
        recordTypeDeveloperName = CaseForceEventTriggerHandler.DR_SALES_PROCESS_RECORD_TYPE;
        
        Test.startTest();  
        
        DRCaseSelector caseSelector = new DRCaseSelector( caseIds );
        
        Map<Id,ONTAP__Case_Force__c> caseMap =  caseSelector.filterByRecordTypeName( recordTypeDeveloperName );
        
        //System.assertEquals( caseSales.Id , caseMap.get( caseSales.Id ).Id , 'Assert error for filterByRecordTypeName');
        
        Map<String,String> bdrMap = caseSelector.getCaseIdToBRDIdMap();
        
        String bdrId = account.OwnerId;
        
        //System.assertEquals( bdrId , bdrMap.get( caseMovement.Id ) , 'Assert error for getCaseIdToBRDIdMap');
        
        Test.stopTest();        
        
    }    

}