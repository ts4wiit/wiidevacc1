/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Rodrigo Resendiz
Proyecto: ISSM - DSD
Descripción: Batch class that execute massive tour deletion job
------ ---------- -------------------------- -----------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------------------------------
1.0    21/05/2018  Rodrigo Resendiz          Created Class       
*******************************************************************************/

public class MassiveTourDeletion_bch implements Database.Batchable<SObject>, Database.AllowsCallouts {
    private static final String TELESALES_RECORDTYPE_NAME = 'Telesales';
    private final Set<Id> tourScope;
    private final String tourRecordType;
    
	public MassiveTourDeletion_bch(Set<Id> tour_set, String tourRecordType){
        tourScope = tour_set;
        this.tourRecordType = tourRecordType;
    }
    
    public List<SObject> start (Database.BatchableContext ctx){
        List<ONTAP__Tour__c> tourScope_lst = new List<ONTAP__Tour__c>();
        for(ONTAP__Tour__c tour:[SELECT Id FROM ONTAP__Tour__c WHERE Id IN: tourScope]){
            tourScope_lst.add(tour);
        }
        return tourScope_lst;
    }
    
    public void execute(Database.BatchableContext ctx, List<SObject> scope){
        List<Id> elmentsToDel_lst = new List<Id>();
        if(tourRecordType != TELESALES_RECORDTYPE_NAME){
            for(Event evt : [SELECT Id FROM Event 
                             WHERE VisitList__c IN: tourScope 
                             LIMIT 10000]){
                elmentsToDel_lst.add(evt.Id);
            }
            TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterDeleteSync');
            TriggerExecutionControl_cls.setAlreadyDone('Event_tgr','AfterDeleteEventsSync');
            if(!test.isRunningTest())
            //EventOperations_cls.deleteEventToExternalObject(new Set<Id>(elmentsToDel_lst));
            Database.delete(elmentsToDel_lst, false);
        }else{
            for(ONCALL__Call__c call : [SELECT Id FROM ONCALL__Call__c WHERE CallList__c IN: tourScope LIMIT 10000]){
                elmentsToDel_lst.add(call.Id);
            }
            Database.delete(elmentsToDel_lst, false);
        }
    }
    
    public void finish(Database.BatchableContext ctx){
        TriggerExecutionControl_cls.setAlreadyDone('Tour_tgr','AfterDeleteSync');
        Map<Id,ONTAP__Tour__c> tourMap = new Map<Id,ONTAP__Tour__c>();
        for(ONTAP__Tour__c tour:[SELECT Id, RecordTypeId 
                                 FROM ONTAP__Tour__c WHERE Id IN: tourScope]){
            tourMap.put(tour.Id, tour);
        }
        //syncDeleteToursToExternalObject(tourMap);
        Database.delete(tourMap.values(), false);
    }
    /**
     * @description Method to delete tours from Heroku
     * @param 
     * @return void
     * @version 1.0
     
    public static void syncDeleteToursToExternalObject(map<Id, ONTAP__Tour__c> mapTour){
    	map<Id,Id> mapIds = new map<Id,Id>();
    	set<Id> setIds = new set<Id>();
    	
    	list<String> lstRecordType = Label.TourRecordTypesToSync.split(',');
    	
    	list<RecordType> lstRT = [Select Id From RecordType Where DeveloperName = :lstRecordType];
    	for(RecordType rt : lstRT)
    		mapIds.put(rt.Id, rt.Id);
    	
    	for(ONTAP__Tour__c objTour : mapTour.values()){
    		if(mapIds.containsKey(objTour.RecordTypeId))
    			setIds.add(objTour.Id);
    	}
    	
    	if(!setIds.isEmpty())
    		deleteExternalToursWS(setIds);
    }**/
    /**
     * @description Method to delete tours from Heroku
     * @param 
     * @return void
     * @version 1.0
     
    public static void deleteExternalToursWS(set<Id> setIds){
    	SyncObjects.DeleteTourObject objSync = new SyncObjects.DeleteTourObject();
    	for(String strId :setIds){
    		SyncObjects.ObjTourDelete objDel = new SyncObjects.ObjTourDelete();
    		objDel.salesforceid = strId;
    		
    		objSync.tours.add(objDel);
    	}
    	
    	if(!objSync.tours.isEmpty()){
            String strStatusCode;
            if(!test.isRunningTest())
    		strStatusCode = SyncObjects.calloutDeleteHerokuTours(objSync);
    		if(strStatusCode == '200'){
    			System.debug('\nDELETE SUCCESS');
    		}

    	}
    }**/
}