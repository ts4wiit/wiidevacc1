public class DRUserVisitsService {
    
    private static Time START_TIME = Time.newInstance(7, 0, 0, 0);
    private static Integer VISIT_DURATION = 15;
    private static final String SUBJECT = 'Planned Visit';
    private static final String EVENT_TYPE = 'Planned Visit';
    private static Integer VISIT_HORIZON_DAYS = 30;
    
    private List<DOVisitPlan__c> visitPlans = new List<DOVisitPlan__c>();
    private Set<String> routesSet = new Set<String>();
    private Set<String> sapIdsSet = new Set<String>();
    private User executionUser = new User();
    List<Account> accounts = new List<Account>();
    Map<String,String> sapToIdMap = new Map<String,String>();
    Map<String,String> sapToAccountNameMap = new Map<String,String>();
    private List<String> weekdays = new List<String>{'Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Suturday'};
    private List<Integer> weekdayNumbers = new List<Integer>{1,2,4,8,16,32,64};    
    private Map<String,Integer> weekdaysMap = new Map<String,Integer>();
    List<Event> events = new List<Event>();
    
    public class UserVisitsServiceException extends Exception {}
    
    public DRUserVisitsService( User executionUser ){
        this.executionUser = executionUser;
    }
    
    public void run(){
        
        this.setWeekdaysMap();
        this.getVisitPlans();
        this.getSets();
        this.getAccounts();
        this.getAccountsMap();
        this.processVisits();
        this.insertEvents();    
    }
    
    private void setWeekdaysMap(){
        
        Integer i = 0;
        
        for( String weekday : weekdays ){
            
			weekdaysMap.put( weekday, weekdayNumbers.get( i ) );
            i++;
            
        }
        
    }
    
    private void getVisitPlans(){
        
        this.visitPlans = [ SELECT CreatedById
                                   ,CreatedDate
                                   ,EndDate__c
                                   ,External_ID__c
                                   ,Id
                                   ,IsActive__c
                                   ,Name
                                   ,OwnerId
                                   ,Recurrence__c
                                   ,Route__c
                                   ,SAPCustomerID__c
                                   ,Sequence__c
                                   ,StartDate__c
                                   ,SystemModstamp
                                   ,Weekday__c 
                              FROM DOVisitPlan__c
                             WHERE IsActive__c = true
                               AND Route__c = : this.executionUser.User_Route__c
                          ORDER BY Route__c,Sequence__c ];      
        
    }
    
    private void getSets(){
        
        for( DOVisitPlan__c visitPlan : visitPlans ){
            
            routesSet.add( visitPlan.Route__c );
            sapIdsSet.add( visitPlan.SAPCustomerID__c );
            
        }        
        
    }
    
        
    private void getAccounts(){
        
        this.accounts = [ SELECT Id,ONTAP__SAPCustomerId__c,Name FROM Account WHERE ONTAP__SAPCustomerId__c in : sapIdsSet  ];
        
    }
    
    private void getAccountsMap(){
        
        for( Account account : accounts ){
            
            this.sapToIdMap.put( account.ONTAP__SAPCustomerId__c,account.Id );
            this.sapToAccountNameMap.put( account.ONTAP__SAPCustomerId__c,account.Name );
            
        }
        
    }
    
    private Map<Integer,Time> initTimeMap(){

         Map<Integer,Time> timeMap = new Map<Integer,Time>();
         Integer intWeekday;
        
         for( String weekday : weekdays ){

             intWeekday = this.weekdaysMap.get( weekday );
             timeMap.put( intWeekday, START_TIME );
             
         }

         return timeMap;
        
    }
    
    private void processVisits(){
        
            System.debug('>>> Processing visits');
            Date startVisitDate;
            Integer intWeekday;
            Date endVisitDate = Date.today().addDays( VISIT_HORIZON_DAYS );
        
            
            Map<Integer,Time> weekdayTimeMap = this.initTimeMap();
            
            if (executionUser.VisitsScheduledUntil__c == null)  
                startVisitDate = Date.today();
            else
                startVisitDate = this.executionUser.VisitsScheduledUntil__c;
            
            for( String weekday : weekdays ){

                intWeekday = this.weekdaysMap.get( weekday );
                
                for( DOVisitPlan__c routePlan : visitPlans ){
                    
                    if ( routePlan.Weekday__c == intWeekday ){
                        
                        Time visitTime = weekdayTimeMap.get( intWeekday );
                        weekdayTimeMap.remove( intWeekday );
                        weekdayTimeMap.put( intWeekday, visitTime.addMinutes( VISIT_DURATION ) );
                        this.createVisits( weekday , routePlan, startVisitDate, endVisitDate, visitTime );
                        
                    }
                    
                }               
                
            }
            
            this.setUserLastDate();
                
    }
    
    private void createVisits( String weekday, DOVisitPlan__c visitPlan, Date startVisitDate, Date endVisitDate, Time visitTime  ){
        
        List<Date> dates = this.getNextWeekDays(  weekday, startVisitDate, endVisitDate );
        
        for( Date myDate : dates ){
            
            Datetime startDateTime = Datetime.newInstance( myDate , visitTime);
            Event event = new Event();
            event = this.createEvent( visitPlan, startDateTime );
            if ( event != null ){
                
                events.add( event );
                
            }
            
        }
        
    }
    
    private List<Date> getNextWeekDays( String weekday, Date startDate, Date endDate ){
        
        List<Date> dates = new List<Date>();
        Date thisDate = startDate;
        String thisWeekday;
        Datetime dummyDate;
        
        while( thisDate <= endDate ){
            
            dummyDate = Datetime.newInstance( thisDate, START_TIME );
            thisWeekday = dummyDate.format('EEEE');           
            
            if( thisWeekday == weekday ){
                
                dates.add( thisDate );
                
            }
            
            thisDate = thisDate.addDays( 1 );
            
        }
        
        return dates;
        
    }
    
    private Event createEvent( DOVisitPlan__c visitPlan, DateTime startDateTime ){
        
        DateTime endDateTime = startDateTime.addMinutes( VISIT_DURATION );
        String whatId = this.sapToIdMap.get( visitPlan.SAPCustomerID__c );
        String ownerId = this.executionUser.Id;
        String accountName = this.sapToAccountNameMap.get( visitPlan.SAPCustomerID__c );
        
        if ( whatId != null ) {
        
            Event event = new Event( WhatId = whatId, OwnerId = ownerId, ActivityDateTime = startDateTime,
                                     DurationInMinutes = VISIT_DURATION,EndDateTime = endDateTime,IsRecurrence = false,
                                     ONTAP__Sequence__c = visitPlan.Sequence__c, subject = accountName , IsAllDayEvent = false,
                                     ONTAP__Event_Type__c = EVENT_TYPE );
            return event;
            
        } else {
            
            return null;
        }
        
    }
    
    private void insertEvents(){
        
        Database.SaveResult[] results = Database.insert( events , false);
        
        for (Database.SaveResult sr : results) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('>>> Successfully created visit: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('>>> The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('>>> Event fields that affected this error: ' + err.getFields());
                }
            }
        }   
    }
    
    private void setUserLastDate(){
        
        Date maxDate = Date.today();
        maxDate = maxDate.addDays( VISIT_HORIZON_DAYS );
        
        User user = new User( Id = this.executionUser.Id, VisitsScheduledUntil__c = maxDate );
        
        update user;
        
    }
}