public class AttachmentSelector {
    
    private List<String> parentIds = new List<String>();
    private Map<String,Attachment> attachmentsMap = new Map<String,Attachment>();
    private Set<String> attachmentIdsSet = new Set<String>();
    private Map<String,List<Attachment>> parentIdToAttachmentsMap = new Map<String,List<Attachment>>();
    
    public AttachmentSelector( List<String> parentIds ){
        
        this.parentIds = parentIds;
        
    }
    
    public Map<String,List<Attachment>> getParentIdToAttachmentsMap(){
        
        this.selectAttachments();
        this.setAttachmentIdsSet();
        this.setAttachmentMap();
        
        if( parentIdToAttachmentsMap.isEmpty() ){
            
            return null;
            
        } else {
            
            return parentIdToAttachmentsMap;
            
        }
        
    }
    
    private void setAttachmentMap(){
        
        for( String parentId : attachmentIdsSet ){

            List<Attachment> attachments = new List<Attachment>();  
            
            for( String attachmentId : this.attachmentsMap.keySet() ){
                              
                if ( this.attachmentsMap.get( attachmentId ).ParentId == parentId ){
                    
                    attachments.add( this.attachmentsMap.get( attachmentId ) );
                    
                }                
                
            }

            parentIdToAttachmentsMap.put( parentId, attachments );
         
        }
        
    }
    
    private void setAttachmentIdsSet(){
        
        for( String attachmentId : this.attachmentsMap.keySet() ){
            
            attachmentIdsSet.add( this.attachmentsMap.get( attachmentId ).ParentId );
            
        }
        
    }
    
    private void selectAttachments(){
        
        List<Attachment> attachments = [ SELECT Body,
                                                BodyLength,
                                                ContentType,
                                                CreatedById,
                                                CreatedDate,
                                                Description,
                                                Id,
                                                IsDeleted,
                                                IsPrivate,
                                                LastModifiedById,
                                                LastModifiedDate,
                                                Name,
                                                OwnerId,
                                                ParentId 
                                           FROM Attachment
                                          WHERE ParentId in : this.parentIds ];
        
        for( Attachment attachment : attachments ){
            
            this.attachmentsMap.put( attachment.Id, attachment );
            
        }
        
    }

}