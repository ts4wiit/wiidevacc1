/* ----------------------------------------------------------------------------
* AB InBev :: Oncall
* ----------------------------------------------------------------------------
* Clase: MockHTTPResponseGeneratorFlexibleData.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 19/12/2019        Daniel Hernández          Creation of methods.
*/
@isTest
global class MockHTTPResponseGeneratorCaseAssetacc implements HttpCalloutMock {
    
    /**
    * Test method for create mock for felxible data call out
    * Created By: c.leal.beltran@accenture.com@accenture.com
    * @param void
    * @return void
    */
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response.
        // Set response values, and
        // return response.
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatus('OK');
        res.setStatusCode(200);
        res.setBody('{"EtAvisos": { "id":"123", "Number":"123", "System":"test", "item": {"NotifNo": "000010397371", "Equipment": "400193352 "} }}');
        
        System.debug('Respuesta'+res);
        return res;
    }
    
    
}