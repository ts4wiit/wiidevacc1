public class DRAccountChangeHelper {
    
    private DRProspectOutboundMessage__e message = new DRProspectOutboundMessage__e();
    private static final Integer DEFAULT_LENGTH = 130000;
    public Map<String,List<Attachment>> accountIdToAttachmentsMap {get;set;}
    
    public DRAccountChangeHelper( DRProspectOutboundMessage__e message ){
        
        this.message = message;
        
    }
         
    public DRProspectOutboundMessage__e enrichProspectEvent(){  
                    
            List<Attachment> attachments = new List<Attachment>();
            System.debug('>>> Attachments');
            System.debug(attachments);
            
            try{
                
                 attachments = accountIdToAttachmentsMap.get( message.AccountId__c );
                
            } catch(Exception e){
                
                System.debug('>>> Attachment error message ' + e.getMessage());
                return null;
                
            }
            
            Integer counter = 0;
        
        try{
            
            for( Attachment attachment : attachments ){

                counter++;

                String bodyContent = EncodingUtil.base64Encode( attachment.body );
                    
                System.debug('>>> Body Size: ' + bodyContent.length());
        
                if ( counter == 1 ){
                    
                    if (  bodyContent.length() <= DEFAULT_LENGTH ){
                            
                            message.frontImageURL__c = bodyContent;
                        
                    } else if (  bodyContent.length() > DEFAULT_LENGTH && bodyContent.length() <= 2 * DEFAULT_LENGTH ){
                            
                            message.frontImageURL__c = bodyContent.substring(0, DEFAULT_LENGTH); 
                            message.frontImageURL2__c = bodyContent.substring(DEFAULT_LENGTH); 
                            
                    } else if (  bodyContent.length() > 2 * DEFAULT_LENGTH &&  bodyContent.length() < 3 * DEFAULT_LENGTH ){
                            
                            message.frontImageURL__c = bodyContent.substring(0, DEFAULT_LENGTH); 
                            message.frontImageURL2__c = bodyContent.substring(DEFAULT_LENGTH, 2 * DEFAULT_LENGTH); 
                            message.frontImageURL3__c = bodyContent.substring(2 * DEFAULT_LENGTH); 
                            
                    } else {
                                                      
                            message.frontImageURL__c = 'Image too large';
                            message.frontImageURL2__c = 'Image too large';
                            message.frontImageURL3__c = 'Image too large';                       
                    }  
                                                                  
                        
                } else if ( counter == 2 ) {
                        
                        
                    if (  bodyContent.length() <= DEFAULT_LENGTH ){
                            
                            message.backImageURL__c = bodyContent;
                        
                    } else if (  bodyContent.length() > DEFAULT_LENGTH && bodyContent.length() <= 2 * DEFAULT_LENGTH ){
                            
                            message.backImageURL__c = bodyContent.substring( 0, DEFAULT_LENGTH ); 
                            message.backImageURL2__c = bodyContent.substring( DEFAULT_LENGTH ); 
                            
                    } else if (  bodyContent.length() > 2 * DEFAULT_LENGTH &&  bodyContent.length() < 3 * DEFAULT_LENGTH ){
                            
                            message.backImageURL__c = bodyContent.substring( 0, DEFAULT_LENGTH ); 
                            message.backImageURL2__c = bodyContent.substring( DEFAULT_LENGTH, 2 * DEFAULT_LENGTH ); 
                            message.backImageURL3__c = bodyContent.substring( 2 * DEFAULT_LENGTH ); 
                            
                    } else {
                                                      
                            message.backImageURL__c = 'Image too large';
                            message.backImageURL2__c = 'Image too large';
                            message.backImageURL3__c = 'Image too large';                       
                    } 
                        
               } else {
                        
                        continue;
                        
              }       
                     
          }
            
        } catch( Exception e ){
            
			System.debug('>>> Attachment: ' + e.getMessage()); 
            System.debug('>>> Attachment: ' + e.getCause());
            message = null;
            
        }

        return message;
        
    }    

}