public with sharing class ISSM_AuthorizationToken_ctr {
    public String strSapCustomerID {get;set;}
    public String strRoute  {get;set;}
    public ISSM_AuthorizationToken_ctr(ApexPages.StandardController controller) {
        strSapCustomerID = '';
        strRoute = '';
        List<ONTAP__Tour_Visit__c> TourVisit = new List<ONTAP__Tour_Visit__c>();
        Id idRecord = controller.getId();
        TourVisit = (!Test.isRunningTest())?[SELECT ONTAP__SAPCustomerId__c, ONTAP__TourId__c FROM ONTAP__Tour_Visit__c WHERE Id =: idRecord LIMIT 1]:[SELECT ONTAP__SAPCustomerId__c, ONTAP__TourId__c FROM ONTAP__Tour_Visit__c LIMIT 1];
        System.debug('***TourVisit: '+TourVisit);
        if (!TourVisit.isEmpty()) {
            if (TourVisit[0].ONTAP__SAPCustomerId__c != null) {
                strSapCustomerID = TourVisit[0].ONTAP__SAPCustomerId__c;
            }
            if (TourVisit[0].ONTAP__TourId__c != null) {
                strRoute = TourVisit[0].ONTAP__TourId__c;
            }
        }
    }
}