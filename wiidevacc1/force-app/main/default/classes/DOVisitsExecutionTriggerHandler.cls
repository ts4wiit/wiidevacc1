public class DOVisitsExecutionTriggerHandler {
    
    private List<DOVisitsExecution__c> visitsExecutions = new List<DOVisitsExecution__c>();
    private List<User> users = new List<User>();
    private List<Id> userIds = new List<Id>();
    
    public DOVisitsExecutionTriggerHandler( List<DOVisitsExecution__c> visitsExecutions ){
        
        this.visitsExecutions = visitsExecutions;
        
    }
    
    public void run(){
        
        this.setUserIds();
        this.getUsers();
        this.execute();
        
    }
    
    private void setUserIds(){
        
        for( DOVisitsExecution__c execution : visitsExecutions ){
            
            userIds.add( execution.User__c );
            
        }		        
        
    }
    
    private void getUsers(){
        
        users = DRUserSelector.getUsersByIds(userIds);
        
    }
    
    private void execute(){
        
        for( User user : Users ){
            
            System.enqueueJob(new DRQueueableVisits( user ));
            
        }        
        
    }

}