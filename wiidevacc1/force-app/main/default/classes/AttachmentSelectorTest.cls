@isTest
public class AttachmentSelectorTest {
    
    @isTest
    public static void testAttachmentSelector(){
        
        List<String> parentIds = new List<String>();
        Map<String,List<Attachment>> attachmentMap  = new Map<String,List<Attachment>>();
        Account prospectAccount = DRFixtureFactory.newDRProspect();
        parentIds.add( prospectAccount.Id );
                
        Attachment attachment = new Attachment();   	
    	attachment.Name = 'Unit Test Attachment';
    	Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
    	attachment.body = bodyBlob;
        attachment.parentId = prospectAccount.id;
        insert attachment;     
        
        Test.startTest();
        
        AttachmentSelector selector = new AttachmentSelector( parentIds );
        attachmentMap = selector.getParentIdToAttachmentsMap();
        
        System.assertEquals(1, attachmentMap.size(), 'Assertion faild :(');
        
        Test.stopTest();
        
    }

}