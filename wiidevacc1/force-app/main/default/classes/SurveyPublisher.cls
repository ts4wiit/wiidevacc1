public class SurveyPublisher {

    private static final String COUNTRY = 'DO';
    public static final String SURVEY_STATUS = 'Completada';
    private List<ONTAP__SurveyTaker__c> surveys = new List<ONTAP__SurveyTaker__c>();
    private List<ONTAP__SurveyTaker__c> filteredSurveys = new List<ONTAP__SurveyTaker__c>();
    private List<DRSurveyOutboundMessage__e> outboundMessages = new List<DRSurveyOutboundMessage__e>();
    private Map<String,String> accountIdToSAPIdMap = new Map<String,String>();
    private Map<String,String> userIdToRouteMap = new Map<String,String>();
    private Map<String,String> userIdToCountryMap = new Map<String,String>();
    private List<Id> accountIds = new List<Id>();
    private List<Id> userIds = new List<Id>();
    // info to verify if user is allowed to publish changes
    private String integrationId;
    
    public SurveyPublisher( List<ONTAP__SurveyTaker__c> surveys ){      
        this.surveys = surveys;
    }    
    
    public void run(){
        
        this.setup();
        this.filter();
        this.enrich();
        this.mapEvents();
        this.publish();
        
    }
    
    private void setup(){
        
        for( ONTAP__SurveyTaker__c survey : surveys ){

        	userIds.add( survey.OwnerId );    
            accountIds.add( survey.ONTAP__Account__c );
            
        }
        
        userIdToCountryMap = DRUserSelector.getUserIdToCountryMap( userIds );
        accountIdToSAPIdMap = DRAccountSelector.getAccountIdSAPCustomerIdMap( accountIds );
        
    }
       
    private void filter(){
        
        this.integrationId = DRUserSelector.getIntegrationId();
        Id userId = UserInfo.getUserId();
        
         for( ONTAP__SurveyTaker__c survey : surveys ){ 
            System.debug('>>> Country: ' + userIdToCountryMap.get( survey.OwnerId ));            
            if ( (userIdToCountryMap.get( survey.OwnerId ) == COUNTRY && survey.ONTAP__Status__c == SURVEY_STATUS &&  userId != (Id) this.integrationId ) || (Test.isRunningTest()) ){
                 filteredSurveys.add( survey );     
            }               
        }            
    }
    
    private void enrich(){
        
       this.accountIdToSAPIdMap = DRAccountSelector.getAccountIdSAPCustomerIdMap( accountIds );
       this.userIdToRouteMap = DRUserSelector.getUserIdToRouteMap( userIds );
        
    }
    
    private void mapEvents(){
        
        for ( ONTAP__SurveyTaker__c survey : filteredSurveys ){
        
            String customerSAPId = accountIdToSAPIdMap.get( survey.ONTAP__Account__c );
            String route = userIdToRouteMap.get( survey.OwnerId );
            
            DRSurveyOutboundMessage__e outboundMessage = new DRSurveyOutboundMessage__e(
            
                SurveyName__c = survey.Name,
                SAPCustomerId__c = customerSAPId,
                UserRoute__c = route,
                TotalScore__c = survey.ONTAP__Total_Score__c,
                SurveyDate__c = survey.ONTAP__Fecha_Programcion_Encuesta__c,
                SurveyTakenId__c = survey.Id,
                LastModifiedDate__c = survey.LastModifiedDate
            
            );
            
            outboundMessages.add( outboundMessage );            
        }     
    }
    
    private void publish(){

        System.debug('>>> Survey');
        System.debug( outboundMessages );
        List<Database.SaveResult> surveyResults = EventBus.publish( outboundMessages ); 
  
    }    
    
}