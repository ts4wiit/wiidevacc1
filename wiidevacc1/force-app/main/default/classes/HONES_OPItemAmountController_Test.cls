/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: HONES_OPItemAmountController_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest public class HONES_OPItemAmountController_Test {
	
    /**
    * Test method to get all the data about an Open Item   
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest public static void testController(){
        Account acc =  New Account(Name='Test');
        insert acc;
        ONTAP__OpenItem__c ob = New ONTAP__OpenItem__c(ONTAP__Account__c=acc.Id, ONTAP__Amount__c=10,ONTAP__DueDate__c=System.today()-1);
        insert ob;
        HONES_OPItemAmountController.getData(acc.Id);
    }
}