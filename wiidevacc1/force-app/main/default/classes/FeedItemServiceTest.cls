/**
* @author João Luiz
* @date 28/05/2019
* @description Class - This class cover the test for FeedItemTrigger ; FeedItemTriggerHandler ; FeedItemService
*                      * SeeAllData needed because ConnectApi 
*/
@IsTest(SeeAllData=true)
public class FeedItemServiceTest {
    
    @isTest static void createMention(){
		ConnectApi.FeedItemInput 		feedItemInput 		= new ConnectApi.FeedItemInput();
        ConnectApi.MentionSegmentInput 	mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        ConnectApi.MessageBodyInput 	messageBodyInput 	= new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput 	textSegmentInput 	= new ConnectApi.TextSegmentInput();
        
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
        mentionSegmentInput.id = Userinfo.getUserId();
        messageBodyInput.messageSegments.add(mentionSegmentInput);
        
        textSegmentInput.text = 'Could you take a look?';
        messageBodyInput.messageSegments.add(textSegmentInput);
        
        feedItemInput.body = messageBodyInput;
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = Userinfo.getUserId();
       
        ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement( Network.getNetworkId(), feedItemInput );
        
        FeedItem fItem = new FeedItem();
		fItem.Body = 'Hello!!!  @Manager ' + userinfo.getuserid();
		fItem.ParentId = userinfo.getuserid();
		fItem.Title = 'FileName';
		insert fItem;
        List<FeedItem> listFeed = new List<FeedItem>{fItem};
            
        Test.startTest();
        	FeedItemService.mentionSegmentItem(listFeed);
        Test.stopTest();
	}
}