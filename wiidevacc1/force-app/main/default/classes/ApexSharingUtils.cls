public class ApexSharingUtils {
    
    private static final String DEFAULT_ACCESS_LEVEL = 'Edit';
    private static final String DEFAULT_ROW_CAUSE = 'Manual';
    private static final String DEFAULT_ACCESS_LEVEL_FIELD = 'AccessLevel';
    public String accessLevel {get; set;}
    public String rowCase {get; set;}
    public String accessLevelField {get;set;}
    private String sObjectName;
    private String idFieldName;
    private Map<String,String> objectIdToUserIdMap;
    String listType;
    List<SObject> castRecords;
    private List<SObject> sharingList = new List<SObject>();
      
    public ApexSharingUtils( String sObjectName, String idFieldName, Map<String,String> objectIdToUserIdMap ){
        
		this.sObjectName = sObjectName;
		this.objectIdToUserIdMap = objectIdToUserIdMap;  
        this.idFieldName = idFieldName;
        this.listType = 'List<' + sObjectName + '>';
        this.castRecords = (List<SObject>)Type.forName( listType ).newInstance();
        this.accessLevel = DEFAULT_ACCESS_LEVEL;
        this.rowCase = DEFAULT_ROW_CAUSE;
        this.accessLevelField = DEFAULT_ACCESS_LEVEL_FIELD;
    }
    
    public void share(){
        
        for( String recordId : objectIdToUserIdMap.keySet() ){
            
            Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get( sObjectName );        
            SObject castRecord = sObjectType.newSObject();
            castRecord.put( this.accessLevelField, this.accessLevel );
            castRecord.put( idFieldName, recordId );
            castRecord.put( 'RowCause',  this.rowCase );
            castRecord.put( 'UserOrGroupId', objectIdToUserIdMap.get( recordId ));
            castRecords.add( castRecord );
          
        }
        
        insert castRecords;        
    }
}