/*******************************************************************************
Desarrollado por: Avanxo México
Autor: Daniel Peñaloza
Proyecto: ISSM - DSD
Descripción: Class with utility methods for test classes (Data Factory, Utilitary methods, etc.)

------ ---------- -------------------------- -----------------------------------
No.    Fecha      Autor                      Descripción
------ ---------- -------------------------- -----------------------------------
1.0    23/08/2017 Daniel Peñaloza            Created Class
1.1    19/04/2017 Rodrigo Resendiz           Add methods for US W-010925
*******************************************************************************/

@isTest
public class TestUtils_tst {
    public static final String PROFILE_NAME_ADMIN              = Label.ProfileName;
    //public static final String PROFILE_NAME_PRESALES           = 'System Administrator';
    public static final String PROFILE_NAME_PRESALES           = 'Administrador del sistema';
    public static final String ROLE_NAME_SALES_FORCE           = 'Fuerza de Ventas FF05';
    public static final String RECORD_TYPE_ACCOUNT_SALESOFFICE = 'SalesOffice';
    public static final String TEAM_MEMBER_ROLE_PRESALES       = 'Preventa';
    public static final String TEAM_MEMBER_ROLE_SALES_MANAGER  = 'Supervisor de Ventas';

    public static final String STR_ENDPOINT_1 = 'https://some-endpoint1.dev';
    public static final String STR_ENDPOINT_2 = 'https://some-endpoint2.dev';
    public static final String STR_ENDPOINT_3 = 'https://some-endpoint3.dev';

    public static final AccountRouteSettings__mdt accountRouteSettings = DevUtils_cls.getAccountRouteSettings();
    public static final VisitPlanSettings__mdt visitPlanSettings = DevUtils_cls.getVisitPlanSettings();
    public static final Map<String, RecordType> mapAccountRecordTypes = DevUtils_cls.getRecordTypes('Account', 'DeveloperName');
    public static final Map<String, RecordType> mapRouteRecordTypes = DevUtils_cls.getRecordTypes('ONTAP__Route__c', 'DeveloperName');
    public static final Map<String, RecordType> mapTourRecordTypes = DevUtils_cls.getRecordTypes('ONTAP__Tour__c', 'DeveloperName');

    public static final String[] lstDays = new List<String> {
        'Monday__c', 'Tuesday__c', 'Wednesday__c', 'Thursday__c', 'Friday__c', 'Saturday__c', 'Sunday__c'
    };

    public static ISSM_PriceEngineConfigWS__c generatePriceEngineConfig(String configName, String accessToken, String endpoint,
                                                                        String header, String method) {
        ISSM_PriceEngineConfigWS__c config = new ISSM_PriceEngineConfigWS__c();

        config.Name = configName;
        config.ISSM_AccessToken__c = accessToken;
        config.ISSM_EndPoint__c = endpoint;
        config.ISSM_HeaderContentType__c = header;
        config.ISSM_Method__c = method;

        return config;
    }

    /**
     * Generate test User
     *
     * @param      profileName  Profile Name
     * @param      userNum      User number (used as identifier)
     *
     * @return     Generated User
     */
    public static User generateUser(String profileName, String roleName, Integer userNum) {
        if (String.isBlank(profileName)) {
            profileName = TestUtils_tst.PROFILE_NAME_PRESALES;
        }

        if (userNum == null) {
            userNum = Integer.valueOf(Math.floor(Math.random() * 10000));
        }

        //Profile p = [SELECT Id FROM Profile WHERE Name = :profileName];
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' OR Name = 'Administrador del sistema'];

        String strEmail = 'testuser' + userNum + '@issm.dsd.dev';

        User usr = new User(
            Alias             = 'testuser',
            Email             = strEmail,
            EmailEncodingKey  = 'UTF-8',
            LastName          = 'Lastname',
            LanguageLocaleKey = 'es_MX',
            LocaleSidKey      = 'es_MX',
            ProfileId         = p.Id,
            TimeZoneSidKey    = 'America/Mexico_City',
            UserName          = strEmail,
            ManagerId      = UserInfo.getUserId()
        );

        return usr;
    }
    /** 1.1
     * Generate test Integration User
     *
     * @param      profileName  Profile Name
     * @param      userNum      User number (used as identifier)
     *
     * @return     Generated User
     */
    public static User generateIntegrationUser(String profileName, String roleName, Integer userNum) {
        if (String.isBlank(profileName)) {
            profileName = TestUtils_tst.PROFILE_NAME_PRESALES;
        }

        if (userNum == null) {
            userNum = Integer.valueOf(Math.floor(Math.random() * 10000));
        }

        //Profile p = [SELECT Id FROM Profile WHERE Name = :profileName];
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' OR Name = 'Administrador del sistema'];

        String strEmail = 'testuser' + userNum + '@issm.dsd.dev';
        Organization org = [SELECT IsSandbox, Name FROM Organization LIMIT 1];
        String uName;
        uName = 'integration.issmuser@gmodelo.com.mx.prod.test';

        User usr = new User(
            Alias             = 'testuser',
            Email             = strEmail,
            EmailEncodingKey  = 'UTF-8',
            LastName          = 'Lastname',
            LanguageLocaleKey = 'es_MX',
            LocaleSidKey      = 'es_MX',
            ProfileId         = p.Id,
            TimeZoneSidKey    = 'America/Mexico_City',
            UserName          = uName,
            ManagerId      = UserInfo.getUserId()
        );

        return usr;
    }

    /**
     * Generate a test Route
     *
     * @param      recordTypeDevName  Record type developer name for route
     * @param      serviceModel       Service model
     * @param      salesOfficeId      Salesoffice Account Id
     * @param      routeManagerId     Id of the Route Manager User
     * @param      routeId            SAP route Id
     * @param      addManagerToTeam   Specifies if it is neccesary add the Route manager to the Account Team
     *
     * @return     Route generated
     */
    public static ONTAP__Route__c generateRoute(String recordTypeDevName, String serviceModel, String salesOfficeId,
                                                String routeManagerId, String routeId) {
        ONTAP__Route__c route = new ONTAP__Route__c();

        // Set default record type
        if(String.isBlank(recordTypeDevName)) {
            recordTypeDevName = 'Sales';
        }

        // Set default Service Model
        if (String.isBlank(serviceModel)) {
            serviceModel = 'Presales';
        }

        // Set default Route Id
        if (String.isBlank(routeId)) {
            routeId = 'ABC123';
        }
        
        ONTAP__Vehicle__c objONTAPVehicle       = new ONTAP__Vehicle__c();
        objONTAPVehicle.ONTAP__VehicleId__c     = '112345-23';
        objONTAPVehicle.ONTAP__VehicleName__c   = 'Zurdo Movil';
        objONTAPVehicle.ONTAP__SalesOffice__c   = salesOfficeId;
        insert objONTAPVehicle;

        // Set Fields for Route record
        route.RecordTypeId          = mapRouteRecordTypes.get(recordTypeDevName).Id;
        route.ServiceModel__c       = serviceModel;
        route.ONTAP__SalesOffice__c = salesOfficeId;
        route.RouteManager__c       = routeManagerId;
        route.OwnerId               = routeManagerId;
        route.ONTAP__RouteId__c     = routeId;
        route.Vehicle__c            = objONTAPVehicle.id;

        return route;
    }

    /**
     * Generate a list of test accounts with specified parameters
     * @param      numAccounts  The number accounts to generate
     * @return     List of generated accounts
     */
    public static Account generateAccount(String recordTypeDevName, String strServiceDays, Integer index, Id salesOfficeId) {
        Account acc = new Account();

        // Set default record type
        if (String.isBlank(recordTypeDevName)) {
            recordTypeDevName = 'Account';
        }

        // Set service days
        if (String.isBlank(strServiceDays) && recordTypeDevName == 'Account') {
            strServiceDays = String.join(new List<String> { 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes' }, ';');
        }

        // Set account index
        if (index == null) {
            index = Integer.valueOf(Math.floor(Math.random() * 10000));
        }

        // Set Fields for Account record
        acc.Name = 'Test Account ' + index;
        acc.RecordTypeId = mapAccountRecordTypes.get(recordTypeDevName).Id;
        acc.ONTAP__Preferred_Service_Days__c = strServiceDays;
        acc.ONTAP__SAPCustomerId__c = 'abcdefghij' + Integer.valueOf(Math.floor(Math.random() * 10000));
        acc.ONTAP__SAP_Number__c = 'abcdefghij' + Integer.valueOf(Math.floor(Math.random() * 10000));
        acc.ISSM_SalesOffice__c = salesOfficeId;
        acc.ONTAP__SalesOgId__c = '3117';
        acc.ONTAP__SalesOffId__c = 'FF05';

        return acc;
    }

    /**
     * Create an Account by Route record
     *
     * @param      routeId      The route identifier
     * @param      accountId    The account identifier
     * @param      frecuencies  The frecuencies for the Account
     *
     * @return     Generated account by route
     */
    public static AccountByRoute__c generateAccountByRoute(Id routeId, Id accountId, Boolean[] frecuencies) {
        AccountByRoute__c accountByRoute = new AccountByRoute__c();

        // Default frecuency
        if (frecuencies == null || frecuencies.size() != 7) {
            frecuencies = new List<Boolean> { true, true, true, true, true, true, true };
        }

        accountByRoute.Route__c = routeId;
        accountByRoute.Account__c = accountId;

        for (Integer i = 0; i < lstDays.size(); i++) {
            accountByRoute.put(lstDays[i], frecuencies[i]);
        }

        return accountByRoute;
    }

    /**
     * Generate a Test Route with all dependencies (Accounts, Route Manager, etc.)
     *
     * @return Generated Route
     */
    public static ONTAP__Route__c generateFullRoute() {
        // Sales Agent (Route Manager)
        User routeManager = TestUtils_tst.generateUser(TestUtils_tst.PROFILE_NAME_PRESALES, TestUtils_tst.ROLE_NAME_SALES_FORCE, 12345);
        insert routeManager;

        // Sales Office Account
        Account salesOfficeAccount = TestUtils_tst.generateAccount(TestUtils_tst.RECORD_TYPE_ACCOUNT_SALESOFFICE, null, 12345, null);
        salesOfficeAccount.ONTAP__SalesOffId__c = 'FF05';
        insert salesOfficeAccount;

        // Customer Account
        Account customerAccount = TestUtils_tst.generateAccount(null, null, 123, salesOfficeAccount.Id);
        insert customerAccount;

        // Add user to Account Team
        DevUtils_cls.addUserToAccountTeam(customerAccount.Id, routeManager.Id,
            new Set<String> { TestUtils_tst.TEAM_MEMBER_ROLE_PRESALES, TestUtils_tst.TEAM_MEMBER_ROLE_SALES_MANAGER });

        // Route
        ONTAP__Route__c route = TestUtils_tst.generateRoute('Sales', 'Presales', salesOfficeAccount.Id, routeManager.Id, '123ABC');
        return route;
    }
    
    /**
     * Generate a Test Route with all dependencies (Accounts, Route Manager, etc.)
     *
     * @return Generated Route
     */
    public static ONTAP__Route__c generateFullRouteBDR() {
        // Sales Agent (Route Manager)
        User routeManager = TestUtils_tst.generateUser(TestUtils_tst.PROFILE_NAME_PRESALES, TestUtils_tst.ROLE_NAME_SALES_FORCE, 12395);
        insert routeManager;

        // Sales Office Account
        Account salesOfficeAccount = TestUtils_tst.generateAccount(TestUtils_tst.RECORD_TYPE_ACCOUNT_SALESOFFICE, null, 12345, null);
        salesOfficeAccount.ONTAP__SalesOffId__c = 'FF05';
        insert salesOfficeAccount;

        // Route
        ONTAP__Route__c route = TestUtils_tst.generateRoute('Sales', 'BDR', salesOfficeAccount.Id, routeManager.Id, '129ABC');
        return route;
    }
    
    /**
     * Generate a Test Route with all dependencies (Accounts, Route Manager, etc.)
     *
     * @return Generated Route
     */
    public static ONTAP__Route__c generateFullRouteTelesale() {
        // Sales Agent (Route Manager)
        User routeManager = TestUtils_tst.generateUser(TestUtils_tst.PROFILE_NAME_PRESALES, TestUtils_tst.ROLE_NAME_SALES_FORCE, 12395);
        insert routeManager;

        // Sales Office Account
        Account salesOfficeAccount = TestUtils_tst.generateAccount(TestUtils_tst.RECORD_TYPE_ACCOUNT_SALESOFFICE, null, 12345, null);
        salesOfficeAccount.ONTAP__SalesOffId__c = 'FF05';
        insert salesOfficeAccount;

        // Route
        ONTAP__Route__c route = TestUtils_tst.generateRoute('Sales', 'Telesales', salesOfficeAccount.Id, routeManager.Id, 'X29ABC');
        return route;
    }

    /**
     * Generate visit plan record
     *
     * @param      routeId      The route identifier
     * @param      startDate    The start date
     * @param      endDate      The end date
     * @param      frecuencies  The frecuencies
     * @param      visitPlanId  The visit plan identifier
     *
     * @return     The visit plan record generated
     */
    public static VisitPlan__c generateVisitPlan(Id routeId, Date startDate, Date endDate,
                                                Boolean[] frecuencies, String visitPlanId) {
        VisitPlan__c visitPlan = new VisitPlan__c();

        // Default frecuency
        if (frecuencies == null || frecuencies.size() != 7) {
            frecuencies = new List<Boolean> { true, true, true, true, true, true, true };
        }

        for (Integer i = 0; i < lstDays.size(); i++) {
            visitPlan.put(lstDays[i], frecuencies[i]);
        }

        // Set values to Visit Plan
        visitPlan.Route__c = routeId;
        visitPlan.ExecutionDate__c = startDate;
        visitPlan.EffectiveDate__c = endDate;
        visitPlan.VisitPlanId__c = visitPlanId;

        return visitPlan;
    }

    /**
     * Generate Account by Visit Plan record
     *
     * @param      visitPlanId   The visit plan identifier
     * @param      accountId     The account identifier
     * @param      weeklyPeriod  The weekly period
     * @param      frecuencies   The frecuencies
     *
     * @return     Account generated for visit plan
     */
    public static AccountByVisitPlan__c generateAccountByVisitPlan(Id visitPlanId, Id accountId, String weeklyPeriod, Boolean[] frecuencies) {
        AccountByVisitPlan__c accByVisitPlan = new AccountByVisitPlan__c();

        // Default frecuency
        if (frecuencies == null || frecuencies.size() != 7) {
            frecuencies = new List<Boolean> { true, true, true, true, true, true, true };
        }

        for (Integer i = 0; i < lstDays.size(); i++) {
            accByVisitPlan.put(lstDays[i], frecuencies[i]);
        }

        accByVisitPlan.VisitPlan__c = visitPlanId;
        accByVisitPlan.Account__c = accountId;
        accByVisitPlan.WeeklyPeriod__c = weeklyPeriod;

        return accByVisitPlan;
    }

    /**
     * Generate Tour record
     * @param  objVisitPlan Visit Plan for Tour
     * @param  objRoute     Route for Tour
     * @return              Tour record generated
     */
    public static ONTAP__Tour__c generateTour(VisitPlan__c objVisitPlan, ONTAP__Route__c objRoute) {
        ONTAP__Tour__c tour = new ONTAP__Tour__c();

        String ownerField = visitPlanSettings.OwnerField__r.DeveloperName;
        ownerField += (ownerField == 'Owner') ? 'Id' : '__c';

        VisitPlanSettings__mdt visitPlanSettings = DevUtils_cls.getVisitPlanSettings();

        tour.VisitPlan__c             = objVisitPlan.Id;
        tour.RecordTypeId             = mapTourRecordTypes.get(objVisitPlan.VisitPlanType__c).Id;
        tour.ONTAP__IsActive__c       = true;
        tour.ONTAP__TourId__c         = visitPlanSettings.NameTourId__c;
        tour.ONTAP__TourDate__c       = System.today().addDays(Integer.valueOf(visitPlanSettings.VisitPeriodConfig__c));
        tour.ONTAP__TourStatus__c     = visitPlanSettings.AssignedTourStatus__c;
        tour.AlternateName__c         = 'Alternate Name';
        tour.TourSubStatus__c         = visitPlanSettings.AssignedTourSubStatus__c;
        tour.OwnerId                  = (Id) objRoute.get(ownerField);
        tour.Route__c                 = objRoute.Id;
        tour.EstimatedDeliveryDate__c = Date.today().addDays(5);

        return tour;
    }

    public static Event[] generateEvents(Integer numEvents, AccountByVisitPlan__c objAccByVisit, ONTAP__Tour__c tour) {
        Event[] lstEvents = new Event[]{};

        String ownerField = visitPlanSettings.OwnerField__r.DeveloperName;
        ownerField += (ownerField == 'Owner') ? 'Id' : '__c';

        VisitPlanSettings__mdt visitPlanSettings = DevUtils_cls.getVisitPlanSettings();
        Datetime now = System.now();

        for (Integer i = 0; i < numEvents; i++) {
            Event objEvent = new Event(
                WhatId                     = objAccByVisit.Account__c,
                Subject                    = visitPlanSettings.VisitSubject__c,
                StartDateTime              = now,
                EndDateTime                = now.addDays(i+1),
                ONTAP__Estado_de_visita__c = visitPlanSettings.InitialVisitStatus__c,
                EventSubestatus__c         = visitPlanSettings.InitialVisitSubStatus__c,
                Sequence__c                = objAccByVisit.Sequence__c,
                CustomerId__c              = objAccByVisit.Account__r.ONTAP__SAPCustomerId__c,
                OwnerId                    = (Id) tour.Route__r.get(ownerField),
                VisitList__c               = tour.Id
            );
            lstEvents.add(objEvent);
        }

        return lstEvents;
    }
}