/**
 * Created by Dejair.Junior on 9/23/2019.
 */
@isTest
public class DRSchedulableProspectPublisher_Test {

    public static String CRON_EXP = '0 0 0 15 3 ? 2022';

    @testSetup
    static void setupProspects() {

        Account prospectAccount = DRFixtureFactory.newDRProspect();

    }

    @isTest
    static void testScheduledJob(){

        Test.startTest();

        String jobId = System.schedule('DRSchedulableProspectPublisher', CRON_EXP , new DRSchedulableProspectPublisher());

        Test.stopTest();

    }

}