/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_CASETYPIFICATION_CLASS_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 08/01/2019           Debbie Zacarias       creation of test class for CS_Case_Retypification_Class 
 */
@isTest
public class CS_Case_Retypification_Class_Test 
{
    
    /**
    * Method for create data 
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @testSetup
    static void setupData()
    {
        Account acc=new Account();
        acc.Name='test1234';
        acc.ONTAP__Email__c='email@mail.com';
        acc.ONTAP__Street__c='street';
        acc.ONTAP__Neighborhood__c='neigh';
        acc.ONTAP__Municipality__c='municipal';
        insert acc;   
     	
        User userHonduras = [select Id, username, Country__c from User where Id = :UserInfo.getUserId()];
        userHonduras.Country__c = 'Honduras';
        update userHonduras;
        
        /*User userHonduras = new User();
        userHonduras.Country__c = 'El Salvador';
        userHonduras.Username= 'juan@acc.com'; 
        userHonduras.LastName= 'Sawuan'; 
        userHonduras.Email= 'juan@acc.com'; 
        userHonduras.Alias= 'Juanis'; 
        userHonduras.CommunityNickname = 'jusuw';
        userHonduras.TimeZoneSidKey= 'America/Bogota'; 
        userHonduras.LocaleSidKey= 'es';
        userHonduras.EmailEncodingKey = 'ISO-8859-1';
        userHonduras.AboutMe = 'yas';
        userHonduras.LanguageLocaleKey = 'es';
        //userHonduras.ProfileId= userinfo.getProfileId();
        userHonduras.ProfileId = [SELECT Id FROM Profile WHERE Name = :TestUtils_tst.PROFILE_NAME_ADMIN LIMIT 1].Id;
        insert userHonduras;*/
        
        User userSalvador = new User();
        userSalvador.CS_On_Vacation__c=true;
        userSalvador.CS_User_Replacement__c=userHonduras.Id;
        userSalvador.Country__c = 'Honduras';
        userSalvador.Username= 'juans@acc.com'; 
        userSalvador.LastName= 'swabns'; 
        userSalvador.Email= 'juans@acc.com'; 
        userSalvador.Alias= 'Juanis'; 
        userSalvador.CommunityNickname = 'jusuws';
        userSalvador.TimeZoneSidKey= 'America/Bogota'; 
        userSalvador.LocaleSidKey= 'es';
        userSalvador.EmailEncodingKey = 'ISO-8859-1';
        userSalvador.AboutMe = 'yas';
        userSalvador.LanguageLocaleKey = 'es';
        //userSalvador.ProfileId= userinfo.getProfileId();
        userSalvador.ProfileId = [SELECT Id FROM Profile WHERE Name = :TestUtils_tst.PROFILE_NAME_ADMIN LIMIT 1].Id;
        insert userSalvador;
          
        acc.OwnerId=userSalvador.Id;
        update acc;
       
        ISSM_AppSetting_cs__c queue = new ISSM_AppSetting_cs__c();
        queue.ISSM_IdQueueWithoutOwner__c = userSalvador.Id;
        queue.Id_Queue_SV__c = userSalvador.Id;
        queue.Id_Queue_SACAgent_SV__c = userSalvador.Id;        
        insert queue;
        
        ISSM_TypificationMatrix__c oNewTypification = new ISSM_TypificationMatrix__c();
        oNewTypification.ISSM_TypificationLevel1__c = 'SAC';
        oNewTypification.ISSM_TypificationLevel2__c = 'Solicitudes';
        oNewTypification.ISSM_TypificationLevel3__c = 'Equipo Frío';
        oNewTypification.ISSM_TypificationLevel4__c = 'Reparación';
        oNewTypification.ISSM_Countries_ABInBev__c = 'Honduras';
        oNewTypification.CS_Dias_Primer_Contacto__c = 2;
        oNewTypification.ISSM_CaseRecordType__c = 'ISSM_GeneralCase';
        oNewTypification.ISSM_AssignedTo__c = 'User';
        oNewTypification.ISSM_OwnerUser__c =userSalvador.Id ;
        oNewTypification.ISSM_Priority__c = 'Medium';
        oNewTypification.CS_Automatic_Closing_Case__c = true;
        oNewTypification.ISSM_OwnerQueue__c = 'WithOut Owner';
        oNewTypification.ISSM_IdQueue__c = '';         
        oNewTypification.ISSM_Email1CommunicationLevel3__c = '';
        oNewTypification.ISSM_Email1CommunicationLevel4__c = '';
        oNewTypification.ISSM_Email2CommunicationLevel3__c = '';
        oNewTypification.ISSM_Email2CommunicationLevel4__c = '';
        oNewTypification.ISSM_Email3CommunicationLevel3__c = '';
        oNewTypification.ISSM_Email3CommunicationLevel4__c = '';
        oNewTypification.ISSM_Email4CommunicationLevel4__c = '';
        oNewTypification.ISSM_CaseClosedSendLetter__c = 'No';
        oNewTypification.CS_Days_to_End__c=3;
        Insert oNewTypification;
        
        
         //Generate typification matrix 
        ISSM_TypificationMatrix__c oNewTypificationS = new ISSM_TypificationMatrix__c();
        oNewTypificationS.ISSM_TypificationLevel1__c = 'SAC';
        oNewTypificationS.ISSM_TypificationLevel2__c = 'Solicitudes';
        oNewTypificationS.ISSM_TypificationLevel3__c = 'Equipo Frío';
        oNewTypificationS.ISSM_TypificationLevel4__c = 'Reparación';
        oNewTypificationS.ISSM_Countries_ABInBev__c = 'El Salvador';
        oNewTypificationS.CS_Dias_Primer_Contacto__c = 2;
        oNewTypificationS.ISSM_CaseRecordType__c = 'ISSM_GeneralCase';
        oNewTypificationS.ISSM_AssignedTo__c = 'User';
        oNewTypificationS.ISSM_OwnerUser__c =userHonduras.Id ;
        oNewTypificationS.ISSM_Priority__c = 'Medium';
        oNewTypificationS.CS_Automatic_Closing_Case__c = true;
        oNewTypificationS.ISSM_OwnerQueue__c = 'WithOut Owner';
		oNewTypification.ISSM_AssignedTo__c = 'User';         
        oNewTypificationS.ISSM_Email1CommunicationLevel3__c = '';
        oNewTypificationS.ISSM_Email1CommunicationLevel4__c = '';
        oNewTypificationS.ISSM_Email2CommunicationLevel3__c = '';
        oNewTypificationS.ISSM_Email2CommunicationLevel4__c = '';
        oNewTypificationS.ISSM_Email3CommunicationLevel3__c = '';
        oNewTypificationS.ISSM_Email3CommunicationLevel4__c = '';
        oNewTypificationS.ISSM_Email4CommunicationLevel4__c = '';
        oNewTypificationS.ISSM_CaseClosedSendLetter__c = 'No';
        oNewTypificationS.CS_Days_to_End__c=3;
        Insert oNewTypificationS;
        
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        //String rtInstallationCooler = Label.CS_RT_Installation_Cooler;
        String rtInstallationCooler = 'CS_RT_Installation_Cooler';
        String rtRetirementCooler = Label.CS_RT_Retirement_Cooler;
        System.debug(rtInstallationCooler);
        Id rectype = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(rtInstallationCooler).getRecordTypeId();
            
        //RecordType rectype = ([SELECT id, DeveloperName FROM RecordType WHERE DeveloperName=:rtInstallationCooler]);
        
        ISSM_TypificationMatrix__c typification = new ISSM_TypificationMatrix__c();   
        typification.ISSM_TypificationLevel1__c = 'SAC';
        typification.ISSM_TypificationLevel2__c = 'Solicitudes';
        typification.ISSM_TypificationLevel3__c = 'Equipo Frío';
        typification.ISSM_TypificationLevel4__c = 'Reparación';
        typification.ISSM_Countries_ABInBev__c = 'Honduras';
        typification.ISSM_CaseRecordType__c = rtInstallationCooler;
        typification.CS_Days_to_End__c = 0.0;        
        insert typification;
        
        Case caseTest = new Case();
        caseTest.Accountid = accountTest.Id;
        caseTest.Description = 'Test Description';
        caseTest.Status = 'New';
        caseTest.Subject = 'Subject';
        caseTest.ISSM_TypificationNumber__c = typification.Id;
        caseTest.HONES_Case_Country__c = 'Honduras';
        caseTest.CS_Send_To_Mule__c = false;
        caseTest.RecordTypeId = rectype;
        insert caseTest;
                        
        accountTest.OwnerId=userSalvador.Id;
        update accountTest;
    }
    
    /**
    * Method for test GetTypification method
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_GetTypification()
    {
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        //String rtInstallationCooler = Label.CS_RT_Installation_Cooler;
        String rtInstallationCooler = 'CS_RT_Installation_Cooler';
        
        Id rectype = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(rtInstallationCooler).getRecordTypeId();
        
        ISSM_TypificationMatrix__c typification = new ISSM_TypificationMatrix__c();   
        typification.ISSM_TypificationLevel1__c = 'SAC';
        typification.ISSM_TypificationLevel2__c = 'Solicitudes';
        typification.ISSM_TypificationLevel3__c = 'Equipo Frío';
        typification.ISSM_TypificationLevel4__c = 'Reparación';
        typification.ISSM_Countries_ABInBev__c = 'Honduras';
        typification.ISSM_CaseRecordType__c = rtInstallationCooler;
        typification.CS_Days_to_End__c = 0.0;        
        insert typification;
        
        Case caso = new Case();
        caso.Accountid = accountTest.Id;
        caso.Description = 'Test Description';
        caso.Status = 'New';
        caso.Subject = 'Subject';
        caso.HONES_Case_Country__c = 'Honduras';
        caso.CS_Send_To_Mule__c = false;
        caso.RecordTypeId = rectype;
        insert caso;
        
        //CS_Case_Retypification_Class Case1 = new CS_Case_Retypification_Class();
        //Case caso = ([SELECT id FROM Case LIMIT 1]);
        
        Case casoReturned;
        casoReturned = CS_Case_Retypification_Class.GetCaseTypification(caso.Id);
        
        System.assertEquals(caso.id, casoReturned.Id);     
    }
    
    /**
    * Method for test GetTypificationN1 method
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_GetTypificationN1()
    {        
        CS_Case_Retypification_Class.GetTypificationN1();
    }
    
    /**
    * Method for test GetTypificationN2 method
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_GetTypificationN2()
    {
        CS_Case_Retypification_Class.GetTypificationN2('SAC');        
    }
    
    /**
    * Method for test GetTypificationN3 method
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_GetTypificationN3()
    {
        CS_Case_Retypification_Class.GetTypificationN3('SAC','Solicitudes');        
    }
    
    /**
    * Method for test GetTypificationN4 method
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_GetTypificationN4()
    {
        CS_Case_Retypification_Class.GetTypificationN4('SAC','Solicitudes','Equipo Frío');        
    }
    
    /**
    * Method for test UpdateTypificationCase method
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_UpdateTypificationCase()
    {
        
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        //String rtInstallationCooler = Label.CS_RT_Installation_Cooler;
        String rtInstallationCooler = 'CS_RT_Installation_Cooler';
        
        Id rectype = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(rtInstallationCooler).getRecordTypeId();
        
        ISSM_TypificationMatrix__c typification = new ISSM_TypificationMatrix__c();   
        typification.ISSM_TypificationLevel1__c = 'SAC';
        typification.ISSM_TypificationLevel2__c = 'Solicitudes';
        typification.ISSM_TypificationLevel3__c = 'Equipo Frío';
        typification.ISSM_TypificationLevel4__c = 'Reparación';
        typification.ISSM_Countries_ABInBev__c = 'Honduras';
        typification.ISSM_CaseRecordType__c = rtInstallationCooler;
        typification.CS_Days_to_End__c = 0.0;        
        insert typification;
        
        Case caso = new Case();
        caso.Accountid = accountTest.Id;
        caso.Description = 'Test Description';
        caso.Status = 'New';
        caso.Subject = 'Subject';
        caso.HONES_Case_Country__c = 'Honduras';
        caso.CS_Send_To_Mule__c = false;
        caso.RecordTypeId = rectype;
        insert caso;
        
        
        //Case caso = [SELECT Id FROM Case LIMIT 1];
        CS_Case_Retypification_Class.UpdateTypificationCase(caso.Id, 'SAC', 'Solicitudes', 'Equipo Frío', 'Reparación');
    }
    /******Eduardo Reyna Gonzalez ereyna@ts4.mx***/
    @isTest
    static void test_UpdateTypificationCaseE()
    {
        
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        //String rtInstallationCooler = Label.CS_RT_Installation_Cooler;
        String rtInstallationCooler = 'CS_RT_Installation_Cooler';
        
        Id rectype = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(rtInstallationCooler).getRecordTypeId();
        
        ISSM_TypificationMatrix__c typification = new ISSM_TypificationMatrix__c();   
        typification.ISSM_TypificationLevel1__c = 'SAC';
        typification.ISSM_TypificationLevel2__c = 'Solicitudes';
        typification.ISSM_TypificationLevel3__c = 'TELVENTA';
        typification.ISSM_TypificationLevel4__c = 'Reparación';
        typification.ISSM_Countries_ABInBev__c = 'Honduras';
        typification.ISSM_CaseRecordType__c = rtInstallationCooler;
        typification.CS_Days_to_End__c = 0.0;        
        insert typification;
        
        Case caso = new Case();
        caso.Accountid = accountTest.Id;
        caso.Description = 'Test Description';
        caso.Status = 'New';
        caso.Subject = 'Subject';
        caso.HONES_Case_Country__c = 'Honduras';
        caso.CS_Send_To_Mule__c = false;
        caso.RecordTypeId = rectype;
        insert caso;
        
        
        //Case caso = [SELECT Id FROM Case LIMIT 1];
        CS_Case_Retypification_Class.UpdateTypificationCase(caso.Id, 'SAC', 'Solicitudes', 'TELVENTA', 'Reparación');
    }
    /******Eduardo Reyna Gonzalez ereyna@ts4.mx***/
    @isTest
    static void test_UpdateTypificationCaseI()
    {
        
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        //String rtInstallationCooler = Label.CS_RT_Installation_Cooler;
        String rtInstallationCooler = 'CS_RT_Installation_Cooler';
        
        Id rectype = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(rtInstallationCooler).getRecordTypeId();
        
        ISSM_TypificationMatrix__c typification = new ISSM_TypificationMatrix__c();   
        typification.ISSM_TypificationLevel1__c = 'SAC';
        typification.ISSM_TypificationLevel2__c = 'Solicitudes';
        typification.ISSM_TypificationLevel3__c = 'PREVENTA';
        typification.ISSM_TypificationLevel4__c = 'Reparación';
        typification.ISSM_Countries_ABInBev__c = 'Honduras';
        typification.ISSM_CaseRecordType__c = rtInstallationCooler;
        typification.CS_Days_to_End__c = 0.0;        
        insert typification;
        
        Case caso = new Case();
        caso.Accountid = accountTest.Id;
        caso.Description = 'Test Description';
        caso.Status = 'New';
        caso.Subject = 'Subject';
        caso.HONES_Case_Country__c = 'Honduras';
        caso.CS_Send_To_Mule__c = false;
        caso.RecordTypeId = rectype;
        insert caso;
        
        
        //Case caso = [SELECT Id FROM Case LIMIT 1];
        CS_Case_Retypification_Class.UpdateTypificationCase(caso.Id, 'SAC', 'Solicitudes', 'PREVENTA', 'Reparación');
    }
    /******Eduardo Reyna Gonzalez ereyna@ts4.mx***/
    @isTest
    static void test_UpdateTypificationCaseM()
    {
        
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        //String rtInstallationCooler = Label.CS_RT_Installation_Cooler;
        String rtInstallationCooler = 'CS_RT_Installation_Cooler';
        
        Id rectype = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(rtInstallationCooler).getRecordTypeId();
        
        ISSM_TypificationMatrix__c typification = new ISSM_TypificationMatrix__c();   
        typification.ISSM_TypificationLevel1__c = 'SAC';
        typification.ISSM_TypificationLevel2__c = 'Solicitudes';
        typification.ISSM_TypificationLevel3__c = 'CREDITO';
        typification.ISSM_TypificationLevel4__c = 'Reparación';
        typification.ISSM_Countries_ABInBev__c = 'Honduras';
        typification.ISSM_CaseRecordType__c = rtInstallationCooler;
        typification.CS_Days_to_End__c = 0.0;        
        insert typification;
        
        Case caso = new Case();
        caso.Accountid = accountTest.Id;
        caso.Description = 'Test Description';
        caso.Status = 'New';
        caso.Subject = 'Subject';
        caso.HONES_Case_Country__c = 'Honduras';
        caso.CS_Send_To_Mule__c = false;
        caso.RecordTypeId = rectype;
        insert caso;
        
        //Case caso = [SELECT Id FROM Case LIMIT 1];
        CS_Case_Retypification_Class.UpdateTypificationCase(caso.Id, 'SAC', 'Solicitudes', 'CREDITO', 'Reparación');
    }
    /******Eduardo Reyna Gonzalez ereyna@ts4.mx***/
    @isTest
    static void test_UpdateTypificationCaseEF()
    {
        
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        //String rtInstallationCooler = Label.CS_RT_Installation_Cooler;
        String rtInstallationCooler = 'CS_RT_Installation_Cooler';
        
        Id rectype = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(rtInstallationCooler).getRecordTypeId();
        
        ISSM_TypificationMatrix__c typification = new ISSM_TypificationMatrix__c();   
        typification.ISSM_TypificationLevel1__c = 'SAC';
        typification.ISSM_TypificationLevel2__c = 'Solicitudes';
        typification.ISSM_TypificationLevel3__c = 'EQUIPO_FRIO';
        typification.ISSM_TypificationLevel4__c = 'Reparación';
        typification.ISSM_Countries_ABInBev__c = 'Honduras';
        typification.ISSM_CaseRecordType__c = rtInstallationCooler;
        typification.CS_Days_to_End__c = 0.0;        
        insert typification;
        
        Case caso = new Case();
        caso.Accountid = accountTest.Id;
        caso.Description = 'Test Description';
        caso.Status = 'New';
        caso.Subject = 'Subject';
        caso.HONES_Case_Country__c = 'Honduras';
        caso.CS_Send_To_Mule__c = false;
        caso.RecordTypeId = rectype;
        insert caso;
        
        //Case caso = [SELECT Id FROM Case LIMIT 1];
        CS_Case_Retypification_Class.UpdateTypificationCase(caso.Id, 'SAC', 'Solicitudes', 'EQUIPO_FRIO', 'Reparación');
    }
    /******Eduardo Reyna Gonzalez ereyna@ts4.mx***/
	@isTest
    static void test_UpdateTypificationCaseTE()
    {
        
        Account accountTest = new Account(Name = 'Account Test');
        insert accountTest;
        
        //String rtInstallationCooler = Label.CS_RT_Installation_Cooler;
        String rtInstallationCooler = 'CS_RT_Installation_Cooler';
        
        Id rectype = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get(rtInstallationCooler).getRecordTypeId();
        
        ISSM_TypificationMatrix__c typification = new ISSM_TypificationMatrix__c();   
        typification.ISSM_TypificationLevel1__c = 'SAC';
        typification.ISSM_TypificationLevel2__c = 'Solicitudes';
        typification.ISSM_TypificationLevel3__c = 'TELECOBRANZA';
        typification.ISSM_TypificationLevel4__c = 'Reparación';
        typification.ISSM_Countries_ABInBev__c = 'Honduras';
        typification.ISSM_CaseRecordType__c = rtInstallationCooler;
        typification.CS_Days_to_End__c = 0.0;        
        insert typification;
        
        Case caso = new Case();
        caso.Accountid = accountTest.Id;
        caso.Description = 'Test Description';
        caso.Status = 'New';
        caso.Subject = 'Subject';
        caso.HONES_Case_Country__c = 'Honduras';
        caso.CS_Send_To_Mule__c = false;
        caso.RecordTypeId = rectype;
        insert caso;
        
        //Case caso = [SELECT Id FROM Case LIMIT 1];
        CS_Case_Retypification_Class.UpdateTypificationCase(caso.Id, 'SAC', 'Solicitudes', 'TELECOBRANZA', 'Reparación');
    }
}