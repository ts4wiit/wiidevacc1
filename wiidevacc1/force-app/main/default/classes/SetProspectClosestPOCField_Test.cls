@isTest
public class SetProspectClosestPOCField_Test {
    
    private static String accountId {get;set;}
    
    @testSetup static void testSetup(){
        
        String recordtypeIdAccount  = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get( 'Account_DO' ).getRecordTypeId();        
        
        List<Account> listNewAccounts = new List<Account>();
        
        listNewAccounts.add(new Account( name = 'Account Test 0', 
                                 recordTypeId = recordtypeIdAccount, 
                           ONTAP__Latitude__c = '-22.7225515', 
                          ONTAP__Longitude__c = '-47.0120014',
                                      ownerId = UserInfo.getUserId()));
        
        listNewAccounts.add(new Account( name = 'Account Test 1', 
                                 recordTypeId = recordtypeIdAccount, 
                           ONTAP__Latitude__c = '-22.7225867', 
                          ONTAP__Longitude__c = '-47.0119776',
                                      ownerId = UserInfo.getUserId()));
        
        listNewAccounts.add(new Account( name = 'Account Test 2', 
                                 recordTypeId = recordtypeIdAccount, 
                           ONTAP__Latitude__c = '-22.7226176', 
                          ONTAP__Longitude__c = '-47.0119981',
                                      ownerId = UserInfo.getUserId()));
        
        insert listNewAccounts;
    }
    
    static{
        //accountId = [SELECT Id FROM Account WHERE name = 'Account Test 0'][0].Id;
    }
    
    @isTest private static void testSetProspectClosestPOCFieldUnitary(){
        
        String recordtypeIdProspect = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get( 'Prospect_DO' ).getRecordTypeId();
		String recordtypeIdAccount  = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get( 'Account_DO' ).getRecordTypeId();        
        
        Account conta = new Account( name = 'Account Test 0', 
               			     recordTypeId = recordtypeIdAccount, 
                       ONTAP__Latitude__c = '-22.7225515', 
                      ONTAP__Longitude__c = '-47.0120014',
                                  ownerId = UserInfo.getUserId());
        
        insert conta;
        
        Account prospect = new Account( name = 'Prospect Test 0', 
                                recordTypeId = recordtypeIdProspect, 
                          ONTAP__Latitude__c = '-22.7225515', 
                         ONTAP__Longitude__c = '-47.0120014');
        
        insert prospect;
        
        String Closest_POC_Code = [SELECT Id, Closest_POC_Code__c FROM Account WHERE name = 'Prospect Test 0'][0].Closest_POC_Code__c;
        
        System.debug('Closest POC Code => ' + Closest_POC_Code);
        System.debug('conta.Id => ' + conta.Id);
        
        System.assertEquals( conta.Id, Closest_POC_Code, 'The prospect did not have the Closest_POC_Code__c field set' );
    }
    
    @isTest private static void testSetProspectClosestPOCFieldMassa(){
        
        String recordtypeIdProspect = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get( 'Prospect_DO' ).getRecordTypeId();  
        String recordtypeIdAccount  = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get( 'Account_DO' ).getRecordTypeId();
        
        Account conta = new Account( name = 'Account Test 0', 
               			     recordTypeId = recordtypeIdAccount, 
                       ONTAP__Latitude__c = '-22.7225515', 
                      ONTAP__Longitude__c = '-47.0120014',
                                  ownerId = UserInfo.getUserId());
        
        insert conta;
        
        List<Account> listNewProspects = new List<Account>();
        
        listNewProspects.add(new Account( name = 'Prospect Test 0', 
                                  recordTypeId = recordtypeIdProspect, 
                            ONTAP__Latitude__c = '-22.7225515', 
                           ONTAP__Longitude__c = '-47.0120014'));
        
        listNewProspects.add(new Account( name = 'Prospect Test 1', 
                                  recordTypeId = recordtypeIdProspect, 
                            ONTAP__Latitude__c = '-22.7225515', 
                           ONTAP__Longitude__c = '-47.0120014'));
        
        listNewProspects.add(new Account( name = 'Prospect Test 2', 
                                  recordTypeId = recordtypeIdProspect, 
                            ONTAP__Latitude__c = '-22.7225515', 
                           ONTAP__Longitude__c = '-47.0120014'));
        
        listNewProspects.add(new Account( name = 'Prospect Test 3', 
                                  recordTypeId = recordtypeIdProspect, 
                            ONTAP__Latitude__c = '-22.7225515', 
                           ONTAP__Longitude__c = '-47.0120014'));
        
        listNewProspects.add(new Account( name = 'Prospect Test 4', 
                                  recordTypeId = recordtypeIdProspect, 
                            ONTAP__Latitude__c = '-22.7225515', 
                           ONTAP__Longitude__c = '-47.0120014'));
        
        listNewProspects.add(new Account( name = 'Prospect Test 5', 
                                  recordTypeId = recordtypeIdProspect, 
                            ONTAP__Latitude__c = '-22.7225515', 
                           ONTAP__Longitude__c = '-47.0120014'));
        
        listNewProspects.add(new Account( name = 'Prospect Test 6', 
                                  recordTypeId = recordtypeIdProspect, 
                            ONTAP__Latitude__c = '-22.7225515', 
                           ONTAP__Longitude__c = '-47.0120014'));
        
        listNewProspects.add(new Account( name = 'Prospect Test 7', 
                                  recordTypeId = recordtypeIdProspect, 
                            ONTAP__Latitude__c = '-22.7225515', 
                           ONTAP__Longitude__c = '-47.0120014'));
        
        listNewProspects.add(new Account( name = 'Prospect Test 8', 
                                  recordTypeId = recordtypeIdProspect, 
                            ONTAP__Latitude__c = '-22.7225515', 
                           ONTAP__Longitude__c = '-47.0120014'));
        
        listNewProspects.add(new Account( name = 'Prospect Test 9', 
                                  recordTypeId = recordtypeIdProspect, 
                            ONTAP__Latitude__c = '-22.7225515', 
                           ONTAP__Longitude__c = '-47.0120014'));
        
        insert listNewProspects;
        
        String Closest_POC_Code = [SELECT Id, Closest_POC_Code__c FROM Account WHERE name = 'Prospect Test 0'][0].Closest_POC_Code__c;
        
        System.debug('Closest POC Code => ' + Closest_POC_Code);
        System.debug('conta.Id => ' + conta.Id);
        
        System.assertEquals( conta.Id, Closest_POC_Code, 'The prospect did not have the Closest_POC_Code__c field set' );
    }
}