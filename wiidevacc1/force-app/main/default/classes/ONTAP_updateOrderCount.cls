/* ----------------------------------------------------------------------------
 * AB InBev :: OnTap - OnCall
 * ----------------------------------------------------------------------------
 * Clase: ONTAP_updateOrderCount.apxc
 * Versión: 1.0.0.0
 *  
 * Historial de Cambios
 * ----------------------------------------------------------------------------
 * Fecha           Usuario            Contacto      						Descripción
 * 03/01/ 2019     Oscar Garcia		  o.a.garcia.martinez_accenture.com     Creación
 */
public class ONTAP_updateOrderCount {
    /*
     * Method that update the DataBase,modify the quantity
     * Created by: o.a.garcia.martinez_accenture.com
     *@params List<ONTAP_Order_Item__c>
	 *return void
*/
    public static void sumCounter(List<ONTAP__Order_Item__c> orderItems){

            List<String> orderItemsId = new List<String>();
            for(ONTAP__Order_Item__c aux : orderItems){
                orderItemsId.add(aux.Id);
            }
            
            List<ONTAP__Order__c> ordersList = new List<ONTAP__Order__c>();
            ordersList = [Select Id, Name, ONCALL__SAP_Order_Item_Counter__c, ONCALL__Total_Order_Item_Quantity__c, (Select Id From ONTAP__Order_Items1__r) from ONTAP__Order__c where Id In (Select ONTAP__CustomerOrder__c from ONTAP__Order_Item__c where Id in: orderItemsId)]; 
            System.debug('orderItem ' + orderItems);
            for(ONTAP__Order__c order : ordersList){
                order.ONCALL__Total_Order_Item_Quantity__c = 0.0;
                order.ONCALL__Total_Order_Item_Quantity__c = order.ONTAP__Order_Items1__r.size();
            }
            //ISSM_TriggerManager_cls.inactivate('CS_TRIGGER_PEDIDO');
            //update ordersList;
            
           /* for(ONTAP__Order__c order : ordersList){
            	ONCALL_orderToJSON sjsn = new ONCALL_orderToJSON();
        	    if(order.ONCALL__Total_Order_Item_Quantity__c == order.ONCALL__SAP_Order_Item_Counter__c) sjsn.createJson(order.Id);  
            }*/
    
    }
}