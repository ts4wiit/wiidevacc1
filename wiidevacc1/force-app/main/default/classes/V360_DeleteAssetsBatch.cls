/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_DeleteAssetsBatch.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 18/12/2018     Carlos Leal        Creation of methods.
 */
global class V360_DeleteAssetsBatch implements Database.Batchable<sObject>{
    // Persistent variables
    global Boolean hasErrors {get; private set;}

    /**
    * Clean the class.
    * @author: c.leal.beltran@accenture.com 
    */
    global V360_DeleteAssetsBatch()
    {  
        this.hasErrors = false;
    }
    
     /**
   * Execute the Query for the account assets objects.
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC
   * @return Database.QueryLocator
   */
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Id V360_Account_AssetRT = Schema.SObjectType.ONTAP__Account_Asset__c.getRecordTypeInfosByDeveloperName().get(GlobalStrings.ASSETS_V360_RECORDTYPE_NAME).getRecordTypeId();
        system.debug('Processing start method'); 
        // Query string for batch Apex
        String query = ''; 
        Date todayDate = System.Today();
        if(System.Test.isRunningTest()){
        	query += 'SELECT Id FROM ONTAP__Account_Asset__c WHERE RecordTypeId=:V360_Account_AssetRT';
        }else{
        	query += 'SELECT Id FROM ONTAP__Account_Asset__c WHERE RecordTypeId=:V360_Account_AssetRT AND LastModifiedDate<:todayDate';
        }
        Database.QueryLocator q = Database.getQueryLocator(query);
        return q;      
    }
    
    /**
   * Method for delete all the account assets objects.
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC, List<ONTAP__Account_Asset__c> objectBatch
   * @return void
   */
    global void execute(Database.BatchableContext BC, List<ONTAP__Account_Asset__c> objectBatch)
    { 	
        delete objectBatch;
        DataBase.emptyRecycleBin(objectBatch);
    }
    
    /**
   * Method finish for close all open process
   * @author: c.leal.beltran@accenture.com
   * @param Database.BatchableContext BC
   * @return void
   */
    global void finish(Database.BatchableContext BC)
    {   
    }
}