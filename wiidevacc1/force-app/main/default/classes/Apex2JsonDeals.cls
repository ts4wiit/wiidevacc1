/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: Apex2JsonDeals.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 18/12/2018           Luis Arturo Parra Rosas           Creation of methods.
*/


public class Apex2JsonDeals {
    
       	/**
        * Model by Apex2JsonDeals
        * Created By: heron.zurita@accenture.com
        * @param void
        * @return void
        */
    
		public String token;
		public String accountId;
		public String salesOrg;
    
    public Apex2JsonDeals(){}

}