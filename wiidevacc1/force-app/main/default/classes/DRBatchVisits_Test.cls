@isTest
public class DRBatchVisits_Test {

    public static String CRON_EXP = '0 0 0 15 3 ? 2022';    
    
    @testSetup
    static void setupVisits() {
        
        //BRD
        Id profileId = [ SELECT Id FROM PROFILE WHERE Name = 'DO BDR' ].get(0).Id;

		User bdrUser = new User(FirstName='BDR', LastName='Agent 1', UserName = 'bdr.do@test.com', Email='bdr@test.com',VisitsScheduledUntil__c = null,
				Alias='dobdr', LocaleSidKey='es', LanguageLocaleKey='es', EmailEncodingKey='ISO-8859-1',ONTAP__Country_Alias__c = 'DO',
				ProfileId = profileId, TimeZoneSidKey='America/Puerto_Rico', Country='DO', isActive = true,User_Route__c='DO1234');

		insert bdrUser;
        
		//Accounts        
        List<Account> accounts = new List<Account>();
        
        Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_DO').getRecordTypeId();
        
        for( Integer i = 1 ; i <= 20 ; i++ ) {
            
            Account account = new Account( Name = 'Name ' + i, ONTAP__SAPCustomerId__c = 'CID' + i, RecordTypeId = accountRecordTypeId, OwnerId = bdrUser.Id,
                                           ONTAP__Contact_First_Name__c = 'Test', 
                                           ONTAP__Contact_Last_Name__c = 'Test' );
            accounts.add( account );
            
        }     
        
        insert accounts;
        
        //Visits
        List<DOVisitPlan__c> visitPlans = new List<DOVisitPlan__c>();
        
        Date startDate = Date.today();
        Date endDate = StartDate.addDays( 365 );
        
        for( Integer i = 1 ; i <= 20 ; i++ ) {
            
            DOVisitPlan__c visitPlan = new DOVisitPlan__c( SAPCustomerID__c = 'CID' + i, Route__c = 'DO1234', External_ID__c = 'DO1234' + 'CID' + i,
                                                           Sequence__c = i , Recurrence__c = 1, StartDate__c = startDate , 
                                                           EndDate__c = endDate, Weekday__c = 16 );
            
            visitPlans.add( visitPlan );
            
        }
        
        insert visitPlans; 
    
    }
    
    
    @isTest
    static void testScheduledJob(){
        
        Test.startTest();
        
        String jobId = System.schedule('DRSchedulableVisits', CRON_EXP , new DRSchedulableVisits());
        
        Test.stopTest();
        
    }     
    
}