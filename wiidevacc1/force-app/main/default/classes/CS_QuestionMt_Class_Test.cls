/* ----------------------------------------------------------------------------
* AB InBev :: Customer Service
* ----------------------------------------------------------------------------
* Clase: CS_QUESTIONMT_CLASS.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 06/02/2019     Debbie Zacarias             Creation of the test class for CS_QUESTIONMT_CLASS
*/
@isTest
private class CS_QuestionMt_Class_Test
{
    
    /**
    * Method for test the method GetTypificationN1 in CS_QUESTIONMT_CLASS 
    * @author: d.zacarias.cantillo@accenture.com
    * @param Void
    * @return Void
    */
    @isTest
    static void test_GetTypificationN1()
    {
        ISSM_TypificationMatrix__c typification = new ISSM_TypificationMatrix__c();
        typification.ISSM_TypificationLevel1__c = 'SAC';
        typification.ISSM_Countries_ABInBev__c = 'Honduras';
        typification.CS_Days_to_End__c = 1;
        Insert typification;
        
        CS_QuestionMt_Class obj = new CS_QuestionMt_Class();
        CS_QuestionMt_Class.GetTypificationN1();
        
    }
    
    /**
    * Method for test the method GetTypificationN2 in CS_QUESTIONMT_CLASS
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_GetTypificationN2()
    {
        ISSM_TypificationMatrix__c typification = new ISSM_TypificationMatrix__c();
        typification.ISSM_TypificationLevel1__c = 'SAC';
        typification.ISSM_TypificationLevel2__c = 'Solicitudes';
        typification.ISSM_Countries_ABInBev__c = 'Honduras';
        typification.CS_Days_to_End__c = 1;
        Insert typification;
        
        CS_QuestionMt_Class obj = new CS_QuestionMt_Class();
        CS_QuestionMt_Class.GetTypificationN2('SAC');
    }
    
    /**
    * Method for test the method GetTypificationN3 in CS_QUESTIONMT_CLASS
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_GetTypificationN3()
    {
        ISSM_TypificationMatrix__c typification = new ISSM_TypificationMatrix__c();
        typification.ISSM_TypificationLevel1__c = 'SAC';
        typification.ISSM_TypificationLevel2__c = 'Solicitudes';
        typification.ISSM_TypificationLevel3__c = 'Equipo Frío';
        typification.ISSM_Countries_ABInBev__c = 'Honduras';
        typification.CS_Days_to_End__c = 1;
        Insert typification;
        
        CS_QuestionMt_Class obj = new CS_QuestionMt_Class();
        CS_QuestionMt_Class.GetTypificationN3('SAC', 'Solicitudes');
    }
    
    /**
    * Method for test the method GetTypificationN4 in CS_QUESTIONMT_CLASS
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void test_GetTypificationN4()
    {
        ISSM_TypificationMatrix__c typification = new ISSM_TypificationMatrix__c();
        typification.ISSM_TypificationLevel1__c = 'SAC';
        typification.ISSM_TypificationLevel2__c = 'Solicitudes';
        typification.ISSM_TypificationLevel3__c = 'Equipo Frío';
        typification.ISSM_TypificationLevel4__c = 'Instalación';
        typification.ISSM_Countries_ABInBev__c = 'Honduras';
        typification.CS_Days_to_End__c = 1;
        Insert typification;
        
        CS_QuestionMt_Class obj = new CS_QuestionMt_Class();
        CS_QuestionMt_Class.GetTypificationN4('SAC', 'Solicitudes', 'Equipo Frío');
    }
    
	/**
    * Method for test the method getPickListValuesIntoList in CS_QUESTIONMT_CLASS
    * @author: d.zacarias.cantillo@accenture.com
    * @param Void
    * @return Void
    */    
    @isTest
    static void test_getPickListValuesIntoList()
    {
        CS_QuestionMt_Class obj = new CS_QuestionMt_Class();
        CS_QuestionMt_Class.getPickListValuesIntoList();
    }
    
    /**
    * Method for test the method createNewQuestion in CS_QUESTIONMT_CLASS
    * @author: d.zacarias.cantillo@accenture.com
    * @param Void
    * @return Void
    */     
    @isTest
    static void test_createNewQuestion()
    {
    Profile p = [Select  Id from Profile limit 1];
    User usr = new User(Country__c = 'Honduras', Username= 'rezznov@co.ab-inbev.com.irep.wiitsit', LastName = 'wiit', Email = 'r.wiit@gmdodelo.com', Alias = 'Rwiit', CommunityNickname = 'apiwwiitt', TimeZoneSidKey = 'America/Bogota', LocaleSidKey = 'es_US', EmailEncodingKey= 'ISO-8859-1', ProfileId = p.id, LanguageLocaleKey = 'es');
        System.runAs(usr){
        ISSM_TypificationMatrix__c typification = new ISSM_TypificationMatrix__c();
        typification.ISSM_TypificationLevel1__c = 'SAC';
        typification.ISSM_TypificationLevel2__c = 'Solicitudes';
        typification.ISSM_TypificationLevel3__c = 'Equipo Frío';
        typification.ISSM_TypificationLevel4__c = 'Instalación';
        typification.ISSM_Countries_ABInBev__c = 'Honduras';
        typification.CS_Days_to_End__c = 1;
        //typification.ISSM_OwnerUser__c = usr.id;
        Insert typification;
        
        CS_QuestionMt_Class obj = new CS_QuestionMt_Class();
        CS_QuestionMt_Class.createNewQuestion('SAC','Solicitudes','Equipo Frío','Instalación','Question Test','Texto','');
        }
    }
    
    /**
    * Method for test the method the fail createNewQuestion in CS_QUESTIONMT_CLASS
    * @author: d.zacarias.cantillo@accenture.com
    * @param Void
    * @return Void
    */     
    @isTest
    static void test_createNewQuestionFail()
    {
    
    
        
        ISSM_TypificationMatrix__c typification = new ISSM_TypificationMatrix__c();
        typification.ISSM_TypificationLevel1__c = 'SAC';
        typification.ISSM_TypificationLevel2__c = 'Solicitudes';
        typification.ISSM_TypificationLevel3__c = 'Equipo Frío';
        typification.ISSM_TypificationLevel4__c = 'Instalación';
        typification.ISSM_Countries_ABInBev__c = 'Honduras';
        typification.CS_Days_to_End__c = 1;
        Insert typification;
        
        CS_QuestionMt_Class obj = new CS_QuestionMt_Class();
        CS_QuestionMt_Class.createNewQuestion('SAC','Solicitudes','Equipo Frío','Instalación','Question Test','Texto','');
        
    }
}