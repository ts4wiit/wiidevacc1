@isTest
global class MockHTTPResponseGeneratorSendOrderBatch implements HttpCalloutMock{
    global HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatus('OK');
        res.setStatusCode(400);
        //New response as etReturn is now an array
        res.setBody('Error en el servicio');
        //res.setBody('{"id": "0225000667","etReturn": {"item": {"type": "S","number": "233","message": "SALES_HEADER_IN procesado con éxito","messageV1": null,"messageV2": null,"messageV3": null,"messageV4": null}}}');        
        System.debug('Respuesta'+res);
        return res;
    }  
}