public class  HONES_BlockOrder_LT {
   
    @AuraEnabled
    public static Boolean hasRelatedOrders(String recordId){
        Boolean hasRelatedOrder = false;
        list<ONTAP__Order__c> ordersRelated  = new list<ONTAP__Order__c>();
       
        ONTAP__Order__c order = [SELECT Id, Name, ONCALL__Call__c, ONTAP__Event_Id__c  FROM ONTAP__Order__c WHERE Id =: recordId];
        if(order.ONCALL__Call__c != null){
            ordersRelated = [SELECT Id FROM ONTAP__Order__c WHERE ONCALL__Call__c =: order.ONCALL__Call__c];
        }else if (order.ONTAP__Event_Id__c != null){
            ordersRelated = [SELECT Id FROM ONTAP__Order__c WHERE ONTAP__Event_Id__c =: order.ONTAP__Event_Id__c];
        }
        
        if(ordersRelated.size() > 0){
            hasRelatedOrder = true;
        }
        return hasRelatedOrder;
    }

    @AuraEnabled
    public static CustomResponse blockOrder(String recordId){
          CustomResponse response = new CustomResponse();
          list<ONTAP__Order__c> orders = new list<ONTAP__Order__c>();
          ONTAP__Order__c order = [SELECT Id, Name, ONCALL__Call__c, ONTAP__Event_Id__c, ONCALL__OnCall_Status__c,
                                          ONCALL__SAP_Order_Response__c, ONTAP__SAP_Order_Number__c,ONTAP__OrderAccount__r.ONTAP__ExternalKey__c
                                   FROM ONTAP__Order__c
                                   WHERE Id =: recordId];
          
          //orders.add(order);

          BlockOrder blockOrder = new BlockOrder();
          blockOrder.id = order.ONTAP__SAP_Order_Number__c;
          blockOrder.sfdcId = order.Id; 
          //ArrayBlockOrder abo = new ArrayBlockOrder();
          //abo.ordersToBlock.add(blockOrder);

          JSONGenerator generator = JSON.createGenerator(true);
          generator.writeObject(blockOrder);
          String jsonBlockOrder = generator.getAsString().replaceAll('\\n','');  
          response = sentJsonBlockOrder (jsonBlockOrder, order);
          System.debug('HONES_BlockOrder_LT::blockOrder' + response);
          return response;
    }

    public static CustomResponse sentJsonBlockOrder(String json ,ONTAP__Order__c order){
        String country = order.ONTAP__OrderAccount__r.ONTAP__ExternalKey__c.Left(2);
        List<End_Point__mdt> endpoint = [SELECT DeveloperName,MasterLabel,Token__c,URL__c 
                                        FROM End_Point__mdt 
                                        WHERE MasterLabel = 'Block_SAP_Order' AND Country__c =: country];
        CustomResponse cResponse= new CustomResponse();

        Http h = new Http();
        HttpRequest httpReq = new HttpRequest();
        httpReq.setEndpoint(endpoint[0].URL__c);
        httpReq.setMethod('POST');
        httpReq.setHeader('Content-Type','application/json;charset=utf-8');
        httpReq.setHeader('bearer', endpoint[0].Token__c);
        httpReq.setBody(json);
        System.debug('sentJsonBlockOrder::json sent: ' +json);
        HTTPResponse authresp = new HTTPResponse();
        try{
            authresp = h.send(httpReq);
            if(authresp.getStatusCode() == 200 && authresp.getBody() != null){
                 System.debug('sentJsonBlockOrder::Success: ' + authresp.getBody());
                 ResponseBlockOrder rbol = new ResponseBlockOrder(); //No se necesitaria si creamos otra clase aparte y colcoamos el metodo de parse como estatico
                 ResponseBlockOrder response = rbol.parse(authresp.getBody());
                 System.debug('sentJsonBlockOrder::response: ' + response);
                 if(response != null){
                      order.ONCALL__SAP_Order_Response__c = response.message;
                      cResponse.success = response.success;
                      cResponse.message = response.message;
                      if(response.success && response.message.contains(GlobalStrings.HONES_BLOCK_STATUS)){
                          cResponse.status = 'OK';
                          order.ONCALL__OnCall_Status__c = 'Cancelled'; 
                      }else{
                          cResponse.status = 'ERROR';
                      }
                 }else{
                     cResponse.status = 'ERROR';
                     cResponse.message = 'CONSULT ADMIN 1:' + authresp.getBody();
                 }
            }else{
                 cResponse.status = 'ERROR';
                 cResponse.message = 'CONSULT ADMIN 2:' + authresp.getBody() + ':: StatusCode:: ' + authresp.getStatusCode();
            }
        }catch(Exception e){
            cResponse.status = 'ERROR';
            cResponse.message = 'CONSULT ADMIN 3: message: ' + e.getMessage() + ', cause: ' + e.getCause() + ', line: ' + e.getLineNumber() + ', json:' + json;  
        }finally{
            ISSM_TriggerManager_cls.inactivate('CS_TRIGGER_PEDIDO');
            update order;
        }
        return cResponse;
    }

    public class BlockOrder {
        String id {get; set;}
        String sfdcId {get; set;}
    }

    public class ArrayBlockOrder{
         /*RJP: Antes se esperaba un arreglo */
        //public list<BlockOrder> ordersToBlock {get;set;}
        public BlockOrder orderToBlock {get;set;}

        public ArrayBlockOrder(){
            //ordersToBlock = new list<BlockOrder>();
            orderToBlock = new BlockOrder();
        }
    }

    public class ResponseBlockOrderList{
        /*RJP: Antes se esperaba un arreglo */
        //public list<ResponseBlockOrder> resultOrders {get;set;}
        public ResponseBlockOrder resultOrder {get;set;}

        public  ResponseBlockOrderList parse(String json){
		    return (ResponseBlockOrderList) System.JSON.deserialize(json, ResponseBlockOrderList.class);
	    }
    }

    public class ResponseBlockOrder{
        public String id {get;set;}
		public String sfdcId {get;set;}
		public boolean success {get;set;}
		public String message {get;set;}	

        public  ResponseBlockOrder parse(String json){
		    return (ResponseBlockOrder) System.JSON.deserialize(json, ResponseBlockOrder.class);
	    }
    }

    
    public class CustomResponse{
        @AuraEnabled public String status {get;set;}
        @AuraEnabled public String message {get;set;}
        @AuraEnabled public boolean success {get;set;}
        public CustomResponse(){}
    }
}