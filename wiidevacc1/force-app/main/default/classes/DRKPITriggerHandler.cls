public class DRKPITriggerHandler {
    
    List<ONTAP__KPI__c> kpis = new List<ONTAP__KPI__c>();
    List<ONTAP__KPI__c> filteredKpis = new List<ONTAP__KPI__c>();
    private Map<String,String> routeToUserIdMap = new Map<String,String>();
    private List<String> routes = new List<String>();
    
    public DRKPITriggerHandler ( List<ONTAP__KPI__c> kpis ){
        
        this.kpis = kpis;
        
    }
    
    public void run(){
        
        this.prepare();
        this.getRoutes();
        this.getRouteToUserIdMap();
        this.setBDRRoutes();
        
    }
    
    
    private void prepare(){
        
        for ( ONTAP__KPI__c kpi : kpis ){
            
            if ( kpi.Country_Code__c == 'DO' ){
 
               filteredKpis.add( kpi );
                
            }

        }        
        
    }
    
    private void getRoutes(){
        
        for ( ONTAP__KPI__c kpi : filteredKpis ){
            
            routes.add( kpi.BDR_Route__c );
            
        }
        
    }
    
    private void getRouteToUserIdMap(){
        
        this.routeToUserIdMap = DRUserSelector.getRouteToUserIdMap( routes );
        
    }
    
    private void setBDRRoutes(){
        
        for ( ONTAP__KPI__c kpi : filteredKpis ){
                
                Id userId = routeToUserIdMap.get( kpi.BDR_Route__c );
                
                if ( userId == null ){
                    
                	userId = UserInfo.getUserId();
                    
                } 
            
           		if( kpi.Account_Sap_Id__c == '-' ){
                
                    kpi.ONTAP__User_id__c = userId;           
                    
                }
               
                kpi.OwnerId = userId;   
                
        }        
        
    }    

}