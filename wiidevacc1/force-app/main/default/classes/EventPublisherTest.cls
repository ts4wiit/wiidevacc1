@isTest
public class EventPublisherTest {
    
    @isTest
    static void testEventPublisher(){
                
        Account account = DRFixtureFactory.newDRAccount();
        
        Test.startTest();
        
        Event event = DRFixtureFactory.newDREvent( account.Id, account.OwnerId);        
        
        event.ONTAP__Estado_de_visita__c = EventPublisher.VISIT_STATUS;
        
        update event;
        
        Test.getEventBus().deliver();
        
        Test.stopTest();
 
    }

}