@isTest
public class DRCaseForceTriggerHandler_Test {
        
    @isTest
    public static void testOrderItemSharing(){
       
       Id caseRecordTypeId = Schema.SObjectType.ONTAP__Case_Force__c.getRecordTypeInfosByDeveloperName().get('DO_Sales_Process').getRecordTypeId(); 
        
       Id profileId = [ SELECT Id FROM PROFILE WHERE Name = 'DO BDR' ].get(0).Id;
        
       Id accountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_DO').getRecordTypeId(); 
        
	   User bdrUser1 = new User(FirstName='BDR1', LastName='Agent 1', UserName = 'bdr1@test.com', Email='bdr1@test.com',
				Alias='alias1', LocaleSidKey='es', LanguageLocaleKey='es', EmailEncodingKey='ISO-8859-1',ONTAP__Country_Alias__c = 'DO',
				ProfileId = profileId, TimeZoneSidKey='America/Puerto_Rico', Country='DO', isActive = true,User_Route__c='DO9999');

	   insert bdrUser1;    
        
       Account account = new Account( Name = 'Account', ONTAP__SAPCustomerId__c = 'CID1', RecordTypeId = accountRecordTypeId, OwnerId = bdrUser1.Id,
                                           ONTAP__Contact_First_Name__c = 'Contact', ONTAP__Contact_Last_Name__c = 'Lastname', BDR_Route__c = 'DO9999' );
      
       insert account;         
        
       ONTAP__Case_Force__c caseForce = new ONTAP__Case_Force__c (          
       	   
           ONTAP__Account__c = account.Id,
           RecordTypeId = caseRecordTypeId
       
       );

        
       insert caseForce;
      
       Test.startTest();
        
       
        
       System.runAs( bdrUser1 ){     
            
           List<ONTAP__Case_Force__c> newCaseForce = [ SELECT Id,OwnerId FROM ONTAP__Case_Force__c ];
    
           System.assertEquals( 1, newCaseForce.size(), 'Ooooops, assertion failed...' );            
                        
       }
        
        List<ONTAP__Case_Force__Share> caseForceShares = [ SELECT Id FROM ONTAP__Case_Force__Share WHERE ParentId =: caseForce.Id AND UserOrGroupId =: bdrUser1.Id ];
        
        System.assertEquals( 1, caseForceShares.size(), 'Ooooops, assertion failed...' );
        
        System.runAs( bdrUser1 ){     
           
           ONTAP__Case_Force__c caseForce2 = new ONTAP__Case_Force__c (          
       	   
               ONTAP__Account__c = account.Id,
               RecordTypeId = caseRecordTypeId
               
           );
               
            insert caseForce2;
               
        }
      
        Test.stopTest();        
               
    }

}