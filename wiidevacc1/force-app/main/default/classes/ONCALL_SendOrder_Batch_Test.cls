@isTest
public class ONCALL_SendOrder_Batch_Test {

    @isTest static void testBatch(){
        Account acc = new Account(Name ='Ricardo Milos Test', ONTAP__SalesOgId__c = 'CS01');
        insert acc;
        
        List<ONTAP__Order__c> lstOrd = new List<ONTAP__Order__c>();
        ONTAP__Order__c ord = new ONTAP__Order__c();
        ord.ONCALL__SAP_Order_Item_Counter__c = 2;
        ord.ONCALL__Total_Order_Item_Quantity__c = 2;
        ord.ONCALL__OnCall_Status__c = 'Closed';
        ord.ISSM_PaymentMethod__c = 'CASH';
        ord.ONTAP__DocumentationType__c = 'ZSV1';
        ord.ONTAP__OrderAccount__c = acc.Id;
        ord.ONTAP__DeliveryDate__c = System.now() + 3;   
        lstOrd.add(ord);
        
        insert lstOrd;
        
        Test.startTest();
		ONCALL_SendOrder_Batch obj01 = new ONCALL_SendOrder_Batch();
        Database.BatchableContext BC;
	    obj01.execute(BC, lstOrd);
  		Id batchprocessId = Database.executeBatch(obj01, 200);
        Test.stopTest();
    }
}