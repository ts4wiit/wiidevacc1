/* ----------------------------------------------------------------------------
* AB InBev :: Oncall
* ----------------------------------------------------------------------------
* Clase: MockHTTPResponseGeneratorFlexibleData.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 30/12/2019        Ricardo Ortega          Creation of methods.
*/
@isTest
global class MockHTTPRespGenCaseAssetaccWithOutEquip implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response.
        // Set response values, and
        // return response.
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatus('OK');
        res.setStatusCode(200);
        res.setBody('{"EtAvisos": { "id":"123", "Number":"123", "System":"test", "item": {"NotifNo": "000010397371", "Equipment": null, "Message": {"EtReturn": {"item":"error,error"}} }}}');
        System.debug('Respuesta'+res);
        return res;
    }
}