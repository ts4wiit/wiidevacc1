@isTest(seeAllData=true)
public class DRSchedulableVisits_Test {

    public static String CRON_EXP = '0 0 0 15 3 ? 2022';    


    @isTest(seeAllData=true)
    static void testScheduledJob(){

        Test.startTest();
        
        String jobId = System.schedule('DRSchedulableVisits',CRON_EXP, new DRSchedulableVisits());
        
        Test.stopTest();
        
    }

    @isTest(seeAllData=true)
    static void testTrigger(){

        Test.startTest();

        User executionUser = [SELECT Id,User_Route__c FROM USER WHERE User_Route__c = 'DO0043'];
        DOVisitsExecution__c execution = new DOVisitsExecution__c(
                Name = 'Test',
                User__c = executionUser.Id
        );

        insert execution;

        Test.stopTest();

    }

}