/**
* @author Marcilio Souza
* @date 28/05/19
* @description Class to be implement in local orgs copy from handler from ONTAP managed packaged
*/
public abstract class Local_TriggerHandler {
    
    protected virtual void beforeInsert() {}
    protected virtual void beforeUpdate() {}
    protected virtual void beforeDelete() {}

    protected virtual void afterInsert() {}
    protected virtual void afterUpdate() {}
    protected virtual void afterDelete() {}
    
    private Local_TriggerHandler_Helper helper = new Local_TriggerHandler_Helper(this);
    
    // name of trigger which executes
    public final String TRIGGER_NAME = String.valueOf(this).substringBefore('_').substringBefore('TriggerHandler') + 'Trigger';
    // public final String TRIGGER_NAME = String.valueOf(this) + '';
    
    // proper lists of Trigger new and old records
    public List<SObject> newList = new List<SObject>(Trigger.new);
    public List<SObject> oldList = new List<SObject>(Trigger.old);

    /**
    * @author Kamil Murawski
    * @date 17/01/2017
    * @description run method - handles trigger execution mechanism
    */
    public void run() {        
        if (validate()) {
          
            switch on Trigger.operationType {                
                when AFTER_INSERT {
                    afterInsert();
                }
                when AFTER_UPDATE{
                    afterupdate();
                }           
                
                when BEFORE_UPDATE {
                    beforeUpdate();
                }
                when BEFORE_INSERT {                   
                    beforeInsert();         
                }
 
                when else {
                    
                }
            }
             
        }
    }
 
    private Boolean validate() {
        Boolean validToRun = false;
        //system.debug('TRIGGER_NAME:'+ String.valueOf(this).substringBefore('_').substringBefore('TriggerHandler'));
        validToRun |= helper.validateTriggerCustomSetting();

        return validToRun;
    }
  
}