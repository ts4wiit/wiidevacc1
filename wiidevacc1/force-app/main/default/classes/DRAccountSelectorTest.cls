@isTest
public class DRAccountSelectorTest {
    
    @isTest
    static void testMethods(){
               
        Test.startTest();

        Account account = DRFixtureFactory.newDRAccount();
        Contact contact = DRFixtureFactory.newDRContact( account.Id );
        
        List<Id> accountIds = new List<Id>();
        accountIds.add( account.Id );      
        
        List<Id> contactIds = new List<Id>();
        contactIds.add( contact.Id );              
        
        // Test getAccountIdRecordTypeDeveloperNameMap
        Map<String,String>  testMap = DRAccountSelector.getAccountIdRecordTypeDeveloperNameMap( accountIds );
        
        String developerName = testMap.get( account.Id );
        
        System.assertEquals( AccountChangeEventTriggerHandler.DR_ACCOUNT_RECORD_TYPE, developerName);
        
        // Test getAccountIdSAPCustomerIdMap
        testMap.clear();
        testMap = DRAccountSelector.getAccountIdSAPCustomerIdMap( accountIds );
        
        String customerId = testMap.get( account.Id );
        
        System.assertEquals( account.ONTAP__SAPCustomerId__c , customerId );
        
        // Test getAccountsByIds
        
        List<Account> accounts = DRAccountSelector.getAccountsByIds( accountIds );
        
        System.assertEquals( 1 , accounts.size() );
        
        // Test getContactIdToAccountMap
        Map<Id,Account> contactMap =  DRAccountSelector.getContactIdToAccountMap( contactIds );
        
        System.assertEquals( account.Id , contactMap.get( contact.Id ).Id );      
        
        // Test getAccountIdToCountryMap
        
        Map<String,String> countryMap = DRAccountSelector.getAccountIdToCountryMap( accountIds );
        
        System.assertEquals( 1, countryMap.size() );          
        
        Test.stopTest();
        
    }        
 
}