/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_TRIGGER_CASE_CLASS.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 03/12/2018      José Luis Vargas         Creacion de la clase para el trigger que se ejecuta al insertar
 *                                          o actualizar un caso standard. Esta clase inserta o actualiza  casos force.
 */
public without sharing class CS_Trigger_Case_Class extends TriggerHandlerCustom
{
 	private Map<Id, Case> MapCaseNew;
    private Map<Id, Case> MapCaseOld;
    private List<Case> lstNewCase;
    private List<case> lstOldCase;
    
    public CS_Trigger_Case_Class()
    {
        this.MapCaseNew = (Map<Id, Case>)Trigger.newMap;
        this.MapCaseOld = (Map<Id, Case>)Trigger.oldMap;
        this.lstNewCase = (List<Case>)Trigger.new;
        this.lstOldCase = (List<Case>)Trigger.old;
    }
    
    /**
    * Method that runs when a case is inserted
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    public override void afterInsert()
    {
        InsertCaseforce(this.lstNewCase);
    }
    
    /**
    * Method that runs when a case force is updated
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return Void
    */
    public override void afterUpdate()
    {
        UpdateCaseForce(this.MapCaseNew);        
    }
    
    /**
    * Method for insert case force when a case standard is inserted
    * @author: jose.l.vargas.lara@accenture.com
    * @param List with the case standard
    * @return Void
    */
    private static void InsertCaseforce(List<Case> lstNewCases)
    {
        string countryCase = '';
        
        List<ONTAP__Case_Force__c> lstNewCaseForce = new List<ONTAP__Case_Force__c>();
        List<ISSM_MappingFieldCase__c> lstMappingFieldsCase = new  List<ISSM_MappingFieldCase__c>();
        List<ISSM_TypificationMatrix__c> lstMatrixRecord = new  List<ISSM_TypificationMatrix__c>();
        
        Map<string, string> MapIdCaseStandard = new Map<string, string>();
        Map<string, string> MapIdCaseForceStandard = new Map<string, string>();

        try
        {
            if(!ISSM_TriggerManager_cls.isInactive('CS_CASE_TRIGGER'))
            {
                /*Realiza la consulta del mapeo de campos de la matrix */
               
                /* Se realiza la consulta del mapeo de campos entre casos standard y casos force */
                lstMappingFieldsCase = GetMappingFieldCase();
                Id recordCaseForceID ;
                /* Se realiza el ciclo a cada uno de los casos que se han creado */
                for(Case oNewCase : lstNewCases)
                {
                    
                    string tipificationsconcat= oNewCase.ISSM_TypificationLevel1__c + '-' + oNewCase.ISSM_TypificationLevel2__c  + '-' + oNewCase.ISSM_TypificationLevel3__c ;
                    System.debug('ContatTyp: '+tipificationsconcat);
                    id idcase = oNewCase.AccountId;
                    System.debug(idcase);
                    lstMatrixRecord = GetMappingFielRecord(tipificationsconcat, idcase);
                    System.debug(lstMatrixRecord);                    
                    for(ISSM_TypificationMatrix__c ofieldCaseRecordType : lstMatrixRecord)
                    {
                        recordCaseForceID =  [SELECT Id FROM RecordType WHERE DeveloperName =: ofieldCaseRecordType.HONES_CF_RecordType__c  AND sObjectType = 'ONTAP__Case_Force__c'].Id;
                        System.debug('Entro' + recordCaseForceID);
                    }
                    ONTAP__Case_Force__c oNewCaseForce = new ONTAP__Case_Force__c();
                    /* Se realiza el ciclo por cada campo que se encuentra en el mapeo de campos */
                    for(ISSM_MappingFieldCase__c ofieldCaseForce : lstMappingFieldsCase)
                    {
                        oNewCaseForce.RecordTypeId = recordCaseForceID;
                        //System.debug('TriggerCaseClass::MappingFieldCase::Field:'+ofieldCaseForce.Name);
                       if(ofieldCaseForce.Name == System.label.CS_CaseCountryField)
                       {
                           countryCase = String.valueOf(oNewCase.get(ofieldCaseForce.Name));
                           System.debug('TriggerCaseClass::InsCF::countryCase:'+countryCase);
                           oNewCaseForce.CS_Country__c = countryCase;
                            if(countryCase == System.Label.CS_Country_Honduras)
                               countryCase = System.Label.CS_CountryCodeHN;
                            else if(countryCase == System.Label.CS_Country_El_Salvador)
                               countryCase = System.Label.CS_CountryCodeSV;
                            else if (countryCase == System.Label.CS_Country_Panama) {
                                countryCase = System.Label.CS_CountryCodePA;
                            }
                            oNewCaseForce.put(System.Label.CS_CaseCountryCodeField, countryCase);
                            System.debug('CaseForceField::CountryCodeField:'+System.Label.CS_CaseCountryCodeField);
                            System.debug('CFF::CountryCodeField::Value:'+countryCase);
                            //oNewCaseForce.put(ofieldCaseForce.ISSM_APICaseForce__c, oNewCase.get(ofieldCaseForce.Name));
                       }
                       else {
                           oNewCaseForce.put(ofieldCaseForce.ISSM_APICaseForce__c, oNewCase.get(ofieldCaseForce.Name));
                       }
                    }
                    System.debug('CaseForce::CaseCountryCode:'+oNewCaseForce.CS_Country_Code__c);
                    oNewCaseForce.ISSM_CaseNumber__c = oNewCase.CaseNumber;
                    MapIdCaseStandard.put(oNewCaseForce.ISSM_CaseNumber__c, oNewCase.Id);
                    lstNewCaseForce.add(oNewCaseForce);                  
                }
                
                ISSM_TriggerManager_cls.inactivate('CS_CASEFORCE_TRIGGER');
                /* Se insertan los casos force */
                system.debug('lstNewCaseForce: ' + lstNewCaseForce);
                Insert lstNewCaseForce;
          
                /* Se inicia el proceso para actualizar el id de casos force en el caso standard */
                for(ONTAP__Case_Force__c oCaseForce : lstNewCaseForce)
                {
                    MapIdCaseForceStandard.put(MapIdCaseStandard.get(oCaseForce.ISSM_CaseNumber__c), oCaseForce.Id);
                }

                UpdateCaseForceNumber(MapIdCaseForceStandard);
            }
        }
        catch(Exception ex)
        {
            System.debug('CS_TRIGGER_CASE_CLASS.InsertCaseforce Message: ' + ex.getMessage());   
            System.debug('CS_TRIGGER_CASE_CLASS.InsertCaseforce Cause: ' + ex.getCause());   
            System.debug('CS_TRIGGER_CASE_CLASS.InsertCaseforce Line number: ' + ex.getLineNumber());   
            System.debug('CS_TRIGGER_CASE_CLASS.InsertCaseforce Stack trace: ' + ex.getStackTraceString());            
        }
    }
    
    /**
    * Method for update case force when a case standard is updated
    * @author: jose.l.vargas.lara@accenture.com
    * @param List with the case standard
    * @return Void
    */
    public static void UpdateCaseForce(Map<Id, Case> MapNewCase)
    {
        string countryCase = '';
        
        List<ISSM_MappingFieldCase__c> lstMappingFieldsCase = new  List<ISSM_MappingFieldCase__c>();
        List<ONTAP__Case_Force__c> lstCaseForce = new List<ONTAP__Case_Force__c>();
        
        Map<Id, ONTAP__Case_Force__c>MapCaseForce = new Map<Id, ONTAP__Case_Force__c>();
        
        ONTAP__Case_Force__c oCaseForce = new ONTAP__Case_Force__c();
        Set<Id> sCaseForce = new Set<Id>();
        
        try
        {
            if(!ISSM_TriggerManager_cls.isInactive('CS_CASE_TRIGGER'))
            {
                 /* Se obtiene el numero de caso force de los casos standard que fueron actualizados */
                 for(Case oCase : MapNewCase.values())
                 {
                     sCaseForce.add(oCase.ISSM_CaseForceNumber__c);
                 }
            
                /* Se realiza la consulta de los casos force en base a los Id Obtenidos */
                lstCaseForce = GetCaseForce(sCaseForce);
            
                /* Se obtienen los id de los casos a actualizar */
                for(ONTAP__Case_Force__c oCaseF : lstCaseForce)
                {
                    MapCaseForce.put(oCaseF.Id, oCaseF);
                }
                lstCaseForce = new List<ONTAP__Case_Force__c>();
            
                /* Se realiza la consulta del mapeo de campos */
                lstMappingFieldsCase = GetMappingFieldCase();
            
                /* Se realiza el ciclo por cada uno de los casos que vienen en el mapa de entrada que fueron actualizados */
                for(Case oCaseUpdate : MapNewCase.values())
                {
                    if(MapCaseForce.containsKey(oCaseUpdate.ISSM_CaseForceNumber__c))
                    {
                        for(ISSM_MappingFieldCase__c ofieldCaseForce : lstMappingFieldsCase)
                        {
                            if(ofieldCaseForce.ISSM_APICaseForce__c != System.label.ISSM_APICaseForceMappingField)
                            {
                                if(ofieldCaseForce.Name == System.label.CS_CaseCountryField)
                                {
                                    countryCase = String.valueOf(oCaseUpdate.get(ofieldCaseForce.Name));
                                    oCaseForce.CS_Country__c = countryCase;
                                    if(countryCase == System.Label.CS_Country_Honduras)
                                        countryCase = System.Label.CS_CountryCodeHN;
                                    else if(countryCase == System.Label.CS_Country_El_Salvador)
                                        countryCase = System.Label.CS_CountryCodeSV;
                                    else if(countryCase == System.Label.CS_Country_Panama)
                                        countryCase = System.Label.CS_CountryCodePA;
                                    oCaseForce.put(System.Label.CS_CaseCountryCodeField, countryCase);
                                    //oCaseForce.put(ofieldCaseForce.ISSM_APICaseForce__c, oCaseForce.get(ofieldCaseForce.Name));
                                }
                                else
                                    oCaseForce.put(ofieldCaseForce.ISSM_APICaseForce__c, oCaseUpdate.get(ofieldCaseForce.Name));
                            }
                        }
                    
                        if(MapCaseForce.containsKey(oCaseUpdate.ISSM_CaseForceNumber__c) && MapCaseForce.get(oCaseUpdate.ISSM_CaseForceNumber__c).Id != null){
                            oCaseForce.Id = MapCaseForce.get(oCaseUpdate.ISSM_CaseForceNumber__c).Id;
                            lstCaseForce.add(oCaseForce);
                        }
                        oCaseForce = new ONTAP__Case_Force__c();
                    }
                }
                
                ISSM_TriggerManager_cls.inactivate('CS_CASEFORCE_TRIGGER');
                Update lstCaseForce;
            }
        }
        catch(Exception ex)
        {
            System.debug('CS_TRIGGER_CASE_CLASS.UpdateCaseForce Message: ' + ex.getMessage());   
            System.debug('CS_TRIGGER_CASE_CLASS.UpdateCaseForce Cause: ' + ex.getCause());   
            System.debug('CS_TRIGGER_CASE_CLASS.UpdateCaseForce Line number: ' + ex.getLineNumber());   
            System.debug('CS_TRIGGER_CASE_CLASS.UpdateCaseForce Stack trace: ' + ex.getStackTraceString());      
        }
    }
    
    /**
    * Method for update the case force number in case standard
    * @author: jose.l.vargas.lara@accenture.com
    * @param List with the case standard
    * @return Void
    */
    private static void UpdateCaseForceNumber(Map<string,string> MapIdCaseForceStandard)
    {
        List<Case> lstCase = new List<Case>();       
        try
        {
            for(string strKey : MapIdCaseForceStandard.KeySet())
            {
               Case oCase = new Case();
               oCase.Id = strKey;
               oCase.ISSM_CaseForceNumber__c = MapIdCaseForceStandard.get(strKey);
               lstCase.add(oCase);
            }
            
            /*Se actualiza la informacion del caso */
            ISSM_TriggerManager_cls.inactivate('CS_CASEFORCE_TRIGGER');
            ISSM_TriggerManager_cls.inactivate('CS_CASE_TRIGGER');
            Update lstCase;
        }
        catch(Exception ex)
        {
            System.debug('CS_TRIGGER_CASE_CLASS.UpdateCaseForceNumber Message: ' + ex.getMessage());   
            System.debug('CS_TRIGGER_CASE_CLASS.UpdateCaseForceNumber Cause: ' + ex.getCause());   
            System.debug('CS_TRIGGER_CASE_CLASS.UpdateCaseForceNumber Line number: ' + ex.getLineNumber());   
            System.debug('CS_TRIGGER_CASE_CLASS.UpdateCaseForceNumber Stack trace: ' + ex.getStackTraceString());   
        }
    }
    
    /**
    * Method for get the mapping fields for insert or update a case force
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return List with the mapping fields
    */
    private  static List<ISSM_TypificationMatrix__c> GetMappingFielRecord(String typification,ID idacc)
    {
    
        List<Account> nombre = [SELECT ONTAP__ExternalKey__c FROM Account WHERE id =: idacc Limit 1];
        String accountCountry = nombre[0].ONTAP__ExternalKey__c.left(2);
        
        List<ISSM_TypificationMatrix__c> lstMappingFieldstypoRecord = new  List<ISSM_TypificationMatrix__c>();
        lstMappingFieldstypoRecord = [SELECT HONES_CF_RecordType__c FROM ISSM_TypificationMatrix__c WHERE ISSM_Typification_Concat__c =: typification AND ISSM_Country_Code__c =: accountCountry];
        System.debug(lstMappingFieldstypoRecord);
        return lstMappingFieldstypoRecord;
    }

    /**
    * Method for get the mapping fields for insert or update a case force
    * @author: jose.l.vargas.lara@accenture.com
    * @param Void
    * @return List with the mapping fields
    */
    private  static List<ISSM_MappingFieldCase__c> GetMappingFieldCase()
    {
       List<ISSM_MappingFieldCase__c> lstMappingFieldsCase = new  List<ISSM_MappingFieldCase__c>();
       lstMappingFieldsCase = [SELECT Name, ISSM_APICaseForce__c FROM ISSM_MappingFieldCase__c WHERE Active__c  = true];
       return lstMappingFieldsCase;
    }
    
    /**
    * Method for get the id and case number of the case force
    * @author: jose.l.vargas.lara@accenture.com
    * @param List with case force
    * @return List with the case number
    */
    private static List<ONTAP__Case_Force__c> GetCaseForce(Set<id>sCaseForce)
    {
        List<ONTAP__Case_Force__c> lstCaseForce = new List<ONTAP__Case_Force__c>();
        lstCaseForce = [SELECT Id, ISSM_CaseNumber__c, ONTAP__Status__c FROM ONTAP__Case_Force__c WHERE Id IN : sCaseForce];
        return lstCaseForce;
    }
}