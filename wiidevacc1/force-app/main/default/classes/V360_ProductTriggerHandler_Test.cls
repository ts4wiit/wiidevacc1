/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_ProductTriggerHandler_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 04/01/2019     Carlos Leal             Creation of methods.
 */
@isTest
private class V360_ProductTriggerHandler_Test{
    
    
    @testSetup
    static void setupTestData(){
        test.startTest();
        List<RecordType> records = [Select Id,Name from RecordType where SobjectType='Account'];
            ONTAP__Product__c product = new ONTAP__Product__c();
            product.ONTAP__MaterialProduct__c = 'Test Product';
            Account acc = new Account();
            acc.Name ='Test';
            acc.ONTAP__Credit_Condition__c= 'Credit';
            acc.ONCALL__KATR4__c = 'test';
            acc.RecordTypeId= records[0].Id;
            acc.ONTAP__Delivery_Delay_Code__c = '24 hrs.';
            acc.ONTAP__SalesOgId__c=GlobalStrings.HONDURAS_ORG_CODE;
            
            acc.ONTAP__ExternalKey__c='SVHBF';
        	insert product;
        	insert acc;
        	product.ONTAP__MaterialProduct__c = 'Test Product updated';
        	update product;
            

        test.stopTest();
    }
  /**
    * Test method for before insert a product 
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
  @isTest static void test_beforeInsert_UseCase1(){
    V360_ProductTriggerHandler obj01 = new V360_ProductTriggerHandler();
    ONTAP__Product__c prod = new ONTAP__Product__c();
    insert(prod);
  }
    
  /**
    * Test method for before update a product
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
  @isTest static void test_beforeUpdate_UseCase1(){
    V360_ProductTriggerHandler obj01 = new V360_ProductTriggerHandler();
    ONTAP__Product__c prod = new ONTAP__Product__c();
    insert(prod);
    prod.ONTAP__MaterialProduct__c = 'Test Product';
    update(prod);
  }
    
  /**
    * Test Method fortranslate codes in fields of a product in salesforce
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
  @isTest static void test_translatecodes_UseCase1(){
    V360_ProductTriggerHandler obj01 = new V360_ProductTriggerHandler();
    List<ONTAP__Product__c> lstprod = new List<ONTAP__Product__c>();
    ONTAP__Product__c prod = new ONTAP__Product__c();
     lstprod.add(prod);
    obj01.translatecodes(lstprod);
  }
}