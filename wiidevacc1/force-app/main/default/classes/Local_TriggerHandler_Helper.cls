/**
* @author Marcilio Souza
* @date 28/05/19
* @description Class to be implement in local orgs copy from handler from ONTAP managed packaged
*/
public with sharing class Local_TriggerHandler_Helper {

    private Local_TriggerHandler handler;
    
    /**
    * @author Kamil Murawski
    * @date 18/01/2017
    * @description TriggerHandler_Helper constructor
    */
    public Local_TriggerHandler_Helper(Local_TriggerHandler handler) {
        this.handler = handler;
    }
    
    /**
    * @author Kamil Murawski
    * @date 18/01/2017
    * @description validateTriggerCustomSetting method - checks Custom Settings for Trigger 
    *   (if none is found, assume it's active)
    * @return Boolean - true if valid
    */
    public Boolean validateTriggerCustomSetting() {
    	system.debug('handler.TRIGGER_NAME:'+ handler.TRIGGER_NAME);
        ONTAP__Triggers_Settings__c triggerSettings = ONTAP__Triggers_Settings__c.getValues(handler.TRIGGER_NAME);
		
        return (triggerSettings != null)
            ? triggerSettings.ONTAP__Active__c
            : true;
    }
}