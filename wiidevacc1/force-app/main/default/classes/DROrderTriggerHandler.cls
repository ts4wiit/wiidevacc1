public with sharing virtual class DROrderTriggerHandler {

    @testvisible public static final String DR_ORDER_RECORD_TYPE = 'DO_Order';
    public List<ONTAP__Order__c> orders = new List<ONTAP__Order__c>();
    public List<ONTAP__Order__c> filteredOrders = new List<ONTAP__Order__c>();
    public List<Account> accounts = new List<Account>();
    public Map<Id,Account> accountIdToAccountMap = new Map<Id,Account>();
    public Map<Id,Id> orderIdToAccountIdMap = new Map<Id,Id>();
    public List<Id> accountIds = new List<Id>();
    private Id orderRecordTypeId;

    public class OrderException extends Exception {}

    public DROrderTriggerHandler(){}

    public DROrderTriggerHandler( List<ONTAP__Order__c> orders ){
        
        this.orders = orders;
        
    }
    
    public virtual void run(){

        this.setRecordType();
        this.filter();
        this.getAccounts();
        this.setOwner();       
        
    }

    public virtual void setRecordType(){

        try{

            orderRecordTypeId = Schema.SObjectType.ONTAP__Order__c.getRecordTypeInfosByDeveloperName().get( DR_ORDER_RECORD_TYPE ).getRecordTypeId();

        } catch( Exception e ){

            throw new OrderException('>>> Error while setting record type: ' + e.getMessage());

        }

    }


    public virtual void filter(){

        for ( ONTAP__Order__c order : orders ){

            if( order.RecordTypeId == orderRecordTypeId ){

                filteredOrders.add( order );
                orderIdToAccountIdMap.put( order.Id, order.ONTAP__OrderAccount__c );
                accountIds.add( order.ONTAP__OrderAccount__c );

            }
        }
    }

    public virtual void getAccounts(){

        accounts = DRAccountSelector.getAccountsByIds( accountIds );

        for( Account account : accounts ){

            accountIdToAccountMap.put(account.Id, account);

        }

    }

    public virtual void setOwner(){
        
        for ( ONTAP__Order__c order : filteredOrders ){

            try{

                order.OwnerId = accountIdToAccountMap.get( order.ONTAP__OrderAccount__c ).OwnerId;

            } catch( Exception e ){

                throw new OrderException('>>> Related account not found: ' + e.getMessage());

            }


        }
    }
}