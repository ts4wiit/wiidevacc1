/* -----------------------------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * -----------------------------------------------------------------------------------------------
 * Clase: CS_AssignAddressCostumer_cls_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * -----------------------------------------------------------------------------------------------
 * Date                 User                   Description
 * 09/01/2019           Debbie Zacarias       Creacion de la clase Test
 * 											  											  
 */
@isTest
private class CS_AssignAddressCostumer_Class_Test
{
    
    /**
    * Method for create data and Test class 'CS_AssignAddressCostumer_cls.apxc'
    * @author: d.zacarias.cantillo@accenture.com
    * @param void
    * @return void
    */     
    @isTest static void test_getCostumerReference_UseCase1()
    {
        /*RecordType rectype = [SELECT id, RecordType.name FROM RecordType WHERE sObjectType = 'Case' LIMIT 1];
        System.debug(rectype);*/
        Account acc = new Account(Name = 'La cholutequita', ONTAP__Street__c = 'Calle1', ONTAP__PostalCode__c = '12345', ONTAP__Neighborhood__c = 'Colonia1');
        insert acc;
        
        Account acc1 = new Account(Name = 'La cholutequita 2', ONTAP__Street__c = 'Calle2', ONTAP__PostalCode__c = '23456', ONTAP__Neighborhood__c = 'Colonia2');
        insert acc1; 
        
        Account acc2 = new Account(Name = 'La cholutequita 3', ONTAP__Street__c = 'Calle3', ONTAP__PostalCode__c = '34567', ONTAP__Neighborhood__c = 'Colonia3');
        insert acc2;
        
        Case caso1 = new Case
        (
            OwnerId = UserInfo.getUserId(),
            //RecordTypeId = rectype.Id,
            AccountId = acc2.Id,
            ISSM_LocalStreet__c = 'Calle1', 
            ISSM_LocalNumber__c = '90', 
            ISSM_PostalCode__c = '12345',
            ISSM_LocalColony__c ='Colonia1'          
        );
        
        insert caso1;
        System.debug(caso1);
        
        Case caso2 = new Case
        (
            OwnerId = UserInfo.getUserId(),
            //RecordTypeId = rectype.Id,
            AccountId = acc2.Id,
            ISSM_LocalStreet__c = 'Calle', 
            ISSM_LocalNumber__c = '90', 
            ISSM_PostalCode__c = '12345',
            ISSM_LocalColony__c ='Colonia1'          
        );
        insert caso2;
        System.debug(caso2);
        
        Case caso3 = new Case
        (
            OwnerId = UserInfo.getUserId(),
            //RecordTypeId = rectype.Id,
            AccountId = acc2.Id,
            ISSM_LocalStreet__c = 'Calle3', 
            ISSM_LocalNumber__c = '90', 
            ISSM_PostalCode__c = '12345', 
            ISSM_LocalColony__c ='Colonia'          
        );
        insert caso3;
        System.debug(caso3); 
        
        Case caso4 = new Case
         (
            OwnerId = UserInfo.getUserId(),
            //RecordTypeId = rectype.Id,
            AccountId = acc2.Id,
            ISSM_LocalStreet__c = 'Calle2', 
            ISSM_LocalNumber__c = '90', 
            ISSM_PostalCode__c = '123', 
            ISSM_LocalColony__c ='Colonia2'          
        );
        
        insert caso4;
        System.debug(caso4); 
        
        Case caso5 = new Case
        (
            OwnerId = UserInfo.getUserId(),
            //RecordTypeId = rectype.Id,
            AccountId = acc2.Id,
            ISSM_LocalStreet__c = 'Calle8', 
            ISSM_LocalNumber__c = '90', 
            ISSM_PostalCode__c = '124', 
            ISSM_LocalColony__c ='Colonia2'          
        );
        insert caso5;
        System.debug(caso5); 
        
        Case caso6 = new Case(
            OwnerId = UserInfo.getUserId(),
            //RecordTypeId = rectype.Id,
            AccountId = acc2.Id,
            ISSM_LocalStreet__c = 'Calle1', 
            ISSM_LocalNumber__c = '90', 
            ISSM_PostalCode__c = '124', 
            ISSM_LocalColony__c ='Colonia4'          
        );
        
        insert caso6;
        System.debug(caso6);
        
        Case caso7 = new Case
        (
            OwnerId = UserInfo.getUserId(),
            //RecordTypeId = rectype.Id,
            AccountId = acc2.Id,
            ISSM_LocalStreet__c = 'Calle', 
            ISSM_LocalNumber__c = '90', 
            ISSM_PostalCode__c = '124', 
            ISSM_LocalColony__c ='Colonia'          
        );
        insert caso7;
        System.debug(caso7);   
        
        test.startTest();
        CS_AssignAddressCostumer_cls obj01 = new CS_AssignAddressCostumer_cls(caso1.Id);
        obj01.getCostumerReference();
        CS_AssignAddressCostumer_cls obj02 = new CS_AssignAddressCostumer_cls(caso2.Id);
        obj02.getCostumerReference();        
        CS_AssignAddressCostumer_cls obj03 = new CS_AssignAddressCostumer_cls(caso3.Id);
        obj03.getCostumerReference();
        CS_AssignAddressCostumer_cls obj04 = new CS_AssignAddressCostumer_cls(caso4.Id);
        obj04.getCostumerReference();
        CS_AssignAddressCostumer_cls obj05 = new CS_AssignAddressCostumer_cls(caso5.Id);
        obj05.getCostumerReference();       
        CS_AssignAddressCostumer_cls obj06 = new CS_AssignAddressCostumer_cls(caso6.Id); 
        obj06.getCostumerReference();
        CS_AssignAddressCostumer_cls obj07 = new CS_AssignAddressCostumer_cls(caso7.Id);
        obj07.getCostumerReference();
        
        Account acc3 = new Account(Name = 'La cholutequita 4', ONTAP__Street__c = 'Calle3', ONTAP__PostalCode__c = '34567', ONTAP__Neighborhood__c = 'Colonia4');
        insert acc3;
        Case caso8 = new Case
        (
            OwnerId = UserInfo.getUserId(),
            AccountId = acc3.Id,
            ISSM_LocalStreet__c = 'Calle', 
            ISSM_LocalNumber__c = '900', 
            ISSM_PostalCode__c = '34567', 
            ISSM_LocalColony__c ='Colonia'          
        );
        insert caso8;
    	CS_AssignAddressCostumer_cls obj08 = new CS_AssignAddressCostumer_cls(caso8.Id);
        obj08.getCostumerReference();
        
        Case caso9 = new Case
        (
            OwnerId = UserInfo.getUserId(),
            AccountId = acc2.Id,
            ISSM_LocalStreet__c = 'Calle3', 
            ISSM_LocalNumber__c = '90', 
            ISSM_PostalCode__c = '12345', 
            ISSM_LocalColony__c ='Colonia4'             
        );
        insert caso9;
        CS_AssignAddressCostumer_cls obj09 = new CS_AssignAddressCostumer_cls(caso9.Id);
        obj09.getCostumerReference();
        test.stopTest();
    }
}