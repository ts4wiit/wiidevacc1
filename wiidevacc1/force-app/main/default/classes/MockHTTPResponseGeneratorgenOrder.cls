/* ----------------------------------------------------------------------------
* AB InBev :: OnCall
* ----------------------------------------------------------------------------
* Clase: MockHttpResponseGeneratorgenOrder.apxc
* Version: 1.0.0.0
*  
 * Change History
* ----------------------------------------------------------------------------
* Date                 User                              Description
* 07/01/2019           Heron Zurita           Creation of methods.
*/

@isTest
global class MockHTTPResponseGeneratorgenOrder implements HttpCalloutMock {
     /*
     * Method that create HTTPResponse where verify the correct answer 
     * Created By:heron.zurita@accenture.com
     * @param HTTPRequest req
     * @return HTTPResponse 
*/
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response.
        // Set response values, and
        // return response.
        
        //System.assertEquals('https://abco-priceengine.herokuapp.com/dealslist', req.getEndpoint());
        //System.assertEquals('POST', req.getMethod());
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setStatus('OK');
        res.setStatusCode(200);             
        res.setBody('{"outputMessages":[],"order":{"accountId":"0011138635","salesOrg":"DC01","cashCreditType":"A","paymentMethod":"CASH","total":13.2,"subtotal":12,"tax":1.2,"discount":0,"basePrice":12,"tax1":1.2,"empties":{"totalAmount":"010","minimumRequired":"01","extraAmount":0},"items":[{"sku":"000000000000004077","quantity":"1","unit":"","freeGood":false,"discount":0,"tax":1.2,"subtotal":12,"total":13.2,"price":13.2,"unitPrice":12,"applieddeals":[]}],"applieddeals":[]}}');
        System.debug('Respuesta'+res);
        
        return res;
    }
    
    
}