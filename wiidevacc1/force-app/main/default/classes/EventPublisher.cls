public class EventPublisher {
    
    public static final String VISIT_STATUS = 'Completado';
    private static final String COUNTRY_CODE = 'DO';
    private List<Event> events = new List<Event>();
    private List<Event> filteredEvents = new List<Event>();
    private List<DREventOutboundMessage__e> outboundMessages = new List<DREventOutboundMessage__e>();
    private Map<String,String> accountIdToSAPIdMap = new Map<String,String>();
    private Map<String,String> userIdToRouteMap = new Map<String,String>();
    private List<Id> accountIds = new List<Id>();
    private List<Id> userIds = new List<Id>();
    // info to verify if user is allowed to publish changes
    private String integrationId;
    
    public EventPublisher( List<Event> events ){      
        this.events = events;
    }    
    
    public void run(){
        this.filter();
        this.enrich();
        this.mapEvents();
        this.publish();
    }
    

    private void filter(){
        
        this.integrationId = DRUserSelector.getIntegrationId();
        Id userId = UserInfo.getUserId();

        for( Event event : events ){
            System.debug('>>> Event:' + event);
            
            if ( event.ONTAP__Country__c == COUNTRY_CODE &&  event.ONTAP__Estado_de_visita__c == VISIT_STATUS && userId != (Id) this.integrationId){
                
                filteredEvents.add( event );
                accountIds.add( event.AccountId );
                userIds.add( event.OwnerId );
                
            }    
            
        }       
    }
    
    private void enrich(){
        
       this.accountIdToSAPIdMap = DRAccountSelector.getAccountIdSAPCustomerIdMap( accountIds );
       this.userIdToRouteMap = DRUserSelector.getUserIdToRouteMap( userIds );
        
    }
    
    private void mapEvents(){
        
        for ( Event event : filteredEvents ){
            
            String recordTypeName = '';
            
            try{
            	recordTypeName = Schema.SObjectType.Event.getRecordTypeInfosById().get( event.RecordTypeId ).getName();
            } catch( Exception e){
                System.debug('>>> ' + e.getMessage());
            }

            String customerSAPId = accountIdToSAPIdMap.get( event.AccountId );
            String route = userIdToRouteMap.get( event.OwnerId );
            System.debug('>>> Route: ' + route );
            
            DREventOutboundMessage__e outboundMessage = new DREventOutboundMessage__e(
            
            	InRangeCheckIn__c = event.ONTAP__In_range_check_in__c,
                ControlInicio__c = event.ONTAP__Control_inicio__c,
                ControlFin__c = event.ONTAP__Control_fin__c,
                Country__c = event.ONTAP__Country__c,
                DuracionRealVisita__c = event.ONTAP__Duracion_real_visita__c,
                EstadoDeVisita__c = event.ONTAP__Estado_de_visita__c,
                EventType__c = event.ONTAP__Event_Type__c,
                LatitudInicio__c = event.ONTAP__Latitud_inicio__c,
                LongitudInicio__c = event.ONTAP__Longitud_inicio__c,
                CheckOutLatitude__c = event.ONTAP__CheckOut_Latitude__c,
                CheckoutLongitude__c = event.ONTAP__CheckOut_Longitude__c,
                UserRoute__c = route,
                ActivityDate__c = event.ActivityDate,
                RecordType__c = recordTypeName,
                SAPCustomerId__c = customerSAPId,
                Weekday__c = event.ONTAP__WeekDay__c,
                EventId__c = event.Id
            
            );
            
            outboundMessages.add( outboundMessage );            
        }     
    }
    
    private void publish(){
  
        System.debug('>>> Visit');
        System.debug( outboundMessages );
        List<Database.SaveResult> eventResults = EventBus.publish( outboundMessages ); 
  
    }
}