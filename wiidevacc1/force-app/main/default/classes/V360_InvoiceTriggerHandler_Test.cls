/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: V360_InvoiceTriggerHandler_Test.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 19/12/2018     Gerardo Martinez        Creation of methods.
 */
@isTest
public class V360_InvoiceTriggerHandler_Test {

    /**
    * Test method for set parameters
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @testSetup
  	static void setupTestData(){           
        Account a = New Account(ONTAP__ExternalKey__c='ABC',Name ='ABC', ONTAP__SAP_Number__c = '0000000ABC');
        insert a;
	}

    /**
    * Test method for relate an Account whit their invoice
    * Created By: g.martinez.cabral@accenture.com
    * @param void
    * @return void
    */
    @isTest static void test_relateAccountsToInvoice_UseCase1(){
        
        test.startTest();
		V360_InvoiceTriggerHandler invoice = new V360_InvoiceTriggerHandler();
        Account acc = new Account();
        acc.ONTAP__SAP_Number__c='1234567890';
        acc.Name='Test';
        insert acc;
       
        ONCALL__Invoice__c invoiceOb = new ONCALL__Invoice__c(V360_CustomerSAPId__c = '0000000ABC');
        invoiceOb.V360_CustomerSAPId__c='1234567890';  
        insert invoiceOb;
        update invoiceOb;

        test.stopTest();
    
    } 
}