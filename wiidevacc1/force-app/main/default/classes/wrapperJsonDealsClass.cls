/* ----------------------------------------------------------------------------
 * AB InBev :: 360 View
 * ----------------------------------------------------------------------------
 * Clase: wrapperJsonDealsClass.apxc
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User                   Description
 * 12/02/2019         Herón Zurita        Creation of methods.
 */
public class wrapperJsonDealsClass {
    /**
	* Structure of the entity which declare its variables with their respective setters and getters.
	* Created By: heron.zurita@accenture.com
	* @param void
	* @return void
	*/
    
    @AuraEnabled public String description{set;get;}

	@AuraEnabled public Boolean optional{set;get;}    

	@AuraEnabled public String pronr{set;get;}

}