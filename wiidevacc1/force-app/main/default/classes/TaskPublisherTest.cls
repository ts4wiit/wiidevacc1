@isTest
public class TaskPublisherTest {
    
    @isTest
    static void testTaskPublisher(){
                
        Account account = DRFixtureFactory.newDRAccount();
        
        Test.startTest();
        
        Task task = DRFixtureFactory.newDRTask( account.Id, account.OwnerId);        
        
        task.Status = 'Closed';
        
        update task;
        
        Test.getEventBus().deliver();
        
        Test.stopTest();
 
    }    

}