/**
* @author Marcilio Souza
* @date 28/05/19
* @description Service for read custom permissions
*/
public with sharing class CustomPermissionService {
    
    /**
     * @description : Get the list of users with the related custom permission
     * @param Set<Id> setIdUser : Set id of users to be check
     * @param String permissionName : Name of the custom permission
     * @return Map<Id,User> : Map of Id User and User with the custom permission assigned
    */
    public static Map<Id,User> getUsersWithCustomPermission(Set<Id> setIdUser, String permissionName)
    {
        Map<Id,User> mapIdByUsers = new Map<Id,User>();
        Set<Id> permissionSetIds = new Set<Id>();
        
        for (SetupEntityAccess access : [SELECT ParentId 
                                           FROM SetupEntityAccess 
                                          WHERE SetupEntityId 
                                              IN (SELECT Id                                                   
                                                    FROM CustomPermission 
                                                    WHERE DeveloperName = :permissionName)]){
            permissionSetIds.add(access.ParentId);
        }

        if(!permissionSetIds.isEmpty()){
            List<User> listUser = [ SELECT Username FROM User WHERE Id IN 
                                    (SELECT AssigneeId FROM PermissionSetAssignment
                                        WHERE PermissionSetId IN :permissionSetIds
                                            AND AssigneeId IN :setIdUser)];
            for(User u : listUser){
                mapIdByUsers.put(u.Id,u);
            }
        }
        return mapIdByUsers;
    }
}