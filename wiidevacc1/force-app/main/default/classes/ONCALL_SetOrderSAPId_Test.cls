/* ----------------------------------------------------------------------------
* AB InBev :: Oncall
* ----------------------------------------------------------------------------
* Clase: ONCALL_SetOrderSAPId_Test.apxc
* Version: 1.0.0.0
*  
* Change History
* ----------------------------------------------------------------------------
* Date                 User                   Description
* 21/01/2019     Carlos Leal            Creation of methods.
*/

@isTest
public class ONCALL_SetOrderSAPId_Test {

    /**
    * Test method for set Sap Order Id
    * Created By: c.leal.beltran@accenture.com
    * @param void
    * @return void
    */
    @isTest
    static void OrderSap_UseCase1(){
    	ONCALL_SetOrderSAPId.setId('Status:ok', 'Status:ok', 'OrdertTest1');
    }
}