trigger LocalFeedCommentTrigger on FeedComment (before insert, before update, after insert, after update) {   
    new LocalFeedComment_TriggerHandler().run();     
}