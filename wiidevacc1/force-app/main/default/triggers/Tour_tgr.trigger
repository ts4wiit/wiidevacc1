/****************************************************************************************************
    General Information
    -------------------
    author: Andrés Garrido
    email: agarrido@avanxo.com
    company: Avanxo Colombia
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Trigger to control ONTAP__Tour__c events

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       19-07-2017        Andrés Garrido               Creation Class
    1.1       31-07-2017        Nelson Sáenz                 Modified Class
****************************************************************************************************/
trigger Tour_tgr on ONTAP__Tour__c (after delete, after insert, after update, before delete, before insert, before update) {
    new VisitControl_TourTriggerHandler().Run();
    public set<id> setIdVisitPlans                 =   new set<Id>();
    if(trigger.isBefore) {
        if(trigger.isInsert){
            /*Inicio 1.1 NS*/
            TourOperations_cls.assignTour(trigger.New);
            /*Fin 1.1 NS*/
        }
        else if(trigger.isUpdate && !TriggerExecutionControl_cls.hasAlreadyDone('Tour_tgr','BeforeUpdate')){
            /*Inicio 1.1 NS*/
            TourOperations_cls.validateTourAssigned(trigger.old, trigger.new);
            TriggerExecutionControl_cls.setAlreadyDone('Tour_tgr','BeforeUpdate');
            /*Fin 1.1 NS*/
        }
        else if(trigger.isDelete){
        }
    }
    else if(trigger.isAfter){
        if(trigger.isInsert && !TriggerExecutionControl_cls.hasAlreadyDone('Tour_tgr','AfterInsertSync')){
			//TourOperations_cls.syncInsertToursToExternalObject(trigger.newMap);
        }
        else if(trigger.isUpdate && !TriggerExecutionControl_cls.hasAlreadyDone('Tour_tgr','AfterUpdateSync')){
            //TourOperations_cls.syncUpdateToursToExternalObject(trigger.old, trigger.new);
            
            //TriggerExecutionControl_cls.setAlreadyDone('Tour_tgr','AfterUpdate');
            
        }
        else if(trigger.isDelete && !TriggerExecutionControl_cls.hasAlreadyDone('Tour_tgr','AfterDeleteSync')){
            TourOperations_cls.syncDeleteToursToExternalObject(trigger.oldMap);
        }
    }
}