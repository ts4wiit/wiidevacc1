trigger DOVisitsExecutionTrigger on DOVisitsExecution__c (after insert) {
    
    DOVisitsExecutionTriggerHandler handler = new DOVisitsExecutionTriggerHandler( Trigger.new );
    handler.run();

}