/****************************************************************************************************
    General Information
    -------------------
    author: Andrés Garrido
    email: agarrido@avanxo.com
    company: Avanxo Colombia
    Project: ISSM DSD
    Customer: AbInBev Grupo Modelo
    Description: Trigger to control VisitPlan__c events

    Information about changes (versions)
    -------------------------------------
    Number    Dates             Author                       Description
    ------    --------          --------------------------   -----------
    1.0       22-06-2017        Andrés Garrido               Creation Class
****************************************************************************************************/
trigger VisitPlan_tgr on VisitPlan__c (after delete, after insert, after update, before delete, before insert, before update) {
    if(trigger.isBefore) {
        
        if(trigger.isInsert){
            VisitPlan_cls ClsVisitPlan = new VisitPlan_cls();
            ClsVisitPlan.ProcessVisitPLan(trigger.new);
        }
        else if(trigger.isUpdate){
            VisitPlan_cls ClsVisitPlan = new VisitPlan_cls();
            ClsVisitPlan.ProcessVisitPLanUpdate(trigger.new,trigger.old); 
        }
        else if(trigger.isDelete){
            VisitPlan_cls ClsVisitPlan = new VisitPlan_cls();
            ClsVisitPlan.manageAggregateAccount(trigger.oldMap);
        }
    }
    else if(trigger.isAfter){
        if(trigger.isInsert){
        }
        else if(trigger.isUpdate){
        }
        else if(trigger.isDelete){
        }
    } 
}