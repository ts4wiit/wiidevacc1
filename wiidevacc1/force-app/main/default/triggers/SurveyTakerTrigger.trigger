trigger SurveyTakerTrigger on ONTAP__SurveyTaker__c ( after insert, after update ) {
    
    SurveyPublisher publisher = new SurveyPublisher( trigger.new );
    publisher.run();

}