/* ----------------------------------------------------------------------------
 * AB InBev :: Customer Service
 * ----------------------------------------------------------------------------
 * Clase: CS_ENTITLEMENT_TRIGGER.apxt
 * Version: 1.0.0.0
 *  
 * Change History
 * ----------------------------------------------------------------------------
 * Date                 User               Description
 * 14/03/2019      Debbie Zacarias         Creacion del trigger que se ejecuta despues de una insercion
 *                                         o actualización en el objeto Entitlement
 */

/**
* Constructor which call the CS_TRIGGER_ENTITLEMENT_CLASS TriggerHandler in the following cases: after insert, after update, before insert, before update
* @author: Debbie Zacarias
* @param void
* @return Void
*/

trigger CS_Entitlement_Trigger on Entitlement (after insert, after update, before insert, before update) 
{   
    new CS_Trigger_Entitlement_Class().run();
}