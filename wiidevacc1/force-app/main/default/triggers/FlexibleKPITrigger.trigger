trigger FlexibleKPITrigger on Flexible_KPI__c ( before insert ) {

    FlexibleKPITriggerHandler handler = new FlexibleKPITriggerHandler( Trigger.new );
    handler.run();
    
}