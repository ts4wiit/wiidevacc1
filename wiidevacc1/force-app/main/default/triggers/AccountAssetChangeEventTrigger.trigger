trigger AccountAssetChangeEventTrigger on ONTAP__Account_Asset__ChangeEvent (after insert) {
    
    AccountAssetChangeEventTriggerHandler accountAssetChangeEventTriggerHandler = new AccountAssetChangeEventTriggerHandler( Trigger.new );
    accountAssetChangeEventTriggerHandler.run();    

}